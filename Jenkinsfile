def MVN_PROJECT_VERSION = ''

pipeline{
	options{
		disableConcurrentBuilds()
		buildDiscarder(logRotator(numToKeepStr: '10'))
	}
	
	environment{
		APP_VERSION = "1.0"
		PROJECT_NAME = "Autism Alliance of Michigan - Core"
		JENKINS_SENDGRID_EMAIL = "heguanelvis@gmail.com"
		PATH="$PATH:/apps/apache-maven-3.6.3/bin:/apps/gradle-6.3/bin:/apps/jdk1.8.0_241/bin:/apps/node-v12.16.2-linux-x64/bin"
	}
	
	agent any
	
	stages{
	
		stage('BUILD'){
		    when {
			   not {
				   branch 'master'
			   }
		   }
			steps{
				script {
                			GIT_COMMIT_HASH = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
				}
				echo "**************************************************"
				echo "Git Hash: ${GIT_COMMIT_HASH}"
				echo "**************************************************"
			    	withCredentials([string(credentialsId: 'email.username', variable: 'EMAIL_USERNAME'), string(credentialsId: 'email.password', variable: 'EMAIL_PASSWORD'), string(credentialsId: 'db.url', variable: 'JENKINS_DB_URL'), string(credentialsId: 'db.username', variable: 'JENKINS_DB_USERNAME'), string(credentialsId: 'db.password', variable: 'JENKINS_DB_PASSWORD'), string(credentialsId: 'jwt.secret', variable: 'JENKINS_JWT_SECRET'), string(credentialsId: 'sendgrid.api', variable: 'JENKINS_SENDGRID_API') , string(credentialsId: 'SPRING_ANGULAR_APP_URL', variable: 'JENKINS_SPRING_ANGULAR_APP_URL') , string(credentialsId: 'SPRING_SERVER_CONTEXT_URL', variable: 'JENKINS_SPRING_SERVER_CONTEXT_URL')]) {
                			 sh '''
			 			mvn -f pom.xml clean compile package -Dmaven.test.skip=true -Dspringapp.core.version=${GIT_COMMIT_HASH} -Dspringapp.mail.username=${EMAIL_USERNAME} -Dspringapp.mail.password=${EMAIL_PASSWORD} -Ddb.url=${JENKINS_DB_URL} -Ddb.username=${JENKINS_DB_USERNAME} -Ddb.password=${JENKINS_DB_PASSWORD} -Djwt=${JENKINS_JWT_SECRET} -Dsendgrid.api=${JENKINS_SENDGRID_API} -Dsendgrid.email=${JENKINS_SENDGRID_EMAIL} -Dangular.url=${JENKINS_SPRING_ANGULAR_APP_URL} -Dspringapp.url=${JENKINS_SPRING_SERVER_CONTEXT_URL}
			 		'''	
                }
			}
			post{
			  success{
		            archiveArtifacts artifacts: 'target/**/*.war', fingerprint: true
			    }
			}
		}
		
		stage(' PROD BUILD'){
			when {
					branch 'master'
				}
			steps{
				script {
                			GIT_COMMIT_HASH = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
				}
				echo "**************************************************"
				echo "Git Hash: ${GIT_COMMIT_HASH}"
				echo "**************************************************"
				withCredentials([string(credentialsId: 'email.username', variable: 'EMAIL_USERNAME'), string(credentialsId: 'email.password', variable: 'EMAIL_PASSWORD'), string(credentialsId: 'db.url.prod', variable: 'PROD_DB_URL'), string(credentialsId: 'db.username.prod', variable: 'PROD_DB_USERNAME'), string(credentialsId: 'db.password.prod', variable: 'PROD_DB_PASSWORD'), string(credentialsId: 'jwt.secret', variable: 'PROD_JWT_SECRET'), string(credentialsId: 'sendgrid.api', variable: 'PROD_SENDGRID_API') , string(credentialsId: 'PROD_ANGULAR_APP_URL', variable: 'PROD_SPRING_ANGULAR_APP_URL') , string(credentialsId: 'PROD_SERVER_CONTEXT_URL', variable: 'PROD_SPRING_SERVER_CONTEXT_URL')]) {
                			 sh '''
			 			mvn -f pom.xml clean compile package -Dmaven.test.skip=true -Dspringapp.core.version=${GIT_COMMIT_HASH} -Dspringapp.mail.username=${EMAIL_USERNAME} -Dspringapp.mail.password=${EMAIL_PASSWORD} -Ddb.url=${PROD_DB_URL} -Ddb.username=${PROD_DB_USERNAME} -Ddb.password=${PROD_DB_PASSWORD} -Djwt=${PROD_JWT_SECRET} -Dsendgrid.api=${PROD_SENDGRID_API} -Dsendgrid.email=${JENKINS_SENDGRID_EMAIL} -Dangular.url=${PROD_SPRING_ANGULAR_APP_URL} -Dspringapp.url=${PROD_SPRING_SERVER_CONTEXT_URL}
			 		'''	
                }
			}
			post{
			  success{
		            archiveArtifacts artifacts: 'target/**/*.war', fingerprint: true
			    }
			}
		}
		
		stage('Deploy'){
		    when {
			   not {
				   branch 'master'
			   }
		   }
		    steps{
				timeout(time:5, unit:'MINUTES'){
					input(message: "Do you want to Deploy?")
				}
				sh '''
				    sh /apps/apache-tomcat-9.0.34/bin/shutdown.sh
				    rm -rf /apps/apache-tomcat-9.0.34/webapps/aaomi
				    cp target/aaomi.war /apps/apache-tomcat-9.0.34/webapps/.
				    sh /apps/apache-tomcat-9.0.34/bin/startup.sh
				'''
			}
			post{
			    always{
			    	emailext body: '''${SCRIPT, template="groovy-html.template"}''',
					mimeType: 'text/HTML',
					to: 'sreenidhi.gundlupet@slalom.com',
					subject: "Successfully Deployed Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
				}
			}
		}
		
		stage('Prod Deploy') {
			when {
					branch 'master'
				}
				
			steps{
					timeout(time:5, unit:'MINUTES'){
						input(message: "Do you want to Deploy?")
					}
					sh '''
						ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 /apps/core/apache-tomcat-9/bin/shutdown.sh
						ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 rm -rf /apps/core/apache-tomcat-9/webapps/aaomi
						scp -i ~/.ssh/id_rsa target/aaomi.war ubuntu@54.81.120.47:/apps/core/apache-tomcat-9/webapps/.
						ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 /apps/core/apache-tomcat-9/bin/startup.sh
					'''
			
			}
			post{
			    always{
					emailext body: '''${SCRIPT, template="groovy-html.template"}''',
					mimeType: 'text/HTML',
					to: 'sreenidhi.gundlupet@slalom.com',
					subject: "Successfully Deployed Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
				}
			}
		}
		
	}
	post{
			    always{
			    	cleanWs()
			    	dir("${env.WORKSPACE}@tmp"){
			    	    deleteDir()
			    	}
		    }
	}
}	
