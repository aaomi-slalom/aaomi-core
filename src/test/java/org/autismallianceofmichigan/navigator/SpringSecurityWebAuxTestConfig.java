package org.autismallianceofmichigan.navigator;

import org.apache.catalina.User;
import org.autismallianceofmichigan.navigator.init.PermissionCodes;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@TestConfiguration
public class SpringSecurityWebAuxTestConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        HashSet<Permission> permissionSet = new HashSet<Permission>(Collections.singletonList(Permission.builder().permCode(PermissionCodes.PERM_VIEW_SYSTEM_ROLES).build()));

//        Account adminUser = new Account();
//        adminUser.setUsername("adminUser");
//        adminUser.setPassword("Gators44");
//        adminUser.setEmail("zachmend@gmail.com");
//        adminUser.setEnabled(true);
//        adminUser.setPermissions(permissionSet);
//        adminUser.setAccountId(1L);
        Account adminActiveUser = new Account();
        adminActiveUser.setUsername("adminUser");
        adminActiveUser.setPassword("Gators44");
        adminActiveUser.setEmail("zachmend@gmail.com");
        adminActiveUser.setEnabled(true);
        adminActiveUser.setPermissions(permissionSet);
        adminActiveUser.setAccountId(1L);

//        Account managerUser = new Account("Manager User", "manager@company.com", "password");
//        Account managerActiveUser = new Account(managerUser, Arrays.asList(
//                new SimpleGrantedAuthority("ROLE_MANAGER"),
//                new SimpleGrantedAuthority("PERM_FOO_READ"),
//                new SimpleGrantedAuthority("PERM_FOO_WRITE"),
//                new SimpleGrantedAuthority("PERM_FOO_MANAGE")
//        ));

        return new InMemoryUserDetailsManager(Collections.singletonList(
                adminActiveUser
        ));
    }
}
