package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.CategoryServicesDto;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceCategoryDto;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceCategoryOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Service;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceCategory;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceCategoryRepository;
import org.autismallianceofmichigan.navigator.service.ServiceCategoryService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ServiceCategoryServiceTest {

    @Mock
    ServiceCategoryRepository serviceCategoryRepository;

    @InjectMocks
    ServiceCategoryService serviceCategoryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private ServiceCategory buildServiceCategory() {
        return ServiceCategory.builder()
                .serviceCategoryId(1L)
                .active(true)
                .category("TestCategory")
                .services(buildSetOfServices())
                .build();
    }

    private Service buildService() {
        ServiceCategory serviceCategory = new ServiceCategory();
        serviceCategory.setCategory("TestCategory");
        serviceCategory.setActive(true);
        serviceCategory.setServiceCategoryId(1L);
        Set<Service> serviceSet = new HashSet<>();
        serviceSet.add(Service.builder()
        .service("Test Service")
        .active(true)
        .serviceId(1L)
        .description("Testing").build());
        serviceCategory.setServices(serviceSet);

        return Service.builder()
                .serviceId(1L)
                .active(true)
                .service("Test Service")
                .description("Testing")
                .serviceCategory(serviceCategory)
                .build();
    }

    private Set<Service> buildSetOfServices() {
        Set<Service> serviceSet = new HashSet<>();
        serviceSet.add(buildService());
        return serviceSet;
    }

    private List<ServiceCategoryDto> buildListOfServiceCategoryDtos() {
        List<ServiceCategoryDto> serviceCategoryDtos = new ArrayList<>();
        serviceCategoryDtos.add(ServiceCategoryDto.builder()
                .serviceCategory("TestCategory")
                .serviceCategoryId(1L)
                .serviceCount(1)
                .build());
        return serviceCategoryDtos;
    }

    private List<ServiceCategory> buildListOfServiceCategory() {
        List<ServiceCategory> serviceCategories = new ArrayList<>();
        serviceCategories.add(buildServiceCategory());
        return serviceCategories;
    }

    private List<CategoryServicesDto> buildListOfCategoryServiceDtos() {
        List<CategoryServicesDto> categoryServicesDtos = new ArrayList<>();
        List<ServiceDto> serviceDtos = new ArrayList<>();

        serviceDtos.add(ServiceDto.builder()
                .service(buildService().getService()
                ).description(buildService().getDescription()).serviceId(buildService().getServiceId()).deletable(true).build());

        categoryServicesDtos.add(CategoryServicesDto.builder()
                .category("TestCategory")
                .catDescription("TestDescription")
                .serviceDtos(serviceDtos)
                .build());

        return categoryServicesDtos;
    }

    private List<ServiceCategoryOptionDto> buildServiceCategoryOptionDtoList() {
        List<ServiceCategoryOptionDto> serviceCategoryOptionDtos = new ArrayList<>();
        serviceCategoryOptionDtos.add(ServiceCategoryOptionDto.builder().serviceCategoryId(1L).serviceCategory("TestCategory").build());
        return serviceCategoryOptionDtos;
    }

    @Test
    public void should_find_by_category() {
        ServiceCategory expected = buildServiceCategory();
        doReturn(Optional.of(expected)).when(serviceCategoryRepository).findByCategory("TestCategory");
        ServiceCategory actual = serviceCategoryService.findByCategory(expected.getCategory());

        assertEquals(expected, actual);
    }

    @Test
    public void should_find_by_categoryId() {
        ServiceCategory expected = buildServiceCategory();
        doReturn(Optional.of(expected)).when(serviceCategoryRepository).findById(1L);
        ServiceCategory actual = serviceCategoryService.findByCategoryId(expected.getServiceCategoryId());

        assertEquals(expected, actual);
    }

    @Test
    public void should_delete_service_category() {
        ServiceCategory serviceCategory = buildServiceCategory();
        serviceCategoryService.deleteServiceCategory(1L);
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(serviceCategoryRepository).deleteById(argumentCaptor.capture());

        assertEquals(1L, argumentCaptor.getValue());
    }

    @Test
    public void should_save_service_category() {
        ServiceCategory serviceCategory = buildServiceCategory();
        serviceCategoryService.saveServiceCategory(serviceCategory);
        ArgumentCaptor<ServiceCategory> argumentCaptor = ArgumentCaptor.forClass(ServiceCategory.class);
        verify(serviceCategoryRepository).save(argumentCaptor.capture());

        assertEquals("TestCategory", argumentCaptor.getValue().getCategory());
    }

    @Test
    public void should_get_all_service_category_dtos() {
        List<ServiceCategory> serviceCategoryList = buildListOfServiceCategory();
        doReturn(serviceCategoryList.stream()).when(serviceCategoryRepository).streamAll();
        List<ServiceCategoryDto> expected = buildListOfServiceCategoryDtos();
        List<ServiceCategoryDto> actual = serviceCategoryService.getAllServiceCategoryDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_service_category_option_dtos() {
        List<ServiceCategoryOptionDto> expected = buildServiceCategoryOptionDtoList();
        doReturn(buildListOfServiceCategory().stream()).when(serviceCategoryRepository).streamAll();
        List<ServiceCategoryOptionDto> actual = serviceCategoryService.getAllServiceCategoryOptionDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_category_service_dtos() {
        List<ServiceCategory> serviceCategoryList = buildListOfServiceCategory();
        doReturn(serviceCategoryList.stream()).when(serviceCategoryRepository).streamAll();
        List<CategoryServicesDto> expected = buildListOfCategoryServiceDtos();
        List<CategoryServicesDto> actual = serviceCategoryService.getAllCategoryServicesDto();

        assertEquals(expected, actual);
    }
}
