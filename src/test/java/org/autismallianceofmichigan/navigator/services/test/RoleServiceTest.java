package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminUserDto.RoleDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Role;
import org.autismallianceofmichigan.navigator.persistence.repository.RoleRepository;
import org.autismallianceofmichigan.navigator.service.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.management.relation.RoleNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {
    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleService roleService;

    private Role buildRole() {
        return Role.builder()
                .role("Testing")
                .build();
    }
    private List<Role> buildListOfRoles() {
        List<Role> listOfRoles = new ArrayList<>();
        listOfRoles.add(Role.builder()
                .role("Testing")
                .description("Test Description")
                .defaultGroup("Group1")
                .build());
        listOfRoles.add(Role.builder()
                .role("MockTest")
                .description("Mock Description")
                .defaultGroup("Group2")
                .build());
        return listOfRoles;
    }
    private List<RoleDto> buildListOfRoleDto(List<Role> roleList) {
       List<RoleDto> roleDtoList = new ArrayList<>();
        for (Role role: roleList
             ) { roleDtoList.add(
            RoleDto.builder()
                    .role(role.getRole())
                    .description(role.getDescription())
                    .defaultGroup(role.getDefaultGroup())
                    .build());
        }
        return roleDtoList;
    }

    @Test
    public void should_have_role() {
        Role role = buildRole();

        doReturn(Optional.of(role)).when(roleRepository).findByRole("Testing");
        boolean actual = roleService.hasRole(role.getRole());
        assertTrue(actual);
    }

    @Test
    public void should_find_by_role() throws RoleNotFoundException {
        Role expected = buildRole();
        doReturn(Optional.of(expected)).when(roleRepository).findByRole("Testing");
        Role actual = roleService.findByRole(expected.getRole());
        assertEquals(expected, actual);
    }

    @Test
    public void should_save_role() {
        Role expected = buildRole();
        doReturn(expected).when(roleRepository).save(expected);
        Role actual = roleService.saveRole(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_roles() {
        List<Role> roleList = buildListOfRoles();
        doReturn(roleList.stream()).when(roleRepository).streamAll();
        List<RoleDto> expected = buildListOfRoleDto(roleList);
        List<RoleDto> actual = roleService.getAllRoleDtos();
        assertEquals(expected, actual);
    }
}
