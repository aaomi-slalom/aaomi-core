package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminUserDto.InheritedPermissionDto;
import org.autismallianceofmichigan.navigator.dto.userProfileDto.UserProfileCreateUpdateRequest;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.UserViewDto;
import org.autismallianceofmichigan.navigator.dto.registrationDto.RegistrationRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountRecoveryRepository;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountRepository;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountVerificationRepository;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.autismallianceofmichigan.navigator.service.RoleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;

import javax.management.relation.RoleNotFoundException;
import javax.security.auth.login.AccountNotFoundException;
import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    AccountRepository accountRepository;

    @Mock
    AccountVerificationRepository accountVerificationRepository;

    @Mock
    AccountRecoveryRepository accountRecoveryRepository;

    @InjectMocks
    AccountService accountService;

    @Mock
    RoleService roleService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Account buildAccount() {
        return Account.builder()
                .accountId(1L)
                .username("zachmend")
                .email("zachmend@gmail.com")
                .password("Gators44")
                .enabled(true)
                .userProfile(buildUserProfile())
                .build();
    }

    private Role buildRole() {
        return Role.builder()
                .roleId(1L)
                .role("NAVIGATOR")
                .description("Nav role")
                .defaultGroup("Group1")
                .createdAt(new Date())
                .build();
    }

    private AccountVerification buildVerificationToken() {
        return AccountVerification.builder()
                .account(buildAccount())
                .verificationToken("TestToken")
                .build();
    }

    private AccountRecovery buildAccountRecovery() {
        return AccountRecovery.builder()
                .account(buildAccount())
                .passwordResetToken("ResetTest")
                .accountRecoveryId(1L)
                .createdAt(new Date())
                .build();
    }

    private UserProfile buildUserProfile() {
        return UserProfile.builder()
                .firstName("Guan")
                .lastName("He")
                .contactNumber("2184736629")
                .build();
    }

    private UserProfileCreateUpdateRequest buildUserProfileUpdateRequest() {
        return UserProfileCreateUpdateRequest.builder()
                .firstName("Zach")
                .lastName("Mendelson")
                .contactNumber("2923459816")
                .build();
    }

    private UserViewDto buildUserViewDto() {
        List<String> permissionCodes = new ArrayList<>();
        permissionCodes.add(buildPermission().getPermCode());
        List<String> accountGroups = new ArrayList<>();
        accountGroups.add(buildAccountGroup().getAccountGroup());
        List<InheritedPermissionDto> inheritedPermissionDtos = new ArrayList<>();
        inheritedPermissionDtos.add(buildInheritedPermissionDto());
        return UserViewDto.builder()
                .firstName(buildUserProfile().getFirstName())
                .lastName(buildUserProfile().getLastName())
                .username(buildAccount().getUsername())
                .email(buildAccount().getEmail())
                .enabled(true)
                .permissions(permissionCodes)
                .groups(accountGroups)
                .inheritedPermissionDtos(inheritedPermissionDtos)
                .build();
    }

    private AccountGroup buildAccountGroup() {
        Set<Permission> permissionSet = new HashSet<>();
        permissionSet.add(buildPermission());
        return AccountGroup.builder()
                .accountGroup("Test Group")
                .accountGroupId(1L)
                .permissions(permissionSet)
                .build();
    }

    private Permission buildPermission() {
        return Permission.builder()
                .permissionId(1L)
                .permCode("TestCode")
                .description("Testing")
                .build();
    }

    private InheritedPermissionDto buildInheritedPermissionDto() {
        return InheritedPermissionDto.builder()
                .permission(buildPermission().getPermCode())
                .group(buildAccountGroup().getAccountGroup())
                .build();
    }

    @Test
    public void should_load_user_details_by_username_or_email() {
        UserDetails expected = buildAccount();
        String username = buildAccount().getUsername();
        doReturn(Optional.of(buildAccount())).when(accountRepository).findByUsername("zachmend");
        doReturn(Optional.of(buildAccount())).when(accountRepository).findByEmail("zachmend@gmail.com");
        UserDetails actual = accountService.loadUserByUsername(username);
        assertEquals(expected, actual);
        actual = accountService.loadUserByUsername(buildAccount().getEmail());
        assertEquals(expected, actual);
    }

    @Test
    public void should_save_account() {
        Account expected = buildAccount();
        doReturn(expected).when(accountRepository).save(expected);
        Account actual = accountService.save(expected);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void should_turn_request_into_account() throws RoleNotFoundException {
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setUsername("ZachM");
        registrationRequest.setPassword("Gators44");
        registrationRequest.setConfirmPassword("Gators44");
        registrationRequest.setRole("NAVIGATOR");
        registrationRequest.setEmail("zach@test.com");

        doReturn(buildRole()).when(roleService).findByRole(eq("ROLE_NAVIGATOR"));

        Account expected = new Account();
        expected.setEmail("zach@test.com");
        expected.setUsername("ZachM");
        expected.setPassword("Gators44");
        expected.setRoles(new HashSet<>(Collections.singletonList(buildRole())));

        Account actual = accountService.toAccount(registrationRequest);
        assertEquals(expected, actual);
    }

    @Test
    public void should_have_username() {
        Account expected = buildAccount();

        doReturn(Optional.of(expected)).when(accountRepository).findByUsername(expected.getUsername());
        boolean actual = accountService.hasUsername(expected.getUsername());
        assertTrue(actual);
    }


    @Test
    public void find_account_by_username_should_succeed() throws AccountNotFoundException {
        Account expected = buildAccount();
        doReturn(Optional.of(expected)).when(accountRepository).findByUsername(expected.getUsername());
        Account actual = accountService.findByUsername(expected.getUsername());

        assertEquals(expected, actual);
    }

    @Test
    public void should_have_email() {
        Account expected = buildAccount();

        doReturn(Optional.of(expected)).when(accountRepository).findByEmail("zachmend@gmail.com");
        boolean actual = accountService.hasEmail(expected.getEmail());
        assertTrue(actual);
    }

    @Test
    public void account_should_be_enabled_with_email() throws AccountNotFoundException {
        Account account = buildAccount();

        doReturn(Optional.of(account)).when(accountRepository).findByEmail("zachmend@gmail.com");
        boolean actual = accountService.isAccountEnabledWithEmail(account.getEmail());
        assertTrue(actual);
    }

    @Test
    public void should_find_by_email() throws AccountNotFoundException {
        Account expected = buildAccount();
        doReturn(Optional.of(expected)).when(accountRepository).findByEmail("zachmend@gmail.com");
        Account actual = accountService.findByEmail(expected.getEmail());

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_account_verification_token() {
        AccountVerification expected = buildVerificationToken();
        doReturn(expected).when(accountVerificationRepository).findByVerificationToken("TestToken");
        AccountVerification actual = accountService.getAccountVerificationByVerificationToken(expected.getVerificationToken());
        assertEquals(expected, actual);
    }

    @Test
    public void should_validate_password_reset_token() {
        String expected = "success";
        doReturn(buildAccountRecovery()).when(accountRecoveryRepository).findByPasswordResetToken("ResetTest");
        String actual = accountService.validatePasswordResetToken(buildAccount().getAccountId(), buildAccountRecovery().getPasswordResetToken());
        assertEquals(expected, actual);
    }

    @Test
    public void should_find_account_by_password_reset_token() {
        Account expected = buildAccount();
        AccountRecovery accountRecovery = buildAccountRecovery();
        doReturn(accountRecovery).when(accountRecoveryRepository).findByPasswordResetToken("ResetTest");
        Account actual = accountService.findAccountByPasswordResetToken(buildAccountRecovery().getPasswordResetToken());
        assertEquals(expected, actual);
    }

    @Test
    public void should_create_account_verification_token() {
        Account account = buildAccount();
        String verificationToken = buildVerificationToken().getVerificationToken();
        accountService.createAccountVerificationToken(account, verificationToken);
        ArgumentCaptor<AccountVerification> accountArgumentCaptor = ArgumentCaptor.forClass(AccountVerification.class);
        verify(accountVerificationRepository).save(accountArgumentCaptor.capture());
        assertEquals("TestToken", accountArgumentCaptor.getValue().getVerificationToken());
    }

    @Test
    public void should_save_registered_account() {
        Account account = buildAccount();
        accountService.saveRegisteredAccount(account);
        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository).save(accountArgumentCaptor.capture());
        assertEquals("zachmend", accountArgumentCaptor.getValue().getUsername());
    }

    @Test
    public void should_create_account_recovery_for_account() {
        Account account = buildAccount();
        String passwordResetToken = buildAccountRecovery().getPasswordResetToken();
        accountService.createAccountRecoveryForAccount(account, passwordResetToken);
        ArgumentCaptor<AccountRecovery> accountRecoveryArgumentCaptor = ArgumentCaptor.forClass(AccountRecovery.class);
        verify(accountRecoveryRepository).save(accountRecoveryArgumentCaptor.capture());
        assertEquals("ResetTest", accountRecoveryArgumentCaptor.getValue().getPasswordResetToken());
        assertEquals("zachmend", accountRecoveryArgumentCaptor.getValue().getAccount().getUsername());
    }

    @Test
    public void should_update_account_password() {
        Account account = buildAccount();
        String updatedPassword = "UpdateTest";
        accountService.updateAccountPassword(account, updatedPassword);
        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository).save(accountArgumentCaptor.capture());
        assertEquals("UpdateTest", accountArgumentCaptor.getValue().getPassword());
    }

    @Test
    public void should_update_user_profile() {
        Account account = buildAccount();
        UserProfileCreateUpdateRequest updateRequest = buildUserProfileUpdateRequest();
        accountService.updateUserProfile(account, updateRequest);
        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository).save(accountArgumentCaptor.capture());
        assertEquals("Zach", accountArgumentCaptor.getValue().getUserProfile().getFirstName());
    }

    @Test
    public void should_find_account_page() {
        Page<Account> expected = Page.empty();
        doReturn(expected).when(accountRepository).findByAccountGroups_AccountGroupOrderByAccountIdDesc(eq("Group One"), argThat(pageable -> pageable.getPageSize() == 3 && pageable.getPageNumber() == 1));
        Page<Account> actual = accountService.findAccountPage("Group One", 1, 3);
        assertEquals(expected, actual);
    }

    @Test
    public void should_find_account_page_by_username() {
        Page<Account> expected = buildAccountPage();
        String accountUsername = "zmend";
        String accountGroup = "Group One";
        int currentPage = 1;
        int pageSize = 3;
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        doReturn(expected).when(accountRepository).findByUsernameIgnoreCaseContainingAndAccountGroups_AccountGroupIgnoreCaseContainingOrUsernameIgnoreCaseContainingAndRoles_DefaultGroupIgnoreCaseContainingOrderByAccountIdDesc(accountUsername, accountGroup, accountUsername, accountGroup, pageable);
        Page<Account> actual = accountService.findAccountPageByUsernameOrAccountGroup(accountUsername, accountGroup, currentPage, pageSize);
        assertEquals(expected, actual);
    }

    @Test
    public void should_turn_account_into_userview_dto() {
        UserViewDto expected = buildUserViewDto();
        Account account = buildAccount();

        Set<AccountGroup> accountGroups = new HashSet<>();
        accountGroups.add(buildAccountGroup());
        account.setAccountGroups(accountGroups);

        Set<Permission> permissionSet = new HashSet<>();
        permissionSet.add(buildPermission());
        account.setPermissions(permissionSet);

        UserViewDto actual = accountService.toUserViewDto(account);

        assertEquals(expected, actual);
    }

    @Test
    public void should_delete_account_by_username() {
        String username = buildAccount().getUsername();
        accountService.deleteAccountByUsername(username);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(accountRepository).deleteAccountByUsername(argumentCaptor.capture());

        assertEquals("zachmend", argumentCaptor.getValue());
    }

    private Page<Account> buildAccountPage() {
        return new Page<Account>() {
            @Override
            public int getTotalPages() {
                return 2;
            }

            @Override
            public long getTotalElements() {
                return 0;
            }

            @Override
            public <U> Page<U> map(Function<? super Account, ? extends U> function) {
                return null;
            }

            @Override
            public int getNumber() {
                return 0;
            }

            @Override
            public int getSize() {
                return 3;
            }

            @Override
            public int getNumberOfElements() {
                return 0;
            }

            @Override
            public List<Account> getContent() {
                return null;
            }

            @Override
            public boolean hasContent() {
                return false;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Account> iterator() {
                return null;
            }
        };

    }
}
