package org.autismallianceofmichigan.navigator.services.test;

import org.aspectj.lang.annotation.Before;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AddressType;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountRepository;
import org.autismallianceofmichigan.navigator.persistence.repository.AddressTypeRepository;
import org.autismallianceofmichigan.navigator.service.AddressTypeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class AddressTypeServiceTest {
    @Mock
    AddressTypeRepository addressTypeRepository;

    @InjectMocks
    AddressTypeService addressTypeService;

    private AddressType buildAddressType() {
        return AddressType.builder()
                .type("MockTests")
                .build();
    }
    @Test
    public void should_have_address_type() {
        AddressType addressType = buildAddressType();

        doReturn(Optional.of(addressType)).when(addressTypeRepository).findByType(eq("MockTests"));
        boolean actual = addressTypeService.hasAddressType(addressType.getType());
        assertTrue(actual);
    }

    @Test
    public void should_save_address_type() {
        AddressType expected = buildAddressType();
        doReturn(expected).when(addressTypeRepository).save(expected);
        AddressType actual = addressTypeService.saveAddressType(expected);

        Assert.assertEquals(expected, actual);
    }
}
