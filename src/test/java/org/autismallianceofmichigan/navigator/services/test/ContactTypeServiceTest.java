package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.providerDto.ContactTypeOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.autismallianceofmichigan.navigator.persistence.repository.ContactTypeRepository;
import org.autismallianceofmichigan.navigator.service.ContactTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class ContactTypeServiceTest {
    @Mock
    ContactTypeRepository contactTypeRepository;

    @InjectMocks
    ContactTypeService contactTypeService;

    private ContactType buildContactType() {
        return ContactType.builder()
                .type("ContactTest")
                .contactTypeId(1L)
                .build();
    }

    private List<ContactType> buildListOfContactTypes() {
        List<ContactType> contactTypes = new ArrayList<>();
        contactTypes.add(buildContactType());
        return contactTypes;
    }

    private List<ContactTypeOptionDto> buildListOfContactTypeOptionDtos() {
        List<ContactTypeOptionDto> contactTypeOptionDtos = new ArrayList<>();
        contactTypeOptionDtos.add(ContactTypeOptionDto.builder().contactTypeId(1L).type("ContactTest").build());
        return contactTypeOptionDtos;
    }

    @Test
    public void should_get_all_contact_type_dtos() {
        List<ContactTypeOptionDto> expected = buildListOfContactTypeOptionDtos();
        doReturn(buildListOfContactTypes().stream()).when(contactTypeRepository).streamAll();
        List<ContactTypeOptionDto> actual = contactTypeService.getAllContactTypeOptionDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_have_contact_type() {
        ContactType contactType = buildContactType();

        doReturn(Optional.of(contactType)).when(contactTypeRepository).findByType("ContactTest");
        boolean actual = contactTypeService.hasContactType(contactType.getType());
        assertTrue(actual);
    }

    @Test
    public void should_save_contact() {
        ContactType expected = buildContactType();
        doReturn(expected).when(contactTypeRepository).save(expected);
        ContactType actual = contactTypeService.saveContactType(expected);
        assertEquals(expected, actual);
    }
}
