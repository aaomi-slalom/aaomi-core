package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.InsuranceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.InsuranceOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Insurance;
import org.autismallianceofmichigan.navigator.persistence.repository.InsuranceRepository;
import org.autismallianceofmichigan.navigator.service.InsuranceService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class InsuranceServiceTest {

    @Mock
    InsuranceRepository insuranceRepository;

    @InjectMocks
    InsuranceService insuranceService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Insurance buildInsurance() {
        return Insurance.builder()
                .insuranceName("Test Insurance")
                .insuranceId(1L)
                .contactNumber("1111111111")
                .website("www.test.com")
                .build();
    }

    private List<Insurance> buildListOfInsurance() {
        List<Insurance> insuranceList = new ArrayList<>();
        insuranceList.add(buildInsurance());
        return insuranceList;
    }

    private List<InsuranceDto> buildListOfInsuranceDtos() {
        List<InsuranceDto> insuranceDtoList = new ArrayList<>();
        insuranceDtoList.add(InsuranceDto.builder()
                .insuranceName(buildInsurance().getInsuranceName())
                .website(buildInsurance().getWebsite())
                .contactNumber(buildInsurance().getContactNumber())
                .insuranceId(buildInsurance().getInsuranceId()).build());
        return insuranceDtoList;
    }

    private List<InsuranceOptionDto> buildListOfInsuranceOptionDtos() {
        List<InsuranceOptionDto> insuranceDtoList = new ArrayList<>();
        insuranceDtoList.add(InsuranceOptionDto.builder()
                .insuranceName(buildInsurance().getInsuranceName())
                .insuranceId(buildInsurance().getInsuranceId()).build());
        return insuranceDtoList;
    }

    @Test
    public void should_have_insurance() {
        Insurance insurance = buildInsurance();
        doReturn(Optional.of(insurance)).when(insuranceRepository).findByInsuranceName("Test Insurance");
        boolean actual = insuranceService.hasInsuranceWithName(insurance.getInsuranceName());

        assertTrue(actual);
    }

    @Test
    public void should_find_insurance_by_id() {
        Insurance expected = buildInsurance();
        doReturn(Optional.of(expected)).when(insuranceRepository).findById(1L);
        Insurance actual = insuranceService.findByInsuranceId(expected.getInsuranceId());

        assertEquals(expected, actual);
    }

    @Test
    public void should_save_insurance() {
        Insurance insurance = buildInsurance();
        insuranceService.saveInsurance(insurance);
        ArgumentCaptor<Insurance> argumentCaptor = ArgumentCaptor.forClass(Insurance.class);
        verify(insuranceRepository).save(argumentCaptor.capture());

        assertEquals("Test Insurance", argumentCaptor.getValue().getInsuranceName());
    }

    @Test
    public void should_delete_insurance() {
        Insurance insurance = buildInsurance();
        insuranceService.deleteById(insurance.getInsuranceId());
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(insuranceRepository).deleteById(argumentCaptor.capture());

        assertEquals(1L, argumentCaptor.getValue());
    }

    @Test
    public void should_get_all_insurance_dtos() {
        doReturn(buildListOfInsurance().stream()).when(insuranceRepository).streamAll();
        List<InsuranceDto> expected = buildListOfInsuranceDtos();
        List<InsuranceDto> actual = insuranceService.getAllInsuranceDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_insurance_option_dtos() {
        doReturn(buildListOfInsurance().stream()).when(insuranceRepository).streamAll();
        List<InsuranceOptionDto> expected = buildListOfInsuranceOptionDtos();
        List<InsuranceOptionDto> actual = insuranceService.getAllInsuranceOptionDtos();

        assertEquals(expected, actual);
    }
}
