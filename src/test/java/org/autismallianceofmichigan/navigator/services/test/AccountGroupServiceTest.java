package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminUserDto.AccountGroupDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountGroupRepository;
import org.autismallianceofmichigan.navigator.service.AccountGroupService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountGroupServiceTest {
    @Mock
    AccountGroupRepository accountGroupRepository;

    @InjectMocks
    AccountGroupService accountGroupService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private AccountGroup buildAccountGroup() {
        return AccountGroup.builder()
                .accountGroup("Test Group")
                .accountGroupId(1L)
                .accounts(buildSetOfAccounts())
                .build();
    }

    private List<AccountGroup> buildListOfAccountGroups() {
        List<AccountGroup> accountGroupList = new ArrayList<>();
        accountGroupList.add(buildAccountGroup());
        accountGroupList.add(AccountGroup.builder()
                .accountGroupId(2L)
                .accountGroup("Group Two")
                .accounts(buildSetOfAccounts())
                .build());
        return accountGroupList;
    }

    private List<AccountGroupDto> buildListOfAccountGroupDtos(List<AccountGroup> accountGroupList) {
        List<AccountGroupDto> accountGroupDtos = new ArrayList<>();
        for (AccountGroup accountGroup: accountGroupList
        ) { accountGroupDtos.add(
                AccountGroupDto.builder()
                        .accountGroup(accountGroup.getAccountGroup())
                        .userCount(accountGroup.getAccounts().size())
                        .build());
        }
        return accountGroupDtos;
    }

    @Test
    public void should_have_account_group() {
        AccountGroup expected = buildAccountGroup();
        doReturn(Optional.of(expected)).when(accountGroupRepository).findByAccountGroup("Test Group");
        boolean actual = accountGroupService.hasAccountGroup(expected.getAccountGroup());
        assertTrue(actual);
    }

    @Test
    public void should_save_account_group() {
        AccountGroup expected = buildAccountGroup();
        doReturn(expected).when(accountGroupRepository).save(expected);
        AccountGroup actual = accountGroupService.saveAccountGroup(expected);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void should_find_by_account_group() {
        AccountGroup expected = buildAccountGroup();
        doReturn(Optional.of(expected)).when(accountGroupRepository).findByAccountGroup("Test Group");
        AccountGroup actual = accountGroupService.findByAccountGroup(expected.getAccountGroup());

        assertEquals(expected, actual);
    }

    @Test
    public void should_stream_all_account_groups() {
        List<AccountGroup> accountGroupList = buildListOfAccountGroups();
        doReturn(accountGroupList.stream()).when(accountGroupRepository).streamAll();
        List<AccountGroupDto> expected = buildListOfAccountGroupDtos(accountGroupList);
        List<AccountGroupDto> actual = accountGroupService.getAllAccountGroupDtos();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_account_group_names() {
        List<String> expected = new ArrayList<>();
        expected.add("Test Group");
        expected.add("Group Two");
        doReturn(buildListOfAccountGroups().stream()).when(accountGroupRepository).streamAll();
        List<String> actual = accountGroupService.getAllAccountGroupNames();
        assertEquals(expected, actual);
    }

    @Test
    public void should_delete_account_group_by_name() {
        String accountGroupName = buildAccountGroup().getAccountGroup();
        accountGroupService.deleteAccountGroupByAccountGroup(accountGroupName);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(accountGroupRepository).deleteAccountGroupByAccountGroup(argumentCaptor.capture());
        assertEquals("Test Group", argumentCaptor.getValue());
    }

    private Set<Account> buildSetOfAccounts() {
        return new Set<Account>() {
            @Override
            public int size() {
                return 2;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Account> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] ts) {
                return null;
            }

            @Override
            public boolean add(Account account) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> collection) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Account> collection) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> collection) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> collection) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
    }
}
