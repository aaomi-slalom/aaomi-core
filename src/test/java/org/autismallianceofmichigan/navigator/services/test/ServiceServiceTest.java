package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Service;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceCategory;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceRepository;
import org.autismallianceofmichigan.navigator.service.ServiceService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ServiceServiceTest {

    @Mock
    ServiceRepository serviceRepository;

    @InjectMocks
    ServiceService serviceService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Service buildService() {
        return Service.builder()
                .serviceId(1L)
                .active(true)
                .service("Test Service")
                .description("Testing")
                .serviceCategory(buildServiceCategory())
                .build();
    }

    private ServiceCategory buildServiceCategory() {
        return ServiceCategory.builder()
                .category("Test Category")
                .active(true)
                .serviceCategoryId(1L)
                .build();
    }

    private List<Service> buildListOfServices() {
        List<Service> services = new ArrayList<>();
        services.add(buildService());
        return services;
    }

    private List<ServiceOptionDto> buildListOfServiceOptionDtos() {
        List<ServiceOptionDto> serviceOptionDtos = new ArrayList<>();
        serviceOptionDtos.add(ServiceOptionDto.builder().serviceId(1L).service("Test Service").category("Test Category").build());
        return serviceOptionDtos;
    }

    @Test
    public void should_find_service_by_id() {
        Service expected = buildService();
        doReturn(Optional.of(expected)).when(serviceRepository).findById(1L);
        Service actual = serviceService.findByServiceId(expected.getServiceId());

        assertEquals(expected, actual);
    }

//    @Test
//    public void should_have_service_with_name_and_category() {
//        Service service = buildService();
//        doReturn(Optional.of(service)).when(serviceRepository).findByServiceAndServiceCategory_Category("Test Service", "Test Category");
//        boolean actual = serviceService.hasServiceWithNameAndCategory(service.getService(), service.getServiceCategory().getCategory());
//
//        assertTrue(actual);
//    }

    @Test
    public void should_save_service() {
        Service service = buildService();
        serviceService.saveService(service);
        ArgumentCaptor<Service> argumentCaptor = ArgumentCaptor.forClass(Service.class);
        verify(serviceRepository).save(argumentCaptor.capture());
        assertEquals("Test Service", argumentCaptor.getValue().getService());
    }

    @Test
    public void should_delete_service() {
        Service service = buildService();
        serviceService.deleteByServiceId(service.getServiceId());
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(serviceRepository).deleteById(argumentCaptor.capture());

        assertEquals(1L, argumentCaptor.getValue());
    }

    @Test
    public void should_get_all_service_option_dtos() {
        List<ServiceOptionDto> expected = buildListOfServiceOptionDtos();
        doReturn(buildListOfServices().stream()).when(serviceRepository).streamAll();
        List<ServiceOptionDto> actual = serviceService.getAllServiceOptionDtos();

        assertEquals(expected, actual);
    }
}
