package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceAgeGroupDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceAgeGroupOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceAgeGroup;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceAgeGroupRepository;
import org.autismallianceofmichigan.navigator.service.ServiceAgeGroupService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ServiceAgeGroupServiceTest {
    @Mock
    ServiceAgeGroupRepository serviceAgeGroupRepository;

    @InjectMocks
    ServiceAgeGroupService serviceAgeGroupService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private ServiceAgeGroup buildServiceAgeGroup() {
        return ServiceAgeGroup.builder()
                .ageEnd(10)
                .ageStart(1)
                .serviceAgeGroupId(1L)
                .build();
    }

    private List<ServiceAgeGroup> buildListOfServiceAgeGroups() {
        List<ServiceAgeGroup> serviceAgeGroups = new ArrayList<>();
        serviceAgeGroups.add(buildServiceAgeGroup());
        return serviceAgeGroups;
    }

    private List<ServiceAgeGroupDto> buildListOfServiceAgeGroupDtos(List<ServiceAgeGroup> serviceAgeGroups) {
        List<ServiceAgeGroupDto> serviceAgeGroupDtos = new ArrayList<>();
        serviceAgeGroupDtos.add(ServiceAgeGroupDto.builder()
                .ageStart(serviceAgeGroups.get(0).getAgeStart())
                .ageEnd(serviceAgeGroups.get(0).getAgeEnd())
                .serviceAgeGroupId(1L)
                .build());

        return serviceAgeGroupDtos;
    }

    private List<ServiceAgeGroupOptionDto> buildListOfServiceAgeGroupOptionDtos(List<ServiceAgeGroup> serviceAgeGroups) {
        List<ServiceAgeGroupOptionDto> serviceAgeGroupOptionDtos = new ArrayList<>();
        serviceAgeGroupOptionDtos.add(ServiceAgeGroupOptionDto.builder()
                .ageStart(serviceAgeGroups.get(0).getAgeStart())
                .ageEnd(serviceAgeGroups.get(0).getAgeEnd())
                .serviceAgeGroupId(1L)
                .ageGroupDisplay("1 - 10")
                .build());

        return serviceAgeGroupOptionDtos;
    }

    @Test
    public void should_get_all_service_group_dtos() {
        List<ServiceAgeGroup> serviceAgeGroups = buildListOfServiceAgeGroups();
        List<ServiceAgeGroupDto> expected = buildListOfServiceAgeGroupDtos(serviceAgeGroups);
        doReturn(serviceAgeGroups.stream()).when(serviceAgeGroupRepository).streamAll();
        List<ServiceAgeGroupDto> actual = serviceAgeGroupService.getAllServiceAgeGroupDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_service_group_option_dtos() {
        List<ServiceAgeGroup> serviceAgeGroups = buildListOfServiceAgeGroups();
        List<ServiceAgeGroupOptionDto> expected = buildListOfServiceAgeGroupOptionDtos(serviceAgeGroups);
        doReturn(serviceAgeGroups.stream()).when(serviceAgeGroupRepository).streamAll();
        List<ServiceAgeGroupOptionDto> actual = serviceAgeGroupService.getAllServiceAgeGroupOptionDtos();

        assertEquals(expected, actual);
    }

    @Test
    public void should_have_age_group_with_start_and_end_age() {
        ServiceAgeGroup serviceAgeGroup = buildServiceAgeGroup();
        doReturn(Optional.of(serviceAgeGroup)).when(serviceAgeGroupRepository).findByAgeStartAndAgeEnd(1, 10);
        boolean actual = serviceAgeGroupService.hasAgeGroupWithAgeStartAndAgeEnd(serviceAgeGroup.getAgeStart(), serviceAgeGroup.getAgeEnd());

        assertTrue(actual);
    }

    @Test
    public void should_find_age_group_by_id() {
        ServiceAgeGroup expected = buildServiceAgeGroup();
        doReturn(Optional.of(expected)).when(serviceAgeGroupRepository).findById(1L);
        ServiceAgeGroup actual = serviceAgeGroupService.findById(expected.getServiceAgeGroupId());

        assertEquals(expected, actual);
    }

    @Test
    public void should_save_age_group() {
        ServiceAgeGroup serviceAgeGroup = buildServiceAgeGroup();
        serviceAgeGroupService.saveServiceAgeGroup(serviceAgeGroup);
        ArgumentCaptor<ServiceAgeGroup> argumentCaptor = ArgumentCaptor.forClass(ServiceAgeGroup.class);
        verify(serviceAgeGroupRepository).save(argumentCaptor.capture());

        assertEquals(10, argumentCaptor.getValue().getAgeEnd());
    }

    @Test
    public void should_delete_age_group() {
        ServiceAgeGroup serviceAgeGroup = buildServiceAgeGroup();
        serviceAgeGroupService.deleteById(serviceAgeGroup.getServiceAgeGroupId());
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(serviceAgeGroupRepository).deleteById(argumentCaptor.capture());

        assertEquals(1L, argumentCaptor.getValue().longValue());
    }
}
