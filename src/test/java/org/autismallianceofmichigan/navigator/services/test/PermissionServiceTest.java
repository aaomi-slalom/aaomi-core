package org.autismallianceofmichigan.navigator.services.test;

import org.autismallianceofmichigan.navigator.dto.adminUserDto.PermissionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.autismallianceofmichigan.navigator.persistence.repository.PermissionRepository;
import org.autismallianceofmichigan.navigator.service.PermissionService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class PermissionServiceTest {
    @Mock
    PermissionRepository permissionRepository;

    @InjectMocks
    PermissionService permissionService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Permission buildPermission() {
        return Permission.builder()
                .permissionId(1L)
                .permCode("TestCode")
                .description("Testing")
                .build();
    }

    private List<Permission> buildListOfPermissions() {
        List<Permission> permissionList = new ArrayList<>();
        permissionList.add(buildPermission());
        permissionList.add(Permission.builder()
                .permCode("TestTwo")
                .permissionId(2L)
                .description("Test")
                .build());
        return permissionList;
    }

    private List<PermissionDto> buildListOfPermissionDtos(List<Permission> permissions) {
        List<PermissionDto> permissionDtos = new ArrayList<>();
        for(Permission permission : permissions) {
            permissionDtos.add(PermissionDto.builder()
                    .description(permission.getDescription())
                    .permission(permission.getPermCode())
                    .build());
        }
        return permissionDtos;
    }

    @Test
    public void should_have_permission() {
        Permission expected = buildPermission();
        doReturn(Optional.of(expected)).when(permissionRepository).findByPermCode("TestCode");
        boolean actual = permissionService.hasPermissionWithPermCode(expected.getPermCode());

        assertTrue(actual);
    }

    @Test
    public void should_save_permission() {
        Permission expected = buildPermission();
        doReturn(expected).when(permissionRepository).save(expected);
        Permission actual = permissionService.savePermission(expected);

        assertEquals(expected, actual);
    }

    @Test
    public void should_find_by_perm_code() {
        Permission expected = buildPermission();
        doReturn(Optional.of(expected)).when(permissionRepository).findByPermCode("TestCode");
        Permission actual = permissionService.findByPermCode(expected.getPermCode());
        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_permission_names() {
        List<String> expected = new ArrayList<>();
        expected.add("TestCode");
        expected.add("TestTwo");
        doReturn(buildListOfPermissions().stream()).when(permissionRepository).streamAll();
        List<String> actual = permissionService.getAllPermissionNames();

        assertEquals(expected, actual);
    }

    @Test
    public void should_get_all_permission_dtos() {
        List<Permission> permissionList = buildListOfPermissions();
        List<PermissionDto> expected = buildListOfPermissionDtos(permissionList);
        doReturn(permissionList.stream()).when(permissionRepository).streamAll();
        List<PermissionDto> actual = permissionService.getAllPermissionDtos();

        assertEquals(expected, actual);
    }
}
