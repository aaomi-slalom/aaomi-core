package org.autismallianceofmichigan.navigator.controller.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.var;
import org.autismallianceofmichigan.navigator.dto.registrationDto.EmailAvailabilityRequest;
import org.autismallianceofmichigan.navigator.dto.registrationDto.RegistrationRequest;
import org.autismallianceofmichigan.navigator.dto.registrationDto.UsernameAvailabilityRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationControllerTest {
//    @Autowired
//    MockMvc mockMvc;
//
//    @MockBean
//    AccountService accountService;
//
//    public static String asJsonString(final Object obj) {
//        try {
//            return new ObjectMapper().writeValueAsString(obj);
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    private Account buildAccount() {
//        return Account.builder()
//                .username("zachmend")
//                .email("zachmend@gmail.com")
//                .password("Gators44")
//                .enabled(true)
//                .build();
//    }
//
//    @Test
//    public void username_availability_POST_success() throws Exception {
//        UsernameAvailabilityRequest usernameAvailabilityRequest = new UsernameAvailabilityRequest();
//        usernameAvailabilityRequest.setUsername("user1");
//        var result = mockMvc.perform(post("/api/username/availability")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(asJsonString(usernameAvailabilityRequest))
//        );
//        result.andExpect(status().isOk());
//    }
//
//    @Test
//    public void email_availability_POST_success() throws Exception {
//        EmailAvailabilityRequest emailAvailabilityRequest = new EmailAvailabilityRequest();
//        emailAvailabilityRequest.setEmail("test@test.com");
//        var result = mockMvc.perform(post("/api/email/enabled/availability")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(asJsonString(emailAvailabilityRequest))
//        );
//        result.andExpect(status().isOk());
//    }

//    @Test
//    public void registration_endpoint_POST_success() throws Exception {
//        RegistrationRequest registrationRequest = new RegistrationRequest();
//        registrationRequest.setUsername("zach");
//        registrationRequest.setEmail("zach@test.com");
//        registrationRequest.setPassword("Gators44");
//        registrationRequest.setConfirmPassword("Gators44");
//        registrationRequest.setRole("NAVIGATOR");
//        registrationRequest.setPasswordMatching(true);
//        var result = mockMvc.perform(post("/api/register")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(asJsonString(registrationRequest))
//        );
//        result.andExpect(status().isOk());
//    }


}
