package org.autismallianceofmichigan.navigator.controller.test;

import org.autismallianceofmichigan.navigator.controller.AdminUserController;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.GroupAccountDto;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.GroupAccountsPageDto;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.ViewGroupUsersRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class AdminUserControllerReturnValuesTest {
    @Mock
    AccountService accountService;

    @Mock
    Environment environment;

    @InjectMocks
    AdminUserController adminUserController;


    private GroupAccountsPageDto buildGroupAccountsPageDto() {
        List<GroupAccountDto> groupAccountDtos = new ArrayList<>();
        groupAccountDtos.add(buildGroupAccountDto());
        return GroupAccountsPageDto.builder()
                .totalElements(1)
                .totalPages(1)
                .groupAccountDtos(groupAccountDtos)
                .build();
    }


    private Account buildAccount() {
        return Account.builder()
                .accountId(1L)
                .username("zach")
                .password("Gators44")
                .email("zach@test.com")
                .build();
    }

    private GroupAccountDto buildGroupAccountDto() {
        return GroupAccountDto.builder()
                .username("zach")
                .email("zach@test.com")
                .firstName("New")
                .lastName("User")
                .rootAdmin(false)
                .image("")
                .build();
    }

    @Test
    public void should_get_group_accounts_dto() {
        ViewGroupUsersRequest viewGroupUsersRequest = new ViewGroupUsersRequest();
        viewGroupUsersRequest.setAccountGroup("GROUP_Test Group");
        viewGroupUsersRequest.setPageSize(3);
        viewGroupUsersRequest.setCurrentPage(2);

        doReturn("app-admin-username").when(environment).getProperty("app-admin-username");

        Page<Account> page = new PageImpl<>(Collections.singletonList(buildAccount()));
        doReturn(page).when(accountService).findAccountPage(
                eq(viewGroupUsersRequest.getAccountGroup()),
                eq(viewGroupUsersRequest.getCurrentPage()),
                eq(viewGroupUsersRequest.getPageSize()));

        GroupAccountsPageDto expected = buildGroupAccountsPageDto();

        GroupAccountsPageDto actual = adminUserController.adminViewGroupUsersEndpoint(viewGroupUsersRequest);

        assertEquals(expected, actual);
    }
}
