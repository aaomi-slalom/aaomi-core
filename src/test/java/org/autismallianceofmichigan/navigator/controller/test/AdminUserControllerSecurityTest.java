package org.autismallianceofmichigan.navigator.controller.test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.autismallianceofmichigan.navigator.controller.AdminUserController;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.autismallianceofmichigan.navigator.service.AccountGroupService;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.autismallianceofmichigan.navigator.service.PermissionService;
import org.autismallianceofmichigan.navigator.service.RoleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdminUserControllerSecurityTest {
    @MockBean
    RoleService roleService;

    @MockBean
    AccountGroupService accountGroupService;

    @MockBean
    PermissionService permissionService;

    @MockBean
    AccountService accountService;

    @MockBean
    Environment environment;

    @InjectMocks
    AdminUserController adminUserController;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    private AccountGroup buildAccountGroup() {
        Set<Permission> permissions = new HashSet<>();
        permissions.add(Permission.builder().permCode("PERM_TEST").description("Testing").build());

        return AccountGroup.builder()
                .accountGroup("TestGroup")
                .accountGroupId(1L)
                .permissions(permissions)
                .build();
    }

    private Permission buildPermission() {
        return Permission.builder().permCode("PERM_TEST").description("Testing").build();
    }

    private Account buildAccount() {
        return Account.builder()
                .accountId(1L)
                .username("zach")
                .password("Gators44")
                .email("zach@test.com")
                .build();
    }

    private List<PermissionDto> buildListOfPermissionDtos() {
        List<PermissionDto> permissionDtos = new ArrayList<>();
        permissionDtos.add(PermissionDto.builder().permission("PERM_VIEW_SYSTEM_PERMISSIONS").description("View all system permissions").build());
        return permissionDtos;
    }

    private List<AccountGroupDto> buildListOfAccountGroupDto() {
        List<AccountGroupDto> accountGroupDtos = new ArrayList<>();
        accountGroupDtos.add(AccountGroupDto.builder().accountGroup("Test Group").build());
        return accountGroupDtos;
    }

    private GroupAccountDto buildGroupAccountDto() {
        return GroupAccountDto.builder()
                .username("zach")
                .email("zach@test.com")
                .firstName("zach")
                .lastName("mend")
                .rootAdmin(false)
                .build();
    }


    @Test
    @WithMockUser(value = "user", authorities = {"PERM_VIEW_SYSTEM_ROLES"})
    public void can_view_system_roles() throws Exception {
        List<RoleDto> expected = new ArrayList<>(Collections.singletonList(RoleDto.builder()
                .role("admin")
                .description("Test Role")
                .build()));
        doReturn(expected).when(roleService).getAllRoleDtos();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/admin/roles/view")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        List<RoleDto> actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<RoleDto>>() {
        });
        assertEquals(expected, actual);
    }

    @Test
    @WithMockUser
    public void cant_view_system_roles() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/roles/view")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(value = "user", authorities = {"PERM_MODIFY_ROLE_DEFAULT_GROUP"})
    public void should_be_able_to_view_all_account_group_names() throws Exception {
        List<String> expected = new ArrayList<>();
        AccountGroup accountGroup = buildAccountGroup();
        expected.add(accountGroup.getAccountGroup());
        doReturn(expected).when(accountGroupService).getAllAccountGroupNames();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/admin/groups/names/view")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        List<String> actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<String>>() {
        });
        assertEquals(expected, actual);
    }

    @Test
    @WithMockUser
    public void should_not_be_able_to_view_all_account_group_names() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/groups/names/view")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(value = "user", authorities = {"PERM_VIEW_SYSTEM_PERMISSIONS"})
    public void should_be_able_to_view_all_permissions() throws Exception {
        List<PermissionDto> expected = buildListOfPermissionDtos();
        doReturn(expected).when(permissionService).getAllPermissionDtos();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/admin/permissions/view")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        List<PermissionDto> actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<PermissionDto>>() {
        });
        assertEquals(expected, actual);
    }

    @Test
    @WithMockUser
    public void should_not_be_able_to_view_all_permissions() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/permissions/view")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"PERM_VIEW_GROUPS"})
    public void should_be_able_to_view_account_groups() throws Exception {
        List<AccountGroupDto> expected = buildListOfAccountGroupDto();
        doReturn(expected).when(accountGroupService).getAllAccountGroupDtos();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/admin/groups/view")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        List<AccountGroupDto> actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<AccountGroupDto>>() {
        });
        assertEquals(expected, actual);
    }

    @Test
    @WithMockUser(authorities = {"PERM_VIEW_SYSTEM_PERMISSIONS"})
    public void should_not_be_able_to_view_account_groups() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/groups/view")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"PERM_CREATE_OR_MODIFY_GROUP"})
    public void should_have_access_to_modify_group_page() throws Exception {
        AccountGroupCreateRequest accountGroupCreateRequest = new AccountGroupCreateRequest();
        accountGroupCreateRequest.setAccountGroup("GROUP_Test Group");
        mvc.perform(MockMvcRequestBuilders.post("/api/admin/group/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(accountGroupCreateRequest)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void should_not_have_access_to_modify_group_page() throws Exception {
        AccountGroupCreateRequest accountGroupCreateRequest = new AccountGroupCreateRequest();
        accountGroupCreateRequest.setAccountGroup("GROUP_Test Group");
        mvc.perform(MockMvcRequestBuilders.post("/api/admin/group/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(accountGroupCreateRequest)))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"PERM_VIEW_GROUPS"})
    public void should_be_able_to_view_group_users() throws Exception {
        ViewGroupUsersRequest viewGroupUsersRequest = new ViewGroupUsersRequest();
        viewGroupUsersRequest.setAccountGroup("GROUP_Test Group");
        viewGroupUsersRequest.setPageSize(3);
        viewGroupUsersRequest.setCurrentPage(2);

        mvc.perform(MockMvcRequestBuilders.post("/api/admin/group/users/view")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(viewGroupUsersRequest)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void should_not_be_able_to_view_group_users() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/admin/group/users/view")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"isAuthenticated()"})
    public void should_succeed_show_admin_group() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/admingroup")
                .accept(MediaType.ALL))
                .andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    public void should_fail_to_see_admin_group() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/admingroup")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"PERM_VIEW_GROUPS"})
    public void should_be_able_to_view_group_permissions() throws Exception {
        AccountGroup accountGroup = buildAccountGroup();
        List<String> expected = new ArrayList<>();
        expected.add("PERM_TEST");
        doReturn(accountGroup).when(accountGroupService).findByAccountGroup("TestGroup");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api/admin/group/permissions/view/TestGroup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(accountGroup)))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        List<String> actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<String>>() {
        });
        assertEquals(expected, actual);
    }

    @Test
    @WithMockUser
    public void should_fail_to_view_group_permissions() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/group/permissions/view/TestGroup")
                .accept(MediaType.ALL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"PERM_CREATE_OR_MODIFY_GROUP"})
    public void should_succeed_add_group_endpoint() throws Exception {
        GroupPermissionAddRequest groupPermissionAddRequest = new GroupPermissionAddRequest();
        groupPermissionAddRequest.setAccountGroup("GROUP_TestGroup");
        groupPermissionAddRequest.setPermission("PERM_TEST");
        doReturn(true).when(accountGroupService).hasAccountGroup(groupPermissionAddRequest.getAccountGroup());
        doReturn(buildAccountGroup()).when(accountGroupService).findByAccountGroup(groupPermissionAddRequest.getAccountGroup());
        doReturn(buildPermission()).when(permissionService).findByPermCode(groupPermissionAddRequest.getPermission());
        mvc.perform(MockMvcRequestBuilders.post("/api/admin/group/permission/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(groupPermissionAddRequest)))
                .andExpect(status().isOk());
    }
}
