package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;
import org.autismallianceofmichigan.navigator.dto.registrationDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountVerification;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.autismallianceofmichigan.navigator.service.MailService;
import org.autismallianceofmichigan.navigator.service.ValidationService;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.management.relation.RoleNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class RegistrationController {
    private final AccountService accountService;
    private final MailService mailService;
    private final ValidationService validationService;
    private final PasswordEncoder passwordEncoder;
    private final Environment env;

    @PreAuthorize("permitAll()")
    @PostMapping("/username/availability")
    public ResponseEntity<?> checkUsernameAvailability(@RequestBody UsernameAvailabilityRequest usernameAvailabilityRequest) {
        boolean usernameFound = accountService.hasUsername(usernameAvailabilityRequest.getUsername());

        return ResponseEntity.ok(UsernameAvailabilityResponse.builder()
                .isUsernameAvailable(!usernameFound)
                .build());
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/email/enabled/availability")
    public ResponseEntity<?> checkEmailAvailability(@RequestBody EmailAvailabilityRequest emailAvailabilityRequest) {
        EmailAvailabilityResponse emailAvailabilityResponse = EmailAvailabilityResponse.builder()
                .isEmailAvailable(true)
                .isAccountEnabled(false)
                .build();

        boolean emailFound = accountService.hasEmail(emailAvailabilityRequest.getEmail());
        if (!emailFound) {
            return ResponseEntity.ok(emailAvailabilityResponse);
        }

        emailAvailabilityResponse.setEmailAvailable(false);
        boolean isAccountEnabled = accountService.isAccountEnabledWithEmail(emailAvailabilityRequest.getEmail());
        emailAvailabilityResponse.setAccountEnabled(isAccountEnabled);
        return ResponseEntity.ok(emailAvailabilityResponse);
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/register")
    public ResponseEntity<?> registerEndPoint(@RequestBody RegistrationRequest registrationRequest)
            throws IOException, RoleNotFoundException {
        if (validationService.hasValidationErrors(registrationRequest)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to create your account...").build());
        }

        registrationRequest.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        Account savedAccount = accountService.save(accountService.toAccount(registrationRequest));
        sendConfirmationEmail(savedAccount.getEmail(), savedAccount);

        return ResponseEntity.ok(RegistrationResponse.builder()
                .message("Please check your email to verify and enable your account.").build());
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/register/verification")
    public RedirectView verifyRegistrationEndpoint(@RequestParam("token") String verificationToken) {
        AccountVerification accountVerification = accountService.getAccountVerificationByVerificationToken(verificationToken);
        int expireInMinutes = 10;
        String angularAppUrl = env.getProperty("angular-app-url");
        String clientVerificationUrl = angularAppUrl + "/register/verification/";

        if (accountVerification == null) {
            return new RedirectView(clientVerificationUrl + UUID.randomUUID().toString());
        } else if (DateUtils.addMinutes(accountVerification.getCreatedAt(), expireInMinutes).compareTo(new Date()) < 0) {
            return new RedirectView(clientVerificationUrl + "expiration");
        }

        Account savedAccount = accountVerification.getAccount();
        savedAccount.setEnabled(true);
        accountService.saveRegisteredAccount(savedAccount);
        return new RedirectView(clientVerificationUrl + "success");
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/register/verification/resend")
    public ResponseEntity<?> resendVerificationEndpoint(@RequestBody ResendVerificationRequest resendVerificationRequest)
            throws IOException {
        if (validationService.hasValidationErrors(resendVerificationRequest)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to resend email...").build());
        }

        String email = resendVerificationRequest.getEmail();
        Account savedAccount = accountService.findByEmail(email);

        if (savedAccount.isEnabled() || savedAccount.isLocked()) {
            return ResponseEntity.badRequest().body(ResendVerificationResponse.builder()
                    .message("Email cannot be resent.")
                    .build());
        }

        sendConfirmationEmail(email, savedAccount);

        return ResponseEntity.ok(ResendVerificationResponse.builder()
                .message("Email is resent. You can send another one after 30 seconds.")
                .build());
    }

    private void sendConfirmationEmail(String to, Account savedAccount) throws IOException {
        String verificationToken = UUID.randomUUID().toString();
        accountService.createAccountVerificationToken(savedAccount, verificationToken);

        String serverContextUrl = env.getProperty("server-context-url");
        String verificationUrl = serverContextUrl + "/api/register/verification?token=" + verificationToken;

        String subject = "Autism Alliance of Michigan Account Verification";
        String content = "Please click on this link to enable your account: " + verificationUrl;

        mailService.sendMail(to, subject, content);
    }
}
