package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class UserProviderController {
    private final AccountService accountService;
    private final ProviderInfoService providerInfoService;
    private final SecurityContextService securityContextService;
    private final ProviderLocationService providerLocationService;
    private final AddressService addressService;
    private final ContactTypeService contactTypeService;
    private final CountyService countyService;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final EmailService emailService;
    private final ContactNumberService contactNumberService;
    private final ProviderAuditService providerAuditService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/check/provider/profile")
    public CheckHasProviderProfileDto checkHasProviderProfileEndpoint() {
        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        boolean isProviderRole = currAccount.getRoles().stream()
                .anyMatch(role -> role.getRole().equals("ROLE_SERVICE_PROVIDER"));

        if (!isProviderRole && !securityContextService.hasAuthority("PERM_VIEW_OWN_PROVIDER_INFO")) {
            throw new AccessDeniedException("Not Authorized");
        }

        boolean hasProviderProfile = currAccount.getProviderInfos().size() > 0;

        return CheckHasProviderProfileDto.builder()
                .hasProviderProfile(hasProviderProfile)
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/provider/profiles")
    public List<UserProviderOverviewDto> getAllAccountProviderProfilesEndpoint() {
        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        boolean isProviderRole = currAccount.getRoles().stream()
                .anyMatch(role -> role.getRole().equals("ROLE_SERVICE_PROVIDER"));

        if (!isProviderRole && !securityContextService.hasAuthority("PERM_VIEW_OWN_PROVIDER_INFO")) {
            throw new AccessDeniedException("Not Authorized");
        }

        Set<ProviderInfo> providerInfos = currAccount.getProviderInfos();
        return providerInfoService.toUserProviderOverviewDtos(providerInfos);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/user/provider/{providerInfoId}")
    public ProviderDto getProviderInfoByIdEndpoint(@PathVariable Long providerInfoId) {
        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        boolean isProviderRole = currAccount.getRoles().stream()
                .anyMatch(role -> role.getRole().equals("ROLE_SERVICE_PROVIDER"));

        if (!isProviderRole && !securityContextService.hasAuthority("PERM_VIEW_OWN_PROVIDER_INFO")) {
            throw new AccessDeniedException("Not Authorized");
        }

        Set<ProviderInfo> providerInfos = currAccount.getProviderInfos();
        if (providerInfos.stream().anyMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId)) {
            return providerInfoService.getProviderInfoById(providerInfoId);
        }

        return null;
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/provider/profile")
    @Transactional
    public ResponseEntity<?> createUnapprovedProviderInfoEndpoint(@RequestBody CreateProviderRequestWithoutAccount createProviderRequestWithoutAccount) {
        String providerName = createProviderRequestWithoutAccount.getProviderInfo().getProviderName();
        if (providerInfoService.hasProviderInfoWithName(providerName)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate provider profile...").build());
        }

        Account currAccount = securityContextService.findCurrAccount();
        boolean isProviderRole = currAccount.getRoles().stream()
                .anyMatch(role -> role.getRole().equals("ROLE_SERVICE_PROVIDER"));

        if (!isProviderRole && !securityContextService.hasAuthority("PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO")) {
            throw new AccessDeniedException("Not Authorized");
        }

        Set<ProviderInfo> providerInfos = currAccount.getProviderInfos();
        ProviderInfo providerInfoSaved = providerInfoService.buildProviderInfo(
                createProviderRequestWithoutAccount.getProviderInfo(),
                createProviderRequestWithoutAccount.getProviderLocations(),
                false,
                currAccount.getUsername()
        );
        ProviderAudit providerAudit1 = providerAuditService.buildAudit(currAccount, providerInfoSaved, providerInfoSaved.getProviderName() + " is created");
        providerInfoSaved.addAuditLogs(providerAudit1);
        ProviderAudit providerAudit2 = providerAuditService.buildAudit(currAccount, providerInfoSaved, "Existing Account - " + currAccount.getUsername() + " is linked with Provider - " + providerInfoSaved.getProviderName());
        providerInfoSaved.addAuditLogs(providerAudit2);

        providerInfos.add(providerInfoSaved);
        currAccount.setProviderInfos(providerInfos);

        accountService.save(currAccount);
        accountService.sendApprovalNotificationToAllAdmins(providerName, "create provider");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider profile created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/{providerInfoId}/location/add")
    @Transactional
    public ResponseEntity<?> addLocationToProviderEndpoint(
            @PathVariable Long providerInfoId,
            @RequestBody CreateProviderLocationRequest createProviderLocationRequest
    ) {
        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        if (currAccount.getProviderInfos().stream().noneMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        Set<ProviderLocation> providerLocations = providerInfo.getProviderLocations();
        ProviderLocation providerLocation = providerLocationService.buildProviderLocation(providerInfo, createProviderLocationRequest, false);

        if (providerLocations.stream().anyMatch(pl -> pl.getLocation().equals(providerLocation.getLocation()))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create location with the same name...").build());
        }

        ProviderAudit providerAudit = providerAuditService.buildAudit(
                currAccount,
                providerInfo,
                String.format("Location - %s is added to Provider - %s (Unapproved Information)",
                        providerLocation.getLocation(),
                        providerInfo.getProviderName())
        );
        providerInfo.addAuditLogs(providerAudit);

        String lastEditUsername = currAccount.getUsername();

        providerLocations.add(providerLocation);
        providerInfo.setProviderLocations(providerLocations);
        providerInfo.setLastEditUsername(lastEditUsername);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "add location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/{providerLocationId}")
    @Transactional
    public ResponseEntity<?> userUpdateProviderLocationNameEndpoint(
            @PathVariable Long providerLocationId,
            @RequestBody UpdateLocationNameRequest updateLocationNameRequest
    ) {
        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                "Location - " + providerLocation.getLocation() + " is updated to Location - " + updateLocationNameRequest.getLocationName() + " (Unapproved Information)"
        );
        providerInfo.addAuditLogs(providerAudit);

        providerLocation.setModifyLocationRequested(true);
        providerLocation.setModifiedLocation(updateLocationNameRequest.getLocationName());

        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "change location name");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location name updated and approval requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/address/{addressId}")
    @Transactional
    public ResponseEntity<?> userUpdateProviderLocationAddressEndpoint(
            @PathVariable Long addressId,
            @RequestBody UpdateLocationAddressRequest updateLocationAddressRequest
    ) {
        Address address = addressService.findById(addressId);
        ProviderInfo providerInfo = address.getProviderLocation().getProviderInfo();
        String providerName = providerInfo.getProviderName();

        if (!isAuthorizedToView(address.getProviderLocation().getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Address - %s, %s, %s %s is updated to %s, %s, %s %s (Unapproved Information)",
                        address.getAddressType().getType(),
                        address.getStreet(),
                        address.getCity(),
                        address.getState(),
                        address.getZip(),
                        updateLocationAddressRequest.getStreet(),
                        updateLocationAddressRequest.getCity(),
                        updateLocationAddressRequest.getState(),
                        updateLocationAddressRequest.getZip()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        address.setModifyAddressRequested(true);
        address.setModifiedCity(updateLocationAddressRequest.getCity());
        address.setModifiedState(updateLocationAddressRequest.getState());
        address.setModifiedStreet(updateLocationAddressRequest.getStreet());
        address.setModifiedZip(updateLocationAddressRequest.getZip());

        providerInfoService.save(providerInfo);
        addressService.save(address);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "change location address");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message(address.getAddressType().getType() +
                        " address updated and approval requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/{locationId}/email/add")
    @Transactional
    public ResponseEntity<?> userAddEmailToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateEmailRequest createEmailRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        List<Email> emails = providerLocation.getEmails();
        ContactType contactType = contactTypeService.findByType(createEmailRequest.getContactType());
        String emailStr = createEmailRequest.getEmail();

        if (emails.stream()
                .anyMatch(e ->
                        e.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                e.getEmail().equals(emailStr))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate email...").build());
        }

        Email email = Email.builder()
                .email(emailStr)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(false)
                .build();

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("New %s email(%s) is added to Location - %s (Unapproved Information)",
                        email.getContactType().getType(),
                        email.getEmail(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        emails.add(email);
        providerLocation.setEmails(emails);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add email to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Requested to add new email").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/{locationId}/contactnumber/add")
    @Transactional
    public ResponseEntity<?> userAddContactNumberToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateContactNumberRequest createContactNumberRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        List<ContactNumber> contactNumbers = providerLocation.getContactNumbers();
        ContactType contactType = contactTypeService.findByType(createContactNumberRequest.getContactType());
        String number = createContactNumberRequest.getNumber();

        if (contactNumbers.stream()
                .anyMatch(n ->
                        n.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                n.getNumber().equals(number))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate contact number...").build());
        }

        ContactNumber contactNumber = ContactNumber.builder()
                .number(number)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(false)
                .build();

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("New %s contact number(%s) is added to Location - %s (Unapproved Information)",
                        contactNumber.getContactType().getType(),
                        contactNumber.getNumber(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        contactNumbers.add(contactNumber);
        providerLocation.setContactNumbers(contactNumbers);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add contact number to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Requested to add new contact number").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/{locationId}/counties")
    @Transactional
    public ResponseEntity<?> userAddServiceCountyToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody UpdateCountiesRequest updateCountiesRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        String providerName = providerInfo.getProviderName();

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        List<County> countiesUpdate = countyService.findCountiesByCountyIds(updateCountiesRequest.getCountyIds());

        String oldCountyNames = providerLocation.getCounties().stream().map(County::getCounty).collect(Collectors.joining(", "));
        String newCountyNames = countiesUpdate.stream().map(County::getCounty).collect(Collectors.joining(", "));
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("Counties(%s) are updated to (%s) for Location - %s (Unapproved Information)",
                        oldCountyNames,
                        newCountyNames,
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        providerLocation.setModifyCountiesRequested(true);
        providerLocation.setModifiedCounties(countiesUpdate);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add county to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service counties updates requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/location/{locationId}/service/add")
    @Transactional
    public ResponseEntity<?> userAddServiceToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody List<CreateLocationSrvcLinkRequest> createLocationSrvcLinkRequests) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        List<LocationSrvcLink> currLocationSrvcLinks = providerLocation.getLocationSrvcLinks();

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.addLocationSrvcLinks(providerLocation, createLocationSrvcLinkRequests, false);

        String serviceName = locationSrvcLinks.get(0).getService().getService();
        String categoryName = locationSrvcLinks.get(0).getService().getServiceCategory().getCategory();
        String whetherInternal = locationSrvcLinks.get(0).getService().isInternal() ? "Internal" : "";
        String insuranceNames = locationSrvcLinks.stream()
                .map(lsl -> lsl.getInsurance().getInsuranceName())
                .distinct()
                .collect(Collectors.joining(", "));
        String ageGroupValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceAgeGroup().getAgeStart() + " - " + lsl.getServiceAgeGroup().getAgeEnd())
                .distinct()
                .collect(Collectors.joining(", "));
        String deliveryValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceDelivery().getDelivery())
                .distinct()
                .collect(Collectors.joining(", "));

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Service - " +
                                "%s(Category: %s) with insurances(%s), " +
                                "deliveries(%s), ageGroups(%s) " +
                                "is added to Provider - %s (Location - %s) (Unapproved Information)",
                        whetherInternal,
                        serviceName,
                        categoryName,
                        insuranceNames,
                        deliveryValues,
                        ageGroupValues,
                        providerInfo.getProviderName(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        currLocationSrvcLinks.addAll(locationSrvcLinks);
        providerLocation.setLocationSrvcLinks(currLocationSrvcLinks);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add service to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("user/provider/location/email/{emailId}")
    @Transactional
    public ResponseEntity<?> userDeleteEmailFromLocationEndpoint(
            @PathVariable Long emailId
    ) {
        if (!emailService.hasEmailWithId(emailId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete email").build());
        }

        Email email = emailService.findById(emailId);
        if (!isAuthorizedToView(email.getProviderLocation().getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        ProviderInfo providerInfo = email.getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s email(%s) is removed from Provider - %s (Unapproved Information)",
                        email.getContactType().getType(),
                        email.getEmail(),
                        providerInfo.getProviderName()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        emailService.requestToDelete(email);
        providerInfoService.save(providerInfo);

        if (email.isCreateApproved()) {
            accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "delete email from location");
        }

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved email deleted or requested to delete approved email").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("user/provider/location/contactnumber/{contactNumberId}")
    @Transactional
    public ResponseEntity<?> userDeleteContactNumberFromLocationEndpoint(
            @PathVariable Long contactNumberId
    ) {
        if (!contactNumberService.hasContactNumberWithId(contactNumberId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete contact number").build());
        }

        ContactNumber contactNumber = contactNumberService.findById(contactNumberId);
        if (!isAuthorizedToView(contactNumber.getProviderLocation().getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        ProviderInfo providerInfo = contactNumber.getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s contact number(%s) is removed from Provider - %s (Unapproved Information)",
                        contactNumber.getContactType().getType(),
                        contactNumber.getNumber(),
                        providerInfo.getProviderName()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        contactNumberService.requestToDelete(contactNumber);
        providerInfoService.save(providerInfo);

        if (contactNumber.isCreateApproved()) {
            accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "delete contact number from location");
        }

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved contact number deleted or requested to delete approved contact number").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("user/provider/location/service/{identifier}")
    @Transactional
    public ResponseEntity<?> userDeleteServiceFromLocationEndpoint(
            @PathVariable String identifier
    ) {
        if (!locationSrvcLinkService.hasLocationSrvcLinkByIdentifier(identifier)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete service").build());
        }

        if (!isAuthorizedToView(locationSrvcLinkService.findByIdentifier(identifier).getProviderLocation().getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.findAllByIdentifier(identifier);
        String serviceName = locationSrvcLinks.get(0).getService().getService();
        String categoryName = locationSrvcLinks.get(0).getService().getServiceCategory().getCategory();
        String whetherInternal = locationSrvcLinks.get(0).getService().isInternal() ? "Internal" : "";
        String insuranceNames = locationSrvcLinks.stream()
                .map(lsl -> lsl.getInsurance().getInsuranceName())
                .distinct()
                .collect(Collectors.joining(", "));
        String ageGroupValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceAgeGroup().getAgeStart() + " - " + lsl.getServiceAgeGroup().getAgeEnd())
                .distinct()
                .collect(Collectors.joining(", "));
        String deliveryValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceDelivery().getDelivery())
                .distinct()
                .collect(Collectors.joining(", "));

        ProviderInfo providerInfo = locationSrvcLinks.get(0).getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Service - " +
                                "%s(Category: %s) with insurances(%s), " +
                                "deliveries(%s), ageGroups(%s) " +
                                "is deleted from Provider - %s (Unapproved Information)",
                        whetherInternal,
                        serviceName,
                        categoryName,
                        insuranceNames,
                        deliveryValues,
                        ageGroupValues,
                        providerInfo.getProviderName())
        );
        providerInfo.addAuditLogs(providerAudit);

        locationSrvcLinkService.requestToDeleteAllByIdentifier(identifier);
        providerInfoService.save(providerInfo);

        if (locationSrvcLinks.get(0).isCreateApproved()) {
            accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "delete service from location");
        }

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved service deleted or requested to delete approved service").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("user/provider/demographics/{providerInfoId}")
    public ResponseEntity<?> userUpdateProviderDemographicsEndpoint(@PathVariable Long providerInfoId, @RequestBody CreateProviderInfoRequest createProviderInfoRequest) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);

        if (!isAuthorizedToView(providerInfo.getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        if (!providerInfo.getProviderName().toLowerCase().equals(createProviderInfoRequest.getProviderName().toLowerCase()) &&
                providerInfoService.hasProviderInfoWithName(createProviderInfoRequest.getProviderName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update provider demographics...").build());
        }

        providerInfo.setModifyRequested(true);
        providerInfo.setModifiedProviderName(createProviderInfoRequest.getProviderName());
        providerInfo.setModifiedWebsite(createProviderInfoRequest.getWebsite());
        providerInfo.setModifiedLogo(createProviderInfoRequest.getLogo());
        providerInfo.setModifiedDescription(createProviderInfoRequest.getDescription());
        providerInfo.setModifiedFacebook(createProviderInfoRequest.getFacebook());
        providerInfo.setModifiedLinkedin(createProviderInfoRequest.getLinkedin());
        providerInfo.setModifiedTwitter(createProviderInfoRequest.getTwitter());
        providerInfo.setModifiedInstagram(createProviderInfoRequest.getInstagram());
        providerInfo.setModifiedYoutube(createProviderInfoRequest.getYoutube());

        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "update provider demographics");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider demographics update requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("user/provider/location/{providerLocationId}")
    public ResponseEntity<?> userDeleteProviderLocationByIdEndpoint(@PathVariable Long providerLocationId) {
        if (!providerLocationService.hasProviderLocationWithId(providerLocationId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to remove location").build());
        }

        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);

        if (!isAuthorizedToView(providerLocation.getProviderInfo().getProviderInfoId())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not Authorized").build());
        }

        providerLocationService.requestToDelete(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved location deleted or requested to delete approved location").build());
    }

    private boolean isAuthorizedToView(Long providerInfoId) {
        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        return currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId);
    }
}
