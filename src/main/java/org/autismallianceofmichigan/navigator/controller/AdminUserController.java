package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.*;
import org.autismallianceofmichigan.navigator.dto.registrationDto.RegistrationResponse;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.autismallianceofmichigan.navigator.persistence.entity.Role;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AdminUserController {
    private final AccountService accountService;
    private final RoleService roleService;
    private final AccountGroupService accountGroupService;
    private final PermissionService permissionService;
    private final ValidationService validationService;
    private final SecurityContextService securityContextService;
    private final PasswordEncoder passwordEncoder;
    private final Environment env;

    @PreAuthorize("hasAuthority('PERM_VIEW_SYSTEM_ROLES')")
    @GetMapping("/admin/roles/view")
    public List<RoleDto> adminViewAllRolesEndpoint() {
        return roleService.getAllRoleDtos();
    }

    @PreAuthorize("hasAuthority('PERM_MODIFY_ROLE_DEFAULT_GROUP')")
    @GetMapping("/admin/groups/names/view")
    public List<String> adminViewAllAccountGroupNamesEndpoint() {
        return accountGroupService.getAllAccountGroupNames();
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_SYSTEM_PERMISSIONS')")
    @GetMapping("/admin/permissions/view")
    public List<PermissionDto> adminViewAllPermissionsEndpoint() {
        return permissionService.getAllPermissionDtos();
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_GROUPS')")
    @GetMapping("/admin/groups/view")
    public List<AccountGroupDto> adminViewAllAccountGroupsEndpoint() {
        return accountGroupService.getAllAccountGroupDtos();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_GROUP')")
    @PostMapping("/admin/group/create")
    public ResponseEntity<?> adminCreateAccountGroupEndpoint(@RequestBody AccountGroupCreateRequest accountGroupCreateRequest) {
        if (validationService.hasValidationErrors(accountGroupCreateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to create account group...").build());
        }

        AccountGroup accountGroup = AccountGroup.builder()
                .accountGroup(accountGroupCreateRequest.getAccountGroup())
                .build();

        accountGroupService.saveAccountGroup(accountGroup);
        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Account group successfully created").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_GROUPS')")
    @PostMapping("/admin/group/users/view")
    public GroupAccountsPageDto adminViewGroupUsersEndpoint(@Valid @RequestBody ViewGroupUsersRequest viewGroupUsersRequest) {
        String adminUsername = env.getProperty("app-admin-username");

        Page<Account> accountPage = accountService.findAccountPage(
                viewGroupUsersRequest.getAccountGroup(),
                viewGroupUsersRequest.getCurrentPage(),
                viewGroupUsersRequest.getPageSize()
        );

        List<GroupAccountDto> groupAccountDtos = accountPage.get().map(account -> {
                    boolean rootAdmin = false;

                    if (account.getUsername().equals(adminUsername)) {
                        rootAdmin = true;
                    }

                    return account.getUserProfile() != null ?
                            GroupAccountDto.builder()
                                    .username(account.getUsername())
                                    .email(account.getEmail())
                                    .firstName(account.getUserProfile().getFirstName())
                                    .lastName(account.getUserProfile().getLastName())
                                    .image(account.getUserProfile().getImage())
                                    .rootAdmin(rootAdmin)
                                    .build()
                            : GroupAccountDto.builder()
                            .username(account.getUsername())
                            .email(account.getEmail())
                            .firstName("New")
                            .lastName("User")
                            .image("")
                            .rootAdmin(rootAdmin)
                            .build();
                }
        ).collect(Collectors.toList());

        return GroupAccountsPageDto.builder()
                .groupAccountDtos(groupAccountDtos)
                .totalElements(accountPage.getTotalElements())
                .totalPages(accountPage.getTotalPages())
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/admin/admingroup")
    public ResponseEntity<?> showAdminGroupEndpoint() {
        String adminGroup = env.getProperty("app-admin-group");

        return ResponseEntity.ok(GenericMessageResponse.builder().message(adminGroup).build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_GROUPS')")
    @GetMapping("/admin/group/permissions/view/{accountGroup}")
    public List<String> adminViewGroupPermissionsEndpoint(@PathVariable String accountGroup) {
        AccountGroup currAccountGroup = accountGroupService.findByAccountGroup(accountGroup);

        return currAccountGroup.getPermissions().stream()
                .map(Permission::getPermCode)
                .collect(Collectors.toList());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_GROUP')")
    @PostMapping("/admin/group/permission/add")
    public ResponseEntity<?> adminAddGroupPermissionEndpoint(@RequestBody GroupPermissionAddRequest groupPermissionAddRequest) {
        if (!accountGroupService.hasAccountGroup(groupPermissionAddRequest.getAccountGroup())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Group not found"));
        }

        AccountGroup currAccountGroup = accountGroupService.findByAccountGroup(groupPermissionAddRequest.getAccountGroup());
        Set<Permission> currPermissions = currAccountGroup.getPermissions();

        Permission permissionToAdd = permissionService.findByPermCode(groupPermissionAddRequest.getPermission());
        currPermissions.add(permissionToAdd);

        currAccountGroup.setPermissions(currPermissions);

        accountGroupService.saveAccountGroup(currAccountGroup);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Permission added to group successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_USERS')")
    @PostMapping("/admin/users/view")
    public UsersPageDto adminViewAllUsersEndpoint(@Valid @RequestBody ViewUsersRequest viewUsersRequest) {
        Page<Account> accountPage = accountService.findAccountPageByUsernameOrAccountGroup(
                viewUsersRequest.getUsernameSearch(),
                viewUsersRequest.getAccountGroupSearch(),
                viewUsersRequest.getCurrentPage(),
                viewUsersRequest.getPageSize()
        );

        List<UserDto> userDtos = accountPage.get().map(account -> {
            List<String> groups = new ArrayList<>();
            if (!account.getAccountGroups().isEmpty()) {
                groups = account.getAccountGroups().stream()
                        .map(AccountGroup::getAccountGroup)
                        .collect(Collectors.toList());
            }

            List<String> defaultGroups = new ArrayList<>();
            if (!account.getRoles().isEmpty()) {
                defaultGroups = account.getRoles().stream()
                        .map(Role::getDefaultGroup)
                        .collect(Collectors.toList());
            }

            defaultGroups.removeAll(groups);

            if (account.getUserProfile() != null) {
                return UserDto.builder()
                        .username(account.getUsername())
                        .email(account.getEmail())
                        .enabled(account.isEnabled())
                        .firstName(account.getUserProfile().getFirstName())
                        .lastName(account.getUserProfile().getLastName())
                        .groups(groups)
                        .defaultGroups(defaultGroups)
                        .image(account.getUserProfile().getImage())
                        .build();
            }

            return UserDto.builder()
                    .username(account.getUsername())
                    .email(account.getEmail())
                    .enabled(account.isEnabled())
                    .firstName("New")
                    .lastName("User")
                    .image("")
                    .groups(groups)
                    .defaultGroups(defaultGroups)
                    .build();
        }).collect(Collectors.toList());

        return UsersPageDto.builder()
                .totalElements(accountPage.getTotalElements())
                .totalPages(accountPage.getTotalPages())
                .userDtos(userDtos)
                .build();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @PostMapping("/admin/user/create")
    public ResponseEntity<?> adminCreateNewUser(@RequestBody UserCreateRequest userCreateRequest) {
        if (validationService.hasValidationErrors(userCreateRequest)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to create user account...").build());
        }

        Account newAccount = Account.builder()
                .username(userCreateRequest.getUsername())
                .email(userCreateRequest.getEmail())
                .password(passwordEncoder.encode(userCreateRequest.getPassword()))
                .locked(true)
                .build();

        accountService.save(newAccount);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CHANGE_USER_PASSWORD')")
    @PostMapping("/admin/user/password/change")
    public ResponseEntity<?> adminChangeUserPassword(@RequestBody UserPasswordChangeRequest userPasswordChangeRequest) {
        if (validationService.hasValidationErrors(userPasswordChangeRequest)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to create user account...").build());
        }

        if (userPasswordChangeRequest.getUsername().equals(env.getProperty("app-admin-username"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Default admin's password cannot be changed from user's page").build());
        }

        Account currAccount = accountService.findByUsername(userPasswordChangeRequest.getUsername());
        currAccount.setPassword(passwordEncoder.encode(userPasswordChangeRequest.getNewPassword()));
        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("User password changed successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_USERS')")
    @GetMapping("/admin/users/user/{username}")
    public UserViewDto adminViewSpecificUserEndpoint(@PathVariable String username) {
        Account currAccount = accountService.findByUsername(username);

        UserViewDto userViewDto = accountService.toUserViewDto(currAccount);
        userViewDto.setAdminGroup(env.getProperty("app-admin-group"));
        userViewDto.setRootAdmin(false);

        if (username.equals(env.getProperty("app-admin-username"))) {
            userViewDto.setRootAdmin(true);
        }

        return userViewDto;
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @GetMapping("/admin/users/user/{username}/disable")
    public ResponseEntity<?> adminDisableUserEndpoint(@PathVariable String username) {
        if (username.equals(env.getProperty("app-admin-username"))) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("App default admin cannot be disabled").build());
        }

        Account currAccount = accountService.findByUsername(username);

        currAccount.setEnabled(false);
        currAccount.setLocked(true);
        accountService.save(currAccount);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account disabled").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @PostMapping("/admin/users/user/username/update")
    public ResponseEntity<?> adminChangeUsernameEndpoint(@Valid @RequestBody UpdateUsernameRequest updateUsernameRequest) {
        String defaultAdminUsername = env.getProperty("app-admin-username");
        boolean isAuthorized = !updateUsernameRequest.getUsername().equals(defaultAdminUsername);

        if (!isAuthorized) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Default admin username cannot be changed").build());
        }

        if (accountService.hasUsername(updateUsernameRequest.getNewUsername())) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("This username already exists").build());
        }

        Account currAccount = accountService.findByUsername(updateUsernameRequest.getUsername());

        currAccount.setUsername(updateUsernameRequest.getNewUsername());
        accountService.save(currAccount);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account username is updated").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @PostMapping("/admin/users/user/email/update")
    public ResponseEntity<?> adminChangeUserEmailEndpoint(@Valid @RequestBody UpdateEmailRequest updateEmailRequest) {
        String defaultAdminUsername = env.getProperty("app-admin-username");
        boolean isAuthorized = !updateEmailRequest.getUsername().equals(defaultAdminUsername) || securityContextService.findCurrAccount().getUsername().equals(defaultAdminUsername);

        if (!isAuthorized) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Only default admin can update default admin email").build());
        }

        if (accountService.hasEmail(updateEmailRequest.getEmail())) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("This email has already been used").build());
        }

        Account currAccount = accountService.findByUsername(updateEmailRequest.getUsername());

        currAccount.setEmail(updateEmailRequest.getEmail());
        accountService.save(currAccount);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account email is updated").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @GetMapping("/admin/users/user/{username}/enable")
    public ResponseEntity<?> adminEnableUserEndpoint(@PathVariable String username) {
        Account currAccount = accountService.findByUsername(username);

        currAccount.setEnabled(true);
        currAccount.setLocked(false);
        accountService.save(currAccount);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account enabled").build());
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_GROUPS')")
    @GetMapping("/admin/user/{username}/groups/notin/view")
    public List<String> adminViewUserAvailableGroupsToAdd(@PathVariable String username) {
        Account currAccount = accountService.findByUsername(username);

        List<String> inGroups = currAccount.getAccountGroups().stream()
                .map(AccountGroup::getAccountGroup)
                .collect(Collectors.toList());

        List<String> allGroups = accountGroupService.getAllAccountGroupNames();
        List<String> notInGroups = new ArrayList<>(allGroups);
        notInGroups.removeAll(inGroups);

        return notInGroups;
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_PERMISSIONS')")
    @GetMapping("/admin/user/{username}/permissions/notin/view")
    public List<String> adminViewUserAvailablePermissionsToAdd(@PathVariable String username) {
        Account currAccount = accountService.findByUsername(username);

        Set<String> inPermissions = currAccount.getPermissions().stream()
                .map(Permission::getPermCode)
                .collect(Collectors.toSet());

        Set<String> inGroupPermissions = currAccount.getAccountGroups().stream()
                .map(group -> group.getPermissions().stream()
                        .map(Permission::getPermCode)
                        .collect(Collectors.toSet()))
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

        inPermissions.addAll(inGroupPermissions);

        List<String> allPermissions = permissionService.getAllPermissionNames();
        List<String> notInPermissions = new ArrayList<>(allPermissions);
        notInPermissions.removeAll(inPermissions);

        return notInPermissions;
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_GROUPS')")
    @PostMapping("/admin/user/group/add")
    public ResponseEntity<?> adminAddGroupToUser(@RequestBody UserAddGroupRequest userAddGroupRequest) {
        if (validationService.hasValidationErrors(userAddGroupRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to add group to user...").build());
        }

        if (userAddGroupRequest.getUsername().equals(env.getProperty("app-admin-username"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Default admin cannot add more user groups").build());
        }

        Account currAccount = accountService.findByUsername(userAddGroupRequest.getUsername());
        Set<AccountGroup> currGroups = currAccount.getAccountGroups();

        AccountGroup accountGroupToAdd = accountGroupService.findByAccountGroup(userAddGroupRequest.getAccountGroup());
        currGroups.add(accountGroupToAdd);
        currAccount.setAccountGroups(currGroups);
        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Group added to user successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_PERMISSIONS')")
    @PostMapping("/admin/user/permission/add")
    public ResponseEntity<?> adminAddPermissionToUser(@RequestBody UserAddPermissionRequest userAddPermissionRequest) {
        if (userAddPermissionRequest.getUsername().equals(env.getProperty("app-admin-username"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Default admin cannot add more user permissions").build());
        }

        if (validationService.hasValidationErrors(userAddPermissionRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to add permission to user...").build());
        }

        Account currAccount = accountService.findByUsername(userAddPermissionRequest.getUsername());
        Set<Permission> currPermissions = currAccount.getPermissions();

        Permission permissionToAdd = permissionService.findByPermCode(userAddPermissionRequest.getPermission());
        currPermissions.add(permissionToAdd);
        currAccount.setPermissions(currPermissions);
        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Permission added to user successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_PERMISSIONS')")
    @DeleteMapping("/admin/user/{username}/group/{accountGroup}/delete")
    public ResponseEntity<?> adminDeleteGroupFromUser(@PathVariable String username, @PathVariable String accountGroup) {
        if (username.equals(env.getProperty("app-admin-username")) && accountGroup.equals(env.getProperty("app-admin-group"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Default admin cannot be removed from admin group").build());
        }

        Account currAccount = accountService.findByUsername(username);
        Set<AccountGroup> currGroups = currAccount.getAccountGroups();

        currGroups.removeIf(currGroup -> currGroup.getAccountGroup().equals(accountGroup));
        currAccount.setAccountGroups(currGroups);
        accountService.save(currAccount);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Group removed from user successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_MANAGE_USER_PERMISSIONS')")
    @DeleteMapping("/admin/user/{username}/permission/{permission}/delete")
    public ResponseEntity<?> adminDeletePermissionFromUser(@PathVariable String username, @PathVariable String permission) {
        Account currAccount = accountService.findByUsername(username);
        Set<Permission> currPermissions = currAccount.getPermissions();

        currPermissions.removeIf(currPermission -> currPermission.getPermCode().equals(permission));
        currAccount.setPermissions(currPermissions);
        accountService.save(currAccount);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Permission removed from user successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_USER')")
    @DeleteMapping("/admin/user/delete/{username}")
    public ResponseEntity<?> adminDeleteUserEndpoint(@PathVariable String username) {
        Account currAccount = accountService.findByUsername(username);
        if (currAccount.isEnabled() || !currAccount.getAccountGroups().isEmpty()) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Could not delete active or account with group privileges").build());
        }

        accountService.deleteAccountByUsername(username);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account removed successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_GROUP')")
    @DeleteMapping("/admin/group/permission/delete/{accountGroup}/{permission}")
    public ResponseEntity<?> adminDeleteGroupPermissionEndpoint(@PathVariable String accountGroup, @PathVariable String permission) {
        if (accountGroup.equals(env.getProperty("app-admin-group"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot delete permission from admin group"));
        }

        if (!accountGroupService.hasAccountGroup(accountGroup)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Group not found"));
        }

        AccountGroup currAccountGroup = accountGroupService.findByAccountGroup(accountGroup);
        Set<Permission> currPermissions = currAccountGroup.getPermissions();

        currPermissions.removeIf(currPermission -> currPermission.getPermCode().equals(permission));
        currAccountGroup.setPermissions(currPermissions);
        accountGroupService.saveAccountGroup(currAccountGroup);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Permission removed from group successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_MODIFY_ROLE_DEFAULT_GROUP')")
    @PostMapping("/admin/role/group/update")
    public ResponseEntity<?> adminUpdateDefaultGroupOnRoleEndpoint(@RequestBody RoleDefaultGroupUpdateRequest roleDefaultGroupUpdateRequest) {
        Role role = roleService.findByRole(roleDefaultGroupUpdateRequest.getRole());

        if (!accountGroupService.hasAccountGroup(roleDefaultGroupUpdateRequest.getDefaultGroup())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Not a valid group")
                    .build());
        }

        role.setDefaultGroup(roleDefaultGroupUpdateRequest.getDefaultGroup());
        roleService.saveRole(role);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Default group updated successfully")
                .build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_GROUP')")
    @DeleteMapping("/admin/group/user/delete/{accountGroup}/{username}")
    public ResponseEntity<?> adminDeleteGroupUserEndpoint(
            @PathVariable String accountGroup, @PathVariable String username
    ) {
        if (!accountGroupService.hasAccountGroup(accountGroup)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Group not found"));
        }

        if (accountGroup.equals(env.getProperty("app-admin-group")) && username.equals(env.getProperty("app-admin-username"))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot delete default admin user from admin group"));
        }

        Account currAccount = accountService.findByUsername(username);

        currAccount.getAccountGroups().removeIf(group -> group.getAccountGroup().equals(accountGroup));
        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account removed from group successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_GROUP')")
    @DeleteMapping("/admin/group/delete/{accountGroup}")
    public ResponseEntity<?> adminDeleteGroupEndpoint(@PathVariable String accountGroup) {
        if (!accountGroupService.hasAccountGroup(accountGroup)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Group not found"));
        }

        if (accountGroupService.findByAccountGroup(accountGroup).getAccounts().size() != 0) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Could not remove group with existing associated accounts"));
        }

        accountGroupService.deleteAccountGroupByAccountGroup(accountGroup);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Group removed successfully").build());
    }
}
