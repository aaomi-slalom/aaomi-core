package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AdminProviderController {
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final ValidationService validationService;
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;
    private final AddressService addressService;
    private final ContactTypeService contactTypeService;
    private final CountyService countyService;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final EmailService emailService;
    private final ContactNumberService contactNumberService;
    private final SecurityContextService securityContextService;
    private final ProviderAuditService providerAuditService;

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO') and hasAuthority('PERM_CREATE_OR_MODIFY_USER')")
    @GetMapping("admin/provider/account/{username}")
    public ExistingUserDto linkProviderWithExistingAccountEndpoint(@PathVariable String username) {
        Account userAccount = accountService.findByUsername(username);
        UserProfile userProfile = userAccount.getUserProfile();

        return ExistingUserDto.builder()
                .username(userAccount.getUsername())
                .firstName(userProfile == null ? "New" : userProfile.getFirstName())
                .lastName(userProfile == null ? "User" : userProfile.getLastName())
                .image(userProfile == null ? "" : userProfile.getImage())
                .build();
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @PostMapping("admin/providers/view")
    public ProvidersPageDto getAllProvidersEndpoint(@Valid @RequestBody ViewProvidersRequest viewProvidersRequest) {
        return providerInfoService.getProvidersPageDtoByViewProvidersRequest(viewProvidersRequest);
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PostMapping("admin/provider")
    public ResponseEntity<?> createAndLinkProviderInfoEndpoint(@RequestBody CreateProviderRequest createProviderRequest) {
        if (providerInfoService.hasProviderInfoWithName(createProviderRequest.getProviderInfo().getProviderName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate provider...").build());
        }

        Account currAccount = securityContextService.findCurrAccount();
        String editUsername = currAccount.getUsername();

        if (createProviderRequest.getAccount().getEmail() != null) {
            if (validationService.hasValidationErrors(createProviderRequest.getAccount())) {
                return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                        .message("Error when trying to create account...").build());
            }

            CreateProviderAccountRequest createProviderAccountRequest = createProviderRequest.getAccount();
            createProviderAccountRequest.setPassword(passwordEncoder.encode(createProviderAccountRequest.getPassword()));
            createProviderRequest.setAccount(createProviderAccountRequest);
        }

        accountService.createOrLinkAccountInitialProviderInfo(createProviderRequest, true, editUsername, currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider profile created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @GetMapping("admin/provider/{providerInfoId}")
    public ProviderDto getProviderInfoByIdEndpoint(@PathVariable Long providerInfoId) {
        return providerInfoService.getProviderInfoById(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_PROVIDER_INFO')")
    @DeleteMapping("admin/provider/{providerInfoId}")
    public ResponseEntity<?> deleteProviderInfoByIdEndpoint(@PathVariable Long providerInfoId) {
        if (!providerInfoService.hasProviderInfoWithId(providerInfoId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete provider information...").build());
        }

        ProviderInfo providerInfoToDelete = providerInfoService.findById(providerInfoId);

        if (providerInfoToDelete.getProviderLocations().size() > 0) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot delete providers with existing locations and services").build());
        }

        accountService.deleteProviderInfos(providerInfoToDelete);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider information deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @DeleteMapping("admin/provider/location/{providerLocationId}")
    public ResponseEntity<?> deleteProviderLocationByIdEndpoint(@PathVariable Long providerLocationId) {
        if (!providerLocationService.hasProviderLocationWithId(providerLocationId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to remove location").build());
        }

        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);
        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(securityContextService.findCurrAccount(), providerInfo, "Location - " + providerLocation.getLocation() + " is deleted from Provider - " + providerInfo.getProviderName());
        providerInfo.addAuditLogs(providerAudit);
        providerInfoService.save(providerInfo);
        providerLocationService.deleteById(providerLocationId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/demographics/{providerInfoId}")
    public ResponseEntity<?> updateProviderDemographicsEndpoint(@PathVariable Long providerInfoId, @RequestBody CreateProviderInfoRequest createProviderInfoRequest) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);

        if (!providerInfo.getProviderName().toLowerCase().equals(createProviderInfoRequest.getProviderName().toLowerCase()) &&
                providerInfoService.hasProviderInfoWithName(createProviderInfoRequest.getProviderName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update provider demographics...").build());
        }


        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format(
                        "Provider Information (" +
                                "Provider Name: %s, " +
                                "Website: %s, Logo, " +
                                "Description: %s, " +
                                "Facebook: %s, " +
                                "LinkedIn: %s, " +
                                "Twitter: %s, " +
                                "Instagram: %s, " +
                                "YouTube: %s) is updated to Provider Information (" +
                                "Provider Name: %s, " +
                                "Website: %s, Logo, " +
                                "Description: %s, " +
                                "Facebook: %s, " +
                                "LinkedIn: %s, " +
                                "Twitter: %s, " +
                                "Instagram: %s, " +
                                "YouTube: %s)",
                        providerInfo.getProviderName(),
                        providerInfo.getWebsite(),
                        providerInfo.getDescription(),
                        providerInfo.getFacebook(),
                        providerInfo.getLinkedin(),
                        providerInfo.getTwitter(),
                        providerInfo.getInstagram(),
                        providerInfo.getYoutube(),
                        createProviderInfoRequest.getProviderName(),
                        createProviderInfoRequest.getWebsite(),
                        createProviderInfoRequest.getDescription(),
                        createProviderInfoRequest.getFacebook(),
                        createProviderInfoRequest.getLinkedin(),
                        createProviderInfoRequest.getTwitter(),
                        createProviderInfoRequest.getInstagram(),
                        createProviderInfoRequest.getYoutube())
        );
        providerInfo.addAuditLogs(providerAudit);

        providerInfo.setProviderName(createProviderInfoRequest.getProviderName());
        providerInfo.setWebsite(createProviderInfoRequest.getWebsite());
        providerInfo.setLogo(createProviderInfoRequest.getLogo());
        providerInfo.setDescription(createProviderInfoRequest.getDescription());
        providerInfo.setFacebook(createProviderInfoRequest.getFacebook());
        providerInfo.setLinkedin(createProviderInfoRequest.getLinkedin());
        providerInfo.setTwitter(createProviderInfoRequest.getTwitter());
        providerInfo.setInstagram(createProviderInfoRequest.getInstagram());
        providerInfo.setYoutube(createProviderInfoRequest.getYoutube());

        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider demographics updated successfully").build());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("provider/providernames/{providerName}")
    public CheckProviderNameDto checkWhetherProviderNameExistsEndpoint(@PathVariable String providerName) {
        return CheckProviderNameDto.builder()
                .hasProviderName(providerInfoService.hasProviderInfoWithName(providerName))
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("provider/providernames/{providerName}/{providerInfoId}")
    public CheckProviderNameDto checkWhetherUpdateProviderNameExistsEndpoint(@PathVariable String providerName, @PathVariable Long providerInfoId) {
        String currProviderName = providerInfoService.findById(providerInfoId).getProviderName().toLowerCase();

        boolean hasProviderName = false;
        if (!currProviderName.equals(providerName.toLowerCase()) && providerInfoService.hasProviderInfoWithName(providerName)) {
            hasProviderName = true;
        }

        return CheckProviderNameDto.builder()
                .hasProviderName(hasProviderName)
                .build();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{providerLocationId}")
    public ResponseEntity<?> updateProviderLocationNameEndpoint(
            @PathVariable Long providerLocationId,
            @RequestBody UpdateLocationNameRequest updateLocationNameRequest
    ) {
        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                "Location - " + providerLocation.getLocation() + " is updated to Location - " + updateLocationNameRequest.getLocationName()
        );
        providerInfo.addAuditLogs(providerAudit);

        providerLocation.setLocation(updateLocationNameRequest.getLocationName());

        providerInfoService.save(providerInfo);
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location name updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/address/{addressId}")
    @Transactional
    public ResponseEntity<?> updateProviderLocationAddressEndpoint(
            @PathVariable Long addressId,
            @RequestBody UpdateLocationAddressRequest updateLocationAddressRequest
    ) {
        Address address = addressService.findById(addressId);

        ProviderInfo providerInfo = address.getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Address - %s, %s, %s %s is updated to %s, %s, %s %s",
                        address.getAddressType().getType(),
                        address.getStreet(),
                        address.getCity(),
                        address.getState(),
                        address.getZip(),
                        updateLocationAddressRequest.getStreet(),
                        updateLocationAddressRequest.getCity(),
                        updateLocationAddressRequest.getState(),
                        updateLocationAddressRequest.getZip()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        address.setCity(updateLocationAddressRequest.getCity());
        address.setState(updateLocationAddressRequest.getState());
        address.setStreet(updateLocationAddressRequest.getStreet());
        address.setZip(updateLocationAddressRequest.getZip());

        providerInfoService.save(providerInfo);
        addressService.save(address);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message(address.getAddressType().getType() +
                        " address updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{locationId}/email/add")
    @Transactional
    public ResponseEntity<?> addEmailToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateEmailRequest createEmailRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        List<Email> emails = providerLocation.getEmails();
        ContactType contactType = contactTypeService.findByType(createEmailRequest.getContactType());
        String emailStr = createEmailRequest.getEmail();

        if (emails.stream()
                .anyMatch(e ->
                        e.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                e.getEmail().equals(emailStr))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate email...").build());
        }

        Email email = Email.builder()
                .email(emailStr)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(true)
                .build();

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("New %s email(%s) is added to Location - %s",
                        email.getContactType().getType(),
                        email.getEmail(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        emails.add(email);
        providerLocation.setEmails(emails);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Email added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{locationId}/contactnumber/add")
    @Transactional
    public ResponseEntity<?> addContactNumberToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateContactNumberRequest createContactNumberRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        List<ContactNumber> contactNumbers = providerLocation.getContactNumbers();
        ContactType contactType = contactTypeService.findByType(createContactNumberRequest.getContactType());
        String number = createContactNumberRequest.getNumber();

        if (contactNumbers.stream()
                .anyMatch(n ->
                        n.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                n.getNumber().equals(number))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate contact number...").build());
        }

        ContactNumber contactNumber = ContactNumber.builder()
                .number(number)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(true)
                .build();

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("New %s contact number(%s) is added to Location - %s",
                        contactNumber.getContactType().getType(),
                        contactNumber.getNumber(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        contactNumbers.add(contactNumber);
        providerLocation.setContactNumbers(contactNumbers);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Contact number added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{locationId}/counties")
    @Transactional
    public ResponseEntity<?> addServiceCountyToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody UpdateCountiesRequest updateCountiesRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        List<County> countiesUpdate = countyService.findCountiesByCountyIds(updateCountiesRequest.getCountyIds());

        String oldCountyNames = providerLocation.getCounties().stream().map(County::getCounty).collect(Collectors.joining(", "));
        String newCountyNames = countiesUpdate.stream().map(County::getCounty).collect(Collectors.joining(", "));
        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("Counties(%s) are updated to (%s) for Location - %s",
                        oldCountyNames,
                        newCountyNames,
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        providerLocation.setCounties(countiesUpdate);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service counties updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{locationId}/service/add")
    @Transactional
    public ResponseEntity<?> addServiceToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody List<CreateLocationSrvcLinkRequest> createLocationSrvcLinkRequests) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        List<LocationSrvcLink> currLocationSrvcLinks = providerLocation.getLocationSrvcLinks();

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.addLocationSrvcLinks(providerLocation, createLocationSrvcLinkRequests, true);

        String serviceName = locationSrvcLinks.get(0).getService().getService();
        String categoryName = locationSrvcLinks.get(0).getService().getServiceCategory().getCategory();
        String whetherInternal = locationSrvcLinks.get(0).getService().isInternal() ? "Internal" : "";
        String insuranceNames = locationSrvcLinks.stream()
                .map(lsl -> lsl.getInsurance().getInsuranceName())
                .distinct()
                .collect(Collectors.joining(", "));
        String ageGroupValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceAgeGroup().getAgeStart() + " - " + lsl.getServiceAgeGroup().getAgeEnd())
                .distinct()
                .collect(Collectors.joining(", "));
        String deliveryValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceDelivery().getDelivery())
                .distinct()
                .collect(Collectors.joining(", "));

        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Service - " +
                                "%s(Category: %s) with insurances(%s), " +
                                "deliveries(%s), ageGroups(%s) " +
                                "is added to Provider - %s (Location - %s)",
                        whetherInternal,
                        serviceName,
                        categoryName,
                        insuranceNames,
                        deliveryValues,
                        ageGroupValues,
                        providerInfo.getProviderName(),
                        providerLocation.getLocation())
        );
        providerInfo.addAuditLogs(providerAudit);

        currLocationSrvcLinks.addAll(locationSrvcLinks);
        providerLocation.setLocationSrvcLinks(currLocationSrvcLinks);
        providerLocationService.save(providerLocation);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/{providerInfoId}/location/add")
    public ResponseEntity<?> addLocationToProviderEndpoint(
            @PathVariable Long providerInfoId,
            @RequestBody CreateProviderLocationRequest createProviderLocationRequest
    ) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        Set<ProviderLocation> providerLocations = providerInfo.getProviderLocations();
        ProviderLocation providerLocation = providerLocationService.buildProviderLocation(providerInfo, createProviderLocationRequest, true);

        if (providerLocations.stream().anyMatch(pl -> pl.getLocation().equals(providerLocation.getLocation()))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create location with the same name...").build());
        }

        Account currAccount = securityContextService.findCurrAccount();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                currAccount,
                providerInfo,
                String.format("Location - %s is added to Provider - %s",
                        providerLocation.getLocation(),
                        providerInfo.getProviderName())
        );
        providerInfo.addAuditLogs(providerAudit);

        String lastEditUsername = currAccount.getUsername();

        providerLocations.add(providerLocation);
        providerInfo.setProviderLocations(providerLocations);
        providerInfo.setLastEditUsername(lastEditUsername);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @DeleteMapping("admin/provider/location/email/{emailId}")
    @Transactional
    public ResponseEntity<?> deleteEmailFromLocationEndpoint(
            @PathVariable Long emailId
    ) {
        if (!emailService.hasEmailWithId(emailId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete email").build());
        }

        Email email = emailService.findById(emailId);
        ProviderInfo providerInfo = email.getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s email(%s) is removed from Provider - %s",
                        email.getContactType().getType(),
                        email.getEmail(),
                        providerInfo.getProviderName()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        emailService.deleteById(emailId);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Email deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @DeleteMapping("admin/provider/location/contactnumber/{contactNumberId}")
    @Transactional
    public ResponseEntity<?> deleteContactNumberFromLocationEndpoint(
            @PathVariable Long contactNumberId
    ) {
        if (!contactNumberService.hasContactNumberWithId(contactNumberId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete contact number").build());
        }

        ContactNumber contactNumber = contactNumberService.findById(contactNumberId);
        ProviderInfo providerInfo = contactNumber.getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s contact number(%s) is removed from Provider - %s",
                        contactNumber.getContactType().getType(),
                        contactNumber.getNumber(),
                        providerInfo.getProviderName()
                )
        );
        providerInfo.addAuditLogs(providerAudit);

        contactNumberService.deleteById(contactNumberId);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Contact number deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @DeleteMapping("admin/provider/location/service/{identifier}")
    @Transactional
    public ResponseEntity<?> deleteServiceFromLocationEndpoint(
            @PathVariable String identifier
    ) {
        if (!locationSrvcLinkService.hasLocationSrvcLinkByIdentifier(identifier)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete service").build());
        }

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.findAllByIdentifier(identifier);
        String serviceName = locationSrvcLinks.get(0).getService().getService();
        String categoryName = locationSrvcLinks.get(0).getService().getServiceCategory().getCategory();
        String whetherInternal = locationSrvcLinks.get(0).getService().isInternal() ? "Internal" : "";
        String insuranceNames = locationSrvcLinks.stream()
                .map(lsl -> lsl.getInsurance().getInsuranceName())
                .distinct()
                .collect(Collectors.joining(", "));
        String ageGroupValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceAgeGroup().getAgeStart() + " - " + lsl.getServiceAgeGroup().getAgeEnd())
                .distinct()
                .collect(Collectors.joining(", "));
        String deliveryValues = locationSrvcLinks.stream()
                .map(lsl -> lsl.getServiceDelivery().getDelivery())
                .distinct()
                .collect(Collectors.joining(", "));

        ProviderInfo providerInfo = locationSrvcLinks.get(0).getProviderLocation().getProviderInfo();
        ProviderAudit providerAudit = providerAuditService.buildAudit(
                securityContextService.findCurrAccount(),
                providerInfo,
                String.format("%s Service - " +
                                "%s(Category: %s) with insurances(%s), " +
                                "deliveries(%s), ageGroups(%s) " +
                                "is deleted from Provider - %s",
                        whetherInternal,
                        serviceName,
                        categoryName,
                        insuranceNames,
                        deliveryValues,
                        ageGroupValues,
                        providerInfo.getProviderName())
        );
        providerInfo.addAuditLogs(providerAudit);

        locationSrvcLinkService.deleteAllByIdentifier(identifier);
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO') or hasAuthority('PERM_VIEW_OWN_PROVIDER_INFO')")
    @GetMapping("admin/provider/{providerInfoId}/accounts")
    public List<ProviderLinkedAccountDto> viewAllProviderAccountsEndpoint(@PathVariable Long providerInfoId) {
        if (!securityContextService.hasAuthority("PERM_VIEW_PROVIDER_INFO")) {
            Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
            if (currAccount.getProviderInfos().stream().noneMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId)) {
                return new ArrayList<>();
            }
        }

        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);

        return providerInfo.getAccounts().stream().map(account -> {
            UserProfile userProfile = account.getUserProfile();

            return ProviderLinkedAccountDto.builder()
                    .email(account.getEmail())
                    .username(account.getUsername())
                    .firstName(userProfile == null ? "New" : userProfile.getFirstName())
                    .lastName(userProfile == null ? "User" : userProfile.getLastName())
                    .image(userProfile == null ? "" : userProfile.getImage())
                    .build();
        }).collect(Collectors.toList());
    }

    @PreAuthorize("hasAuthority('PERM_LINK_OR_UNLINK_ACCOUNT')")
    @GetMapping("admin/provider/{providerInfoId}/account/{username}")
    public ResponseEntity<?> linkAccountsWithProviderEndpoint(@PathVariable Long providerInfoId, @PathVariable String username) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        Account currAccount = accountService.findByUsername(username);

        Set<Account> accounts = providerInfo.getAccounts();
        Set<ProviderInfo> providerInfos = currAccount.getProviderInfos();

        accounts.add(currAccount);
        providerInfos.add(providerInfo);

        providerInfo.setAccounts(accounts);
        currAccount.setProviderInfos(providerInfos);

        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account linked with provider successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_LINK_OR_UNLINK_ACCOUNT')")
    @DeleteMapping("admin/provider/{providerInfoId}/account/{username}")
    public ResponseEntity<?> unlinkAccountsWithProviderEndpoint(@PathVariable Long providerInfoId, @PathVariable String username) {
        ProviderInfo currProviderInfo = providerInfoService.findById(providerInfoId);
        Account currAccount = accountService.findByUsername(username);

        Set<Account> accounts = currProviderInfo.getAccounts();
        Set<ProviderInfo> providerInfos = currAccount.getProviderInfos();

        accounts.removeIf(account -> account.getUsername().equals(username));
        providerInfos.removeIf(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId);

        currProviderInfo.setAccounts(accounts);
        currAccount.setProviderInfos(providerInfos);

        accountService.save(currAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unlinked account from provider successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/{locationId}/internal")
    public ResponseEntity<?> updateInternalByLocationId(@PathVariable Long locationId, @RequestBody UpdateInternalRequest updateInternalRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.setInternal(updateInternalRequest.isInternal());
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location's internal status updated").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO')")
    @PatchMapping("admin/provider/location/service/{identifier}/internal")
    public ResponseEntity<?> updateInternalByLocationId(@PathVariable String identifier, @RequestBody UpdateInternalRequest updateInternalRequest) {
        locationSrvcLinkService.updateGroupedServicesInternalStatusByIdentifier(identifier, updateInternalRequest.isInternal());

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service's internal status updated").build());
    }
}
