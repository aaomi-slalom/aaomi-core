package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.fileDto.CreateFileRequest;
import org.autismallianceofmichigan.navigator.dto.fileDto.ProviderFileDto;
import org.autismallianceofmichigan.navigator.dto.noteDto.CreateNoteRequest;
import org.autismallianceofmichigan.navigator.dto.noteDto.NoteDto;
import org.autismallianceofmichigan.navigator.dto.noteDto.NoteOptionDto;
import org.autismallianceofmichigan.navigator.dto.registrationDto.RegistrationResponse;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class NotesAndFilesController {
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final AccountService accountService;
    private final NoteTypeService noteTypeService;
    private final SecurityContextService securityContextService;
    private final ProviderFileService providerFileService;
    private final NoteService noteService;

    @PreAuthorize("hasAuthority('PERM_VIEW_NOTES')")
    @GetMapping("/provider/{providerInfoId}/notes")
    public List<NoteDto> providerViewAllNotesEndpoint(@PathVariable Long providerInfoId) {
        return providerInfoService.getAllNoteDtosByProviderInfoId(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_NOTE')")
    @PostMapping("/provider/note")
    public ResponseEntity<?> saveNewProviderNote(@Valid @RequestBody CreateNoteRequest createNoteRequest) {
        Long providerInfoId = createNoteRequest.getProviderInfoId();
        String noteLinkId = createNoteRequest.getNoteLinkId();
        String noteTypeStr = createNoteRequest.getType();

        NoteType noteType = noteTypeService.findByNoteType(createNoteRequest.getType());

        Note note = Note.builder()
                .note(createNoteRequest.getNote())
                .title(createNoteRequest.getTitle())
                .noteType(noteType)
                .providerInfo(null)
                .providerLocation(null)
                .locationSrvcLink(null)
                .build();

        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        Note providerNote = providerInfo.addNote(note);

        if (noteTypeStr.equals("Location")) {
            Long noteLinkIdConverted = Long.parseLong(noteLinkId);
            ProviderLocation providerLocation = providerLocationService.findById(noteLinkIdConverted);
            providerNote = providerLocation.addNote(note);
        } else if (noteTypeStr.equals("Service")) {
            LocationSrvcLink locationSrvcLink = locationSrvcLinkService.findByIdentifier(noteLinkId);
            providerNote = locationSrvcLink.addNote(note);
        }

        Account author = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        author.addNote(providerNote);
        accountService.save(author);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Note created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_NOTE')")
    @GetMapping("/provider/{providerInfoId}/note/options")
    public List<NoteOptionDto> getAllNoteOptionsByProviderIdEndpoint(@PathVariable long providerInfoId) {
        return providerInfoService.getAllNoteOptionDtosByProviderInfoId(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_PROVIDER_FILE') or hasAuthority('PERM_CREATE_OWN_PROVIDER_FILE')")
    @PostMapping("/provider/file")
    public ResponseEntity<?> uploadProviderFileEndpoint(@RequestBody CreateFileRequest createFileRequest) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.getProviderInfoId() == createFileRequest.getProviderInfoId());

        if (!isOwn && !securityContextService.hasAuthority("PERM_CREATE_PROVIDER_FILE")) {
            throw new AccessDeniedException("Not Authorized");
        }

        ProviderInfo providerInfo = providerInfoService.findById(createFileRequest.getProviderInfoId());
        ProviderFile file = ProviderFile.builder()
                .fileName(createFileRequest.getFileName())
                .file(createFileRequest.getFile())
                .build();
        file = providerInfo.addProviderFile(file);

        Account author = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        author.addProviderFile(file);
        accountService.save(author);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("File uploaded successfully").build());
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/provider/{providerInfoId}/files")
    public List<ProviderFileDto> getFilesByProviderInfoIdEndpoint(@PathVariable Long providerInfoId) {
        return providerInfoService.getAllProviderFileDtosByProviderInfoId(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_PROVIDER_FILE') or hasAuthority('PERM_DELETE_OWN_PROVIDER_FILE')")
    @DeleteMapping("provider/file/{providerFileId}")
    public ResponseEntity<?> deleteProviderFileEndpoint(@PathVariable Long providerFileId) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.hasProviderFileWithId(providerFileId));

        if (!isOwn && !securityContextService.hasAuthority("PERM_DELETE_PROVIDER_FILE")) {
            throw new AccessDeniedException("Not Authorized");
        }

        if (!providerFileService.hasProviderFileWithId(providerFileId)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to delete file...").build());
        }

        providerFileService.deleteById(providerFileId);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("File deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_NOTE')")
    @DeleteMapping("provider/note/{noteId}")
    public ResponseEntity<?> deleteNoteEndpoint(@PathVariable Long noteId) {
        if (!noteService.hasNoteWithNoteId(noteId)) {
            return ResponseEntity.badRequest().body(RegistrationResponse.builder()
                    .message("Error when trying to delete note...").build());
        }

        noteService.deleteById(noteId);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Note deleted successfully").build());
    }
}
