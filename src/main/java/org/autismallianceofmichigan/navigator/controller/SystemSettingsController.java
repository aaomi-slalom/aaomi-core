package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.systemSettingDto.SystemSettingDto;
import org.autismallianceofmichigan.navigator.dto.systemSettingDto.UpdateSystemSettingsRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.SystemSetting;
import org.autismallianceofmichigan.navigator.service.SystemSettingsService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class SystemSettingsController {
    private final SystemSettingsService systemSettingsService;

    @PreAuthorize("permitAll()")
    @GetMapping("/systemsettings")
    public SystemSettingDto getCurrentSettingsEndpoint() {
        return systemSettingsService.getSystemSettingsDto();
    }

    @PreAuthorize("hasAuthority('PERM_CHANGE_SYSTEM_SETTINGS')")
    @PostMapping("/systemsettings/backgroundimage")
    public ResponseEntity<?> updateExistingSystemSettingsBackgroundImageEndpoint(@RequestBody UpdateSystemSettingsRequest updateSystemSettingsRequest) {
        SystemSetting systemSetting = systemSettingsService.getSystemSetting();
        systemSetting.setBackgroundImage(updateSystemSettingsRequest.getUpdateString());
        systemSettingsService.save(systemSetting);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Background image updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CHANGE_SYSTEM_SETTINGS')")
    @PostMapping("/systemsettings/searchsupportnote")
    public ResponseEntity<?> updateExistingSystemSettingsSearchSupportNoteEndpoint(@RequestBody UpdateSystemSettingsRequest searchSupportNoteRequest) {
        SystemSetting systemSetting = systemSettingsService.getSystemSetting();
        systemSetting.setSearchSupportNote(searchSupportNoteRequest.getUpdateString());
        systemSettingsService.save(systemSetting);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Search support note updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CHANGE_SYSTEM_SETTINGS')")
    @PostMapping("/systemsettings/technicalsupportnote")
    public ResponseEntity<?> updateExistingSystemSettingsTechnicalSupportNoteEndpoint(@RequestBody UpdateSystemSettingsRequest technicalSupportNoteRequest) {
        SystemSetting systemSetting = systemSettingsService.getSystemSetting();
        systemSetting.setTechnicalSupportNote(technicalSupportNoteRequest.getUpdateString());
        systemSettingsService.save(systemSetting);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Technical support note updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CHANGE_SYSTEM_SETTINGS')")
    @PostMapping("/systemsettings/disclaimer")
    public ResponseEntity<?> updateExistingSystemSettingsDisclaimerEndpoint(@RequestBody UpdateSystemSettingsRequest updateDisclaimerRequest) {
        SystemSetting systemSetting = systemSettingsService.getSystemSetting();
        systemSetting.setDisclaimer(updateDisclaimerRequest.getUpdateString());
        systemSettingsService.save(systemSetting);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Disclaimer updated successfully").build());
    }
}
