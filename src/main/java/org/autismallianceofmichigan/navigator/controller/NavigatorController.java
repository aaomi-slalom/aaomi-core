package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class NavigatorController {
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final ValidationService validationService;
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;
    private final SecurityContextService securityContextService;
    private final AddressService addressService;
    private final ContactTypeService contactTypeService;
    private final CountyService countyService;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final EmailService emailService;
    private final ContactNumberService contactNumberService;

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PostMapping("navigator/provider")
    public ResponseEntity<?> createUnapprovedProviderInfoEndpoint(@RequestBody CreateProviderRequest createProviderRequest) {
        String providerName = createProviderRequest.getProviderInfo().getProviderName();
        if (providerInfoService.hasProviderInfoWithName(providerName)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate provider profile...").build());
        }

        Account currAccount = securityContextService.findCurrAccount();
        String editUsername = currAccount.getUsername();

        if (createProviderRequest.getAccount().getEmail() != null) {
            if (validationService.hasValidationErrors(createProviderRequest.getAccount())) {
                return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                        .message("Error when trying to create account...").build());
            }

            CreateProviderAccountRequest createProviderAccountRequest = createProviderRequest.getAccount();
            createProviderAccountRequest.setPassword(passwordEncoder.encode(createProviderAccountRequest.getPassword()));
            createProviderRequest.setAccount(createProviderAccountRequest);
        }

        accountService.createOrLinkAccountInitialProviderInfo(createProviderRequest, false, editUsername, currAccount);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "create provider");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider profile created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/{providerInfoId}/location/add")
    public ResponseEntity<?> addLocationToProviderEndpoint(
            @PathVariable Long providerInfoId,
            @RequestBody CreateProviderLocationRequest createProviderLocationRequest
    ) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        Set<ProviderLocation> providerLocations = providerInfo.getProviderLocations();
        ProviderLocation providerLocation = providerLocationService.buildProviderLocation(providerInfo, createProviderLocationRequest, false);

        if (providerLocations.stream().anyMatch(pl -> pl.getLocation().equals(providerLocation.getLocation()))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create location with the same name...").build());
        }

        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        String lastEditUsername = currAccount.getUsername();

        providerLocations.add(providerLocation);
        providerInfo.setProviderLocations(providerLocations);
        providerInfo.setLastEditUsername(lastEditUsername);
        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "add location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/{providerLocationId}")
    public ResponseEntity<?> navigatorUpdateProviderLocationNameEndpoint(
            @PathVariable Long providerLocationId,
            @RequestBody UpdateLocationNameRequest updateLocationNameRequest
    ) {
        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        providerLocation.setModifyLocationRequested(true);
        providerLocation.setModifiedLocation(updateLocationNameRequest.getLocationName());

        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "change location name");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location name updated and approval requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/address/{addressId}")
    public ResponseEntity<?> navigatorUpdateProviderLocationAddressEndpoint(
            @PathVariable Long addressId,
            @RequestBody UpdateLocationAddressRequest updateLocationAddressRequest
    ) {
        Address address = addressService.findById(addressId);
        String providerName = address.getProviderLocation().getProviderInfo().getProviderName();

        address.setModifyAddressRequested(true);
        address.setModifiedCity(updateLocationAddressRequest.getCity());
        address.setModifiedState(updateLocationAddressRequest.getState());
        address.setModifiedStreet(updateLocationAddressRequest.getStreet());
        address.setModifiedZip(updateLocationAddressRequest.getZip());

        addressService.save(address);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "change location address");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message(address.getAddressType().getType() +
                        " address updated and approval requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/{locationId}/email/add")
    public ResponseEntity<?> navigatorAddEmailToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateEmailRequest createEmailRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        List<Email> emails = providerLocation.getEmails();
        ContactType contactType = contactTypeService.findByType(createEmailRequest.getContactType());
        String emailStr = createEmailRequest.getEmail();

        if (emails.stream()
                .anyMatch(e ->
                        e.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                e.getEmail().equals(emailStr))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate email...").build());
        }

        Email email = Email.builder()
                .email(emailStr)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(false)
                .build();

        emails.add(email);
        providerLocation.setEmails(emails);
        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add email to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Email added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/{locationId}/contactnumber/add")
    public ResponseEntity<?> navigatorAddContactNumberToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody CreateContactNumberRequest createContactNumberRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        List<ContactNumber> contactNumbers = providerLocation.getContactNumbers();
        ContactType contactType = contactTypeService.findByType(createContactNumberRequest.getContactType());
        String number = createContactNumberRequest.getNumber();

        if (contactNumbers.stream()
                .anyMatch(n ->
                        n.getContactType().getContactTypeId() == contactType.getContactTypeId() &&
                                n.getNumber().equals(number))) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot add duplicate contact number...").build());
        }

        ContactNumber contactNumber = ContactNumber.builder()
                .number(number)
                .providerLocation(providerLocation)
                .contactType(contactType)
                .createApproved(false)
                .build();

        contactNumbers.add(contactNumber);
        providerLocation.setContactNumbers(contactNumbers);
        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add contact number to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Contact number added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/{locationId}/counties")
    public ResponseEntity<?> navigatorAddServiceCountyToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody UpdateCountiesRequest updateCountiesRequest) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        List<County> countiesUpdate = countyService.findCountiesByCountyIds(updateCountiesRequest.getCountyIds());

        providerLocation.setModifyCountiesRequested(true);
        providerLocation.setModifiedCounties(countiesUpdate);
        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add county to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service counties updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/location/{locationId}/service/add")
    public ResponseEntity<?> navigatorAddServiceToLocationEndpoint(@PathVariable Long locationId, @Valid @RequestBody List<CreateLocationSrvcLinkRequest> createLocationSrvcLinkRequests) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        String providerName = providerLocation.getProviderInfo().getProviderName();

        List<LocationSrvcLink> currLocationSrvcLinks = providerLocation.getLocationSrvcLinks();

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.addLocationSrvcLinks(providerLocation, createLocationSrvcLinkRequests, false);

        currLocationSrvcLinks.addAll(locationSrvcLinks);
        providerLocation.setLocationSrvcLinks(currLocationSrvcLinks);
        providerLocationService.save(providerLocation);

        accountService.sendApprovalNotificationToAllAdmins(providerName, "add service to location");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service added successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("navigator/provider/location/email/{emailId}")
    public ResponseEntity<?> navigatorDeleteEmailFromLocationEndpoint(
            @PathVariable Long emailId
    ) {
        if (!emailService.hasEmailWithId(emailId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete email").build());
        }

        Email email = emailService.findById(emailId);

        emailService.requestToDelete(email);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved email deleted or requested to delete approved email").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("navigator/provider/location/contactnumber/{contactNumberId}")
    public ResponseEntity<?> navigatorDeleteContactNumberFromLocationEndpoint(
            @PathVariable Long contactNumberId
    ) {
        if (!contactNumberService.hasContactNumberWithId(contactNumberId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete contact number").build());
        }

        ContactNumber contactNumber = contactNumberService.findById(contactNumberId);

        contactNumberService.requestToDelete(contactNumber);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved contact number deleted or requested to delete approved contact number").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("navigator/provider/location/service/{identifier}")
    public ResponseEntity<?> navigatorDeleteServiceFromLocationEndpoint(
            @PathVariable String identifier
    ) {
        if (!locationSrvcLinkService.hasLocationSrvcLinkByIdentifier(identifier)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete service").build());
        }

        locationSrvcLinkService.requestToDeleteAllByIdentifier(identifier);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved service deleted or requested to delete approved service").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @PatchMapping("navigator/provider/demographics/{providerInfoId}")
    public ResponseEntity<?> navigatorUpdateProviderDemographicsEndpoint(@PathVariable Long providerInfoId, @RequestBody CreateProviderInfoRequest createProviderInfoRequest) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);

        if (!providerInfo.getProviderName().toLowerCase().equals(createProviderInfoRequest.getProviderName().toLowerCase()) &&
                providerInfoService.hasProviderInfoWithName(createProviderInfoRequest.getProviderName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update provider demographics...").build());
        }

        providerInfo.setModifyRequested(true);
        providerInfo.setModifiedProviderName(createProviderInfoRequest.getProviderName());
        providerInfo.setModifiedWebsite(createProviderInfoRequest.getWebsite());
        providerInfo.setModifiedLogo(createProviderInfoRequest.getLogo());
        providerInfo.setModifiedDescription(createProviderInfoRequest.getDescription());
        providerInfo.setModifiedFacebook(createProviderInfoRequest.getFacebook());
        providerInfo.setModifiedLinkedin(createProviderInfoRequest.getLinkedin());
        providerInfo.setModifiedTwitter(createProviderInfoRequest.getTwitter());
        providerInfo.setModifiedInstagram(createProviderInfoRequest.getInstagram());
        providerInfo.setModifiedYoutube(createProviderInfoRequest.getYoutube());

        providerInfoService.save(providerInfo);

        accountService.sendApprovalNotificationToAllAdmins(providerInfo.getProviderName(), "update provider demographics");

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider demographics update requested").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @DeleteMapping("navigator/provider/location/{providerLocationId}")
    public ResponseEntity<?> navigatorDeleteProviderLocationByIdEndpoint(@PathVariable Long providerLocationId) {
        if (!providerLocationService.hasProviderLocationWithId(providerLocationId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to remove location").build());
        }

        ProviderLocation providerLocation = providerLocationService.findById(providerLocationId);

        providerLocationService.requestToDelete(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Unapproved location deleted or requested to delete approved location").build());
    }
}
