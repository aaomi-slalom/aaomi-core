package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.locationDto.InternalLocationDto;
import org.autismallianceofmichigan.navigator.dto.locationDto.PublicLocationDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.PublicProvidersPageDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.QueryProvidersRequest;
import org.autismallianceofmichigan.navigator.service.ProviderInfoService;
import org.autismallianceofmichigan.navigator.service.ProviderLocationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class SearchController {
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;

    @PreAuthorize("permitAll()")
    @PostMapping("providers/search")
    public PublicProvidersPageDto queryPublicProvidersPageEndpoint(@RequestBody QueryProvidersRequest queryProvidersRequest) {
        return providerInfoService.getPublicProvidersPageDtoByQueryProvidersRequest(queryProvidersRequest);
    }

    @PreAuthorize("permitAll()")
    @PostMapping("providers/search/report")
    public PublicProvidersPageDto generatePublicProvidersReportEndpoint(@RequestBody QueryProvidersRequest queryProvidersRequest) {
        return providerInfoService.generateSearchResultsReport(queryProvidersRequest);
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @PostMapping("providers/search/internal")
    public PublicProvidersPageDto internalQueryPublicProvidersPageEndpoint(@RequestBody QueryProvidersRequest queryProvidersRequest) {
        return providerInfoService.getInternalProvidersPageDtoByQueryProvidersRequest(queryProvidersRequest);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("public/provider/{providerLocationId}")
    public PublicLocationDto viewSpecificProviderLocationById(@PathVariable Long providerLocationId) {
        return providerLocationService.getPublicLocationDtoById(providerLocationId);
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @GetMapping("internal/provider/{providerLocationId}")
    public InternalLocationDto internalViewSpecificProviderLocationById(@PathVariable Long providerLocationId) {
        return providerLocationService.getInternalLocationDtoById(providerLocationId);
    }
}
