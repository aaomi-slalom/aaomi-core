package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.messageDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.entity.Message;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class MessagesController {
    private final MessageService messageService;
    private final SecurityContextService securityContextService;
    private final AccountService accountService;
    private final AccountGroupService accountGroupService;
    private final MailService mailService;
    private final Environment env;

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/user/messages/sent")
    public SentMessagesPageDto viewSentMessagesEndpoint(@RequestBody ViewMessagesRequest viewMessagesRequest) {
        Page<Message> sentMessagesPage = messageService.getSentMessagesPageByUsername(viewMessagesRequest.getUsername(),
                viewMessagesRequest.getCurrentPage(),
                viewMessagesRequest.getPageSize()
        );

        List<SentMessageDto> sentMessageDtos = sentMessagesPage.get().map(message -> {
            List<String> recipientUsernames = message.getRecipientAccounts().stream()
                    .map(Account::getUsername).collect(Collectors.toList());
            return SentMessageDto.builder()
                    .messageId(message.getMessageId())
                    .recipientUsernames(recipientUsernames)
                    .message(message.getMessage())
                    .messageTime(message.getMessageTime())
                    .subject(message.getSubject())
                    .build();
        }).collect(Collectors.toList());

        return SentMessagesPageDto.builder()
                .sentMessages(sentMessageDtos)
                .totalElements(sentMessagesPage.getTotalElements())
                .totalPages(sentMessagesPage.getTotalPages())
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/user/messages/received")
    public ReceivedMessagePageDto viewReceivedMessagesEndpoint(@RequestBody ViewMessagesRequest viewMessagesRequest) {
        Page<Message> receivedMessagesPage = messageService.getReceivedMessagesPageByUsername(viewMessagesRequest.getUsername(),
                viewMessagesRequest.getCurrentPage(),
                viewMessagesRequest.getPageSize());

        List<ReceivedMessageDto> receivedMessages = receivedMessagesPage.get().map(message ->
                ReceivedMessageDto.builder()
                        .messageId(message.getMessageId())
                        .sender(message.getSender())
                        .subject(message.getSubject())
                        .message(message.getMessage())
                        .messageTime(message.getMessageTime())
                        .build()
        ).collect(Collectors.toList());

        return ReceivedMessagePageDto.builder()
                .receivedMessages(receivedMessages)
                .totalElements(receivedMessagesPage.getTotalElements())
                .totalPages(receivedMessagesPage.getTotalPages())
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/view/accountgroups/external")
    public List<ExternalMessageAccountGroupsDto> getAccountGroupDtosForExternalUsers() {
        return accountGroupService.getAccountGroupDtosExternalUsers();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/messages/accountgroups")
    public List<MessageAccountGroupsDto> getAllMessageAccountGroupDtosEndpoint() {
        return accountService.getAllMessageAccountGroupDtos();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/message/{messageId}")
    public ReceivedMessageDto viewSpecificMessage(@PathVariable Long messageId) {
        Message message = messageService.findById(messageId);

        if (!message.isRead()) {
            message.setRead(true);
            messageService.saveMessage(message);
        }

        return ReceivedMessageDto.builder()
                .sender(message.getSender())
                .subject(message.getSubject())
                .message(message.getMessage())
                .messageId(messageId)
                .messageTime(message.getMessageTime())
                .build();
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/message/externaluser")
    public ResponseEntity<?> saveNewMessageExternalEndpoint(@RequestBody ExternalCreateMessageRequest externalCreateMessageRequest) {
        Account sender = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        Message message = Message.builder()
                .subject(externalCreateMessageRequest.getSubject())
                .message(externalCreateMessageRequest.getMessage())
                .build();

        Set<Account> recipients = new HashSet<>();
        externalCreateMessageRequest.getAccountGroups().forEach(accountGroupName -> {
            AccountGroup accountGroup = accountGroupService.findByAccountGroup(accountGroupName.getAccountGroup());
            recipients.addAll(accountGroup.getAccounts());
        });

        recipients.forEach(account -> {
            List<Message> receivedMessages = account.getReceivedMessages();
            receivedMessages.add(message);
            account.setReceivedMessages(receivedMessages);

            String angularAppUrl = env.getProperty("angular-app-url");
            String subjectLine = "New Message From: " + sender.getUsername();
            String messageContent = message.getSubject() +
                    "\r\n" +
                    "\r\n" +
                    message.getMessage() +
                    "\r\n" +
                    "\r\n" +
                    "Click link to view message: " + angularAppUrl + "/user/messages";

            try {
                mailService.sendMail(account.getEmail(), subjectLine, messageContent);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        List<Message> sentMessages = sender.getSentMessages();
        sentMessages.add(message);
        sender.setSentMessages(sentMessages);

        message.setSenderAccounts(new HashSet<>(Collections.singletonList(sender)));
        message.setRecipientAccounts(recipients);
        messageService.saveMessage(message);

        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Message sent successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_BULK_MESSAGING')")
    @PostMapping("/message")
    public ResponseEntity<?> saveNewMessageInternalEndpoint(@RequestBody CreateMessageRequest createMessageRequest) {
        Account sender = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        if (sender == null) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("User not authenticated").build());
        }

        Message message = Message.builder()
                .subject(createMessageRequest.getSubject())
                .message(createMessageRequest.getMessage())
                .messageTime(new Date())
                .build();

        Set<Account> recipients = createMessageRequest.getRecipients().stream().map(username -> {
            Account recipient = accountService.findByUsername(username);
            List<Message> receivedMessages = recipient.getReceivedMessages();
            receivedMessages.add(message);
            recipient.setReceivedMessages(receivedMessages);

            String subjectLine = "New Message From: " + sender.getUsername();
            String angularAppUrl = env.getProperty("angular-app-url");
            String messageContent = message.getSubject() +
                    "\r\n" +
                    "\r\n" +
                    message.getMessage() +
                    "\r\n" +
                    "\r\n" +
                    "Click link to view message: " + angularAppUrl + "/user/messages";

            try {
                mailService.sendMail(recipient.getEmail(), subjectLine , messageContent);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return recipient;
        }).collect(Collectors.toSet());

        List<Message> sentMessages = sender.getSentMessages();
        sentMessages.add(message);
        sender.setSentMessages(sentMessages);

        message.setSenderAccounts(new HashSet<>(Collections.singletonList(sender)));
        message.setRecipientAccounts(recipients);
        messageService.saveMessage(message);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Message sent successfully").build());
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/user/{username}/receivedmessage/{messageId}")
    public ResponseEntity<?> deleteReceivedMessageEndpoint(@PathVariable Long messageId, @PathVariable String username) {
        accountService.deleteReceivedMessageById(username, messageId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Message deleted successfully").build());
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/user/{username}/sentmessage/{messageId}")
    public ResponseEntity<?> deleteSentMessageEndpoint(@PathVariable Long messageId, @PathVariable String username) {
        accountService.deleteSentMessageById(username, messageId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Message deleted successfully").build());
    }
}
