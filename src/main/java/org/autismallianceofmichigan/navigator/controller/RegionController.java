package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.CreateRegionRequest;
import org.autismallianceofmichigan.navigator.dto.regionDto.RegionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.RegionOptionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.UpdateRegionRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.County;
import org.autismallianceofmichigan.navigator.persistence.entity.Region;
import org.autismallianceofmichigan.navigator.service.RegionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class RegionController {
    private final RegionService regionService;

    @GetMapping("region/view/counties")
    public List<MichiganCountyOptionDto> viewAllCountiesWithoutAssignedRegionForNewRegion() {
        return regionService.getAllUnassignedCountyDtos();
    }

    @GetMapping("region/{regionId}/view/counties")
    public List<MichiganCountyOptionDto> viewAllCountiesWithoutAssignedRegionForExistingRegion(@PathVariable Long regionId) {
        return regionService.getAllMichiganCountyOptionDtosByRegionId(regionId);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("region/view")
    public List<RegionDto> viewAllRegionsEndpoint() {
        return regionService.getAllRegionDtos();
    }

    @GetMapping("region/options")
    public List<RegionOptionDto> getAllRegionOptionDtosEndpoint() {
        return regionService.getAllRegionOptionDtos();
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("region/save")
    public ResponseEntity<?> saveNewRegionEndpoint(@RequestBody CreateRegionRequest createRegionRequest) {
        Region region = Region.builder()
                .region(createRegionRequest.getRegion())
                .build();
        regionService.saveRegion(region);

        Set<County> counties = createRegionRequest.getCountyDtos().stream().map(countyDto -> County.builder()
                .county(countyDto.getCounty())
                .countyId(countyDto.getCountyId())
                .region(region)
                .build()).collect(Collectors.toSet());
        region.setCounties(counties);

        regionService.saveRegion(region);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Region created successfully").build());
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping("region/{regionId}/update")
    public ResponseEntity<?> updateRegionEndpoint(@PathVariable Long regionId, @RequestBody UpdateRegionRequest updateRegionRequest) {
        regionService.updateExistingRegion(regionId, updateRegionRequest);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Region updated successfully").build());
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("region/delete/{regionId}")
    public ResponseEntity<?> deleteRegionEndpoint(@PathVariable Long regionId) {
        regionService.deleteById(regionId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Region deleted successfully").build());
    }
}
