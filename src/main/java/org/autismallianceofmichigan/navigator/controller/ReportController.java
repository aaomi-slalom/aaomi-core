package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.QueryProvidersRequest;
import org.autismallianceofmichigan.navigator.dto.reportDto.GenericReportDto;
import org.autismallianceofmichigan.navigator.service.ReportService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class ReportController {
    private final ReportService reportService;

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @GetMapping("internal/providers/grant/report")
    public GenericReportDto createGrantReport() {
        return reportService.createGrantReport();
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @PostMapping("internal/providers/contact/report")
    public GenericReportDto createContactReport(@RequestBody QueryProvidersRequest queryProvidersRequest) {
        return reportService.createContactReport(queryProvidersRequest);
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO')")
    @PostMapping("internal/providers/standalone/report")
    public GenericReportDto createStandaloneReport(@RequestBody QueryProvidersRequest queryProvidersRequest) {
        return reportService.createStandaloneReport(queryProvidersRequest);
    }
}
