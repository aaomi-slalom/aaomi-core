package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.AboutPageDto;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AboutPageController {
    private final Environment env;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/admin/about")
    public AboutPageDto viewVersionInformationEndpoint() {
        return AboutPageDto.builder()
                .coreVersion("1.0.76-700fa8")
                .uxVersion("1.0.93-735930")
                .build();
    }
}
