package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.authDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.security.jwt.JwtHandler;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.autismallianceofmichigan.navigator.service.MailService;
import org.autismallianceofmichigan.navigator.service.SecurityContextService;
import org.autismallianceofmichigan.navigator.service.ValidationService;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AuthController {
    private final AccountService accountService;
    private final ValidationService validationService;
    private final MailService mailService;
    private final SecurityContextService securityContextService;
    private final AuthenticationManager authenticationManager;
    private final JwtHandler jwtHandler;
    private final PasswordEncoder passwordEncoder;
    private final Environment env;

    @PreAuthorize("permitAll()")
    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticateEndpoint(@RequestBody AuthRequest authRequest)
            throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsernameOrEmail(),
                            authRequest.getPassword())
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(GenericMessageResponse.builder()
                    .message(e.getLocalizedMessage()).build());
        }

        Account account = (Account) accountService.loadUserByUsername(authRequest.getUsernameOrEmail());

        account.setLastLoggedIn(LocalDateTime.now());
        accountService.save(account);

        final String jwt = jwtHandler.generateToken(account);

        return ResponseEntity.ok(AuthResponse.builder().jwt(jwt).build());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/password/change")
    public ResponseEntity<?> changePasswordEndpoint(@RequestBody PasswordChangeRequest passwordChangeRequest) {
        if (validationService.hasValidationErrors(passwordChangeRequest)) {
            return ResponseEntity.badRequest().body(PasswordChangeResponse.builder()
                    .message("Unable to change password").build());
        }

        Account currentAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());

        if (currentAccount == null) {
            return ResponseEntity.badRequest().body(PasswordChangeResponse.builder()
                    .message("Unable to change password").build());
        }

        if (passwordEncoder.matches(passwordChangeRequest.getOldPassword(), currentAccount.getPassword())) {
            String encodedNewPassword = passwordEncoder.encode(passwordChangeRequest.getNewPassword());
            accountService.updateAccountPassword(currentAccount, encodedNewPassword);
            return ResponseEntity.ok(PasswordChangeResponse.builder()
                    .message("Password updated successfully").build());
        }

        return ResponseEntity.badRequest().body(PasswordChangeResponse.builder()
                .message("Unable to change password").build());
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/password/reset")
    public ResponseEntity<?> resetPasswordEndpoint(@RequestBody PasswordResetRequest passwordResetRequest)
            throws IOException {
        if (validationService.hasValidationErrors(passwordResetRequest)) {
            return ResponseEntity.badRequest().body(PasswordResetResponse.builder()
                    .message("Error when trying to send reset email...").build());
        }

        String email = passwordResetRequest.getEmail();
        Account foundAccount = accountService.findByEmail(email);

        sendPasswordResetEmail(email, foundAccount);

        return ResponseEntity.ok(PasswordResetResponse.builder()
                .message("An email has been sent to you to reset your account.")
                .build());
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/reset/verify")
    public ResponseEntity<?> verifyResetLinkAndPopulateEmail(@RequestBody VerifyResetRequest verifyResetRequest) {
        long id = verifyResetRequest.getId();
        String token = verifyResetRequest.getToken();
        String result = accountService.validatePasswordResetToken(id, token);

        if (result == null) {
            return ResponseEntity.badRequest().body(VerifyResetResponse.builder()
                    .message("Reset failed.")
                    .build());
        } else if (result.equals("expired")) {
            return ResponseEntity.badRequest().body(VerifyResetResponse.builder()
                    .message("Reset token has expired")
                    .build());
        }

        Account existingAccount = accountService.findAccountByPasswordResetToken(token);

        return ResponseEntity.ok(VerifyResetResponse.builder()
                .email(existingAccount.getEmail())
                .build());
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/password/update")
    public ResponseEntity<?> updatePasswordEndpoint(@RequestBody UpdatePasswordRequest updatePasswordRequest) {
        if (validationService.hasValidationErrors(updatePasswordRequest)) {
            return ResponseEntity.badRequest().body(UpdatePasswordResponse.builder()
                    .message("Error when trying to update password...").build());
        }

        long accountId = updatePasswordRequest.getAccountId();
        String passwordResetToken = updatePasswordRequest.getPasswordResetToken();
        String result = accountService.validatePasswordResetToken(accountId, passwordResetToken);

        if (result == null) {
            return ResponseEntity.badRequest().body(UpdatePasswordResponse.builder()
                    .message("Reset failed.")
                    .build());
        } else if (result.equals("expired")) {
            return ResponseEntity.badRequest().body(UpdatePasswordResponse.builder()
                    .message("Reset token has expired")
                    .build());
        }

        Account existingAccount = accountService.findAccountByPasswordResetToken(passwordResetToken);
        accountService.updateAccountPassword(existingAccount,
                passwordEncoder.encode(updatePasswordRequest.getPassword()));

        return ResponseEntity.ok(UpdatePasswordResponse.builder()
                .message("Account is reset successfully")
                .build());
    }

    private void sendPasswordResetEmail(String to, Account foundAccount) throws IOException {
        String passwordResetToken = UUID.randomUUID().toString();
        accountService.createAccountRecoveryForAccount(foundAccount, passwordResetToken);
        String angularAppUrl = env.getProperty("angular-app-url");
        String clientResetUrl = angularAppUrl + "/password/reset?id=" + foundAccount.getAccountId()
                + "&token=" + passwordResetToken;

        String subject = "Autism Alliance of Michigan Account Recovery";
        String content = "Click on this link to reset your password: " + clientResetUrl;

        mailService.sendMail(to, subject, content);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/credentials/validate")
    private void validateCredentialsEndpoint() {

    }
}
