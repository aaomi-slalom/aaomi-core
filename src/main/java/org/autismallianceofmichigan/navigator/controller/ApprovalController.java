package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.approvalDto.*;
import org.autismallianceofmichigan.navigator.dto.locationDto.UnapprovedLocationDto;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class ApprovalController {
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final ValidationService validationService;
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;
    private final AddressService addressService;
    private final ContactTypeService contactTypeService;
    private final CountyService countyService;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final EmailService emailService;
    private final ContactNumberService contactNumberService;

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/accounts")
    public List<UnapprovedAccountDto> viewAllUnapprovedAccountEndpoint() {
        return accountService.findUnapprovedAccounts();
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/account/{username}")
    public ResponseEntity<?> approveAccountEndpoint(@PathVariable String username) {
        accountService.approveAccountByUsername(username);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @DeleteMapping("/disapproval/account/{username}")
    public ResponseEntity<?> disapproveAccountEndpoint(@PathVariable String username) {
        accountService.disapproveAndDeleteByUsername(username);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Account disapproved and removed").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/providers")
    public List<UnapprovedProviderOverviewDto> showAllUnapprovedProviders() {
        return providerInfoService.getAllUnapprovedProviders();
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/provider/{providerInfoId}")
    public ResponseEntity<?> approveCreatedProviderEndpoint(@PathVariable Long providerInfoId) {
        providerInfoService.approveCreatedProvider(providerInfoId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/locations")
    public List<UnapprovedLocationOverviewDto> showAllUnapprovedLocations() {
        return providerLocationService.getAllUnapprovedLocations();
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/location/{locationId}")
    public ResponseEntity<?> approveCreatedLocationEndpoint(@PathVariable Long locationId) {
        providerLocationService.approveCreatedLocation(locationId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/unapproved/location/{locationId}")
    public UnapprovedLocationDto getUnapprovedLocationDtoByIdEndpoint(@PathVariable Long locationId) {
        return providerLocationService.getUnapprovedLocationDtoById(locationId);
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/unapproved/modified/providers")
    public List<UnapprovedModifiedProviderOverviewDto> getUnapprovedModifiedProviderOverviewDtosEndpoint() {
        return providerInfoService.getAllUnapprovedModifiedProviderOverviewDtos();
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/demographics/{providerInfoId}/approve")
    public ResponseEntity<?> approveModifiedDemographics(@PathVariable Long providerInfoId) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        providerInfo.approveModifiedDemographics();
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider demographics updates approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/demographics/{providerInfoId}/disapprove")
    public ResponseEntity<?> disapproveModifiedDemographics(@PathVariable Long providerInfoId) {
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        providerInfo.disapproveModifiedDemographics();
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Provider demographics updates disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/{locationId}/approve")
    public ResponseEntity<?> approveDeleteLocation(@PathVariable Long locationId) {
        if (!providerLocationService.hasProviderLocationWithId(locationId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to remove location").build());
        }

        providerLocationService.deleteById(locationId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/{locationId}/disapprove")
    public ResponseEntity<?> disapproveDeleteLocation(@PathVariable Long locationId) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.setDeleteRequested(false);
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location deletion disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/name/{locationId}/approve")
    public ResponseEntity<?> approveModifiedLocationName(@PathVariable Long locationId) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.approveLocationName();
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location name update approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/name/{locationId}/disapprove")
    public ResponseEntity<?> disapproveModifiedLocationName(@PathVariable Long locationId) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.disapproveLocationName();
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Location name update disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/address/{addressId}/approve")
    public ResponseEntity<?> approveModifiedAddress(@PathVariable Long addressId) {
        Address address = addressService.findById(addressId);
        address.approveModifiedAddress();
        addressService.save(address);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Modified address update approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/address/{addressId}/disapprove")
    public ResponseEntity<?> disapproveModifiedAddress(@PathVariable Long addressId) {
        Address address = addressService.findById(addressId);
        address.disapproveModifiedAddress();
        addressService.save(address);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Modified address update disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/added/location/email/{emailId}/approve")
    public ResponseEntity<?> approveAddedEmail(@PathVariable Long emailId) {
        Email email = emailService.findById(emailId);
        email.setCreateApproved(true);
        emailService.save(email);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("New email approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/email/{emailId}/approve")
    public ResponseEntity<?> approveDeleteEmail(@PathVariable Long emailId) {
        if (!emailService.hasEmailWithId(emailId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete email").build());
        }

        emailService.deleteById(emailId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Email deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/email/{emailId}/disapprove")
    public ResponseEntity<?> disapproveDeleteEmail(@PathVariable Long emailId) {
        Email email = emailService.findById(emailId);
        email.setDeleteRequested(false);
        emailService.save(email);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Email deletion disapproved").build());
    }


    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/added/location/contactnumber/{contactNumberId}/approve")
    public ResponseEntity<?> approveAddedContactNumber(@PathVariable Long contactNumberId) {
        ContactNumber contactNumber = contactNumberService.findById(contactNumberId);
        contactNumber.setCreateApproved(true);
        contactNumberService.save(contactNumber);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("New contact number approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/contactnumber/{contactNumberId}/approve")
    public ResponseEntity<?> approveDeleteContactNumber(@PathVariable Long contactNumberId) {
        if (!contactNumberService.hasContactNumberWithId(contactNumberId)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete contact number").build());
        }

        contactNumberService.deleteById(contactNumberId);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Contact number deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/contactnumber/{contactNumberId}/disapprove")
    public ResponseEntity<?> disapproveDeleteContactNumber(@PathVariable Long contactNumberId) {
        ContactNumber contactNumber = contactNumberService.findById(contactNumberId);
        contactNumber.setDeleteRequested(false);
        contactNumberService.save(contactNumber);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Contact number deletion disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/added/location/service/{identifier}/approve")
    public ResponseEntity<?> approveAddedLocationService(@PathVariable String identifier) {
        locationSrvcLinkService.approveAddedGroupedLocationServiceByIdentifier(identifier);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("New service approved").build());
    }

    @GetMapping("/approval/delete/location/service/{identifier}/approve")
    public ResponseEntity<?> approveDeleteLocationService(@PathVariable String identifier) {
        if (!locationSrvcLinkService.hasLocationSrvcLinkByIdentifier(identifier)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to delete service").build());
        }

        locationSrvcLinkService.deleteAllByIdentifier(identifier);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/delete/location/service/{identifier}/disapprove")
    public ResponseEntity<?> disapproveDeleteLocationService(@PathVariable String identifier) {
        locationSrvcLinkService.disapproveDeleteAllByIdentifier(identifier);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Service deletion disapproved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/counties/{locationId}/approve")
    public ResponseEntity<?> approveModifiedCountiesByLocationId(@PathVariable Long locationId) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.approveCounties();
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Modified counties approved").build());
    }

    @PreAuthorize("hasAuthority('PERM_PROCESS_APPROVALS')")
    @GetMapping("/approval/modified/location/counties/{locationId}/disapprove")
    public ResponseEntity<?> disapproveModifiedCountiesByLocationId(@PathVariable Long locationId) {
        ProviderLocation providerLocation = providerLocationService.findById(locationId);
        providerLocation.disapproveCounties();
        providerLocationService.save(providerLocation);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("Modified counties disapproved").build());
    }
}
