package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.CreateWaitTimeRequest;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.UpdateWaitTimeRequest;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.WaitTimeDto;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.WaitTimeOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class WaitTimesController {
    private final WaitTimeTypeService waitTimeTypeService;
    private final WaitTimeService waitTimeService;
    private final ProviderInfoService providerInfoService;
    private final ProviderLocationService providerLocationService;
    private final SecurityContextService securityContextService;

    @PreAuthorize("hasAuthority('PERM_VIEW_WAIT_TIMES') or hasAuthority('PERM_VIEW_OWN_WAIT_TIMES')")
    @GetMapping("/provider/{providerInfoId}/waittimes")
    public List<WaitTimeDto> viewAllWaitTimeDtosByIdEndpoint(@PathVariable Long providerInfoId) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId);

        if (!isOwn && !securityContextService.hasAuthority("PERM_VIEW_WAIT_TIMES")) {
            throw new AccessDeniedException("Not Authorized");
        }

        return providerInfoService.getAllWaitTimeDtosByProviderInfoId(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_WAIT_TIME') or hasAuthority('PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME')")
    @GetMapping("/provider/{providerInfoId}/waittime/options")
    public List<WaitTimeOptionDto> viewAllWaitTimeOptionDtosByIdEndpoint(@PathVariable Long providerInfoId) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId);

        if (!isOwn && !securityContextService.hasAuthority("PERM_CREATE_OR_MODIFY_WAIT_TIME")) {
            throw new AccessDeniedException("Not Authorized");
        }

        return providerInfoService.getAllWaitOptionDtosByProviderInfoId(providerInfoId);
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_WAIT_TIME') or hasAuthority('PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME')")
    @PostMapping("/provider/waittime")
    public ResponseEntity<?> saveWaitTimeEndpoint(@RequestBody CreateWaitTimeRequest createWaitTimeRequest) {
        Long providerInfoId = createWaitTimeRequest.getProviderInfoId();

        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.getProviderInfoId() == providerInfoId);

        if (!isOwn && !securityContextService.hasAuthority("PERM_CREATE_OR_MODIFY_WAIT_TIME")) {
            throw new AccessDeniedException("Not Authorized");
        }

        String waitTimeLinkId = createWaitTimeRequest.getWaitTimeLinkId();
        String waitTimeTypeStr = createWaitTimeRequest.getWaitTimeType();

        WaitTimeType waitTimeType = waitTimeTypeService.findByWaitTimeType(waitTimeTypeStr);
        WaitTime waitTime = WaitTime.builder()
                .title(createWaitTimeRequest.getTitle())
                .waitTime(createWaitTimeRequest.getWaitTime())
                .waitTimeType(waitTimeType)
                .providerInfo(null)
                .providerLocation(null)
                .build();
        ProviderInfo providerInfo = providerInfoService.findById(providerInfoId);
        providerInfo.addWaitTime(waitTime);

        if (waitTimeTypeStr.equals("Location")) {
            Long waitTimeLinkIdConverted = Long.parseLong(waitTimeLinkId);
            ProviderLocation providerLocation = providerLocationService.findById(waitTimeLinkIdConverted);
            providerLocation.addWaitTime(waitTime);
        }
        providerInfoService.save(providerInfo);

        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Wait time created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_WAIT_TIME') or hasAuthority('PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME')")
    @PatchMapping("/provider/waittimes/{waitTimeId}")
    public ResponseEntity<?> updateExistingWaitTimeEntryEndpoint(
            @PathVariable Long waitTimeId,
            @RequestBody UpdateWaitTimeRequest updateWaitTimeRequest
    ) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.hasWaitTimeWithId(waitTimeId));

        if (!isOwn && !securityContextService.hasAuthority("PERM_CREATE_OR_MODIFY_WAIT_TIME")) {
            throw new AccessDeniedException("Not Authorized");
        }

        WaitTime waitTime = waitTimeService.findById(waitTimeId);
        waitTime.setTitle(updateWaitTimeRequest.getTitle());
        waitTime.setWaitTime(updateWaitTimeRequest.getWaitTime());
        waitTime.setCreatedAt(new Date());
        waitTimeService.save(waitTime);

        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Wait time updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_DELETE_WAIT_TIME') or hasAuthority('PERM_DELETE_OWN_WAIT_TIME')")
    @DeleteMapping("/provider/waittime/{waitTimeId}")
    public ResponseEntity<?> deleteWaitTimeEndpoint(@PathVariable Long waitTimeId) {
        Account currAccount = securityContextService.findCurrAccount();

        boolean isOwn = currAccount.getProviderInfos().stream()
                .anyMatch(providerInfo -> providerInfo.hasWaitTimeWithId(waitTimeId));

        if (!isOwn && !securityContextService.hasAuthority("PERM_DELETE_WAIT_TIME")) {
            throw new AccessDeniedException("Not Authorized");
        }

        waitTimeService.deleteWaitTimeById(waitTimeId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Wait time deleted successfully").build());
    }
}
