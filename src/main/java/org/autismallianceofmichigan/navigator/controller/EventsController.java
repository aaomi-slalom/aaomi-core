//package org.autismallianceofmichigan.navigator.controller;
//
//import lombok.AllArgsConstructor;
//import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
//import org.autismallianceofmichigan.navigator.dto.eventDto.CreateEventRequest;
//import org.autismallianceofmichigan.navigator.dto.eventDto.EventDto;
//import org.autismallianceofmichigan.navigator.dto.eventDto.UpdateEventRequest;
//import org.autismallianceofmichigan.navigator.init.PermissionCodes;
//import org.autismallianceofmichigan.navigator.persistence.entity.Account;
//import org.autismallianceofmichigan.navigator.persistence.entity.Event;
//import org.autismallianceofmichigan.navigator.service.AccountService;
//import org.autismallianceofmichigan.navigator.service.EventsService;
//import org.autismallianceofmichigan.navigator.service.SecurityContextService;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@CrossOrigin
//@RestController
//@AllArgsConstructor
//@RequestMapping("/api")
//public class EventsController {
//    private final EventsService eventsService;
//    private final AccountService accountService;
//    private final SecurityContextService securityContextService;
//
//    @PreAuthorize("permitAll()")
//    @GetMapping("events")
//    public List<EventDto> viewAllEvents() {
//        return eventsService.getAllEventDtos();
//    }
//
//    @PreAuthorize("isAuthenticated()")
//    @GetMapping("events/currentuser")
//    public List<EventDto> viewMyEvents() {
//        Account currentUser = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
//
//        return eventsService.getAllEventDtosForAccount(currentUser);
//    }
//
//    @PreAuthorize("permitAll()")
//    @GetMapping("event/{eventId}")
//    public EventDto viewEvent(@PathVariable Long eventId) {
//        return eventsService.getEventDtoById(eventId);
//    }
//
//    @PreAuthorize("hasAuthority('PERM_CREATE_EVENTS')")
//    @PostMapping("event")
//    public ResponseEntity<?> saveNewEventEndpoint(@RequestBody CreateEventRequest createEventRequest) {
//        Account creator = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
//
//        Event event = Event.builder()
//                .title(createEventRequest.getTitle())
//                .description(createEventRequest.getDescription())
//                .website(createEventRequest.getWebsite())
//                .cost(createEventRequest.getCost())
//                .free(createEventRequest.getFree())
//                .image(createEventRequest.getImage())
//                .startDate(createEventRequest.getStartDateTime())
//                .endDate(createEventRequest.getEndDateTime())
//                .organizer(creator)
//                .build();
//
//        eventsService.save(event);
//
//        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Event created successfully").build());
//    }
//
//    @PreAuthorize("isAuthenticated()")
//    @DeleteMapping("event/{eventId}")
//    public ResponseEntity<?> deleteEventEndpoint(@PathVariable Long eventId) {
//        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
//
//        if (securityContextService.hasAuthority(PermissionCodes.PERM_MODIFY_EVENTS)
//            || (eventsService.eventIsOrganizedByAccount(eventId, currAccount))) {
//            eventsService.deleteById(eventId);
//        } else {
//            throw new AccessDeniedException("Not Authorized");
//        }
//
//        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Event deleted successfully").build());
//    }
//
//    @PreAuthorize("isAuthenticated()")
//    @PatchMapping("event/{eventId}")
//    public ResponseEntity<?> updateEventEndpoint(@PathVariable Long eventId, @RequestBody UpdateEventRequest updateEventRequest) {
//        Account currAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
//
//        if (securityContextService.hasAuthority(PermissionCodes.PERM_MODIFY_EVENTS)
//                || (eventsService.eventIsOrganizedByAccount(eventId, currAccount))) {
//            eventsService.updateExistingEvent(eventId, updateEventRequest);
//        } else {
//            throw new AccessDeniedException("Not Authorized");
//        }
//
//        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Event updated successfully").build());
//    }
//
//}
