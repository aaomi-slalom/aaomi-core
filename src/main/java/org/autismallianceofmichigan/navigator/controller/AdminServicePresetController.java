package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.*;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AdminServicePresetController {
    private final ServiceCategoryService serviceCategoryService;
    private final ServiceService serviceService;
    private final InsuranceService insuranceService;
    private final ServiceAgeGroupService serviceAgeGroupService;
    private final ContactTypeService contactTypeService;
    private final ServiceDeliveryService serviceDeliveryService;
    private final CountyService countyService;
    private final ValidationService validationService;

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY')")
    @PostMapping("admin/service/category")
    public ResponseEntity<?> createServiceCategoryEndpoint(@RequestBody ServiceCategoryCreateUpdateRequest serviceCategoryCreateUpdateRequest) {
        if (validationService.hasValidationErrors(serviceCategoryCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to create service category...").build());
        }

        ServiceCategory serviceCategory = ServiceCategory.builder()
                .category(serviceCategoryCreateUpdateRequest.getCategory())
                .description(serviceCategoryCreateUpdateRequest.getDescription())
                .active(true)
                .build();

        serviceCategoryService.saveServiceCategory(serviceCategory);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service category successfully created").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY')")
    @PatchMapping("admin/service/category/{serviceCategoryId}")
    public ResponseEntity<?> updateServiceCategoryEndpoint(
            @PathVariable Long serviceCategoryId,
            @RequestBody ServiceCategoryCreateUpdateRequest serviceCategoryCreateUpdateRequest
    ) {
        if (validationService.hasValidationErrors(serviceCategoryCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update service category...").build());
        }

        ServiceCategory serviceCategory = serviceCategoryService.findByCategoryId(serviceCategoryId);
        serviceCategory.setCategory(serviceCategoryCreateUpdateRequest.getCategory());
        serviceCategory.setDescription(serviceCategoryCreateUpdateRequest.getDescription());
        serviceCategoryService.saveServiceCategory(serviceCategory);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service category successfully updated").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY')")
    @DeleteMapping("admin/service/category/{serviceCategoryId}")
    public ResponseEntity<?> deleteServiceCategoryEndpoint(@PathVariable Long serviceCategoryId) {
        ServiceCategory serviceCategory = serviceCategoryService.findByCategoryId(serviceCategoryId);

        if (serviceCategory.getServices().size() != 0) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Service categories with existing services cannot be deleted").build());
        }

        serviceCategoryService.deleteServiceCategory(serviceCategoryId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service category deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_SERVICE_CATEGORIES')")
    @GetMapping("admin/service/categories")
    public List<ServiceCategoryDto> showAllServiceCategoriesEndpoint() {
        return serviceCategoryService.getAllServiceCategoryDtos();
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_SERVICES')")
    @GetMapping("admin/services")
    public List<CategoryServicesDto> showAllServicesCategorizedEndpoint() {
        return serviceCategoryService.getAllCategoryServicesDto();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("admin/public/services")
    public List<CategoryServicesDto> showAllPublicServicesCategorizedEndpoint() {
        return serviceCategoryService.getAllPublicCategoryServicesDto();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE')")
    @PostMapping("admin/service")
    public ResponseEntity<?> createServiceEndpoint(@RequestBody ServiceCreateUpdateRequest serviceCreateUpdateRequest) {
        if (validationService.hasValidationErrors(serviceCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to create service...").build());
        }

        if (serviceService.hasServiceWithName(serviceCreateUpdateRequest.getService())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate service").build());
        }

        ServiceCategory currServiceCategory = serviceCategoryService.findByCategory(serviceCreateUpdateRequest.getCategory());

        Service service = Service.builder()
                .service(serviceCreateUpdateRequest.getService())
                .description(serviceCreateUpdateRequest.getDescription())
                .serviceCategory(currServiceCategory)
                .internal(serviceCreateUpdateRequest.isInternal())
                .active(true)
                .build();

        serviceService.saveService(service);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE')")
    @PatchMapping("admin/service/{serviceId}")
    public ResponseEntity<?> updateServiceEndpoint(
            @PathVariable Long serviceId,
            @RequestBody ServiceCreateUpdateRequest serviceCreateUpdateRequest
    ) {
        if (validationService.hasValidationErrors(serviceCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update service...").build());
        }

        Service currService = serviceService.findByServiceId(serviceId);
        ServiceCategory categoryUpdate = serviceCategoryService.findByCategory(serviceCreateUpdateRequest.getCategory());

        currService.setService(serviceCreateUpdateRequest.getService());
        currService.setServiceCategory(categoryUpdate);
        currService.setDescription(serviceCreateUpdateRequest.getDescription());
        currService.setInternal(serviceCreateUpdateRequest.isInternal());

        serviceService.saveService(currService);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE')")
    @DeleteMapping("admin/service/{serviceId}")
    public ResponseEntity<?> deleteServiceEndpoint(@PathVariable Long serviceId) {
        Service currService = serviceService.findByServiceId(serviceId);
        if (currService.getLocationSrvcLinks().size() > 0) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot delete service currently associated with providers").build());
        }

        serviceService.deleteByServiceId(serviceId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Service deleted successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_INSURANCES')")
    @GetMapping("admin/insurances")
    public List<InsuranceDto> showAllInsurancesEndpoint() {
        return insuranceService.getAllInsuranceDtos();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_INSURANCE')")
    @PostMapping("admin/insurance")
    public ResponseEntity<?> createInsuranceEndpoint(@RequestBody InsuranceCreateUpdateRequest insuranceCreateUpdateRequest) {
        if (validationService.hasValidationErrors(insuranceCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to create insurance...").build());
        }

        if (insuranceService.hasInsuranceWithName(insuranceCreateUpdateRequest.getInsuranceName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate insurance").build());
        }

        Insurance insurance = Insurance.builder()
                .insuranceName(insuranceCreateUpdateRequest.getInsuranceName())
                .contactNumber(insuranceCreateUpdateRequest.getContactNumber())
                .website(insuranceCreateUpdateRequest.getWebsite())
                .build();

        insuranceService.saveInsurance(insurance);
        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Insurance created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_INSURANCE')")
    @PatchMapping("/admin/insurance/{insuranceId}")
    public ResponseEntity<?> updateInsuranceEndpoint(
            @PathVariable Long insuranceId,
            @RequestBody InsuranceCreateUpdateRequest insuranceCreateUpdateRequest
    ) {
        if (validationService.hasValidationErrors(insuranceCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update insurance...").build());
        }

        Insurance currInsurance = insuranceService.findByInsuranceId(insuranceId);
        if (currInsurance.getInsuranceName().equals(insuranceCreateUpdateRequest.getInsuranceName())) {
            currInsurance.setContactNumber(insuranceCreateUpdateRequest.getContactNumber());
            currInsurance.setWebsite(insuranceCreateUpdateRequest.getWebsite());
            insuranceService.saveInsurance(currInsurance);
            return ResponseEntity.ok().body(GenericMessageResponse.builder()
                    .message("Insurance updated successfully").build());
        } else if (insuranceService.hasInsuranceWithName(insuranceCreateUpdateRequest.getInsuranceName())) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot update insurance because there exists one with the same name").build());
        }

        currInsurance.setInsuranceName(insuranceCreateUpdateRequest.getInsuranceName());
        currInsurance.setContactNumber(insuranceCreateUpdateRequest.getContactNumber());
        currInsurance.setWebsite(insuranceCreateUpdateRequest.getWebsite());
        insuranceService.saveInsurance(currInsurance);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Insurance updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_INSURANCE')")
    @DeleteMapping("/admin/insurance/{insuranceId}")
    public ResponseEntity<?> deleteInsuranceEndpoint(@PathVariable Long insuranceId) {
        int serviceCount = insuranceService.findByInsuranceId(insuranceId).getLocationSrvcLinks().size();

        if (serviceCount == 0) {
            insuranceService.deleteById(insuranceId);

            return ResponseEntity.ok().body(GenericMessageResponse.builder()
                    .message("Insurance deleted successfully").build());
        }

        return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                .message("Cannot delete insurance currently associated with providers").build());
    }

    @PreAuthorize("hasAuthority('PERM_VIEW_AGE_GROUPS')")
    @GetMapping("/admin/agegroups")
    public List<ServiceAgeGroupDto> showAllServiceAgeGroupsEndpoint() {
        return serviceAgeGroupService.getAllServiceAgeGroupDtos();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_AGE_GROUP')")
    @PostMapping("/admin/agegroup")
    public ResponseEntity<?> createAgeGroupEndpoint(@RequestBody ServiceAgeGroupCreateUpdateRequest serviceAgeGroupCreateUpdateRequest) {
        if (validationService.hasValidationErrors(serviceAgeGroupCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to create age group...").build());
        }

        if (serviceAgeGroupService.hasAgeGroupWithAgeStartAndAgeEnd(
                serviceAgeGroupCreateUpdateRequest.getAgeStart(),
                serviceAgeGroupCreateUpdateRequest.getAgeEnd()
        )) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot create duplicate age group").build());
        }

        ServiceAgeGroup serviceAgeGroup = ServiceAgeGroup.builder()
                .ageStart(serviceAgeGroupCreateUpdateRequest.getAgeStart())
                .ageEnd(serviceAgeGroupCreateUpdateRequest.getAgeEnd())
                .build();

        serviceAgeGroupService.saveServiceAgeGroup(serviceAgeGroup);
        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Age group created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_AGE_GROUP')")
    @PatchMapping("/admin/agegroup/{serviceAgeGroupId}")
    public ResponseEntity<?> updateAgeGroupEndpoint(
            @PathVariable Long serviceAgeGroupId,
            @RequestBody ServiceAgeGroupCreateUpdateRequest serviceAgeGroupCreateUpdateRequest
    ) {
        if (validationService.hasValidationErrors(serviceAgeGroupCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Error when trying to update age group...").build());
        }

        ServiceAgeGroup currAgeGroup = serviceAgeGroupService.findById(serviceAgeGroupId);
        if (currAgeGroup.getAgeStart() == serviceAgeGroupCreateUpdateRequest.getAgeStart() &&
                currAgeGroup.getAgeEnd() == serviceAgeGroupCreateUpdateRequest.getAgeEnd()) {
            return ResponseEntity.ok().body(GenericMessageResponse.builder()
                    .message("Age group updated successfully").build());
        } else if (serviceAgeGroupService.hasAgeGroupWithAgeStartAndAgeEnd(
                serviceAgeGroupCreateUpdateRequest.getAgeStart(),
                serviceAgeGroupCreateUpdateRequest.getAgeEnd()
        )) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("Cannot update age group because there exists one with same ages").build());
        }

        currAgeGroup.setAgeStart(serviceAgeGroupCreateUpdateRequest.getAgeStart());
        currAgeGroup.setAgeEnd(serviceAgeGroupCreateUpdateRequest.getAgeEnd());
        serviceAgeGroupService.saveServiceAgeGroup(currAgeGroup);

        return ResponseEntity.ok().body(GenericMessageResponse.builder()
                .message("Age group updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_AGE_GROUP')")
    @DeleteMapping("/admin/agegroup/{serviceAgeGroupId}")
    public ResponseEntity<?> deleteAgeGroupEndpoint(@PathVariable Long serviceAgeGroupId) {
        int serviceCount = serviceAgeGroupService.findById(serviceAgeGroupId).getLocationSrvcLinks().size();

        if (serviceCount == 0) {
            serviceAgeGroupService.deleteById(serviceAgeGroupId);

            return ResponseEntity.ok().body(GenericMessageResponse.builder()
                    .message("Age group deleted successfully").build());
        }

        return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                .message("Cannot delete age group associated with providers").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_PROVIDER_INFO') or " +
            "hasAuthority('PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO')")
    @GetMapping("/admin/provider/service/options")
    public List<ServiceOptionDto> viewServiceOptionsEndpoint() {
        return serviceService.getAllServiceOptionDtos();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/provider/service/options")
    public List<ServiceOptionDto> viewPublicServiceOptionsEndpoint() {
        return serviceService.getAllPublicServiceOptionDtos();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/admin/provider/category/options")
    public List<ServiceCategoryOptionDto> viewServiceCategoryOptionsEndpoint() {
        return serviceCategoryService.getAllServiceCategoryOptionDtos();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/admin/provider/insurance/options")
    public List<InsuranceOptionDto> viewInsuranceOptionsEndpoint() {
        return insuranceService.getAllInsuranceOptionDtos();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/admin/provider/agegroup/options")
    public List<ServiceAgeGroupOptionDto> viewAgeGroupOptionsEndpoint() {
        return serviceAgeGroupService.getAllServiceAgeGroupOptionDtos();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/admin/provider/contacttype/options")
    public List<ContactTypeOptionDto> viewContactTypeOptionsEndpoint() {
        return contactTypeService.getAllContactTypeOptionDtos();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/admin/provider/delivery/options")
    public List<ServiceDeliveryOptionDto> viewServiceDeliveryOptionsEndpoint() {
        return serviceDeliveryService.getAllServiceDeliveryOptionDtos();
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES')")
    @PostMapping("/admin/provider/delivery/create")
    public ResponseEntity<?> createNewServiceDeliveryEndpoint(@RequestBody ServiceDeliveryCreateUpdateRequest serviceDeliveryCreateRequest) {
        ServiceDelivery serviceDelivery = ServiceDelivery.builder()
                .delivery(serviceDeliveryCreateRequest.getDelivery())
                .build();
        serviceDeliveryService.save(serviceDelivery);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Service Delivery created successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES')")
    @PatchMapping("/admin/provider/delivery/{serviceDeliveryId}/update")
    public ResponseEntity<?> updateServiceDeliveryEndpoint(@PathVariable Long serviceDeliveryId, @RequestBody ServiceDeliveryCreateUpdateRequest serviceDeliveryUpdateRequest) {
        serviceDeliveryService.updateServiceDelivery(serviceDeliveryId, serviceDeliveryUpdateRequest);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Service Delivery updated successfully").build());
    }

    @PreAuthorize("hasAuthority('PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES')")
    @DeleteMapping("/admin/provider/delivery/{serviceDeliveryId}/delete")
    public ResponseEntity<?> deleteServiceDeliveryEndpoint(@PathVariable Long serviceDeliveryId) {
        serviceDeliveryService.deleteById(serviceDeliveryId);
        return ResponseEntity.ok().body(GenericMessageResponse.builder().message("Service Delivery deleted successfully").build());
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/admin/provider/micounty/options")
    public List<MichiganCountyOptionDto> viewMICountyOptionsEndpoint() {
        return countyService.getAllMichiganCountyOptions();
    }
}
