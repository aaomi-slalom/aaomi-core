package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.GenericMessageResponse;
import org.autismallianceofmichigan.navigator.dto.userProfileDto.UserProfileCreateUpdateRequest;
import org.autismallianceofmichigan.navigator.dto.userProfileDto.UserProfileResponse;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.UserProfile;
import org.autismallianceofmichigan.navigator.service.AccountService;
import org.autismallianceofmichigan.navigator.service.SecurityContextService;
import org.autismallianceofmichigan.navigator.service.ValidationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class ProfileController {
    private final AccountService accountService;
    private final ValidationService validationService;
    private final SecurityContextService securityContextService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/userprofile/show")
    public ResponseEntity<?> ShowUserProfileEndpoint() {
        Account currentAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        if (currentAccount == null) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("User not authenticated").build());
        }

        UserProfile userProfile = currentAccount.getUserProfile();

        if (userProfile == null) return ResponseEntity.ok().body(null);

        UserProfileResponse userProfileResponse = UserProfileResponse.builder()
                .firstName(userProfile.getFirstName())
                .lastName(userProfile.getLastName())
                .contactNumber(userProfile.getContactNumber())
                .website(userProfile.getWebsite())
                .bio(userProfile.getBio())
                .image(userProfile.getImage())
                .build();

        return ResponseEntity.ok(userProfileResponse);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/userprofile/edit")
    public ResponseEntity<?> editUserProfileEndpoint(@RequestBody UserProfileCreateUpdateRequest userProfileCreateUpdateRequest) {
        Account currentAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        if (currentAccount == null) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("User not authenticated").build());
        }

        accountService.updateUserProfile(currentAccount, userProfileCreateUpdateRequest);
        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("User profile updated successfully!").build());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/userprofile/create")
    public ResponseEntity<?> CreateUserProfileEndpoint(@RequestBody UserProfileCreateUpdateRequest userProfileCreateUpdateRequest) {
        if (validationService.hasValidationErrors(userProfileCreateUpdateRequest)) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("User profile has invalid values, creation failed...").build());
        }

        Account currentAccount = accountService.findByEmail(securityContextService.getCurrentAccountEmail());
        if (currentAccount == null) {
            return ResponseEntity.badRequest().body(GenericMessageResponse.builder()
                    .message("User not authenticated").build());
        }

        UserProfile userProfile = UserProfile.builder()
                .firstName(userProfileCreateUpdateRequest.getFirstName())
                .lastName(userProfileCreateUpdateRequest.getLastName())
                .bio(userProfileCreateUpdateRequest.getBio())
                .contactNumber(userProfileCreateUpdateRequest.getContactNumber())
                .image(userProfileCreateUpdateRequest.getImage())
                .website(userProfileCreateUpdateRequest.getWebsite())
                .account(currentAccount)
                .build();

        currentAccount.setUserProfile(userProfile);
        accountService.save(currentAccount);

        return ResponseEntity.ok(GenericMessageResponse.builder()
                .message("User profile created successfully!").build());
    }
}
