package org.autismallianceofmichigan.navigator.controller;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.auditDto.AuditDto;
import org.autismallianceofmichigan.navigator.service.ProviderAuditService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AuditController {
    private final ProviderAuditService providerAuditService;

    @PreAuthorize("hasAuthority('PERM_VIEW_PROVIDER_INFO') or hasAuthority('PERM_VIEW_OWN_PROVIDER_INFO')")
    @GetMapping("provider/{providerInfoId}/audit")
    public List<AuditDto> getProviderInfoByIdEndpoint(@PathVariable Long providerInfoId) {
        return providerAuditService.getAllAuditDtosByProviderInfoId(providerInfoId);
    }
}
