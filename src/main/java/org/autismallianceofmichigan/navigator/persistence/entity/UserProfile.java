package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_profiles")
public class UserProfile {
    @Id
    private Long userProfileId;

    @OneToOne
    @MapsId
    @JoinColumn(name = "account_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Account account;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "website", length = 1024)
    private String website;

    @Column(name = "contact_number", length = 15, nullable = false)
    private String contactNumber;

    @Column(name = "bio", columnDefinition = "text")
    private String bio;

    @Lob
    @Column(name = "image")
    private String image;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
}
