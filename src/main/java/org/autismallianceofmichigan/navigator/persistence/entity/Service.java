package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "services")
public class Service {
    @Id
    @Column(name = "service_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long serviceId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "service", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LocationSrvcLink> locationSrvcLinks = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "service_category_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ServiceCategory serviceCategory;

    @Column(name = "service", unique = true, nullable = false)
    private String service;

    @Column(name = "description", length = 2048)
    private String description;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "internal", nullable = false)
    private boolean internal;
}
