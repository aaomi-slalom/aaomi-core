package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "provider_audits")
public class ProviderAudit {
    @Id
    @Column(name = "provider_audit_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long providerAuditId;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Account account;

    @ManyToOne
    @JoinColumn(name = "provider_info_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderInfo providerInfo;

    @Column(name = "change_log", columnDefinition = "text", nullable = false)
    private String changeLog;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditTime;
}
