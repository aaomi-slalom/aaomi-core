package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "messages")
public class Message {
    @Id
    @Column(name = "message_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long messageId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "sentMessages")
    @JsonBackReference
    private Set<Account> senderAccounts = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "receivedMessages")
    @JsonBackReference
    private Set<Account> recipientAccounts = new HashSet<>();

    @Column(name = "subject", length = 2048, nullable = false)
    private String subject;

    @Column(name = "message", nullable = false, columnDefinition = "text")
    private String message;

    @Column(name = "is_read", nullable = false)
    private boolean isRead;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date messageTime;

    public void removeSenderByUsername(String username) {
        senderAccounts.removeIf(account -> account.getUsername().equals(username));
    }

    public void removeRecipientAccountByUsername(String username) {
        recipientAccounts.removeIf(account -> account.getUsername().equals(username));
    }

    public boolean isOrphan() {
        return senderAccounts.isEmpty() && recipientAccounts.isEmpty();
    }

    public String getSender() {
        if (senderAccounts.size() > 0) {
            return new ArrayList<>(senderAccounts).get(0).getUsername();
        }
        return "System";
    }
}
