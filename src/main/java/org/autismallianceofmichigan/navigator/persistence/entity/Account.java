package org.autismallianceofmichigan.navigator.persistence.entity;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "accounts")
public class Account implements UserDetails {
    @Id
    @Column(name = "account_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long accountId;

    @Pattern(regexp = "^(\\d|\\w)+$", message = "Username must contain no special characters or whitespace")
    @Column(name = "username", length = 50, unique = true, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    @Size(min = 8, message = "Password minimum is 8 characters")
    @Pattern.List({
            @Pattern(regexp = "(?=.*[0-9]).+", message = "Password must contain one digit"),
            @Pattern(regexp = "(?=.*[a-z]).+", message = "Password must contain one lowercase letter"),
            @Pattern(regexp = "(?=.*[A-Z]).+", message = "Password must contain one upper letter"),
            @Pattern(regexp = "(?=\\S+$).+", message = "Password must contain no whitespace")
    })
    private String password;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "locked", nullable = false)
    private boolean locked;

    @Column(name = "last_logged_in")
    private LocalDateTime lastLoggedIn;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private AccountVerification accountVerification;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private AccountRecovery accountRecovery;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private UserProfile userProfile;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProviderAudit> approvalLogs = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Note> notes;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProviderFile> providerFiles = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "account_provider_info_links",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "provider_info_id")})
    @JsonManagedReference
    private Set<ProviderInfo> providerInfos = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "account_sent_messages_links",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "message_id")})
    @JsonManagedReference
    @OrderBy("messageTime desc")
    private List<Message> sentMessages = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "account_received_messages_links",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "message_id")})
    @JsonManagedReference
    @OrderBy("messageTime desc")
    private List<Message> receivedMessages = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "authorities",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    @JsonManagedReference
    private Set<Role> roles = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "account_group_links",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "account_group_id")})
    @JsonManagedReference
    private Set<AccountGroup> accountGroups = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "account_permission_links",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    @JsonManagedReference
    private Set<Permission> permissions = new HashSet<>();

    public Set<Permission> getAllPermissions() {
        Set<Permission> allPermissions = accountGroups.stream()
                .map(AccountGroup::getPermissions)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        allPermissions.addAll(permissions);

        return allPermissions;
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return getAllPermissions().stream().map(permission ->
                new SimpleGrantedAuthority(permission.getPermCode())).collect(Collectors.toSet());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public void addNote(Note note) {
        notes.add(note);
        note.setAuthor(this);
    }

    public void addProviderFile(ProviderFile providerFile) {
        providerFiles.add(providerFile);
        providerFile.setAuthor(this);
    }

    public void removeReceivedMessageById(Long messageId) {
        receivedMessages = receivedMessages.stream().filter(message -> {
            if (message.getMessageId() == messageId) {
                message.removeRecipientAccountByUsername(username);
            }

            return message.getMessageId() != messageId;
        }).collect(Collectors.toList());
    }

    public void removeSentMessageById(Long messageId) {
        sentMessages = sentMessages.stream().filter(message -> {
            if (message.getMessageId() == messageId) {
                message.removeSenderByUsername(username);
            }

            return message.getMessageId() != messageId;
        }).collect(Collectors.toList());
    }
}
