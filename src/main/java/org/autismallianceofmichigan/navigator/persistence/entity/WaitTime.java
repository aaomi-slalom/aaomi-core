package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "wait_times")
public class WaitTime {
    @Id
    @Column(name = "wait_time_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long waitTimeId;

    @ManyToOne
    @JoinColumn(name = "wait_time_type_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private WaitTimeType waitTimeType;

    @ManyToOne
    @JoinColumn(name = "provider_info_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderInfo providerInfo;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "wait_time", columnDefinition = "text", nullable = false)
    private String waitTime;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
}
