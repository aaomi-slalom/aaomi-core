package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "counties")
public class County {
    @Id
    @Column(name = "county_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long countyId;

    @ManyToOne
    @JoinColumn(name = "region_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Region region;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "counties")
    @JsonBackReference
    private Set<ProviderLocation> providerLocations = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "modifiedCounties")
    @JsonBackReference
    private Set<ProviderLocation> modifiedProviderLocations = new HashSet<>();

    @Column(name = "county", nullable = false)
    private String county;
}
//select all counties, join county with location with service
