package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "service_deliveries")
public class ServiceDelivery {
    @Id
    @Column(name = "service_delivery_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long serviceDeliveryId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "serviceDelivery", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LocationSrvcLink> locationSrvcLinks = new HashSet<>();

    @Column(name = "delivery", nullable = false)
    private String delivery;
}
