package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "contact_types")
public class ContactType {
    @Id
    @Column(name = "contact_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long contactTypeId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "contactType", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Email> emails;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "contactType", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ContactNumber> contactNumbers;

    @Column(name = "type", length = 50, nullable = false)
    private String type;

    @Column(name = "description", columnDefinition = "text")
    private String description;
}
