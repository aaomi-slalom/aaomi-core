package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "provider_files")
public class ProviderFile {
    @Id
    @Column(name = "provider_file_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long providerFileId;

    @ManyToOne
    @JoinColumn(name = "provider_info_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderInfo providerInfo;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Account author;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Lob
    @Column(name = "file", nullable = false)
    private String file;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
}
