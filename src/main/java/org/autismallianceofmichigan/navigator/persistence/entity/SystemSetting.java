package org.autismallianceofmichigan.navigator.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "system_settings")
public class SystemSetting {
    @Id
    @Column(name = "system_setting_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long systemSettingId;

    @Lob
    @Column(name = "background_image")
    private String backgroundImage;

    @Column(name = "search_support_note", columnDefinition = "text")
    private String searchSupportNote;

    @Column(name = "technical_support_note", columnDefinition = "text")
    private String technicalSupportNote;

    @Column(name = "disclaimer", columnDefinition = "text")
    private String disclaimer;
}
