package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "wait_time_types")
public class WaitTimeType {
    @Id
    @Column(name = "wait_time_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long waitTimeTypeId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "waitTimeType", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WaitTime> waitTimes = new HashSet<>();

    @Column(name = "wait_time_type", length = 50, nullable = false)
    private String waitTimeType;
}
