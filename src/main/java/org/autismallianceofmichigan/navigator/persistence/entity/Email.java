package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "emails")
public class Email {
    @Id
    @Column(name = "email_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long emailId;

    @ManyToOne
    @JoinColumn(name = "contact_type_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ContactType contactType;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    @Column(name = "delete_requested", nullable = false)
    private boolean deleteRequested;

    @Column(name = "published", nullable = false)
    private boolean published;
}
