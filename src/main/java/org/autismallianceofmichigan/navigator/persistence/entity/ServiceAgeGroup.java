package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "service_age_groups")
public class ServiceAgeGroup {
    @Id
    @Column(name = "service_age_group_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long serviceAgeGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "serviceAgeGroup", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LocationSrvcLink> locationSrvcLinks = new HashSet<>();

    @Column(name = "age_start", columnDefinition = "tinyint unsigned", nullable = false)
    private int ageStart;

    @Column(name = "age_end", columnDefinition = "tinyint unsigned", nullable = false)
    private int ageEnd;
}
