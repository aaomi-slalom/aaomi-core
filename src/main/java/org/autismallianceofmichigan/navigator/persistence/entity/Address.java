package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "addresses")
public class Address {
    @Id
    @Column(name = "address_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long addressId;

    @ManyToOne
    @JoinColumn(name = "address_type_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private AddressType addressType;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @Column(name = "street", columnDefinition = "text", nullable = false)
    private String street;

    @Column(name = "modified_street", columnDefinition = "text")
    private String modifiedStreet;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "modified_city")
    private String modifiedCity;

    @Column(name = "state", length = 2, nullable = false)
    private String state;

    @Column(name = "modified_state", length = 2)
    private String modifiedState;

    @Column(name = "zip", length = 10, nullable = false)
    private String zip;

    @Column(name = "modified_zip", length = 10)
    private String modifiedZip;

    @Column(name = "published", nullable = false)
    private boolean published;

    @Column(name = "modify_address_requested", nullable = false)
    private boolean modifyAddressRequested;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    public void approveModifiedAddress() {
        if (modifyAddressRequested) {
            street = modifiedStreet;
            modifiedStreet = null;

            city = modifiedCity;
            modifiedCity = null;

            state = modifiedState;
            modifiedState = null;

            zip = modifiedZip;
            modifiedZip = null;

            modifyAddressRequested = false;
        }
    }

    public void disapproveModifiedAddress() {
        modifiedStreet = null;
        modifiedCity = null;
        modifiedState = null;
        modifiedZip = null;
        modifyAddressRequested = false;
    }

    public boolean isValid() {
        if (street != null && city != null && state != null && zip != null) {
            return !street.isEmpty() && !city.isEmpty() && !state.isEmpty() && !zip.isEmpty();
        }

        return false;
    }
}
