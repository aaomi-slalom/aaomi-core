package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "contact_numbers")
public class ContactNumber {
    @Id
    @Column(name = "contact_number_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long contactNumberId;

    @ManyToOne
    @JoinColumn(name = "contact_type_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ContactType contactType;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "published", nullable = false)
    private boolean published;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    @Column(name = "delete_requested", nullable = false)
    private boolean deleteRequested;
}
