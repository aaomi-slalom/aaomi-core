package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "notes")
public class Note {
    @Id
    @Column(name = "note_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long noteId;

    @ManyToOne
    @JoinColumn(name = "note_type_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private NoteType noteType;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Account author;

    @ManyToOne
    @JoinColumn(name = "provider_info_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderInfo providerInfo;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @ManyToOne
    @JoinColumn(name = "location_srvc_link_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private LocationSrvcLink locationSrvcLink;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "note", columnDefinition = "text", nullable = false)
    private String note;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
}

