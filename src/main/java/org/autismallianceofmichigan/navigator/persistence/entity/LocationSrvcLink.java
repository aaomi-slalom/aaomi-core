package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "location_srvc_links")
public class LocationSrvcLink {
    @Id
    @Column(name = "location_srvc_link_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long locationSrvcLinkId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "locationSrvcLink", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Note> notes;

    @ManyToOne
    @JoinColumn(name = "service_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Service service;

    @ManyToOne
    @JoinColumn(name = "service_age_group_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ServiceAgeGroup serviceAgeGroup;

    @ManyToOne
    @JoinColumn(name = "insurance_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "service_delivery_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ServiceDelivery serviceDelivery;

    @ManyToOne
    @JoinColumn(name = "provider_location_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderLocation providerLocation;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "internal", nullable = false)
    private boolean internal;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    @Column(name = "delete_requested", nullable = false)
    private boolean deleteRequested;

    @Column(name = "identifier", nullable = false)
    private String identifier;

    public Note addNote(Note note) {
        notes.add(note);
        note.setLocationSrvcLink(this);
        return note;
    }
}
