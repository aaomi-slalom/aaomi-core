package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "provider_locations")
public class ProviderLocation {
    @Id
    @Column(name = "provider_location_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long providerLocationId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Note> notes;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<WaitTime> waitTimes;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LocationSrvcLink> locationSrvcLinks = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Email> emails = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContactNumber> contactNumbers = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerLocation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Address> addresses = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "provider_location_county_links",
            joinColumns = {@JoinColumn(name = "provider_location_id")},
            inverseJoinColumns = {@JoinColumn(name = "county_id")})
    @JsonManagedReference
    private List<County> counties = new ArrayList<>();

    @Column(name = "modify_counties_requested", nullable = false)
    private boolean modifyCountiesRequested;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "provider_location_modified_county_links",
            joinColumns = {@JoinColumn(name = "provider_location_id")},
            inverseJoinColumns = {@JoinColumn(name = "county_id")})
    @JsonManagedReference
    private List<County> modifiedCounties = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "provider_info_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private ProviderInfo providerInfo;

    @Column(name = "location", length = 1024, nullable = false)
    private String location;

    @Column(name = "modified_location", length = 1024)
    private String modifiedLocation;

    @Column(name = "modify_location_requested", nullable = false)
    private boolean modifyLocationRequested;

    @Column(name = "internal", nullable = false)
    private boolean internal;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    @Column(name = "delete_requested", nullable = false)
    private boolean deleteRequested;

    public Note addNote(Note note) {
        notes.add(note);
        note.setProviderLocation(this);
        return note;
    }

    public void addWaitTime(WaitTime waitTime) {
        waitTimes.add(waitTime);
        waitTime.setProviderLocation(this);
    }

    public void approveAllCreatedEmails() {
        emails.forEach(email -> {
            email.setCreateApproved(true);
        });
    }

    public void approveAllCreatedContactNumbers() {
        contactNumbers.forEach(contactNumber -> {
            contactNumber.setCreateApproved(true);
        });
    }

    public void approveAllCreatedAddresses() {
        addresses.forEach(address -> {
            address.setCreateApproved(true);
        });
    }

    public void approveAllCreatedLocationSrvcLinks() {
        locationSrvcLinks.forEach(lsl -> {
            lsl.setCreateApproved(true);
        });
    }

    // location name approval
    public String getModifiedLocation() {
        if (modifyLocationRequested) {
            return modifiedLocation;
        }

        return null;
    }

    // county approvals
    public Supplier<Stream<County>> getModifiedCounties() {
        if (isModifyCountiesRequested()) {
            return () -> modifiedCounties.stream();
        }

        return Stream::empty;
    }

    // email approvals
    public Supplier<Stream<Email>> getUnapprovedAddedEmails() {
        return () -> emails.stream().
                filter(email -> !email.isCreateApproved());
    }

    public Supplier<Stream<Email>> getUnapprovedDeletedEmails() {
        return () -> emails.stream()
                .filter(Email::isDeleteRequested);
    }

    public Supplier<Stream<Email>> getCurrentEmails() {
        return () -> emails.stream()
                .filter(Email::isCreateApproved);
    }

    public List<String> listCurrentEmailsByType(String type) {
        return getCurrentEmails().get()
                .filter(email -> email.getContactType().getType().equals(type))
                .map(Email::getEmail)
                .collect(Collectors.toList());
    }

    // contact number approvals
    public Supplier<Stream<ContactNumber>> getUnapprovedAddedContactNumbers() {
        return () -> contactNumbers.stream().
                filter(contactNumber -> !contactNumber.isCreateApproved());
    }

    public Supplier<Stream<ContactNumber>> getUnapprovedDeletedContactNumbers() {
        return () -> contactNumbers.stream()
                .filter(ContactNumber::isDeleteRequested);
    }

    public Supplier<Stream<ContactNumber>> getCurrentContactNumbers() {
        return () -> contactNumbers.stream()
                .filter(ContactNumber::isCreateApproved);
    }

    public List<String> listCurrentContactNumbersByType(String type) {
        return getCurrentContactNumbers().get()
                .filter(contactNumber -> contactNumber.getContactType().getType().equals(type))
                .map(ContactNumber::getNumber)
                .collect(Collectors.toList());
    }

    // mailing address approvals
    public Address getMailingAddress() {
        List<Address> approvedMailingAddresses = addresses.stream()
                .filter(address -> address.getAddressType().getType().equals("Mailing"))
                .collect(Collectors.toList());

        if (approvedMailingAddresses.size() > 0) {
            return approvedMailingAddresses.get(0);
        }

        return null;
    }

    public Address getApprovedMailingAddress() {
        List<Address> approvedMailingAddresses = addresses.stream()
                .filter(address -> address.isCreateApproved() &&
                        address.getAddressType().getType().equals("Mailing"))
                .collect(Collectors.toList());

        if (approvedMailingAddresses.size() > 0) {
            return approvedMailingAddresses.get(0);
        }

        return null;
    }

    public Address getUnapprovedModifiedMailingAddress() {
        List<Address> unapprovedModifiedMailingAddresses = addresses.stream()
                .filter(address -> address.isModifyAddressRequested() &&
                        address.getAddressType().getType().equals("Mailing"))
                .collect(Collectors.toList());

        if (unapprovedModifiedMailingAddresses.size() > 0) {
            return unapprovedModifiedMailingAddresses.get(0);
        }

        return null;
    }

    public Address getUnapprovedAddedMailingAddress() {
        List<Address> unapprovedAddedMailingAddresses = addresses.stream()
                .filter(address -> !address.isCreateApproved() &&
                        address.getAddressType().getType().equals("Mailing"))
                .collect(Collectors.toList());

        if (unapprovedAddedMailingAddresses.size() > 0) {
            return unapprovedAddedMailingAddresses.get(0);
        }

        return null;
    }

    // service address approvals
    public Address getServiceAddress() {
        List<Address> approvedServiceAddresses = addresses.stream()
                .filter(address -> address.getAddressType().getType().equals("Service"))
                .collect(Collectors.toList());

        if (approvedServiceAddresses.size() > 0) {
            return approvedServiceAddresses.get(0);
        }

        return null;
    }

    public Address getApprovedServiceAddress() {
        List<Address> approvedServiceAddresses = addresses.stream()
                .filter(address -> address.isCreateApproved() &&
                        address.getAddressType().getType().equals("Service"))
                .collect(Collectors.toList());

        if (approvedServiceAddresses.size() > 0) {
            return approvedServiceAddresses.get(0);
        }

        return null;
    }

    public Address getUnapprovedModifiedServiceAddress() {
        List<Address> unapprovedModifiedServiceAddresses = addresses.stream()
                .filter(address -> address.isModifyAddressRequested() &&
                        address.getAddressType().getType().equals("Service"))
                .collect(Collectors.toList());

        if (unapprovedModifiedServiceAddresses.size() > 0) {
            return unapprovedModifiedServiceAddresses.get(0);
        }

        return null;
    }

    public Address getUnapprovedAddedServiceAddress() {
        List<Address> unapprovedAddedServiceAddresses = addresses.stream()
                .filter(address -> !address.isCreateApproved() &&
                        address.getAddressType().getType().equals("Service"))
                .collect(Collectors.toList());

        if (unapprovedAddedServiceAddresses.size() > 0) {
            return unapprovedAddedServiceAddresses.get(0);
        }

        return null;
    }

    // service approvals
    public Supplier<Stream<LocationSrvcLink>> getUnapprovedAddedLocationSrvcLinks() {
        return () -> locationSrvcLinks.stream().
                filter(locationSrvcLink -> !locationSrvcLink.isCreateApproved());
    }

    public Supplier<Stream<LocationSrvcLink>> getUnapprovedDeletedLocationSrvcLinks() {
        return () -> locationSrvcLinks.stream()
                .filter(LocationSrvcLink::isDeleteRequested);
    }

    public Supplier<Stream<LocationSrvcLink>> getCurrentLocationSrvcLinks() {
        return () -> locationSrvcLinks.stream()
                .filter(LocationSrvcLink::isCreateApproved);
    }

    @PrePersist
    public void updateProviderUpdateTime() {
        providerInfo.setUpdatedAt(LocalDateTime.now());
    }

    public void approveLocationName() {
        if (modifyLocationRequested) {
            location = modifiedLocation;
            modifiedLocation = null;
            modifyLocationRequested = false;
        }
    }

    public void disapproveLocationName() {
        modifiedLocation = null;
        modifyLocationRequested = false;
    }

    public void approveCounties() {
        if (modifyCountiesRequested) {
            counties = modifiedCounties;
            modifiedCounties = new ArrayList<>();
            modifyCountiesRequested = false;
        }
    }

    public void disapproveCounties() {
        modifiedCounties = new ArrayList<>();
        modifyCountiesRequested = false;
    }
}
