package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account_groups")
public class AccountGroup {
    @Id
    @Column(name = "account_group_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long accountGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "accountGroups")
    @JsonBackReference
    private Set<Account> accounts = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "group_permission_links",
            joinColumns = {@JoinColumn(name = "account_group_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    @JsonManagedReference
    private Set<Permission> permissions = new HashSet<>();

    @Column(name = "account_group", length = 50, unique = true)
    private String accountGroup;
}