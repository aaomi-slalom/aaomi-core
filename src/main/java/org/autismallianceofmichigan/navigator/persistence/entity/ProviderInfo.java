package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "provider_infos")
public class ProviderInfo {
    @Id
    @Column(name = "provider_info_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long providerInfoId;

    @Column(name = "provider_name", unique = true, nullable = false)
    private String providerName;

    @Column(name = "modified_provider_name", unique = true)
    private String modifiedProviderName;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProviderLocation> providerLocations = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerInfo", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<ProviderFile> providerFiles = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerInfo", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Note> notes = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WaitTime> waitTimes = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "providerInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProviderAudit> auditLogs = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "providerInfos")
    @JsonBackReference
    private Set<Account> accounts = new HashSet<>();

    @Lob
    @Column(name = "logo")
    private String logo;

    @Lob
    @Column(name = "modified_logo")
    private String modifiedLogo;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "modified_description", columnDefinition = "text")
    private String modifiedDescription;

    @Column(name = "website", length = 2048)
    private String website;

    @Column(name = "modified_website", columnDefinition = "text")
    private String modifiedWebsite;

    @Column(name = "facebook", columnDefinition = "text")
    private String facebook;

    @Column(name = "modified_facebook", columnDefinition = "text")
    private String modifiedFacebook;

    @Column(name = "twitter", columnDefinition = "text")
    private String twitter;

    @Column(name = "modified_twitter", columnDefinition = "text")
    private String modifiedTwitter;

    @Column(name = "linkedin", columnDefinition = "text")
    private String linkedin;

    @Column(name = "modified_linkedin", columnDefinition = "text")
    private String modifiedLinkedin;

    @Column(name = "instagram", columnDefinition = "text")
    private String instagram;

    @Column(name = "modified_instagram", columnDefinition = "text")
    private String modifiedInstagram;

    @Column(name = "youtube", columnDefinition = "text")
    private String youtube;

    @Column(name = "modified_youtube", columnDefinition = "text")
    private String modifiedYoutube;

    @Column(name = "create_approved", nullable = false)
    private boolean createApproved;

    @Column(name = "delete_requested", nullable = false)
    private boolean deleteRequested;

    @Column(name = "modify_requested", nullable = false)
    private boolean modifyRequested;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "last_edit_username")
    private String lastEditUsername;

    public Note addNote(Note note) {
        notes.add(note);
        note.setProviderInfo(this);
        return note;
    }

    public void addWaitTime(WaitTime waitTime) {
        waitTimes.add(waitTime);
        waitTime.setProviderInfo(this);
    }

    public ProviderFile addProviderFile(ProviderFile providerFile) {
        providerFiles.add(providerFile);
        providerFile.setProviderInfo(this);
        return providerFile;
    }

    public void approveAllCreatedLocations() {
        providerLocations.forEach(pl -> {
            pl.setCreateApproved(true);
            pl.approveAllCreatedEmails();
            pl.approveAllCreatedContactNumbers();
            pl.approveAllCreatedAddresses();
            pl.approveAllCreatedLocationSrvcLinks();
        });
    }

    public void approveModifiedDemographics() {
        if (isModifyRequested()) {
            providerName = modifiedProviderName;
            modifiedProviderName = null;

            description = modifiedDescription;
            modifiedDescription = null;

            facebook = modifiedFacebook;
            modifiedFacebook = null;

            linkedin = modifiedLinkedin;
            modifiedLinkedin = null;

            twitter = modifiedTwitter;
            modifiedTwitter = null;

            instagram = modifiedInstagram;
            modifiedInstagram = null;

            youtube = modifiedYoutube;
            modifiedYoutube = null;

            website = modifiedWebsite;
            modifiedWebsite = null;

            logo = modifiedLogo;
            modifiedLogo = null;

            modifyRequested = false;
        }
    }

    public void disapproveModifiedDemographics() {
        modifiedProviderName = null;
        modifiedDescription = null;
        modifiedFacebook = null;
        modifiedLinkedin = null;
        modifiedTwitter = null;
        modifiedInstagram = null;
        modifiedYoutube = null;
        modifiedWebsite = null;
        modifiedLogo = null;
        modifyRequested = false;
    }

    // audit
    public void addAuditLogs(ProviderAudit providerAudit) {
        if (auditLogs == null) {
            auditLogs = new ArrayList<>();
        }

        auditLogs.add(providerAudit);
        providerAudit.setProviderInfo(this);
    }

    // files
    public List<String> getAllFileNames() {
        return providerFiles.stream().map(ProviderFile::getFileName).collect(Collectors.toList());
    }

    public boolean hasProviderFileWithId(Long providerFileId) {
        return providerFiles.stream().anyMatch(providerFile -> providerFile.getProviderFileId() == providerFileId);
    }

    // wait times
    public boolean hasWaitTimeWithId(Long waitTimeId) {
        return waitTimes.stream().anyMatch(waitTime -> waitTime.getWaitTimeId() == waitTimeId);
    }
}
