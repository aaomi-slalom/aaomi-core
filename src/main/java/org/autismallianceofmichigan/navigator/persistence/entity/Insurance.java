package org.autismallianceofmichigan.navigator.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "insurances")
public class Insurance {
    @Id
    @Column(name = "insurance_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long insuranceId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LocationSrvcLink> locationSrvcLinks = new HashSet<>();

    @Column(name = "insurance_name", nullable = false)
    private String insuranceName;

    @Column(name = "website", length = 1024)
    private String website;

    @Column(name = "contact_number", length = 15)
    private String contactNumber;
}
