package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ProviderInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface ProviderInfoRepository extends JpaRepository<ProviderInfo, Long> {
    Optional<ProviderInfo> findByProviderName(String providerName);

    Page<ProviderInfo> findByProviderNameIgnoreCaseContainingOrderByProviderInfoIdDesc(String providerName, Pageable pageable);

    @Query("select count(distinct lsl.identifier) from LocationSrvcLink lsl where lsl.providerLocation.providerInfo.providerInfoId = :providerInfoId and lsl.deleteRequested = false")
    int countGroupedServicesByProviderInfoId(@Param("providerInfoId") Long providerInfoId);

    void deleteByProviderInfoId(Long providerInfoId);

    Stream<ProviderInfo> findAllByCreateApprovedIsFalse();

    @Query("select distinct pi from ProviderInfo pi " +
            "join pi.providerLocations pl " +
            "join pl.locationSrvcLinks lsl " +
            "join pl.emails e " +
            "join pl.contactNumbers cn " +
            "join pl.addresses ad where " +
            "pi.createApproved = false or " +
            "pi.modifyRequested = true or " +
            "pi.deleteRequested = true or " +
            "pl.createApproved = false or " +
            "pl.modifyLocationRequested = true or " +
            "pl.modifyCountiesRequested = true or " +
            "pl.deleteRequested = true or " +
            "e.createApproved = false or " +
            "e.deleteRequested = true or " +
            "cn.createApproved = false or " +
            "cn.deleteRequested = true or " +
            "ad.createApproved = false or " +
            "ad.modifyAddressRequested = true or " +
            "lsl.createApproved = false or " +
            "lsl.deleteRequested = true")
    Stream<ProviderInfo> findAllModifiedProviderInfos();

    @Query("select distinct pi from ProviderInfo pi " +
            "join pi.providerLocations pl " +
            "join pl.locationSrvcLinks lsl " +
            "join pl.emails e " +
            "join pl.contactNumbers cn " +
            "join pl.addresses ad where " +
            "pi.providerInfoId = :providerInfoId and " +
            "(pi.createApproved = false or " +
            "pi.modifyRequested = true or " +
            "pi.deleteRequested = true or " +
            "pl.createApproved = false or " +
            "pl.modifyLocationRequested = true or " +
            "pl.modifyCountiesRequested = true or " +
            "pl.deleteRequested = true or " +
            "e.createApproved = false or " +
            "e.deleteRequested = true or " +
            "cn.createApproved = false or " +
            "cn.deleteRequested = true or " +
            "ad.createApproved = false or " +
            "ad.modifyAddressRequested = true or " +
            "lsl.createApproved = false or " +
            "lsl.deleteRequested = true)")
    Optional<ProviderInfo> findPendingApprovedProviderInfoByProviderInfoId(@Param("providerInfoId") Long providerInfoId);
}
