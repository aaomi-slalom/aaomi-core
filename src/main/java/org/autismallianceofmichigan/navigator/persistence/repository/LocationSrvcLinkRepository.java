package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.LocationSrvcLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.stream.Stream;

public interface LocationSrvcLinkRepository extends JpaRepository<LocationSrvcLink, Long> {
    List<LocationSrvcLink> findByIdentifier(String identifier);

    Stream<LocationSrvcLink> streamAllByIdentifier(String identifier);

    boolean existsByIdentifier(String identifier);

    void deleteAllByIdentifier(String identifier);

    @Query("select distinct lsl from LocationSrvcLink lsl " +
            "join lsl.service s " +
            "join lsl.providerLocation pl " +
            "join pl.counties c " +
            "where c.region.regionId = :regionId and " +
            "s.serviceCategory.serviceCategoryId = :categoryId")
    Stream<LocationSrvcLink> streamAllGroupedServicesByRegionIdAndCategoryId(@Param("regionId") Long regionId, @Param("categoryId") Long categoryId);

    @Query("select count(distinct pi) from ProviderInfo pi " +
            "join pi.providerLocations pl " +
            "join pl.locationSrvcLinks lsl " +
            "join pl.counties c " +
            "where c.region.regionId = :regionId and " +
            "lsl.service.serviceCategory.serviceCategoryId = :categoryId and " +
            "lsl.service.serviceId = :serviceId")
    int countProvidersByRegionIdAndCategoryIdAndServiceId(@Param("regionId") Long regionId, @Param("categoryId") Long categoryId, @Param("serviceId") Long serviceId);
}
