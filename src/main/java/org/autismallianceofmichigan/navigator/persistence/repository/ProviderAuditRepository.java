package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ProviderAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.stream.Stream;

public interface ProviderAuditRepository extends JpaRepository<ProviderAudit, Long> {
    @Query("select pa from ProviderAudit pa" +
            " where pa.providerInfo.providerInfoId = :providerInfoId " +
            "order by pa.auditTime desc")
    Stream<ProviderAudit> streamAllByProviderInfoId(@Param("providerInfoId") Long providerInfoId);
}
