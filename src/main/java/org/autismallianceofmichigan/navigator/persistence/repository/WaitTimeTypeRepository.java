package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.WaitTimeType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WaitTimeTypeRepository extends JpaRepository<WaitTimeType, Long> {

    Optional<WaitTimeType> findByWaitTimeType(String waitTimeType);
}
