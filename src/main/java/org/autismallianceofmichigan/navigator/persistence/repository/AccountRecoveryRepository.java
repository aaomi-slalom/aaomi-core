package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.AccountRecovery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRecoveryRepository extends JpaRepository<AccountRecovery, Long> {
    AccountRecovery findByPasswordResetToken(String passwordResetToken);
}
