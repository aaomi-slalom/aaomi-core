package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ServiceDelivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface ServiceDeliveryRepository extends JpaRepository<ServiceDelivery, Long> {
    @Query("select sd from ServiceDelivery sd order by sd.delivery")
    Stream<ServiceDelivery> streamAll();

    Optional<ServiceDelivery> findByServiceDeliveryId(Long serviceDeliveryId);

    Optional<ServiceDelivery> findByDelivery(String delivery);

    @Query("select count(distinct lsl.identifier) from LocationSrvcLink lsl where lsl.serviceDelivery.serviceDeliveryId = :serviceDeliveryId")
    int countGroupedServicesByServiceDeliveryId(@Param("serviceDeliveryId") Long serviceDeliveryId);
}
