package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MessageRepository extends JpaRepository<Message, Long> {
    @Query("select m from Message m " +
            "join m.senderAccounts sa " +
            "where sa.username = :username " +
            "order by m.messageTime desc")
    Page<Message> findSentMessagesByAccountUsername(@Param("username") String username, Pageable pageable);

    @Query("select m from Message m " +
            "join m.recipientAccounts ra " +
            "where ra.username = :username " +
            "order by m.messageTime desc")
    Page<Message> findReceivedMessagesByAccountUsername(@Param("username") String username, Pageable pageable);
}
