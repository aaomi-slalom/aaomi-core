package org.autismallianceofmichigan.navigator.persistence.repository;

import java.util.List;
import org.autismallianceofmichigan.navigator.persistence.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.repository.query.Param;

public interface ServiceRepository extends JpaRepository<Service, Long> {
    Optional<Service> findByService(String service);

    @Query("select s from Service s order by s.service")
    Stream<Service> streamAll();

    @Query("select s from Service s where s.internal=false order by s.service")
    Stream<Service> streamAllByInternalFalse();

    Stream<Service> streamAllByServiceCategory_ServiceCategoryIdOrderByService(Long serviceCategoryId);
    
    @Query(value = "select s.service_id,s.service,s.description,s.internal,(select count(*) from location_srvc_links where service_id=s.service_id) as deletable from services s where s.service_category_id = :serviceCategoryId", nativeQuery = true)
    List<Object[]> displayServicesFromCategoryId(@Param("serviceCategoryId") Long serviceCategoryId);
}
