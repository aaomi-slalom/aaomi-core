package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Address;
import org.autismallianceofmichigan.navigator.persistence.entity.AddressType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {
    List<Address> findByStreetAndCityAndStateAndZipAndAddressType(String street, String city, String state, String zip, AddressType addressType);
}
