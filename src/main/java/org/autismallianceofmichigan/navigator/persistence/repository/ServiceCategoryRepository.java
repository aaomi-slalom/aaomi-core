package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory, Long> {
    @Query("select sc from ServiceCategory sc order by sc.category")
    Stream<ServiceCategory> streamAll();

    Optional<ServiceCategory> findByCategory(String category);

    @Query("select count(distinct lsl.identifier) from LocationSrvcLink lsl where lsl.service.serviceCategory.serviceCategoryId = :serviceCategoryId")
    int countGroupedServicesByServiceCategoryId(@Param("serviceCategoryId") Long serviceCategoryId);

    @Query("select count(distinct pi.providerInfoId) from ProviderInfo pi " +
            "join pi.providerLocations pl " +
            "join pl.counties c " +
            "join pl.locationSrvcLinks lsl " +
            "join lsl.service s " +
            "where c.region.regionId = :regionId and " +
            "s.serviceCategory.serviceCategoryId = :categoryId")
    int countProvidersByRegionIdAndCategoryId(@Param("regionId") Long regionId, @Param("categoryId") Long categoryId);

    @Query("select distinct sc from ServiceCategory sc " +
            "join sc.services s " +
            "join s.locationSrvcLinks lsl " +
            "join lsl.providerLocation pl " +
            "join pl.counties c " +
            "where c.region.regionId = :regionId")
    Stream<ServiceCategory> streamAllCategoriesByRegionId(@Param("regionId") Long regionId);
}
