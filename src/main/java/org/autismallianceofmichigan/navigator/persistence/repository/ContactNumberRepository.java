package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ContactNumber;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactNumberRepository extends JpaRepository<ContactNumber, Long> {
    List<ContactNumber> findByNumberAndContactType(String number, ContactType contactType);
}
