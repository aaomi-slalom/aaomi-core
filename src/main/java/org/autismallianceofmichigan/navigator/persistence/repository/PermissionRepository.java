package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Optional<Permission> findByPermCode(String permCode);

    @Query(value = "select p from Permission p order by p.permCode")
    Stream<Permission> streamAll();
}
