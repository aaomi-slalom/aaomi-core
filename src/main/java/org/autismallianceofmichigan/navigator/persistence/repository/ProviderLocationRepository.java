package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ProviderLocation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface ProviderLocationRepository extends JpaRepository<ProviderLocation, Long> {
    @Query("select distinct pl from ProviderLocation pl " +
            "left join pl.notes n " +
            "join pl.counties c " +
            "join pl.locationSrvcLinks lsl " +
            "join lsl.service s where " +
            "(:providerName is null or pl.providerInfo.providerName like concat('%', :providerName, '%')) and " +
            "(coalesce(:categoryIds, null) is null or s.serviceCategory.serviceCategoryId in (:categoryIds)) and " +
            "(coalesce(:serviceIds, null) is null or s.serviceId in (:serviceIds)) and " +
            "(coalesce(:insuranceIds, null) is null or lsl.insurance.insuranceId in (:insuranceIds)) and " +
            "(coalesce(:ageGroupIds, null) is null or lsl.serviceAgeGroup.serviceAgeGroupId in (:ageGroupIds)) and " +
            "(coalesce(:deliveryIds, null) is null or lsl.serviceDelivery.serviceDeliveryId in (:deliveryIds)) and " +
            "(coalesce(:countyIds, null) is null or c.countyId in (:countyIds)) and " +
            "(coalesce(:regionIds, null) is null or c.region.regionId in (:regionIds)) and " +
            "(:keyword is null or pl.providerInfo.description like concat('%', :keyword, '%') " +
            "or pl.location like concat('%', :keyword, '%') " +
            "or pl.providerInfo.providerName like concat('%', :keyword, '%')) and " +
            "(pl.internal = false) and " +
            "(pl.createApproved = true) and " +
            "(pl.providerInfo.createApproved = true) " +
            "order by pl.providerInfo.providerName")
    Page<ProviderLocation> queryProviderLocationByConditions(
            @Param("providerName") String providerName,
            @Param("categoryIds") List<Long> categoryIds,
            @Param("serviceIds") List<Long> serviceIds,
            @Param("insuranceIds") List<Long> insuranceIds,
            @Param("ageGroupIds") List<Long> ageGroupIds,
            @Param("deliveryIds") List<Long> deliveryIds,
            @Param("countyIds") List<Long> countyIds,
            @Param("regionIds") List<Long> regionIds,
            @Param("keyword") String keyword,
            Pageable pageable
    );

    @Query("select distinct pl from ProviderLocation pl " +
            "left join pl.notes n " +
            "join pl.counties c " +
            "join pl.locationSrvcLinks lsl " +
            "join lsl.service s where " +
            "(:providerName is null or pl.providerInfo.providerName like concat('%', :providerName, '%')) and " +
            "(coalesce(:categoryIds, null) is null or s.serviceCategory.serviceCategoryId in (:categoryIds)) and " +
            "(coalesce(:serviceIds, null) is null or s.serviceId in (:serviceIds)) and " +
            "(coalesce(:insuranceIds, null) is null or lsl.insurance.insuranceId in (:insuranceIds)) and " +
            "(coalesce(:ageGroupIds, null) is null or lsl.serviceAgeGroup.serviceAgeGroupId in (:ageGroupIds)) and " +
            "(coalesce(:deliveryIds, null) is null or lsl.serviceDelivery.serviceDeliveryId in (:deliveryIds)) and " +
            "(coalesce(:countyIds, null) is null or c.countyId in (:countyIds)) and " +
            "(coalesce(:regionIds, null) is null or c.region.regionId in (:regionIds)) and " +
            "(:keyword is null " +
            "or pl.providerInfo.description like concat('%', :keyword, '%') " +
            "or n.title like concat('%', :keyword, '%') " +
            "or n.note like concat('%', :keyword, '%') " +
            "or pl.location like concat('%', :keyword, '%') " +
            "or pl.providerInfo.providerName like concat('%', :keyword, '%')) and " +
            "(pl.createApproved = true) and " +
            "(pl.providerInfo.createApproved = true) " +
            "order by pl.providerInfo.providerName")
    Page<ProviderLocation> internalQueryProviderLocationByConditions(
            @Param("providerName") String providerName,
            @Param("categoryIds") List<Long> categoryIds,
            @Param("serviceIds") List<Long> serviceIds,
            @Param("insuranceIds") List<Long> insuranceIds,
            @Param("ageGroupIds") List<Long> ageGroupIds,
            @Param("deliveryIds") List<Long> deliveryIds,
            @Param("countyIds") List<Long> countyIds,
            @Param("regionIds") List<Long> regionIds,
            @Param("keyword") String keyword,
            Pageable pageable
    );

    @Query("select distinct pl from ProviderLocation pl " +
            "left join pl.notes n " +
            "join pl.counties c " +
            "join pl.locationSrvcLinks lsl " +
            "join lsl.service s where " +
            "(:providerName is null or pl.providerInfo.providerName like concat('%', :providerName, '%')) and " +
            "(coalesce(:categoryIds, null) is null or s.serviceCategory.serviceCategoryId in (:categoryIds)) and " +
            "(coalesce(:serviceIds, null) is null or s.serviceId in (:serviceIds)) and " +
            "(coalesce(:insuranceIds, null) is null or lsl.insurance.insuranceId in (:insuranceIds)) and " +
            "(coalesce(:ageGroupIds, null) is null or lsl.serviceAgeGroup.serviceAgeGroupId in (:ageGroupIds)) and " +
            "(coalesce(:deliveryIds, null) is null or lsl.serviceDelivery.serviceDeliveryId in (:deliveryIds)) and " +
            "(coalesce(:countyIds, null) is null or c.countyId in (:countyIds)) and " +
            "(coalesce(:regionIds, null) is null or c.region.regionId in (:regionIds)) and " +
            "(:keyword is null " +
            "or pl.providerInfo.description like concat('%', :keyword, '%') " +
            "or n.title like concat('%', :keyword, '%') " +
            "or n.note like concat('%', :keyword, '%') " +
            "or pl.location like concat('%', :keyword, '%') " +
            "or pl.providerInfo.providerName like concat('%', :keyword, '%')) and " +
            "(pl.createApproved = true) and " +
            "(pl.providerInfo.createApproved = true) and " +
            "(pl.providerInfo.accounts is empty)" +
            "order by pl.providerInfo.providerName")
    Page<ProviderLocation> internalQueryStandaloneProviderLocationByConditions(
            @Param("providerName") String providerName,
            @Param("categoryIds") List<Long> categoryIds,
            @Param("serviceIds") List<Long> serviceIds,
            @Param("insuranceIds") List<Long> insuranceIds,
            @Param("ageGroupIds") List<Long> ageGroupIds,
            @Param("deliveryIds") List<Long> deliveryIds,
            @Param("countyIds") List<Long> countyIds,
            @Param("regionIds") List<Long> regionIds,
            @Param("keyword") String keyword,
            Pageable pageable
    );

    Stream<ProviderLocation> findAllByCreateApprovedIsFalseAndProviderInfo_CreateApprovedIsTrue();

    @Query("select distinct pl from ProviderLocation pl " +
            "join pl.locationSrvcLinks lsl " +
            "join pl.emails e " +
            "join pl.contactNumbers cn " +
            "join pl.addresses ad where " +
            "pl.providerLocationId = :providerLocationId and " +
            "pl.deleteRequested = false and " +
            "(pl.modifyLocationRequested = true or " +
            "pl.modifyCountiesRequested = true or " +
            "e.createApproved = false or " +
            "e.deleteRequested = true or " +
            "cn.createApproved = false or " +
            "cn.deleteRequested = true or " +
            "ad.createApproved = false or " +
            "ad.modifyAddressRequested = true or " +
            "lsl.createApproved = false or " +
            "lsl.deleteRequested = true)")
    Optional<ProviderLocation> findModifiedProviderLocationById(Long providerLocationId);
}
