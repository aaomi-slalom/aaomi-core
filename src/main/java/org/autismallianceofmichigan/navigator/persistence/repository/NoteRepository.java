package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {

}
