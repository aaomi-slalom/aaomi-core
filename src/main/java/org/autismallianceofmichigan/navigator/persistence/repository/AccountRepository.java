package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.entity.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByUsername(String username);

    Optional<Account> findByEmail(String email);

    void deleteAccountByUsername(String username);

    boolean existsByUsername(String username);

    Page<Account> findByAccountGroups_AccountGroupOrderByAccountIdDesc(String accountGroup, Pageable pageable);

    Page<Account> findByUsernameIgnoreCaseContainingOrderByAccountIdDesc(String username, Pageable pageable);

    Page<Account> findByUsernameIgnoreCaseContainingAndAccountGroups_AccountGroupIgnoreCaseContainingOrUsernameIgnoreCaseContainingAndRoles_DefaultGroupIgnoreCaseContainingOrderByAccountIdDesc(String username, String accountGroup, String usernameSame, String defaultGroup, Pageable pageable);

    Stream<Account> findByAccountGroupsIsEmptyAndRolesIsNotNull();
}
