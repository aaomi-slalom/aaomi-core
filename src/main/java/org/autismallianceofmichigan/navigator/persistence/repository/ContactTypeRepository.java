package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

public interface ContactTypeRepository extends JpaRepository<ContactType, Long> {
    @Query("select ct from ContactType ct order by ct.type")
    Stream<ContactType> streamAll();

    Optional<ContactType> findByType(String type);
}
