package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.NoteType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NoteTypeRepository extends JpaRepository<NoteType, Long> {
    Optional<NoteType> findByNoteType(String noteType);
}
