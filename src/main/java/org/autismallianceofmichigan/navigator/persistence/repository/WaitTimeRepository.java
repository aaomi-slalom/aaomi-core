package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.WaitTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WaitTimeRepository extends JpaRepository<WaitTime, Long> {
    @Modifying
    @Query("delete from WaitTime wt where wt.waitTimeId = :waitTimeId")
    void forceDeleteWaitTimesByWaitTimeId(@Param("waitTimeId") Long waitTimeId);
}
