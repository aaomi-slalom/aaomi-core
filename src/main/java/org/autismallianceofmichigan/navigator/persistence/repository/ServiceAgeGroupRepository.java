package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ServiceAgeGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface ServiceAgeGroupRepository extends JpaRepository<ServiceAgeGroup, Long> {
    @Query("select sag from ServiceAgeGroup sag order by sag.ageStart")
    Stream<ServiceAgeGroup> streamAll();

    Optional<ServiceAgeGroup> findByAgeStartAndAgeEnd(int ageStart, int ageEnd);

    @Query("select count(distinct lsl.identifier) from LocationSrvcLink lsl where lsl.serviceAgeGroup.serviceAgeGroupId = :ageGroupId")
    int countGroupedServicesByAgeGroupId(@Param("ageGroupId") Long ageGroupId);
}
