package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.ProviderFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProviderFileRepository extends JpaRepository<ProviderFile, Long> {

}
