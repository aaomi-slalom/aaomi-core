package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {
    @Query("select i from Insurance i order by i.insuranceName")
    Stream<Insurance> streamAll();

    Optional<Insurance> findByInsuranceName(String insuranceName);

    @Query("select count(distinct lsl.identifier) from LocationSrvcLink lsl where lsl.insurance.insuranceId = :insuranceId")
    int countGroupedServicesByInsuranceId(@Param("insuranceId") Long insuranceId);
}

