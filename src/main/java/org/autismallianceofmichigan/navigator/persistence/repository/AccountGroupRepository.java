package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

public interface AccountGroupRepository extends JpaRepository<AccountGroup, Long> {
    Optional<AccountGroup> findByAccountGroup(String accountGroup);

    @Query("select ag from AccountGroup ag order by ag.accountGroup")
    Stream<AccountGroup> streamAll();

    void deleteAccountGroupByAccountGroup(String accountGroup);
}
