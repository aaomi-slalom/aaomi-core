package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.County;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface CountyRepository extends JpaRepository<County, Long> {
    Optional<County> findByCounty(String county);

    List<County> findByCountyIdIn(List<Long> counties);

    @Query("select c from County c order by c.county")
    Stream<County> streamAll();

    Stream<County> streamAllByRegion_RegionIdOrderByCounty(Long regionId);

    @Query("select count (distinct lsl.identifier) from LocationSrvcLink lsl " +
            "join lsl.providerLocation pl " +
            "join pl.counties c " +
            "where c.countyId = :countyId")
    int countGroupedServicesByCountyId(@Param("countyId") Long countyId);
}
