package org.autismallianceofmichigan.navigator.persistence.repository;

import org.autismallianceofmichigan.navigator.persistence.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface RegionRepository extends JpaRepository<Region, Long> {
    Optional<Region> findByRegion(String region);

    @Query("select r from Region r order by r.region")
    Stream<Region> streamAll();

    @Query("select count (distinct lsl.identifier) from LocationSrvcLink lsl " +
            "join lsl.providerLocation pl " +
            "join pl.counties c " +
            "where c.region.regionId = :regionId")
    int countGroupedServicesByRegionId(@Param("regionId") Long regionId);

    @Query("select count (distinct pi.providerInfoId) from ProviderInfo pi " +
            "join pi.providerLocations pl " +
            "join pl.counties c " +
            "where c.region.regionId = :regionId")
    int countProvidersByRegionId(@Param("regionId") Long regionId);
}
