package org.autismallianceofmichigan.navigator.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.autismallianceofmichigan.navigator.exception.NoSecretKeyException;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.entity.Role;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class JwtHandler {
    private static final String BEARER_PREFIX = "Bearer ";
    private final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;

    @Value("${secret-key}")
    private String SECRET_KEY;

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(token)
                .getBody();
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(Account account) throws NoSecretKeyException {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, account.getUsername(), account.getAuthorities(), account.getRoles(), account.getAccountGroups());
    }

    private String createToken(Map<String, Object> claims, String subject, Collection<? extends GrantedAuthority> permissions, Set<Role> roles, Set<AccountGroup> groups) throws NoSecretKeyException {
        if (SECRET_KEY.isEmpty()) {
            throw new NoSecretKeyException("No Secret Key found!");
        }

        int validDurationMillis = 14_400_000; // 4 hour
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        Set<String> permissionNames = permissions.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        Set<String> roleNames = roles.stream().map(Role::getRole).collect(Collectors.toSet());
        Set<String> groupNames = groups.stream().map(AccountGroup::getAccountGroup).collect(Collectors.toSet());

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .claim("permissions", permissionNames)
                .claim("roles", roleNames)
                .claim("groups", groupNames)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + validDurationMillis))
                .signWith(signatureAlgorithm, signingKey)
                .compact();
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);

        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
