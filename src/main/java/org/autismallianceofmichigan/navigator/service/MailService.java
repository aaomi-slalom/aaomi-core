package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
@AllArgsConstructor
public class MailService {
    private final Environment env;
    private final MailSender mailSender;

    public void sendMail(String to, String subject, String content) throws IOException {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(content);

        mailSender.send(simpleMailMessage);
    }
}
