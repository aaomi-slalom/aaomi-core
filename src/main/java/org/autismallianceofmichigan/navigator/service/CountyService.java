package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderLocationRequest;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;
import org.autismallianceofmichigan.navigator.exception.CountyNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.County;
import org.autismallianceofmichigan.navigator.persistence.repository.CountyRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class CountyService {
    private final CountyRepository countyRepository;

    public County findByCounty(String county) {
        Optional<County> countyOptional = countyRepository.findByCounty(county);
        return countyOptional.orElseThrow(() -> new CountyNotFoundException("County not found"));
    }

    public List<County> findCountiesByCountyIds(List<Long> countyIds) {
        return countyRepository.findByCountyIdIn(countyIds);
    }

    public County findByCountyId(Long countyId) {
        Optional<County> countyOptional = countyRepository.findById(countyId);
        return countyOptional.orElseThrow(() -> new CountyNotFoundException("County not found"));
    }

    public void saveCounty(County county) {
        countyRepository.save(county);
    }

    public boolean hasCounty(String county) {
        return countyRepository.findByCounty(county).isPresent();
    }

    public List<County> getAllCounties() {
        return countyRepository.findAll();
    }

    @Transactional
    public List<MichiganCountyOptionDto> getAllMichiganCountyOptions() {
        return countyRepository.streamAll().map(county ->
                MichiganCountyOptionDto.builder()
                        .countyId(county.getCountyId())
                        .county(county.getCounty())
                        .build()
        ).collect(Collectors.toList());
    }

    public List<County> createOrUseExistingCounties(CreateProviderLocationRequest createProviderLocationRequest) {
        return createProviderLocationRequest.getCounties()
                .stream()
                .map(createCountyRequest -> findByCounty(createCountyRequest.getCounty()))
                .collect(Collectors.toList());
    }

    @Transactional
    public Stream<County> streamAllCountiesByRegionId(Long regionId) {
        return countyRepository.streamAllByRegion_RegionIdOrderByCounty(regionId);
    }

    public int countServicesByCountyId(Long countyId) {
        return countyRepository.countGroupedServicesByCountyId(countyId);
    }
}
