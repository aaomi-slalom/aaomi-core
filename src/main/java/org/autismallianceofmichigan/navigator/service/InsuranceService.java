package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.InsuranceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.InsuranceOptionDto;
import org.autismallianceofmichigan.navigator.exception.InsuranceNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.Insurance;
import org.autismallianceofmichigan.navigator.persistence.repository.InsuranceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class InsuranceService {
    private final InsuranceRepository insuranceRepository;

    public boolean hasInsuranceWithName(String insuranceName) {
        return insuranceRepository.findByInsuranceName(insuranceName).isPresent();
    }

    public Insurance findByInsuranceId(Long insuranceId) {
        Optional<Insurance> insuranceOptional = insuranceRepository.findById(insuranceId);
        return insuranceOptional.orElseThrow(() -> new InsuranceNotFoundException("Insurance not found"));
    }

    public void deleteById(Long insuranceId) {
        insuranceRepository.deleteById(insuranceId);
    }

    public void saveInsurance(Insurance insurance) {
        insuranceRepository.save(insurance);
    }

    @Transactional
    public List<InsuranceOptionDto> getAllInsuranceOptionDtos() {
        return insuranceRepository.streamAll().map(insurance ->
                InsuranceOptionDto.builder()
                        .insuranceId(insurance.getInsuranceId())
                        .insuranceName(insurance.getInsuranceName())
                        .build()
        ).collect(Collectors.toList());
    }

    @Transactional
    public List<InsuranceDto> getAllInsuranceDtos() {
        return insuranceRepository.streamAll().map(insurance -> {
                    int serviceCount = insuranceRepository.countGroupedServicesByInsuranceId(insurance.getInsuranceId());

                    return InsuranceDto.builder()
                            .insuranceId(insurance.getInsuranceId())
                            .insuranceName(insurance.getInsuranceName())
                            .website(insurance.getWebsite())
                            .contactNumber(insurance.getContactNumber())
                            .serviceCount(serviceCount)
                            .build();
                }
        ).collect(Collectors.toList());
    }
}
