package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.exception.WaitTimeTypeNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.WaitTimeType;
import org.autismallianceofmichigan.navigator.persistence.repository.WaitTimeTypeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class WaitTimeTypeService {
    private final WaitTimeTypeRepository waitTimeTypeRepository;

    public WaitTimeType findByWaitTimeType(String waitTimeType) {
        Optional<WaitTimeType> waitTimeTypeOptional = waitTimeTypeRepository.findByWaitTimeType(waitTimeType);
        return waitTimeTypeOptional.orElseThrow(() -> new WaitTimeTypeNotFoundException("Wait time type not found"));
    }

    public boolean hasWaitTimeType(String waitTimeType) {
        return waitTimeTypeRepository.findByWaitTimeType(waitTimeType).isPresent();
    }

    public void saveWaitTimeType(WaitTimeType waitTimeType) {
        waitTimeTypeRepository.save(waitTimeType);
    }
}
