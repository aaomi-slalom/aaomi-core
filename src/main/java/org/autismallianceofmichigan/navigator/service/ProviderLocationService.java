package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.approvalDto.UnapprovedLocationOverviewDto;
import org.autismallianceofmichigan.navigator.dto.locationDto.InternalLocationDto;
import org.autismallianceofmichigan.navigator.dto.locationDto.OtherLocationDto;
import org.autismallianceofmichigan.navigator.dto.locationDto.PublicLocationDto;
import org.autismallianceofmichigan.navigator.dto.locationDto.UnapprovedLocationDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.exception.ProviderLocationNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.persistence.repository.ProviderLocationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class ProviderLocationService {
    private final ProviderLocationRepository providerLocationRepository;
    private final LocationSrvcLinkService locationSrvcLinkService;
    private final EmailService emailService;
    private final ContactNumberService contactNumberService;
    private final CountyService countyService;
    private final AddressService addressService;

    public void deleteById(Long providerLocationId) {
        providerLocationRepository.deleteById(providerLocationId);
    }

    public void requestToDelete(ProviderLocation providerLocation) {
        if (!providerLocation.isCreateApproved()) {
            deleteById(providerLocation.getProviderLocationId());
        } else {
            providerLocation.setDeleteRequested(true);
            save(providerLocation);
        }
    }

    public boolean hasProviderLocationWithId(Long providerLocationId) {
        return providerLocationRepository.existsById(providerLocationId);
    }

    public ProviderLocation findById(Long providerLocationId) {
        Optional<ProviderLocation> providerLocationOptional = providerLocationRepository.findById(providerLocationId);
        return providerLocationOptional.orElseThrow(() -> new ProviderLocationNotFoundException("ProviderLocation not found"));
    }

    public void save(ProviderLocation providerLocation) {
        providerLocationRepository.save(providerLocation);
    }

    public ProviderLocation buildProviderLocation(
            ProviderInfo providerInfo,
            CreateProviderLocationRequest createProviderLocationRequest,
            boolean createApproved
    ) {
        ProviderLocation providerLocation = ProviderLocation.builder()
                .internal(createProviderLocationRequest.isInternal())
                .location(createProviderLocationRequest.getLocationName())
                .createApproved(createApproved)
                .providerInfo(providerInfo)
                .build();

        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkService.createLocationSrvcLinks(providerLocation, createProviderLocationRequest, createApproved);
        List<Email> emails = emailService.createEmails(providerLocation, createProviderLocationRequest, createApproved);
        List<ContactNumber> contactNumbers = contactNumberService.createContactNumbers(providerLocation, createProviderLocationRequest, createApproved);
        List<County> counties = countyService.createOrUseExistingCounties(createProviderLocationRequest);
        List<Address> addresses = addressService.createAddresses(providerLocation, createProviderLocationRequest, createApproved);

        providerLocation.setLocationSrvcLinks(locationSrvcLinks);
        providerLocation.setEmails(emails);
        providerLocation.setContactNumbers(contactNumbers);
        providerLocation.setAddresses(addresses);
        providerLocation.setCounties(counties);

        return providerLocation;
    }

    public Page<ProviderLocation> createSearchResultsReport(
            String providerName,
            List<Long> categoryIds,
            List<Long> serviceIds,
            List<Long> insuranceIds,
            List<Long> ageGroupIds,
            List<Long> deliveryIds,
            List<Long> countyIds,
            List<Long> regionIds,
            String keyword
    ) {
        Pageable unpaged = Pageable.unpaged();

        return providerLocationRepository.queryProviderLocationByConditions(
                providerName,
                categoryIds,
                serviceIds,
                insuranceIds,
                ageGroupIds,
                deliveryIds,
                countyIds,
                regionIds,
                keyword,
                unpaged
        );
    }

    public Page<ProviderLocation> queryProviderLocationPageByConditions(
            String providerName,
            List<Long> categoryIds,
            List<Long> serviceIds,
            List<Long> insuranceIds,
            List<Long> ageGroupIds,
            List<Long> deliveryIds,
            List<Long> countyIds,
            List<Long> regionIds,
            String keyword,
            int currentPage,
            int pageSize
    ) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);

        return providerLocationRepository.queryProviderLocationByConditions(
                providerName,
                categoryIds,
                serviceIds,
                insuranceIds,
                ageGroupIds,
                deliveryIds,
                countyIds,
                regionIds,
                keyword,
                pageable
        );
    }

    public Page<ProviderLocation> internalQueryAllProviderLocationPageByConditions(
            String providerName,
            List<Long> categoryIds,
            List<Long> serviceIds,
            List<Long> insuranceIds,
            List<Long> ageGroupIds,
            List<Long> deliveryIds,
            List<Long> countyIds,
            List<Long> regionIds,
            String keyword
    ) {
        Pageable pageable = Pageable.unpaged();

        return providerLocationRepository.internalQueryProviderLocationByConditions(
                providerName,
                categoryIds,
                serviceIds,
                insuranceIds,
                ageGroupIds,
                deliveryIds,
                countyIds,
                regionIds,
                keyword,
                pageable
        );
    }

    public Page<ProviderLocation> internalQueryStandaloneProviderLocationPageByConditions(
            String providerName,
            List<Long> categoryIds,
            List<Long> serviceIds,
            List<Long> insuranceIds,
            List<Long> ageGroupIds,
            List<Long> deliveryIds,
            List<Long> countyIds,
            List<Long> regionIds,
            String keyword
    ) {
        Pageable pageable = Pageable.unpaged();

        return providerLocationRepository.internalQueryStandaloneProviderLocationByConditions(
                providerName,
                categoryIds,
                serviceIds,
                insuranceIds,
                ageGroupIds,
                deliveryIds,
                countyIds,
                regionIds,
                keyword,
                pageable
        );
    }

    public Page<ProviderLocation> internalQueryProviderLocationPageByConditions(
            String providerName,
            List<Long> categoryIds,
            List<Long> serviceIds,
            List<Long> insuranceIds,
            List<Long> ageGroupIds,
            List<Long> deliveryIds,
            List<Long> countyIds,
            List<Long> regionIds,
            String keyword,
            int currentPage,
            int pageSize
    ) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);

        return providerLocationRepository.internalQueryProviderLocationByConditions(
                providerName,
                categoryIds,
                serviceIds,
                insuranceIds,
                ageGroupIds,
                deliveryIds,
                countyIds,
                regionIds,
                keyword,
                pageable
        );
    }

    public PublicProvidersPageDto toPublicProviderInfoDtos(Page<ProviderLocation> providerLocationPage) {
        List<PublicProviderInfoDto> publicProviderInfoDtos = providerLocationPage.get().map(providerLocation -> {
            ProviderInfo providerInfo = providerLocation.getProviderInfo();

            String serviceAddressStr = createAddress(providerLocation.getApprovedServiceAddress());

            String emailStr = "";
            Optional<Email> informationalEmail = providerLocation.getCurrentEmails().get()
                    .filter(email -> email.getContactType().getType().equals("Informational"))
                    .findFirst();
            if (informationalEmail.isPresent()) {
                emailStr = informationalEmail.get().getEmail();
            }

            String contactNumberStr = "";
            Optional<ContactNumber> informationalContactNumber = providerLocation.getCurrentContactNumbers().get()
                    .filter(contactNumber -> contactNumber.getContactType().getType().equals("Informational"))
                    .findFirst();
            if (informationalContactNumber.isPresent()) {
                contactNumberStr = informationalContactNumber.get().getNumber();
            }

            return PublicProviderInfoDto.builder()
                    .providerInfoId(providerInfo.getProviderInfoId())
                    .logo(providerInfo.getLogo())
                    .address(serviceAddressStr)
                    .website(providerInfo.getWebsite())
                    .providerName(providerInfo.getProviderName())
                    .facebook(providerInfo.getFacebook())
                    .twitter(providerInfo.getTwitter())
                    .linkedin(providerInfo.getLinkedin())
                    .instagram(providerInfo.getInstagram())
                    .youtube(providerInfo.getYoutube())
                    .email(emailStr)
                    .contactNumber(contactNumberStr)
                    .suitableLocationId(providerLocation.getProviderLocationId())
                    .locationName(providerLocation.getLocation())
                    .build();
        }).collect(Collectors.toList());

        return PublicProvidersPageDto.builder()
                .publicProviderInfoDtos(publicProviderInfoDtos)
                .totalElements(providerLocationPage.getTotalElements())
                .totalPages(providerLocationPage.getTotalPages())
                .build();
    }

    public InternalLocationDto getInternalLocationDtoById(Long providerLocationId) {
        ProviderLocation providerLocation = findById(providerLocationId);

        if (!providerLocation.isCreateApproved()) {
            return null;
        }

        List<String> informationalEmails = providerLocation.listCurrentEmailsByType("Informational");
        List<String> additionalEmails = providerLocation.listCurrentEmailsByType("Other - Fax Number/Additional Email");
        List<String> informationalContactNumbers = providerLocation.listCurrentContactNumbersByType("Informational");
        List<String> faxNumbers = providerLocation.listCurrentContactNumbersByType("Other - Fax Number/Additional Email");

        String address = createAddress(providerLocation.getApprovedServiceAddress());

        // internal
        List<String> adminEmails = providerLocation.listCurrentEmailsByType("Administrative");
        List<String> mktEmails = providerLocation.listCurrentEmailsByType("Marketing");
        List<String> adminContactNumbers = providerLocation.listCurrentContactNumbersByType("Administrative");
        List<String> mktContactNumbers = providerLocation.listCurrentContactNumbersByType("Marketing");

        String mailingAddress = createAddress(providerLocation.getApprovedMailingAddress());

        // provider location
        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        List<String> files = providerInfo.getAllFileNames();

        List<OtherLocationDto> otherLocations = providerInfo.getProviderLocations().stream()
                .filter(pl -> pl.getProviderLocationId() != providerLocationId && pl.isCreateApproved())
                .map(pl ->
                        OtherLocationDto.builder()
                                .locationId(pl.getProviderLocationId())
                                .locationName(pl.getLocation())
                                .build())
                .collect(Collectors.toList());

        List<LocationServiceDto> locationServiceDtos = toGroupedServices(providerLocation.getCurrentLocationSrvcLinks().get(), true);

        return InternalLocationDto.builder()
                .providerInfoId(providerInfo.getProviderInfoId())
                .providerName(providerInfo.getProviderName())
                .otherLocations(otherLocations)
                .website(providerInfo.getWebsite())
                .locationServiceDtos(locationServiceDtos)
                .logo(providerInfo.getLogo())
                .twitter(providerInfo.getTwitter())
                .linkedin(providerInfo.getLinkedin())
                .facebook(providerInfo.getFacebook())
                .instagram(providerInfo.getInstagram())
                .youtube(providerInfo.getYoutube())
                .description(providerInfo.getDescription())
                .emails(informationalEmails)
                .additionalEmails(additionalEmails)
                .contactNumbers(informationalContactNumbers)
                .faxNumbers(faxNumbers)
                .address(address)
                .locationName(providerLocation.getLocation())
                .files(files)
                .internalMailingAddress(mailingAddress)
                .internalAdminEmails(adminEmails)
                .internalMktEmails(mktEmails)
                .internalAdminContactNumbers(adminContactNumbers)
                .internalMktContactNumbers(mktContactNumbers)
                .build();
    }

    public PublicLocationDto getPublicLocationDtoById(Long providerLocationId) {
        ProviderLocation providerLocation = findById(providerLocationId);

        if (providerLocation.isInternal() || !providerLocation.isCreateApproved()) {
            return null;
        }

        List<String> informationalEmails = providerLocation.listCurrentEmailsByType("Informational");
        List<String> additionalEmails = providerLocation.listCurrentEmailsByType("Other - Fax Number/Additional Email");
        List<String> informationalContactNumbers = providerLocation.listCurrentContactNumbersByType("Informational");
        List<String> faxNumbers = providerLocation.listCurrentContactNumbersByType("Other - Fax Number/Additional Email");
        String address = createAddress(providerLocation.getApprovedServiceAddress());

        // provider location
        ProviderInfo providerInfo = providerLocation.getProviderInfo();
        List<String> files = providerInfo.getAllFileNames();

        List<OtherLocationDto> otherLocations = providerInfo.getProviderLocations().stream()
                .filter(pl -> pl.getProviderLocationId() != providerLocationId && !pl.isInternal() && pl.isCreateApproved())
                .map(pl ->
                        OtherLocationDto.builder()
                                .locationId(pl.getProviderLocationId())
                                .locationName(pl.getLocation())
                                .build())
                .collect(Collectors.toList());

        List<LocationServiceDto> locationServiceDtos = toGroupedServices(providerLocation.getCurrentLocationSrvcLinks().get(), false);

        return PublicLocationDto.builder()
                .providerInfoId(providerInfo.getProviderInfoId())
                .providerName(providerInfo.getProviderName())
                .otherLocations(otherLocations)
                .website(providerInfo.getWebsite())
                .locationServiceDtos(locationServiceDtos)
                .logo(providerInfo.getLogo())
                .twitter(providerInfo.getTwitter())
                .linkedin(providerInfo.getLinkedin())
                .facebook(providerInfo.getFacebook())
                .instagram(providerInfo.getInstagram())
                .youtube(providerInfo.getYoutube())
                .description(providerInfo.getDescription())
                .emails(informationalEmails)
                .additionalEmails(additionalEmails)
                .contactNumbers(informationalContactNumbers)
                .faxNumbers(faxNumbers)
                .address(address)
                .locationName(providerLocation.getLocation())
                .files(files)
                .build();
    }

    public List<EmailDto> convertEmailsFromLocation(ProviderLocation providerLocation) {
        return providerLocation.getEmails().stream().map(email -> EmailDto.builder()
                .emailId(email.getEmailId())
                .email(email.getEmail())
                .type(email.getContactType().getType())
                .build())
                .collect(Collectors.toList());
    }

    public List<ContactNumberDto> convertContactNumbersFromLocation(ProviderLocation providerLocation) {
        return providerLocation.getContactNumbers().stream().map(contactNumber -> ContactNumberDto.builder()
                .contactNumberId(contactNumber.getContactNumberId())
                .number(contactNumber.getNumber())
                .type(contactNumber.getContactType().getType())
                .build())
                .collect(Collectors.toList());
    }

    public List<MichiganCountyOptionDto> convertCountiesFromLocation(ProviderLocation providerLocation) {
        return providerLocation.getCounties().stream().map(county ->
                MichiganCountyOptionDto.builder()
                        .county(county.getCounty())
                        .countyId(county.getCountyId())
                        .build()
        ).collect(Collectors.toList());
    }

    @Transactional
    public List<UnapprovedLocationOverviewDto> getAllUnapprovedLocations() {
        return providerLocationRepository.findAllByCreateApprovedIsFalseAndProviderInfo_CreateApprovedIsTrue()
                .map(pl -> {
                            ProviderInfo providerInfo = pl.getProviderInfo();
                            return UnapprovedLocationOverviewDto.builder()
                                    .providerName(providerInfo.getProviderName())
                                    .createdAt(providerInfo.getCreatedAt())
                                    .editedBy(providerInfo.getLastEditUsername())
                                    .locationName(pl.getLocation())
                                    .locationId(pl.getProviderLocationId())
                                    .build();
                        }
                )
                .collect(Collectors.toList());
    }

    public void approveCreatedLocation(Long locationId) {
        ProviderLocation providerLocation = findById(locationId);

        providerLocation.setCreateApproved(true);
        providerLocation.approveAllCreatedEmails();
        providerLocation.approveAllCreatedContactNumbers();
        providerLocation.approveAllCreatedAddresses();
        providerLocation.approveAllCreatedLocationSrvcLinks();

        save(providerLocation);
    }

    public UnapprovedLocationDto getUnapprovedLocationDtoById(Long locationId) {
        ProviderLocation providerLocation = findById(locationId);
        List<LocationServiceDto> locationServiceDtos = toGroupedServices(providerLocation.getCurrentLocationSrvcLinks().get(), true);
        List<EmailDto> emailDtos = convertEmailsFromLocation(providerLocation);
        List<ContactNumberDto> contactNumberDtos = convertContactNumbersFromLocation(providerLocation);
        List<MichiganCountyOptionDto> michiganCountyOptionDtos = convertCountiesFromLocation(providerLocation);

        String serviceAddress = createAddress(providerLocation.getServiceAddress());
        String mailingAddress = createAddress(providerLocation.getMailingAddress());
        ProviderInfo providerInfo = providerLocation.getProviderInfo();

        return UnapprovedLocationDto.builder()
                .providerInfoId(providerInfo.getProviderInfoId())
                .providerName(providerInfo.getProviderName())
                .locationId(providerLocation.getProviderLocationId())
                .locationName(providerLocation.getLocation())
                .serviceCounties(michiganCountyOptionDtos)
                .locationServiceDtos(locationServiceDtos)
                .emailDtos(emailDtos)
                .contactNumberDtos(contactNumberDtos)
                .serviceAddress(serviceAddress)
                .mailingAddress(mailingAddress)
                .build();
    }

    public String createAddress(Address address) {
        if (address == null) {
            return "";
        }

        if (!address.isValid()) {
            return "";
        }

        return address.getStreet() +
                ", " + address.getCity() +
                ", " + address.getState() +
                " " + address.getZip();
    }

    public boolean isLocationModified(Long providerLocationId) {
        return providerLocationRepository.findModifiedProviderLocationById(providerLocationId).isPresent();
    }

    public List<LocationServiceDto> toGroupedServices(Stream<LocationSrvcLink> locationSrvcLinksArg, boolean showInternal) {
        Map<String, List<LocationSrvcLink>> groupedServicesMap = showInternal ?
                locationSrvcLinksArg.collect(Collectors.groupingBy(LocationSrvcLink::getIdentifier)) :
                locationSrvcLinksArg.filter(lsl -> !lsl.isInternal() && !lsl.getService().isInternal())
                        .collect(Collectors.groupingBy(LocationSrvcLink::getIdentifier));

        List<LocationServiceDto> locationServiceDtos = new ArrayList<>();
        for (List<LocationSrvcLink> locationSrvcLinks : groupedServicesMap.values()) {
            Set<String> insurances = locationSrvcLinks.stream().map(locationSrvcLink -> locationSrvcLink.getInsurance().getInsuranceName()).collect(Collectors.toSet());
            Set<String> deliveries = locationSrvcLinks.stream().map(locationSrvcLink -> locationSrvcLink.getServiceDelivery().getDelivery()).collect(Collectors.toSet());
            Set<LocationAgeGroupDto> ageGroups = locationSrvcLinks.stream()
                    .map(locationSrvcLink -> LocationAgeGroupDto.builder()
                            .ageStart(locationSrvcLink.getServiceAgeGroup().getAgeStart())
                            .ageEnd(locationSrvcLink.getServiceAgeGroup().getAgeEnd())
                            .build())
                    .collect(Collectors.toSet());

            LocationServiceDto locationServiceDto = LocationServiceDto.builder()
                    .identifier(locationSrvcLinks.get(0).getIdentifier())
                    .category(locationSrvcLinks.get(0).getService().getServiceCategory().getCategory())
                    .serviceName(locationSrvcLinks.get(0).getService().getService())
                    .insurances(new ArrayList<>(insurances))
                    .deliveries(new ArrayList<>(deliveries))
                    .locationAgeGroupDtos(new ArrayList<>(ageGroups))
                    .serviceDescription(locationSrvcLinks.get(0).getService().getDescription())
                    .categoryDescription(locationSrvcLinks.get(0).getService().getServiceCategory().getDescription())
                    .internal(locationSrvcLinks.get(0).isInternal())
                    .build();

            locationServiceDtos.add(locationServiceDto);
        }

        locationServiceDtos.sort(Comparator.comparing(LocationServiceDto::getCategory)
                .thenComparing(LocationServiceDto::getServiceName));

        return locationServiceDtos;
    }
}