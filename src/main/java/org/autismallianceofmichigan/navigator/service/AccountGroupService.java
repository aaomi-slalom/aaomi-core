package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.AccountGroupDto;
import org.autismallianceofmichigan.navigator.dto.messageDto.ExternalMessageAccountGroupsDto;
import org.autismallianceofmichigan.navigator.exception.AccountGroupNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.AccountGroup;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountGroupRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class AccountGroupService {
    private final AccountGroupRepository accountGroupRepository;

    public boolean hasAccountGroup(String accountGroup) {
        return accountGroupRepository.findByAccountGroup(accountGroup).isPresent();
    }

    public AccountGroup saveAccountGroup(AccountGroup accountGroup) {
        return accountGroupRepository.save(accountGroup);
    }

    public AccountGroup findByAccountGroup(String accountGroup) throws AccountGroupNotFoundException {
        Optional<AccountGroup> accountGroupOptional = accountGroupRepository.findByAccountGroup(accountGroup);

        return accountGroupOptional.orElseThrow(() -> new AccountGroupNotFoundException("AccountGroup not found"));
    }

    @Transactional
    public Stream<AccountGroup> streamAllAccountGroups() {
        return accountGroupRepository.streamAll();
    }

    @Transactional
    public List<AccountGroupDto> getAllAccountGroupDtos() {
        return streamAllAccountGroups().map(accountGroup ->
                AccountGroupDto.builder()
                        .accountGroup(accountGroup.getAccountGroup())
                        .userCount(accountGroup.getAccounts().size())
                        .build()
        ).collect(Collectors.toList());
    }

    @Transactional
    public List<ExternalMessageAccountGroupsDto> getAccountGroupDtosExternalUsers() {
        List<AccountGroup> accountGroupDtosExternal = new ArrayList<>();
        AccountGroup accountGroupAdmin = findByAccountGroup("GROUP_ADMIN");
        AccountGroup accountGroupNav = findByAccountGroup("GROUP_NAVIGATOR");
        accountGroupDtosExternal.add(accountGroupAdmin);
        accountGroupDtosExternal.add(accountGroupNav);

        return accountGroupDtosExternal.stream().map(accountGroup ->
                ExternalMessageAccountGroupsDto.builder()
                        .accountGroupId(accountGroup.getAccountGroupId())
                        .accountGroup(accountGroup.getAccountGroup())
                        .userCount(accountGroup.getAccounts().size())
                        .build()
        ).collect(Collectors.toList());
    }
    @Transactional
    public List<String> getAllAccountGroupNames() {
        return streamAllAccountGroups()
                .map(AccountGroup::getAccountGroup)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteAccountGroupByAccountGroup(String accountGroup) {
        accountGroupRepository.deleteAccountGroupByAccountGroup(accountGroup);
    }
}
