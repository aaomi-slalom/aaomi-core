package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceDeliveryCreateUpdateRequest;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceDeliveryOptionDto;
import org.autismallianceofmichigan.navigator.exception.ServiceDeliveryNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceDelivery;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceDeliveryRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ServiceDeliveryService {
    private final ServiceDeliveryRepository serviceDeliveryRepository;

    @Transactional
    public List<ServiceDeliveryOptionDto> getAllServiceDeliveryOptionDtos() {
        return serviceDeliveryRepository.streamAll().map(serviceDelivery -> {
            int serviceCount = serviceDeliveryRepository.countGroupedServicesByServiceDeliveryId(serviceDelivery.getServiceDeliveryId());
            return ServiceDeliveryOptionDto.builder()
                    .serviceDeliveryId(serviceDelivery.getServiceDeliveryId())
                    .delivery(serviceDelivery.getDelivery())
                    .serviceCount(serviceCount)
                    .build();
        }).collect(Collectors.toList());
    }

    public ServiceDelivery findById(Long serviceDeliveryId) {
        Optional<ServiceDelivery> serviceDeliveryOptional = serviceDeliveryRepository.findByServiceDeliveryId(serviceDeliveryId);
        return serviceDeliveryOptional.orElseThrow(() -> new ServiceDeliveryNotFoundException("ServiceDelivery not found"));
    }

    public boolean hasServiceDelivery(String serviceDelivery) {
        return serviceDeliveryRepository.findByDelivery(serviceDelivery).isPresent();
    }

    public void save(ServiceDelivery serviceDelivery) {
        serviceDeliveryRepository.save(serviceDelivery);
    }

    public void deleteById(Long serviceDeliveryId) {
        ServiceDelivery currServiceDelivery = findById(serviceDeliveryId);
        if(currServiceDelivery.getLocationSrvcLinks().size() == 0) {
            serviceDeliveryRepository.deleteById(serviceDeliveryId);
        }
    }

    public void updateServiceDelivery(Long serviceDeliveryId, ServiceDeliveryCreateUpdateRequest serviceDeliveryCreateUpdateRequest) {
        ServiceDelivery serviceDelivery = findById(serviceDeliveryId);
        serviceDelivery.setDelivery(serviceDeliveryCreateUpdateRequest.getDelivery());

        serviceDeliveryRepository.save(serviceDelivery);
    }
}
