package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.exception.WaitTimeNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.WaitTime;
import org.autismallianceofmichigan.navigator.persistence.repository.WaitTimeRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class WaitTimeService {
    private final WaitTimeRepository waitTimeRepository;

    @Transactional
    public void deleteWaitTimeById(Long waitTimeId) {
        waitTimeRepository.forceDeleteWaitTimesByWaitTimeId(waitTimeId);
    }

    public WaitTime findById(Long waitTimeId) {
        Optional<WaitTime> waitTime = waitTimeRepository.findById(waitTimeId);
        return waitTime.orElseThrow(() -> new WaitTimeNotFoundException("Wait time not found"));
    }

    public void save(WaitTime waitTime) {
        waitTimeRepository.save(waitTime);
    }
}
