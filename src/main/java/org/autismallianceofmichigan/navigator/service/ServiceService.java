package org.autismallianceofmichigan.navigator.service;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceOptionDto;
import org.autismallianceofmichigan.navigator.exception.ServiceNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceDto;

@Service
@AllArgsConstructor
@Transactional
public class ServiceService {
    private final ServiceRepository serviceRepository;

    public org.autismallianceofmichigan.navigator.persistence.entity.Service findByServiceId(Long serviceId) {
        Optional<org.autismallianceofmichigan.navigator.persistence.entity.Service> serviceOptional = serviceRepository.findById(serviceId);
        return serviceOptional.orElseThrow(() -> new ServiceNotFoundException("Service not found"));
    }

    public boolean hasServiceWithName(String service) {
        return serviceRepository.findByService(service).isPresent();
    }

    public void saveService(org.autismallianceofmichigan.navigator.persistence.entity.Service service) {
        serviceRepository.save(service);
    }

    public void deleteByServiceId(Long serviceId) {
        serviceRepository.deleteById(serviceId);
    }

    
    public List<ServiceOptionDto> getAllServiceOptionDtos() {
        return serviceRepository.streamAll().map(service ->
                ServiceOptionDto.builder()
                        .serviceId(service.getServiceId())
                        .service(service.getService())
                        .category(service.getServiceCategory().getCategory())
                        .build()
        ).collect(Collectors.toList());
    }

    
    public List<ServiceOptionDto> getAllPublicServiceOptionDtos() {
        return serviceRepository.streamAllByInternalFalse().map(service ->
                ServiceOptionDto.builder()
                        .serviceId(service.getServiceId())
                        .service(service.getService())
                        .category(service.getServiceCategory().getCategory())
                        .build()
        ).collect(Collectors.toList());
    }

    
    public Stream<org.autismallianceofmichigan.navigator.persistence.entity.Service> streamAllByServiceCategoryIdAlphabetically(Long serviceCategoryId) {
        return serviceRepository.streamAllByServiceCategory_ServiceCategoryIdOrderByService(serviceCategoryId);
    }
    
    public List<ServiceDto> displayServicesFromCategoryId(Long serviceCategoryId) {
        List<ServiceDto> serviceDto =new ArrayList<>();
        serviceRepository.displayServicesFromCategoryId(serviceCategoryId).forEach((displayServices) -> {
            serviceDto.add(new ServiceDto(Long.parseLong(displayServices[0].toString()),displayServices[1].toString(),displayServices[2].toString(),displayServices[3].equals("1"),Integer.parseInt(displayServices[4].toString()) > 0));
        });
        return serviceDto;
    }
    
    public List<ServiceDto> displayPublicServicesFromCategoryId(Long serviceCategoryId) {
        List<ServiceDto> serviceDto =new ArrayList<>();
        serviceRepository.displayServicesFromCategoryId(serviceCategoryId).forEach((displayServices) -> {
        	if(!displayServices[3].equals("1")) {
        		serviceDto.add(new ServiceDto(Long.parseLong(displayServices[0].toString()),displayServices[1].toString(),displayServices[2].toString(),displayServices[3].equals("1"),Integer.parseInt(displayServices[4].toString()) > 0));
        	}
            
        });
        return serviceDto;
    }
}
