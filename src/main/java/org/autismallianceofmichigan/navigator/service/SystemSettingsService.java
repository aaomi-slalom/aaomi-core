package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.systemSettingDto.SystemSettingDto;
import org.autismallianceofmichigan.navigator.exception.SystemSettingNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.SystemSetting;
import org.autismallianceofmichigan.navigator.persistence.repository.SystemSettingsRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class SystemSettingsService {
    private final SystemSettingsRepository systemSettingsRepository;

    public boolean hasSystemSetting() {
        return systemSettingsRepository.existsById(1L);
    }

    public SystemSetting getSystemSetting() {
        Optional<SystemSetting> systemSetting = systemSettingsRepository.findById(1L);
        return systemSetting.orElseThrow(() -> new SystemSettingNotFoundException("SystemSetting not found"));
    }

    public void save(SystemSetting systemSetting) {
        systemSettingsRepository.save(systemSetting);
    }

    public SystemSettingDto getSystemSettingsDto() {
        SystemSetting systemSetting = getSystemSetting();

        return SystemSettingDto.builder()
                .backgroundImage(systemSetting.getBackgroundImage())
                .searchSupportNote(systemSetting.getSearchSupportNote())
                .technicalSupportNote(systemSetting.getTechnicalSupportNote())
                .disclaimer(systemSetting.getDisclaimer())
                .build();
    }
}
