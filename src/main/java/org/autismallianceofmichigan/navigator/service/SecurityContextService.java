package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SecurityContextService {
    private final AccountService accountService;

    public String getCurrentAccountEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Account authAccount = (Account) authentication.getPrincipal();
            return authAccount.getEmail();
        }

        return null;
    }

    public boolean hasAuthority(String authority) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority));
    }

    public boolean hasAuthorities(List<String> authorities) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<String> allAuthorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return allAuthorities.containsAll(authorities);
    }

    public Account findCurrAccount() {
        return accountService.findByEmail(getCurrentAccountEmail());
    }
}
