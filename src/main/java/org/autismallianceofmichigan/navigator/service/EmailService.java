package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderLocationRequest;
import org.autismallianceofmichigan.navigator.exception.EmailNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.autismallianceofmichigan.navigator.persistence.entity.Email;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderLocation;
import org.autismallianceofmichigan.navigator.persistence.repository.EmailRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmailService {
    private final EmailRepository emailRepository;
    private final ContactTypeService contactTypeService;

    public Email findById(Long emailId) {
        Optional<Email> emailOptional = emailRepository.findById(emailId);
        return emailOptional.orElseThrow(() -> new EmailNotFoundException("Email not found"));
    }

    public boolean hasEmailWithId(Long emailId) {
        return emailRepository.existsById(emailId);
    }

    public void deleteById(Long emailId) {
        emailRepository.deleteById(emailId);
    }

    public void save(Email email) {
        emailRepository.save(email);
    }

    public void requestToDelete(Email email) {
        if (!email.isCreateApproved()) {
            deleteById(email.getEmailId());
        } else {
            email.setDeleteRequested(true);
            save(email);
        }
    }

    public List<Email> createEmails(ProviderLocation providerLocation, CreateProviderLocationRequest createProviderLocationRequest, boolean createApproved) {
        return createProviderLocationRequest.getEmails().stream().map(createEmailRequest -> {
                    ContactType contactType = contactTypeService.findByType(createEmailRequest.getContactType());

                    return Email.builder()
                            .email(createEmailRequest.getEmail())
                            .contactType(contactType)
                            .providerLocation(providerLocation)
                            .createApproved(createApproved)
                            .build();
                }
        ).collect(Collectors.toList());
    }
}
