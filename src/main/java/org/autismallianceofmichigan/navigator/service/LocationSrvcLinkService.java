package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateLocationSrvcLinkRequest;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderLocationRequest;
import org.autismallianceofmichigan.navigator.exception.LocationSrvcLinkNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.persistence.repository.LocationSrvcLinkRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class LocationSrvcLinkService {
    private final LocationSrvcLinkRepository locationSrvcLinkRepository;
    private final ServiceService serviceService;
    private final ServiceDeliveryService serviceDeliveryService;
    private final ServiceAgeGroupService serviceAgeGroupService;
    private final InsuranceService insuranceService;

    public boolean hasLocationSrvcLinkByIdentifier(String identifier) {
        return locationSrvcLinkRepository.existsByIdentifier(identifier);
    }

    @Transactional
    public void deleteAllByIdentifier(String identifier) {
        locationSrvcLinkRepository.deleteAllByIdentifier(identifier);
    }

    @Transactional
    public void requestToDeleteAllByIdentifier(String identifier) {
        locationSrvcLinkRepository.streamAllByIdentifier(identifier).forEach(locationSrvcLink -> {
            if (locationSrvcLink.isCreateApproved()) {
                locationSrvcLink.setDeleteRequested(true);
                save(locationSrvcLink);
            } else {
                locationSrvcLinkRepository.deleteById(locationSrvcLink.getLocationSrvcLinkId());
            }
        });
    }

    @Transactional
    public void disapproveDeleteAllByIdentifier(String identifier) {
        locationSrvcLinkRepository.streamAllByIdentifier(identifier).forEach(locationSrvcLink -> {
            locationSrvcLink.setDeleteRequested(false);
            save(locationSrvcLink);
        });
    }

    @Transactional
    public void approveAddedGroupedLocationServiceByIdentifier(String identifier) {
        locationSrvcLinkRepository.streamAllByIdentifier(identifier).forEach(locationSrvcLink -> {
            locationSrvcLink.setCreateApproved(true);
            save(locationSrvcLink);
        });
    }

    public LocationSrvcLink findByIdentifier(String identifier) {
        List<LocationSrvcLink> locationSrvcLinks = locationSrvcLinkRepository.findByIdentifier(identifier);

        if (locationSrvcLinks.size() == 0) {
            throw new LocationSrvcLinkNotFoundException("LocationSrvcLink not found");
        }

        return locationSrvcLinks.get(0);
    }

    public List<LocationSrvcLink> findAllByIdentifier(String identifier) {
        return locationSrvcLinkRepository.findByIdentifier(identifier);
    }

    @Transactional
    public void updateGroupedServicesInternalStatusByIdentifier(String identifier, boolean internal) {
        locationSrvcLinkRepository.streamAllByIdentifier(identifier).forEach(lsl -> {
            lsl.setInternal(internal);
            save(lsl);
        });
    }

    public LocationSrvcLink save(LocationSrvcLink locationSrvcLink) {
        return locationSrvcLinkRepository.save(locationSrvcLink);
    }

    public List<LocationSrvcLink> createLocationSrvcLinks(
            ProviderLocation providerLocation,
            CreateProviderLocationRequest createProviderLocationRequest,
            boolean approved
    ) {
        return createProviderLocationRequest
                .getLocationSrvcLinks()
                .stream()
                .map(createLocationSrvcLinkRequest ->
                        generateLocationSrvcLinkFromCreateLocationSrvcLinkRequest(providerLocation, createLocationSrvcLinkRequest, approved)
                )
                .collect(Collectors.toList());
    }

    public List<LocationSrvcLink> addLocationSrvcLinks(
            ProviderLocation providerLocation,
            List<CreateLocationSrvcLinkRequest> createLocationSrvcLinkRequests,
            boolean approved
    ) {
        return createLocationSrvcLinkRequests
                .stream()
                .map(createLocationSrvcLinkRequest ->
                        generateLocationSrvcLinkFromCreateLocationSrvcLinkRequest(providerLocation, createLocationSrvcLinkRequest, approved)
                )
                .collect(Collectors.toList());
    }

    private LocationSrvcLink generateLocationSrvcLinkFromCreateLocationSrvcLinkRequest(
            ProviderLocation providerLocation,
            CreateLocationSrvcLinkRequest createLocationSrvcLinkRequest,
            boolean createApproved
    ) {
        org.autismallianceofmichigan.navigator.persistence.entity.Service service = serviceService.findByServiceId(createLocationSrvcLinkRequest.getServiceId());
        ServiceDelivery serviceDelivery = serviceDeliveryService.findById(createLocationSrvcLinkRequest.getServiceDeliveryId());
        ServiceAgeGroup serviceAgeGroup = serviceAgeGroupService.findById(createLocationSrvcLinkRequest.getAgeGroupId());
        Insurance insurance = insuranceService.findByInsuranceId(createLocationSrvcLinkRequest.getInsuranceId());

        return LocationSrvcLink.builder()
                .internal(createLocationSrvcLinkRequest.isInternal())
                .service(service)
                .insurance(insurance)
                .serviceAgeGroup(serviceAgeGroup)
                .serviceDelivery(serviceDelivery)
                .providerLocation(providerLocation)
                .identifier(createLocationSrvcLinkRequest.getIdentifier())
                .active(true)
                .createApproved(createApproved)
                .build();
    }

    @Transactional
    Stream<LocationSrvcLink> streamAllGroupedServicesByRegionIdAndCategoryId(Long regionId, Long categoryId) {
        return locationSrvcLinkRepository.streamAllGroupedServicesByRegionIdAndCategoryId(regionId, categoryId);
    }

    int countProvidersByRegionIdAndCategoryIdAndServiceId(Long regionId, Long categoryId, Long serviceId) {
        return locationSrvcLinkRepository.countProvidersByRegionIdAndCategoryIdAndServiceId(regionId, categoryId, serviceId);
    }
}
