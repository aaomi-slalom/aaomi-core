package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceAgeGroupDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceAgeGroupOptionDto;
import org.autismallianceofmichigan.navigator.exception.ServiceAgeGroupNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceAgeGroup;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceAgeGroupRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ServiceAgeGroupService {
    private final ServiceAgeGroupRepository serviceAgeGroupRepository;

    @Transactional
    public List<ServiceAgeGroupDto> getAllServiceAgeGroupDtos() {
        return serviceAgeGroupRepository.streamAll().map(serviceAgeGroup -> {
            int serviceCount = serviceAgeGroupRepository.countGroupedServicesByAgeGroupId(serviceAgeGroup.getServiceAgeGroupId());

            return ServiceAgeGroupDto.builder()
                    .serviceAgeGroupId(serviceAgeGroup.getServiceAgeGroupId())
                    .ageStart(serviceAgeGroup.getAgeStart())
                    .ageEnd(serviceAgeGroup.getAgeEnd())
                    .serviceCount(serviceCount)
                    .build();
        }).collect(Collectors.toList());
    }

    @Transactional
    public List<ServiceAgeGroupOptionDto> getAllServiceAgeGroupOptionDtos() {
        return serviceAgeGroupRepository.streamAll().map(serviceAgeGroup ->
                ServiceAgeGroupOptionDto.builder()
                        .serviceAgeGroupId(serviceAgeGroup.getServiceAgeGroupId())
                        .ageStart(serviceAgeGroup.getAgeStart())
                        .ageEnd(serviceAgeGroup.getAgeEnd())
                        .ageGroupDisplay(serviceAgeGroup.getAgeStart() + " - " + serviceAgeGroup.getAgeEnd())
                        .build()
        ).collect(Collectors.toList());
    }

    public void saveServiceAgeGroup(ServiceAgeGroup serviceAgeGroup) {
        serviceAgeGroupRepository.save(serviceAgeGroup);
    }

    public boolean hasAgeGroupWithAgeStartAndAgeEnd(int ageStart, int ageEnd) {
        return serviceAgeGroupRepository.findByAgeStartAndAgeEnd(ageStart, ageEnd).isPresent();
    }

    public ServiceAgeGroup findById(Long serviceAgeGroupId) {
        Optional<ServiceAgeGroup> serviceAgeGroupOptional = serviceAgeGroupRepository.findById(serviceAgeGroupId);
        return serviceAgeGroupOptional.orElseThrow(() -> new ServiceAgeGroupNotFoundException("ServiceAgeGroup not found"));
    }

    public void deleteById(Long serviceAgeGroupId) {
        serviceAgeGroupRepository.deleteById(serviceAgeGroupId);
    }
}
