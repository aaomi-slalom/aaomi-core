package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.RegionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.RegionOptionDto;
import org.autismallianceofmichigan.navigator.dto.regionDto.UpdateRegionRequest;
import org.autismallianceofmichigan.navigator.dto.reportDto.RegionCategoryProviderCountDto;
import org.autismallianceofmichigan.navigator.dto.reportDto.RegionCategoryServiceProviderCountDto;
import org.autismallianceofmichigan.navigator.dto.reportDto.RegionProviderCountDto;
import org.autismallianceofmichigan.navigator.exception.RegionNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.County;
import org.autismallianceofmichigan.navigator.persistence.entity.Region;
import org.autismallianceofmichigan.navigator.persistence.repository.RegionRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    private final CountyService countyService;
    private final ServiceCategoryService serviceCategoryService;
    private final LocationSrvcLinkService locationSrvcLinkService;

    public void saveRegion(Region region) {
        regionRepository.save(region);
    }

    public Region findByRegionId(Long regionId) {
        Optional<Region> region = regionRepository.findById(regionId);
        return region.orElseThrow(() -> new RegionNotFoundException("Region not found"));
    }

    public void updateExistingRegion(Long regionId, UpdateRegionRequest updateRegionRequest) {
        Region currRegion = findByRegionId(regionId);
        currRegion.setRegion(updateRegionRequest.getRegion());
        Set<County> counties = updateRegionRequest.getCountyDtos().stream().map(countyDto ->
                County.builder()
                        .county(countyService.findByCountyId(countyDto.getCountyId()).getCounty())
                        .countyId(countyDto.getCountyId())
                        .region(currRegion)
                        .build()).collect(Collectors.toSet());

        Set<County> existingCounties = currRegion.getCounties();
        for (County existingCounty :
                existingCounties) {
            if (!counties.contains(existingCounty)) {
                existingCounty.setRegion(null);
            }
        }
        currRegion.setCounties(counties);

        regionRepository.save(currRegion);
    }

    @Transactional
    public List<RegionDto> getAllRegionDtos() {
        return regionRepository.streamAll().map(region -> {
            List<MichiganCountyOptionDto> counties = countyService.streamAllCountiesByRegionId(region.getRegionId())
                    .map(county -> MichiganCountyOptionDto.builder()
                            .county(county.getCounty())
                            .countyId(county.getCountyId())
                            .numberOfServices(countyService.countServicesByCountyId(county.getCountyId()))
                            .build()
                    ).collect(Collectors.toList());

            return RegionDto.builder()
                    .regionId(region.getRegionId())
                    .region(region.getRegion())
                    .counties(counties)
                    .numberOfServices(countServicesByRegionId(region.getRegionId()))
                    .build();
        }).collect(Collectors.toList());
    }

    @Transactional
    public List<RegionOptionDto> getAllRegionOptionDtos() {
        return regionRepository.streamAll().map(region -> {
            List<MichiganCountyOptionDto> counties = countyService.streamAllCountiesByRegionId(region.getRegionId())
                    .map(county -> MichiganCountyOptionDto.builder()
                            .county(county.getCounty())
                            .countyId(county.getCountyId())
                            .build()
                    ).collect(Collectors.toList());

            return RegionOptionDto.builder()
                    .regionId(region.getRegionId())
                    .region(region.getRegion())
                    .build();
        }).collect(Collectors.toList());
    }

    public List<MichiganCountyOptionDto> getAllMichiganCountyOptionDtosByRegionId(Long regionId) {
        Region currRegion = findByRegionId(regionId);

        List<County> currentlySelectedCounties = new ArrayList<>(currRegion.getCounties());
        List<MichiganCountyOptionDto> countyOptions = currentlySelectedCounties.stream().map(county ->
                MichiganCountyOptionDto.builder()
                        .countyId(county.getCountyId())
                        .county(county.getCounty())
                        .build()
        ).collect(Collectors.toList());

        countyOptions.addAll(getAllUnassignedCountyDtos());

        return countyOptions;
    }

    public List<MichiganCountyOptionDto> getAllUnassignedCountyDtos() {
        Stream<County> countiesWithoutRegionStream = countyService.getAllCounties().stream()
                .filter(county -> county.getRegion() == null);

        return countiesWithoutRegionStream.map(county ->
                MichiganCountyOptionDto.builder()
                        .countyId(county.getCountyId())
                        .county(county.getCounty())
                        .build()
        ).collect(Collectors.toList());
    }

    public void deleteById(Long regionId) {
        Region currRegion = findByRegionId(regionId);
        currRegion.getCounties().forEach(county -> {
            county.setRegion(null);
            countyService.saveCounty(county);
        });
        currRegion.setCounties(new HashSet<>());
        regionRepository.save(currRegion);
        regionRepository.deleteById(regionId);
    }

    public int countServicesByRegionId(Long regionId) {
        return regionRepository.countGroupedServicesByRegionId(regionId);
    }

    @Transactional
    public List<RegionProviderCountDto> createRegionProviderCountDtos() {
        return regionRepository.streamAll().map(r -> {
            Long regionId = r.getRegionId();
            int providerCount = regionRepository.countProvidersByRegionId(regionId);

            List<RegionCategoryProviderCountDto> categoryProviderCounts = serviceCategoryService.streamAllCategoriesByRegionId(regionId).map(c -> {
                Long categoryId = c.getServiceCategoryId();
                int categoryProviderCount = serviceCategoryService.countProvidersByRegionIdAndCategoryId(regionId, categoryId);

                List<RegionCategoryServiceProviderCountDto> serviceProviderCounts = locationSrvcLinkService.streamAllGroupedServicesByRegionIdAndCategoryId(regionId, categoryId).map(lsl -> {
                    Long serviceId = lsl.getService().getServiceId();
                    int serviceProviderCount = locationSrvcLinkService.countProvidersByRegionIdAndCategoryIdAndServiceId(regionId, categoryId, serviceId);

                    return RegionCategoryServiceProviderCountDto.builder()
                            .serviceId(lsl.getService().getServiceId())
                            .providerCount(serviceProviderCount)
                            .service(lsl.getService().getService())
                            .build();
                }).distinct().collect(Collectors.toList());

                return RegionCategoryProviderCountDto.builder()
                        .categoryId(categoryId)
                        .providerCount(categoryProviderCount)
                        .category(c.getCategory())
                        .serviceProviderCounts(serviceProviderCounts)
                        .build();
            }).distinct().collect(Collectors.toList());

            return RegionProviderCountDto.builder()
                    .regionId(regionId)
                    .providerCount(providerCount)
                    .categoryProviderCounts(categoryProviderCounts)
                    .region(r.getRegion())
                    .build();
        }).distinct().collect(Collectors.toList());
    }
}
