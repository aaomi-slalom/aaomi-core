package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.persistence.repository.NoteRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NoteService {
    private final NoteRepository noteRepository;

    public boolean hasNoteWithNoteId(Long noteId) {
        return noteRepository.findById(noteId).isPresent();
    }

    public void deleteById(Long noteId) {
        noteRepository.deleteById(noteId);
    }
}
