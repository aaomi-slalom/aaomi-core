package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.autismallianceofmichigan.navigator.dto.providerDto.QueryProvidersRequest;
import org.autismallianceofmichigan.navigator.dto.reportDto.GenericReportDto;
import org.autismallianceofmichigan.navigator.dto.reportDto.RegionProviderCountDto;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderInfo;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderLocation;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@AllArgsConstructor
public class ReportService {
    private final RegionService regionService;
    private final ProviderLocationService providerLocationService;

    public GenericReportDto createStandaloneReport(QueryProvidersRequest queryProvidersRequest) {
        Page<ProviderLocation> providerLocationPage = providerLocationService.internalQueryStandaloneProviderLocationPageByConditions(
                queryProvidersRequest.getProviderName(),
                queryProvidersRequest.getCategoryIds(),
                queryProvidersRequest.getServiceIds(),
                queryProvidersRequest.getInsuranceIds(),
                queryProvidersRequest.getAgeGroupIds(),
                queryProvidersRequest.getDeliveryIds(),
                queryProvidersRequest.getCountyIds(),
                queryProvidersRequest.getRegionIds(),
                queryProvidersRequest.getKeyword()
        );

        String reportFile = null;
        String[] columns = {
                "Provider_Name",
                "Provider_Location_Id",
                "Provider_Location_Name",
                "Administrative_Contact_Numbers",
                "Informational_Contact_Numbers",
                "Marketing_Contact_Numbers",
                "Fax_Numbers",
                "Administrative_Emails",
                "Informational_Emails",
                "Marketing_Emails",
                "Additional_Emails",
                "Mailing_Address",
                "Service_Address",
                "Website",
                "Facebook",
                "LinkedIn",
                "Twitter",
                "Instagram",
                "YouTube",
                "Is_Provider_Location_Internal"
        };

        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("Standalone_Provider_Location_Information");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setFillForegroundColor(IndexedColors.TEAL.getIndex());

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            AtomicInteger rowNum = new AtomicInteger(1);

            providerLocationPage.get().forEach(pl -> {
                ProviderInfo providerInfo = pl.getProviderInfo();

                String adminNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Administrative"));
                String infoNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Informational"));
                String mktNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Marketing"));
                String faxNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Other - Fax Number/Additional Email"));

                String adminEmails = String.join(", ", pl.listCurrentEmailsByType("Administrative"));
                String infoEmails = String.join(", ", pl.listCurrentEmailsByType("Informational"));
                String mktEmails = String.join(", ", pl.listCurrentEmailsByType("Marketing"));
                String additionalEmails = String.join(", ", pl.listCurrentEmailsByType("Other - Fax Number/Additional Email"));

                String mailingAddress = providerLocationService.createAddress(pl.getApprovedMailingAddress());
                String serviceAddress = providerLocationService.createAddress(pl.getApprovedServiceAddress());

                Row row = sheet.createRow(rowNum.get());

                row.createCell(0).setCellValue(providerInfo.getProviderName());
                row.createCell(1).setCellValue(pl.getProviderLocationId());
                row.createCell(2).setCellValue(pl.getLocation());
                row.createCell(3).setCellValue(adminNumbers);
                row.createCell(4).setCellValue(infoNumbers);
                row.createCell(5).setCellValue(mktNumbers);
                row.createCell(6).setCellValue(faxNumbers);
                row.createCell(7).setCellValue(adminEmails);
                row.createCell(8).setCellValue(infoEmails);
                row.createCell(9).setCellValue(mktEmails);
                row.createCell(10).setCellValue(additionalEmails);
                row.createCell(11).setCellValue(mailingAddress);
                row.createCell(12).setCellValue(serviceAddress);
                row.createCell(13).setCellValue(providerInfo.getWebsite());
                row.createCell(14).setCellValue(providerInfo.getFacebook());
                row.createCell(15).setCellValue(providerInfo.getLinkedin());
                row.createCell(16).setCellValue(providerInfo.getTwitter());
                row.createCell(17).setCellValue(providerInfo.getInstagram());
                row.createCell(18).setCellValue(providerInfo.getYoutube());
                row.createCell(19).setCellValue(pl.isInternal());

                rowNum.addAndGet(1);
            });

            workbook.write(out);

            reportFile = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," +
                    Base64.getEncoder().encodeToString(out.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return GenericReportDto.builder()
                .reportFile(reportFile)
                .build();
    }

    public GenericReportDto createContactReport(QueryProvidersRequest queryProvidersRequest) {
        Page<ProviderLocation> providerLocationPage = providerLocationService.internalQueryAllProviderLocationPageByConditions(
                queryProvidersRequest.getProviderName(),
                queryProvidersRequest.getCategoryIds(),
                queryProvidersRequest.getServiceIds(),
                queryProvidersRequest.getInsuranceIds(),
                queryProvidersRequest.getAgeGroupIds(),
                queryProvidersRequest.getDeliveryIds(),
                queryProvidersRequest.getCountyIds(),
                queryProvidersRequest.getRegionIds(),
                queryProvidersRequest.getKeyword()
        );

        String reportFile = null;
        String[] columns = {
                "Provider_Name",
                "Provider_Location_Id",
                "Provider_Location_Name",
                "Administrative_Contact_Numbers",
                "Informational_Contact_Numbers",
                "Marketing_Contact_Numbers",
                "Fax_Numbers",
                "Administrative_Emails",
                "Informational_Emails",
                "Marketing_Emails",
                "Additional_Emails",
                "Mailing_Address",
                "Service_Address"
        };

        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("Provider_Location_Contact_Information");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setFillForegroundColor(IndexedColors.TEAL.getIndex());

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            AtomicInteger rowNum = new AtomicInteger(1);

            providerLocationPage.get().forEach(pl -> {
                String adminNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Administrative"));
                String infoNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Informational"));
                String mktNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Marketing"));
                String faxNumbers = String.join(", ", pl.listCurrentContactNumbersByType("Other - Fax Number/Additional Email"));

                String adminEmails = String.join(", ", pl.listCurrentEmailsByType("Administrative"));
                String infoEmails = String.join(", ", pl.listCurrentEmailsByType("Informational"));
                String mktEmails = String.join(", ", pl.listCurrentEmailsByType("Marketing"));
                String additionalEmails = String.join(", ", pl.listCurrentEmailsByType("Other - Fax Number/Additional Email"));

                String mailingAddress = providerLocationService.createAddress(pl.getApprovedMailingAddress());
                String serviceAddress = providerLocationService.createAddress(pl.getApprovedServiceAddress());

                Row row = sheet.createRow(rowNum.get());

                row.createCell(0).setCellValue(pl.getProviderInfo().getProviderName());
                row.createCell(1).setCellValue(pl.getProviderLocationId());
                row.createCell(2).setCellValue(pl.getLocation());
                row.createCell(3).setCellValue(adminNumbers);
                row.createCell(4).setCellValue(infoNumbers);
                row.createCell(5).setCellValue(mktNumbers);
                row.createCell(6).setCellValue(faxNumbers);
                row.createCell(7).setCellValue(adminEmails);
                row.createCell(8).setCellValue(infoEmails);
                row.createCell(9).setCellValue(mktEmails);
                row.createCell(10).setCellValue(additionalEmails);
                row.createCell(11).setCellValue(mailingAddress);
                row.createCell(12).setCellValue(serviceAddress);

                rowNum.addAndGet(1);
            });

            workbook.write(out);

            reportFile = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," +
                    Base64.getEncoder().encodeToString(out.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return GenericReportDto.builder()
                .reportFile(reportFile)
                .build();
    }

    public GenericReportDto createGrantReport() {
        List<RegionProviderCountDto> providerCountDtos = regionService.createRegionProviderCountDtos();

        String reportFile = null;
        String[] columns = {
                "Region_Id",
                "Region_Name",
                "Category_Id",
                "Category_Name",
                "Service_Id",
                "Service_Name",
                "Provider_Count"
        };

        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("Region_Category_Service_Provider_Breakdown");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setFillForegroundColor(IndexedColors.TEAL.getIndex());

            // CellStyle for region
            CellStyle regionCellStyle = workbook.createCellStyle();
            regionCellStyle.setFillBackgroundColor(IndexedColors.TEAL.getIndex());
            regionCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            regionCellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());

            // CellStyle for category
            CellStyle categoryCellStyle = workbook.createCellStyle();
            categoryCellStyle.setFillBackgroundColor(IndexedColors.TEAL.getIndex());
            categoryCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            categoryCellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());

            // Wrapped CellStyle
            CellStyle wrappedStyle = workbook.createCellStyle();
            wrappedStyle.setWrapText(true);

            // Instructions
            Row instruction2Row = sheet.createRow(0);
            for (int i = 1; i < 5; i++) {
                Row mergingRow = sheet.createRow(i);
                mergingRow.createCell(0);
            }
            Cell noteCell = instruction2Row.createCell(0);
            noteCell.setCellValue("Note - Each row represents a region/category/service, " +
                    "you can find the\ntotal number of providers in a specific region, " +
                    "the number of providers\nthat offer a certain category of service in that specific region, and " +
                    "the\nnumber of providers that offer a specific service in that specific region.\n" +
                    "The data at a certain level is colored.");
            noteCell.setCellStyle(wrappedStyle);
            for (int j = 1; j < 7; j++) {
                instruction2Row.createCell(j);
            }
            sheet.addMergedRegion(new CellRangeAddress(0, 4, 0, 6));

            Row instructionRow = sheet.createRow(5);
            Cell instructionRegionCell = instructionRow.createCell(0);
            instructionRegionCell.setCellValue("Orange - Region Level Data");
            instructionRegionCell.setCellStyle(regionCellStyle);
            instructionRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(5, 5, 0, 1));
            Cell instructionCategoryCell = instructionRow.createCell(2);
            instructionCategoryCell.setCellValue("Yellow - Category Level Data");
            instructionCategoryCell.setCellStyle(categoryCellStyle);
            instructionRow.createCell(3);
            sheet.addMergedRegion(new CellRangeAddress(5, 5, 2, 3));
            Cell instructionServiceCell = instructionRow.createCell(4);
            instructionServiceCell.setCellValue("Not Colored - Service Level Data");
            instructionRow.createCell(5);
            sheet.addMergedRegion(new CellRangeAddress(5, 5, 4, 5));

            // Row for Header
            Row headerRow = sheet.createRow(6);

            // Header
            for (int col = 0; col < columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            AtomicInteger rowNum = new AtomicInteger(7);
            providerCountDtos.forEach(pcd -> {
                Row row = sheet.createRow(rowNum.get());

                Cell regionId = row.createCell(0);
                regionId.setCellValue(pcd.getRegionId());
                regionId.setCellStyle(regionCellStyle);

                Cell region = row.createCell(1);
                region.setCellValue(pcd.getRegion());
                region.setCellStyle(regionCellStyle);

                Cell regionProviderCount = row.createCell(6);
                regionProviderCount.setCellValue(pcd.getProviderCount());
                regionProviderCount.setCellStyle(regionCellStyle);
                rowNum.addAndGet(1);

                pcd.getCategoryProviderCounts().forEach(cpc -> {
                    Row cpcRow = sheet.createRow(rowNum.get());
                    cpcRow.createCell(0).setCellValue(pcd.getRegionId());
                    cpcRow.createCell(1).setCellValue(pcd.getRegion());

                    Cell categoryId = cpcRow.createCell(2);
                    categoryId.setCellValue(cpc.getCategoryId());
                    categoryId.setCellStyle(categoryCellStyle);

                    Cell category = cpcRow.createCell(3);
                    category.setCellValue(cpc.getCategory());
                    category.setCellStyle(categoryCellStyle);

                    Cell categoryProviderCount = cpcRow.createCell(6);
                    categoryProviderCount.setCellValue(cpc.getProviderCount());
                    categoryProviderCount.setCellStyle(categoryCellStyle);
                    rowNum.addAndGet(1);

                    cpc.getServiceProviderCounts().forEach(spc -> {
                        Row spcRow = sheet.createRow(rowNum.get());
                        spcRow.createCell(0).setCellValue(pcd.getRegionId());
                        spcRow.createCell(1).setCellValue(pcd.getRegion());
                        spcRow.createCell(2).setCellValue(cpc.getCategoryId());
                        spcRow.createCell(3).setCellValue(cpc.getCategory());

                        spcRow.createCell(4).setCellValue(spc.getServiceId());
                        spcRow.createCell(5).setCellValue(spc.getService());
                        spcRow.createCell(6).setCellValue(spc.getProviderCount());
                        rowNum.addAndGet(1);
                    });
                });
            });

            workbook.write(out);

            reportFile = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," +
                    Base64.getEncoder().encodeToString(out.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return GenericReportDto.builder()
                .reportFile(reportFile)
                .build();
    }
}
