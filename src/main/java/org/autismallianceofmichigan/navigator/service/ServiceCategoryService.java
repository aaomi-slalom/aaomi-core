package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.CategoryServicesDto;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceCategoryDto;
import org.autismallianceofmichigan.navigator.dto.adminServicePresetDto.ServiceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.ServiceCategoryOptionDto;
import org.autismallianceofmichigan.navigator.exception.ServiceCategoryNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ServiceCategory;
import org.autismallianceofmichigan.navigator.persistence.repository.ServiceCategoryRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
@Transactional
public class ServiceCategoryService {

    private final ServiceCategoryRepository serviceCategoryRepository;
    private final ServiceService serviceService;

    public ServiceCategory findByCategory(String category) {
        Optional<ServiceCategory> serviceCategoryOptional = serviceCategoryRepository.findByCategory(category);
        return serviceCategoryOptional.orElseThrow(() -> new ServiceCategoryNotFoundException("ServiceCategory not found"));
    }

    public ServiceCategory findByCategoryId(Long categoryId) {
        Optional<ServiceCategory> serviceCategoryOptional = serviceCategoryRepository.findById(categoryId);
        return serviceCategoryOptional.orElseThrow(() -> new ServiceCategoryNotFoundException("ServiceCategory not found"));
    }

    public void deleteServiceCategory(Long serviceCategoryId) {
        serviceCategoryRepository.deleteById(serviceCategoryId);
    }

    public void saveServiceCategory(ServiceCategory serviceCategory) {
        serviceCategoryRepository.save(serviceCategory);
    }

    public List<ServiceCategoryOptionDto> getAllServiceCategoryOptionDtos() {
        return serviceCategoryRepository.streamAll().map(serviceCategory
                -> ServiceCategoryOptionDto.builder()
                        .serviceCategoryId(serviceCategory.getServiceCategoryId())
                        .serviceCategory(serviceCategory.getCategory())
                        .build()
        ).collect(Collectors.toList());
    }

    public List<ServiceCategoryDto> getAllServiceCategoryDtos() {
        return serviceCategoryRepository.streamAll().map(serviceCategory -> {
            int serviceCount = serviceCategoryRepository.countGroupedServicesByServiceCategoryId(serviceCategory.getServiceCategoryId());

            return ServiceCategoryDto.builder()
                    .serviceCategoryId(serviceCategory.getServiceCategoryId())
                    .serviceCategory(serviceCategory.getCategory())
                    .description(serviceCategory.getDescription())
                    .serviceCount(serviceCount)
                    .build();
        }
        ).collect(Collectors.toList());
    }

    public List<CategoryServicesDto> getAllPublicCategoryServicesDto() {
        List<CategoryServicesDto> customiseCategoryServicesDto = new ArrayList<>();
        serviceCategoryRepository.streamAll().forEach(serviceCategory -> {
            customiseCategoryServicesDto.add(new CategoryServicesDto(serviceCategory.getCategory(), serviceCategory.getDescription(), serviceService.displayPublicServicesFromCategoryId(serviceCategory.getServiceCategoryId())));
        });
        return customiseCategoryServicesDto;
    }

    public List<CategoryServicesDto> getAllCategoryServicesDto() {
        List<CategoryServicesDto> customiseCategoryServicesDto = new ArrayList<>();
        serviceCategoryRepository.streamAll().forEach(serviceCategory -> {
            customiseCategoryServicesDto.add(new CategoryServicesDto(serviceCategory.getCategory(), serviceCategory.getDescription(), serviceService.displayServicesFromCategoryId(serviceCategory.getServiceCategoryId())));
        });
        return customiseCategoryServicesDto;
    }

    int countProvidersByRegionIdAndCategoryId(Long regionId, Long categoryId) {
        return serviceCategoryRepository.countProvidersByRegionIdAndCategoryId(regionId, categoryId);
    }

    Stream<ServiceCategory> streamAllCategoriesByRegionId(Long regionId) {
        return serviceCategoryRepository.streamAllCategoriesByRegionId(regionId);
    }
}
