package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderLocationRequest;
import org.autismallianceofmichigan.navigator.exception.ContactNumberNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactNumber;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderLocation;
import org.autismallianceofmichigan.navigator.persistence.repository.ContactNumberRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContactNumberService {
    private final ContactTypeService contactTypeService;
    private final ContactNumberRepository contactNumberRepository;

    public ContactNumber findById(Long contactNumberId) {
        Optional<ContactNumber> contactNumberOptional = contactNumberRepository.findById(contactNumberId);
        return contactNumberOptional.orElseThrow(() -> new ContactNumberNotFoundException("Contact number not found"));
    }

    public boolean hasContactNumberWithId(Long contactNumberId) {
        return contactNumberRepository.existsById(contactNumberId);
    }

    public void deleteById(Long contactNumberId) {
        contactNumberRepository.deleteById(contactNumberId);
    }

    public void save(ContactNumber contactNumber) {
        contactNumberRepository.save(contactNumber);
    }

    public void requestToDelete(ContactNumber contactNumber) {
        if (!contactNumber.isCreateApproved()) {
            deleteById(contactNumber.getContactNumberId());
        } else {
            contactNumber.setDeleteRequested(true);
            save(contactNumber);
        }
    }

    public List<ContactNumber> createContactNumbers(
            ProviderLocation providerLocation,
            CreateProviderLocationRequest createProviderLocationRequest,
            boolean createApproved
    ) {
        return createProviderLocationRequest.getContactNumbers().stream().map(createContactNumberRequest -> {
            String number = createContactNumberRequest.getNumber();
            ContactType contactType = contactTypeService.findByType(createContactNumberRequest.getContactType());

            return ContactNumber.builder()
                    .number(number)
                    .contactType(contactType)
                    .providerLocation(providerLocation)
                    .createApproved(createApproved)
                    .build();
        }).collect(Collectors.toList());
    }
}
