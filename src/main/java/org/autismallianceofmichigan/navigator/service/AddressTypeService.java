package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.exception.AddressTypeNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.AddressType;
import org.autismallianceofmichigan.navigator.persistence.repository.AddressTypeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AddressTypeService {
    private final AddressTypeRepository addressTypeRepository;

    public AddressType findByType(String addressType) {
        Optional<AddressType> addressTypeOptional = addressTypeRepository.findByType(addressType);
        return addressTypeOptional.orElseThrow(() -> new AddressTypeNotFoundException("AddressType not found"));
    }

    public boolean hasAddressType(String type) {
        return addressTypeRepository.findByType(type).isPresent();
    }

    public AddressType saveAddressType(AddressType addressType) {
        return addressTypeRepository.save(addressType);
    }
}
