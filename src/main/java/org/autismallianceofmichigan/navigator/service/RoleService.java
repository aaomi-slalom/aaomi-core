package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.RoleDto;
import org.autismallianceofmichigan.navigator.exception.RoleNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.Role;
import org.autismallianceofmichigan.navigator.persistence.repository.RoleRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public boolean hasRole(String role) {
        return roleRepository.findByRole(role).isPresent();
    }

    public Role findByRole(String role) {
        Optional<Role> roleOptional = roleRepository.findByRole(role);
        return roleOptional.orElseThrow(() -> new RoleNotFoundException("Role Not Found"));
    }

    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Transactional
    public Stream<Role> streamAllRoles() {
        return roleRepository.streamAll();
    }

    @Transactional
    public List<RoleDto> getAllRoleDtos() {
        return streamAllRoles().map(role ->
                RoleDto.builder()
                        .role(role.getRole())
                        .description(role.getDescription())
                        .defaultGroup(role.getDefaultGroup())
                        .build()
        ).collect(Collectors.toList());
    }
}
