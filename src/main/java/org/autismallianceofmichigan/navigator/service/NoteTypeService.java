package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.exception.NoteTypeNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.NoteType;
import org.autismallianceofmichigan.navigator.persistence.repository.NoteTypeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class NoteTypeService {
    private final NoteTypeRepository noteTypeRepository;

    public NoteType findByNoteType(String noteType) {
        Optional<NoteType> noteTypeOptional = noteTypeRepository.findByNoteType(noteType);
        return noteTypeOptional.orElseThrow(()-> new NoteTypeNotFoundException("Note type not found"));
    }

    public boolean hasNoteType(String noteType) {
        return noteTypeRepository.findByNoteType(noteType).isPresent();
    }

    public void save(NoteType noteType) {
        noteTypeRepository.save(noteType);
    }
}
