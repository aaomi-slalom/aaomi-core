package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.eventDto.EventDto;
import org.autismallianceofmichigan.navigator.dto.eventDto.UpdateEventRequest;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.Event;
import org.autismallianceofmichigan.navigator.persistence.repository.EventRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class EventsService {
    private final EventRepository eventRepository;

    public List<EventDto> getAllEventDtos() {
        return eventRepository.findByOrderByStartDateAsc().stream()
                .map(event ->
                        EventDto.builder()
                                .eventId(event.getEventId())
                                .title(event.getTitle())
                                .description(event.getDescription())
                                .website(event.getWebsite())
                                .image(event.getImage())
                                .cost(event.getCost())
                                .free(event.isFree())
                                .startDate(event.getStartDate())
                                .endDate(event.getEndDate())
                                .organizer(event.getOrganizer().getUsername())
                                .build()
                ).collect(Collectors.toList());
    }

    public List<EventDto> getAllEventDtosForAccount(Account currentUser) {
        return eventRepository.findByOrganizerOrderByStartDateAsc(currentUser).stream()
                .map(event ->
                        EventDto.builder()
                                .eventId(event.getEventId())
                                .title(event.getTitle())
                                .description(event.getDescription())
                                .website(event.getWebsite())
                                .image(event.getImage())
                                .cost(event.getCost())
                                .free(event.isFree())
                                .startDate(event.getStartDate())
                                .endDate(event.getEndDate())
                                .organizer(event.getOrganizer().getUsername())
                                .build()
                ).collect(Collectors.toList());
    }

    public EventDto getEventDtoById(Long eventId) {
        Event event = eventRepository.getOne(eventId);
        return EventDto.builder()
                .eventId(event.getEventId())
                .title(event.getTitle())
                .description(event.getDescription())
                .website(event.getWebsite())
                .image(event.getImage())
                .cost(event.getCost())
                .free(event.isFree())
                .startDate(event.getStartDate())
                .endDate(event.getEndDate())
                .organizer(event.getOrganizer().getUsername())
                .build();
    }

    public Event save(Event event) { return eventRepository.save(event); }

    @Transactional
    public void deleteById(Long eventId) {
        eventRepository.deleteById(eventId);
    }

    @Transactional
    public void updateExistingEvent(Long eventId, UpdateEventRequest updateEventRequest) {
        Event existingEvent = eventRepository.getOne(eventId);

        Event event = Event.builder()
                .eventId(eventId)
                .title(updateEventRequest.getTitle())
                .description(updateEventRequest.getDescription())
                .website(updateEventRequest.getWebsite())
                .image(updateEventRequest.getImage())
                .cost(updateEventRequest.getCost())
                .free(updateEventRequest.isFree())
                .startDate(updateEventRequest.getStartDate())
                .endDate(updateEventRequest.getEndDate())
                .organizer(existingEvent.getOrganizer())
                .build();

        eventRepository.save(event);
    }

    public boolean eventIsOrganizedByAccount(Long eventId, Account account) {
        Event event = eventRepository.getOne(eventId);
        return account.getAccountId() == event.getOrganizer().getAccountId();
    }
}
