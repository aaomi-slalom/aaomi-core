package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.PermissionDto;
import org.autismallianceofmichigan.navigator.exception.PermissionNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.Permission;
import org.autismallianceofmichigan.navigator.persistence.repository.PermissionRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PermissionService {
    private final PermissionRepository permissionRepository;

    public boolean hasPermissionWithPermCode(String permCode) {
        return permissionRepository.findByPermCode(permCode).isPresent();
    }

    public Permission savePermission(Permission permission) {
        return permissionRepository.save(permission);
    }

    public Permission findByPermCode(String permCode) {
        Optional<Permission> permissionOptional = permissionRepository.findByPermCode(permCode);
        return permissionOptional.orElseThrow(() -> new PermissionNotFoundException("Permission not found"));
    }

    @Transactional
    public Stream<Permission> streamAllPermissions() {
        return permissionRepository.streamAll();
    }

    @Transactional
    public List<String> getAllPermissionNames() {
        return streamAllPermissions()
                .map(Permission::getPermCode)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<PermissionDto> getAllPermissionDtos() {
        return streamAllPermissions().map(permission ->
                PermissionDto.builder()
                        .permission(permission.getPermCode())
                        .description(permission.getDescription())
                        .build()
        ).collect(Collectors.toList());
    }
}
