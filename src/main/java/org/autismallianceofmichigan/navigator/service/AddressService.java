package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderLocationRequest;
import org.autismallianceofmichigan.navigator.exception.AddressNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.Address;
import org.autismallianceofmichigan.navigator.persistence.entity.AddressType;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderLocation;
import org.autismallianceofmichigan.navigator.persistence.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;
    private final AddressTypeService addressTypeService;

    public Address findById(Long addressId) {
        Optional<Address> addressOptional = addressRepository.findById(addressId);
        return addressOptional.orElseThrow(() -> new AddressNotFoundException("Address not found"));
    }

    public List<Address> createAddresses(
            ProviderLocation providerLocation,
            CreateProviderLocationRequest createProviderLocationRequest,
            boolean createApproved
    ) {
        return createProviderLocationRequest.getAddresses()
                .stream()
                .map(createAddressRequest -> {
                    AddressType addressType = addressTypeService.findByType(createAddressRequest.getAddressType());
                    String city = createAddressRequest.getCity();
                    String state = createAddressRequest.getState();
                    String street = createAddressRequest.getStreet();
                    String zip = createAddressRequest.getZip();

                    return Address.builder()
                            .addressType(addressType)
                            .city(city)
                            .state(state)
                            .street(street)
                            .zip(zip)
                            .providerLocation(providerLocation)
                            .createApproved(createApproved)
                            .build();
                })
                .collect(Collectors.toList());
    }

    public void save(Address address) {
        addressRepository.save(address);
    }
}
