package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.approvalDto.UnapprovedModifiedProviderOverviewDto;
import org.autismallianceofmichigan.navigator.dto.approvalDto.UnapprovedProviderOverviewDto;
import org.autismallianceofmichigan.navigator.dto.fileDto.ProviderFileDto;
import org.autismallianceofmichigan.navigator.dto.noteDto.NoteDto;
import org.autismallianceofmichigan.navigator.dto.noteDto.NoteOptionDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.*;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.WaitTimeDto;
import org.autismallianceofmichigan.navigator.dto.waitTimeDto.WaitTimeOptionDto;
import org.autismallianceofmichigan.navigator.exception.ProviderInfoNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.persistence.repository.ProviderInfoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProviderInfoService {
    private final ProviderInfoRepository providerInfoRepository;
    private final ProviderLocationService providerLocationService;

    public List<ProviderFileDto> getAllProviderFileDtosByProviderInfoId(Long providerInfoId) {
        ProviderInfo providerInfo = findById(providerInfoId);

        return providerInfo.getProviderFiles().stream().map(providerFile ->
                ProviderFileDto.builder()
                        .providerFileId(providerFile.getProviderFileId())
                        .createdAt(providerFile.getCreatedAt())
                        .file(providerFile.getFile())
                        .fileName(providerFile.getFileName())
                        .author(providerFile.getAuthor().getUsername())
                        .build()
        ).collect(Collectors.toList());
    }

    @Transactional
    public List<UnapprovedProviderOverviewDto> getAllUnapprovedProviders() {
        return providerInfoRepository.findAllByCreateApprovedIsFalse().map(upr ->
                UnapprovedProviderOverviewDto.builder()
                        .providerInfoId(upr.getProviderInfoId())
                        .providerName(upr.getProviderName())
                        .createdAt(upr.getCreatedAt())
                        .editedBy(upr.getLastEditUsername())
                        .build())
                .collect(Collectors.toList());
    }

    public List<NoteDto> getAllNoteDtosByProviderInfoId(Long providerInfoId) {
        ProviderInfo providerInfo = findById(providerInfoId);

        return providerInfo.getNotes().stream().map(note -> {
            String source = null;
            if (note.getProviderLocation() != null) {
                source = note.getProviderLocation().getLocation();
            } else if (note.getLocationSrvcLink() != null) {
                source = note.getLocationSrvcLink().getService().getService();
            } else {
                source = note.getProviderInfo().getProviderName();
            }

            return NoteDto.builder()
                    .noteId(note.getNoteId())
                    .note(note.getNote())
                    .type(note.getNoteType().getNoteType())
                    .title(note.getTitle())
                    .source(source)
                    .author(note.getAuthor().getUsername())
                    .createdAt(note.getCreatedAt())
                    .build();
        }).collect(Collectors.toList());
    }

    public boolean hasProviderInfoWithName(String providerName) {
        return providerInfoRepository.findByProviderName(providerName).isPresent();
    }

    public ProviderInfo save(ProviderInfo providerInfo) {
        return providerInfoRepository.save(providerInfo);
    }

    @Transactional
    public ProvidersPageDto getProvidersPageDtoByViewProvidersRequest(ViewProvidersRequest viewProvidersRequest) {
        Page<ProviderInfo> providerInfoPage = findProviderInfoByProviderName(
                viewProvidersRequest.getProviderNameSearch(),
                viewProvidersRequest.getCurrentPage(),
                viewProvidersRequest.getPageSize()
        );

        List<ProviderInfoDto> providerInfoDtos = providerInfoPage.get().map(providerInfo -> {
            Set<ProviderLocation> locations = providerInfo.getProviderLocations();

            String email = "";
            String contactNumber = "";
            if (locations.size() > 0) {
                List<Email> emailsFiltered = new ArrayList<ProviderLocation>(locations).get(0).getEmails()
                        .stream()
                        .filter(e ->
                                e.getContactType().getType().equals("Administrative")
                        ).collect(Collectors.toList());

                if (emailsFiltered.size() > 0) {
                    email = emailsFiltered.get(0).getEmail();
                }

                List<ContactNumber> contactNumbersFiltered = new ArrayList<ProviderLocation>(locations)
                        .get(0)
                        .getContactNumbers()
                        .stream()
                        .filter(n ->
                                n.getContactType().getType().equals("Administrative")
                        ).collect(Collectors.toList());

                if (contactNumbersFiltered.size() > 0) {
                    contactNumber = contactNumbersFiltered.get(0).getNumber();
                }
            }

            int serviceCount = providerInfoRepository.countGroupedServicesByProviderInfoId(providerInfo.getProviderInfoId());

            boolean isStandalone = providerInfo.getAccounts().size() == 0;

            boolean approved = !providerInfoRepository.findPendingApprovedProviderInfoByProviderInfoId(providerInfo.getProviderInfoId()).isPresent();

            return ProviderInfoDto.builder()
                    .providerInfoId(providerInfo.getProviderInfoId())
                    .providerName(providerInfo.getProviderName())
                    .logo(providerInfo.getLogo())
                    .email(email)
                    .contactNumber(contactNumber)
                    .locationCount(locations.size())
                    .serviceCount(serviceCount)
                    .isStandalone(isStandalone)
                    .approved(approved)
                    .build();
        }).collect(Collectors.toList());

        return ProvidersPageDto.builder()
                .totalElements(providerInfoPage.getTotalElements())
                .totalPages(providerInfoPage.getTotalPages())
                .providerInfoDtos(providerInfoDtos)
                .build();
    }

    public ProviderInfo buildProviderInfo(
            CreateProviderInfoRequest createProviderInfoRequest,
            List<CreateProviderLocationRequest> createProviderLocationRequests,
            boolean createApproved,
            String editedBy
    ) {
        ProviderInfo providerInfo = ProviderInfo.builder()
                .providerName(createProviderInfoRequest.getProviderName())
                .description(createProviderInfoRequest.getDescription())
                .website(createProviderInfoRequest.getWebsite())
                .logo(createProviderInfoRequest.getLogo())
                .facebook(createProviderInfoRequest.getFacebook())
                .twitter(createProviderInfoRequest.getTwitter())
                .linkedin(createProviderInfoRequest.getLinkedin())
                .instagram(createProviderInfoRequest.getInstagram())
                .youtube(createProviderInfoRequest.getYoutube())
                .createApproved(createApproved)
                .lastEditUsername(editedBy)
                .build();

        Set<ProviderLocation> providerLocations = createProviderLocationRequests.stream().map(
                createProviderLocationRequest -> providerLocationService.buildProviderLocation(providerInfo, createProviderLocationRequest, createApproved)
        ).collect(Collectors.toSet());

        providerInfo.setProviderLocations(providerLocations);

        return save(providerInfo);
    }

    public Page<ProviderInfo> findProviderInfoByProviderName(String providerName, int currentPage, int pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        return providerInfoRepository.findByProviderNameIgnoreCaseContainingOrderByProviderInfoIdDesc(providerName, pageable);
    }

    public PublicProvidersPageDto generateSearchResultsReport(QueryProvidersRequest queryProvidersRequest) {
        Page<ProviderLocation> providerLocationPage = providerLocationService.createSearchResultsReport(
                queryProvidersRequest.getProviderName(),
                queryProvidersRequest.getCategoryIds(),
                queryProvidersRequest.getServiceIds(),
                queryProvidersRequest.getInsuranceIds(),
                queryProvidersRequest.getAgeGroupIds(),
                queryProvidersRequest.getDeliveryIds(),
                queryProvidersRequest.getCountyIds(),
                queryProvidersRequest.getRegionIds(),
                queryProvidersRequest.getKeyword()
        );

        return providerLocationService.toPublicProviderInfoDtos(providerLocationPage);
    }

    public PublicProvidersPageDto getPublicProvidersPageDtoByQueryProvidersRequest(QueryProvidersRequest queryProvidersRequest) {
        Page<ProviderLocation> providerLocationPage = providerLocationService.queryProviderLocationPageByConditions(
                queryProvidersRequest.getProviderName(),
                queryProvidersRequest.getCategoryIds(),
                queryProvidersRequest.getServiceIds(),
                queryProvidersRequest.getInsuranceIds(),
                queryProvidersRequest.getAgeGroupIds(),
                queryProvidersRequest.getDeliveryIds(),
                queryProvidersRequest.getCountyIds(),
                queryProvidersRequest.getRegionIds(),
                queryProvidersRequest.getKeyword(),
                queryProvidersRequest.getCurrentPage(),
                queryProvidersRequest.getPageSize()
        );

        return providerLocationService.toPublicProviderInfoDtos(providerLocationPage);
    }

    public PublicProvidersPageDto getInternalProvidersPageDtoByQueryProvidersRequest(QueryProvidersRequest queryProvidersRequest) {
        Page<ProviderLocation> providerLocationPage = providerLocationService.internalQueryProviderLocationPageByConditions(
                queryProvidersRequest.getProviderName(),
                queryProvidersRequest.getCategoryIds(),
                queryProvidersRequest.getServiceIds(),
                queryProvidersRequest.getInsuranceIds(),
                queryProvidersRequest.getAgeGroupIds(),
                queryProvidersRequest.getDeliveryIds(),
                queryProvidersRequest.getCountyIds(),
                queryProvidersRequest.getRegionIds(),
                queryProvidersRequest.getKeyword(),
                queryProvidersRequest.getCurrentPage(),
                queryProvidersRequest.getPageSize()
        );

        return providerLocationService.toPublicProviderInfoDtos(providerLocationPage);
    }

    public ProviderInfo findById(Long providerInfoId) {
        Optional<ProviderInfo> providerInfo = providerInfoRepository.findById(providerInfoId);
        return providerInfo.orElseThrow(() -> new ProviderInfoNotFoundException("ProviderInfo not found"));
    }

    public List<UserProviderOverviewDto> toUserProviderOverviewDtos(Set<ProviderInfo> providerInfos) {
        return providerInfos.stream().map(providerInfo -> {
            int serviceCount = providerInfoRepository.countGroupedServicesByProviderInfoId(providerInfo.getProviderInfoId());

            return UserProviderOverviewDto.builder()
                    .providerInfoId(providerInfo.getProviderInfoId())
                    .providerName(providerInfo.getProviderName())
                    .logo(providerInfo.getLogo())
                    .website(providerInfo.getWebsite())
                    .locationCount(providerInfo.getProviderLocations().size())
                    .serviceCount(serviceCount)
                    .build();
        }).collect(Collectors.toList());
    }

    public ProviderDto getProviderInfoById(Long providerInfoId) {
        ProviderInfo providerInfo = findById(providerInfoId);

        boolean isStandAlone = providerInfo.getAccounts().size() == 0;
        ProviderAccountDto providerAccountDto = null;
        if (!isStandAlone) {
            Account providerAccount = new ArrayList<>(providerInfo.getAccounts()).get(0);
            UserProfile userProfile = providerAccount.getUserProfile();

            providerAccountDto = ProviderAccountDto.builder()
                    .firstName(userProfile == null ? "New" : userProfile.getFirstName())
                    .lastName(userProfile == null ? "User" : userProfile.getLastName())
                    .image(userProfile == null ? "" : userProfile.getImage())
                    .email(providerAccount.getEmail())
                    .build();
        }

        String modifiedProviderName = null;
        String modifiedDescription = null;
        String modifiedLogo = null;
        String modifiedLinkedin = null;
        String modifiedFacebook = null;
        String modifiedTwitter = null;
        String modifiedInstagram = null;
        String modifiedYoutube = null;
        String modifiedWebsite = null;

        if (providerInfo.isModifyRequested()) {
            modifiedProviderName = providerInfo.getModifiedProviderName();
            modifiedDescription = providerInfo.getModifiedDescription();
            modifiedLogo = providerInfo.getModifiedLogo();
            modifiedLinkedin = providerInfo.getModifiedLinkedin();
            modifiedFacebook = providerInfo.getModifiedFacebook();
            modifiedTwitter = providerInfo.getModifiedTwitter();
            modifiedInstagram = providerInfo.getModifiedInstagram();
            modifiedYoutube = providerInfo.getModifiedYoutube();
            modifiedWebsite = providerInfo.getModifiedWebsite();
        }

        DetailedProviderInfoDto detailedProviderInfoDto = DetailedProviderInfoDto.builder()
                .providerInfoId(providerInfoId)
                .providerName(providerInfo.getProviderName())
                .description(providerInfo.getDescription())
                .logo(providerInfo.getLogo())
                .facebook(providerInfo.getFacebook())
                .linkedin(providerInfo.getLinkedin())
                .twitter(providerInfo.getTwitter())
                .instagram(providerInfo.getInstagram())
                .youtube(providerInfo.getYoutube())
                .website(providerInfo.getWebsite())
                .modifiedDescription(modifiedDescription)
                .modifiedFacebook(modifiedFacebook)
                .modifiedLinkedin(modifiedLinkedin)
                .modifiedLogo(modifiedLogo)
                .modifiedProviderName(modifiedProviderName)
                .modifiedTwitter(modifiedTwitter)
                .modifiedInstagram(modifiedInstagram)
                .modifiedYoutube(modifiedYoutube)
                .modifiedWebsite(modifiedWebsite)
                .modifyRequested(providerInfo.isModifyRequested())
                .createApproved(providerInfo.isCreateApproved())
                .build();


        List<ProviderLocationDto> providerLocationDtos = providerInfo.getProviderLocations()
                .stream().map(providerLocation -> {
                    List<EmailDto> emailDtos = providerLocation.getCurrentEmails().get().map(this::toEmailDto).collect(Collectors.toList());
                    List<ContactNumberDto> contactNumberDtos = providerLocation.getCurrentContactNumbers().get().map(this::toContactNumberDto).collect(Collectors.toList());
                    List<EmailDto> unapprovedNewEmails = providerLocation.getUnapprovedAddedEmails().get().map(this::toEmailDto).collect(Collectors.toList());
                    List<ContactNumberDto> unapprovedAddedNewContactNumbers = providerLocation.getUnapprovedAddedContactNumbers().get().map(this::toContactNumberDto).collect(Collectors.toList());

                    List<MichiganCountyOptionDto> serviceCounties = providerLocation.getCounties().stream()
                            .map(this::toMichiganCountyOptionDto).collect(Collectors.toList());

                    // addresses
                    Address mailingAddress = providerLocation.getMailingAddress();
                    Address serviceAddress = providerLocation.getServiceAddress();
                    boolean hasServiceAddress = serviceAddress != null;

                    // location services
                    List<LocationServiceDto> locationServiceDtos = providerLocationService.toGroupedServices(providerLocation.getCurrentLocationSrvcLinks().get(), true);
                    List<LocationServiceDto> unapprovedAddedNewServices = providerLocationService.toGroupedServices(providerLocation.getUnapprovedAddedLocationSrvcLinks().get(), true);

                    String modifiedLocation = providerLocation.getModifiedLocation();

                    List<MichiganCountyOptionDto> modifiedCounties = providerLocation.getModifiedCounties().get()
                            .map(this::toMichiganCountyOptionDto).collect(Collectors.toList());

                    List<EmailDto> unapprovedAddedEmails = providerLocation.getUnapprovedAddedEmails().get()
                            .map(this::toEmailDto).collect(Collectors.toList());

                    List<EmailDto> unapprovedDeletedEmails = providerLocation.getUnapprovedDeletedEmails().get()
                            .map(this::toEmailDto).collect(Collectors.toList());

                    List<ContactNumberDto> unapprovedAddedContactNumbers = providerLocation.getUnapprovedAddedContactNumbers().get()
                            .map(this::toContactNumberDto).collect(Collectors.toList());

                    List<ContactNumberDto> unapprovedDeletedContactNumbers = providerLocation.getUnapprovedDeletedContactNumbers().get()
                            .map(this::toContactNumberDto).collect(Collectors.toList());

                    List<LocationServiceDto> unapprovedAddedGroupedServices = providerLocationService.toGroupedServices(providerLocation.getUnapprovedAddedLocationSrvcLinks().get(), true);
                    List<LocationServiceDto> unapprovedDeletedGroupedServices = providerLocationService.toGroupedServices(providerLocation.getUnapprovedDeletedLocationSrvcLinks().get(), true);

                    Address modifiedMailingAddress = providerLocation.getUnapprovedModifiedMailingAddress();
                    Address modifiedServiceAddress = providerLocation.getUnapprovedModifiedServiceAddress();

                    // provider location
                    return ProviderLocationDto.builder()
                            .locationId(providerLocation.getProviderLocationId())
                            .locationName(providerLocation.getLocation())
                            .mailingAddressId(mailingAddress.getAddressId())
                            .mailingCity(mailingAddress.getCity())
                            .mailingState(mailingAddress.getState())
                            .mailingStreet(mailingAddress.getStreet())
                            .mailingZip(mailingAddress.getZip())
                            .serviceAddressId(hasServiceAddress ? serviceAddress.getAddressId() : null)
                            .serviceCity(hasServiceAddress ? serviceAddress.getCity() : null)
                            .serviceZip(hasServiceAddress ? serviceAddress.getZip() : null)
                            .serviceState(hasServiceAddress ? serviceAddress.getState() : null)
                            .serviceStreet(hasServiceAddress ? serviceAddress.getStreet() : null)
                            .serviceCounties(serviceCounties)
                            .emailDtos(emailDtos)
                            .contactNumberDtos(contactNumberDtos)
                            .locationServiceDtos(locationServiceDtos)
                            .modifiedLocationName(modifiedLocation)
                            .modifiedCounties(modifiedCounties)
                            .modifiedServiceAddress(modifiedServiceAddress)
                            .modifiedMailingAddress(modifiedMailingAddress)
                            .unapprovedAddedEmails(unapprovedAddedEmails)
                            .unapprovedDeletedEmails(unapprovedDeletedEmails)
                            .unapprovedAddedContactNumbers(unapprovedAddedContactNumbers)
                            .unapprovedDeletedContactNumbers(unapprovedDeletedContactNumbers)
                            .unapprovedAddedGroupedServices(unapprovedAddedGroupedServices)
                            .unapprovedDeletedGroupedServices(unapprovedDeletedGroupedServices)
                            .createApproved(providerLocation.isCreateApproved())
                            .deleteRequested(providerLocation.isDeleteRequested())
                            .modifyRequested(providerLocationService.isLocationModified(providerLocation.getProviderLocationId()))
                            .internal(providerLocation.isInternal())
                            .unapprovedAddedNewServices(unapprovedAddedNewServices)
                            .unapprovedAddedNewContactNumbers(unapprovedAddedNewContactNumbers)
                            .unapprovedAddedNewEmails(unapprovedNewEmails)
                            .build();
                }).collect(Collectors.toList());

        return ProviderDto.builder()
                .accountDto(providerAccountDto)
                .providerInfoDto(detailedProviderInfoDto)
                .providerLocationDtos(providerLocationDtos)
                .build();
    }

    public List<WaitTimeOptionDto> getAllWaitOptionDtosByProviderInfoId(Long providerInfoId) {
        List<WaitTimeOptionDto> waitTimeOptionDtos = new ArrayList<>();
        ProviderInfo providerInfo = findById(providerInfoId);

        WaitTimeOptionDto waitTimeOptionDto = WaitTimeOptionDto.builder()
                .waitTimeLinkId(String.valueOf(providerInfoId))
                .source(providerInfo.getProviderName())
                .waitTimeType("Provider")
                .build();
        waitTimeOptionDtos.add(waitTimeOptionDto);

        List<WaitTimeOptionDto> locationOptionDtos = providerInfo.getProviderLocations().stream().map(providerLocation ->
                WaitTimeOptionDto.builder()
                        .waitTimeType("Location")
                        .waitTimeLinkId(String.valueOf(providerLocation.getProviderLocationId()))
                        .source(providerLocation.getLocation())
                        .build()).collect(Collectors.toList());
        waitTimeOptionDtos.addAll(locationOptionDtos);

        return waitTimeOptionDtos;
    }

    public List<WaitTimeDto> getAllWaitTimeDtosByProviderInfoId(Long providerInfoId) {
        ProviderInfo providerInfo = findById(providerInfoId);
        return providerInfo.getWaitTimes().stream().map(waitTime -> {
            String source = null;
            if (waitTime.getProviderLocation() != null) {
                source = waitTime.getProviderLocation().getLocation();
            } else {
                source = waitTime.getProviderInfo().getProviderName();
            }

            return WaitTimeDto.builder()
                    .waitTimeId(waitTime.getWaitTimeId())
                    .title(waitTime.getTitle())
                    .waitTime(waitTime.getWaitTime())
                    .source(source)
                    .waitTimeType(waitTime.getWaitTimeType().getWaitTimeType())
                    .createdAt(waitTime.getCreatedAt())
                    .build();
        }).collect(Collectors.toList());
    }

    public List<NoteOptionDto> getAllNoteOptionDtosByProviderInfoId(long providerInfoId) {
        List<NoteOptionDto> noteOptionDtos = new ArrayList<>();
        ProviderInfo providerInfo = findById(providerInfoId);

        NoteOptionDto noteOptionDto = NoteOptionDto.builder()
                .noteLinkId(String.valueOf(providerInfoId))
                .source(providerInfo.getProviderName())
                .noteType("Provider")
                .build();
        noteOptionDtos.add(noteOptionDto);

        List<NoteOptionDto> locationNoteOptionDtos = providerInfo.getProviderLocations().stream()
                .map(providerLocation -> {
//                    Set<NoteOptionDto> serviceNoteOptionDtos = providerLocation.getLocationSrvcLinks().stream().map(
//                            locationSrvcLink ->
//                                    NoteOptionDto.builder()
//                                            .noteLinkId(locationSrvcLink.getIdentifier())
//                                            .source(locationSrvcLink.getService().getService())
//                                            .noteType("Service")
//                                            .build()
//                    ).collect(Collectors.toSet());
//                    noteOptionDtos.addAll(serviceNoteOptionDtos);

                    return NoteOptionDto.builder()
                            .noteLinkId(String.valueOf(providerLocation.getProviderLocationId()))
                            .source(providerLocation.getLocation())
                            .noteType("Location")
                            .build();
                }).collect(Collectors.toList());

        noteOptionDtos.addAll(locationNoteOptionDtos);

        return noteOptionDtos;
    }

    public boolean hasProviderInfoWithId(Long providerInfoId) {
        return providerInfoRepository.findById(providerInfoId).isPresent();
    }

    public void approveCreatedProvider(Long providerInfoId) {
        ProviderInfo providerInfo = findById(providerInfoId);

        providerInfo.setCreateApproved(true);
        providerInfo.approveAllCreatedLocations();

        save(providerInfo);
    }

    public int countGroupedServicesByProviderInfoId(Long providerInfoId) {
        return providerInfoRepository.countGroupedServicesByProviderInfoId(providerInfoId);
    }

    @Transactional
    public void deleteByProviderInfoId(Long providerInfoId) {
        providerInfoRepository.deleteByProviderInfoId(providerInfoId);
    }

    @Transactional
    public List<UnapprovedModifiedProviderOverviewDto> getAllUnapprovedModifiedProviderOverviewDtos() {
        return providerInfoRepository.findAllModifiedProviderInfos()
                .map(this::toUnapprovedModifiedProviderOverviewDto)
                .collect(Collectors.toList());
    }

    public UnapprovedModifiedProviderOverviewDto toUnapprovedModifiedProviderOverviewDto(ProviderInfo providerInfo) {
        return UnapprovedModifiedProviderOverviewDto.builder()
                .editedBy(providerInfo.getLastEditUsername())
                .updatedAt(providerInfo.getUpdatedAt())
                .providerInfoId(providerInfo.getProviderInfoId())
                .providerName(providerInfo.getProviderName())
                .build();
    }

    private EmailDto toEmailDto(Email email) {
        return EmailDto.builder()
                .email(email.getEmail())
                .type(email.getContactType().getType())
                .emailId(email.getEmailId())
                .build();
    }

    private ContactNumberDto toContactNumberDto(ContactNumber contactNumber) {
        return ContactNumberDto.builder()
                .contactNumberId(contactNumber.getContactNumberId())
                .type(contactNumber.getContactType().getType())
                .number(contactNumber.getNumber())
                .build();
    }

    private MichiganCountyOptionDto toMichiganCountyOptionDto(County county) {
        return MichiganCountyOptionDto.builder()
                .countyId(county.getCountyId())
                .county(county.getCounty())
                .build();
    }

    public Optional<ProviderInfo> findPendingApprovedProviderInfoByProviderInfoId(Long providerInfoId) {
        return providerInfoRepository.findPendingApprovedProviderInfoByProviderInfoId(providerInfoId);
    }
}
