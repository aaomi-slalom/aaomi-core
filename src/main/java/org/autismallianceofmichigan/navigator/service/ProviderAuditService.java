package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.auditDto.AuditDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Account;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderAudit;
import org.autismallianceofmichigan.navigator.persistence.entity.ProviderInfo;
import org.autismallianceofmichigan.navigator.persistence.repository.ProviderAuditRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProviderAuditService {
    private final ProviderAuditRepository providerAuditRepository;

    public ProviderAudit buildAudit(Account auditor, ProviderInfo providerInfo, String changeLog) {
        return ProviderAudit.builder()
                .account(auditor)
                .providerInfo(providerInfo)
                .changeLog(changeLog)
                .build();
    }

    @Transactional
    public List<AuditDto> getAllAuditDtosByProviderInfoId(Long providerInfoId) {
        return providerAuditRepository.streamAllByProviderInfoId(providerInfoId).map(al -> {
            ProviderInfo providerInfo = al.getProviderInfo();

            return AuditDto.builder()
                    .auditedBy(al.getAccount().getUsername())
                    .auditedTime(al.getAuditTime())
                    .changeLog(al.getChangeLog())
                    .providerName(providerInfo.getProviderName())
                    .build();
        }).collect(Collectors.toList());
    }
}
