package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.persistence.repository.ProviderFileRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProviderFileService {
    private final ProviderFileRepository providerFileRepository;

    public boolean hasProviderFileWithId(Long providerFileId) {
        return providerFileRepository.findById(providerFileId).isPresent();
    }

    public void deleteById(Long providerFileId) {
        providerFileRepository.deleteById(providerFileId);
    }
}
