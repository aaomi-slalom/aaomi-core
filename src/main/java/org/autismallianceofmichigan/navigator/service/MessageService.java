package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.exception.MessageNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.Message;
import org.autismallianceofmichigan.navigator.persistence.repository.MessageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class MessageService {
    private final MessageRepository messageRepository;

    public Message findById(Long messageId) {
        Optional<Message> message = messageRepository.findById(messageId);
        return message.orElseThrow(() -> new MessageNotFoundException("Message not found"));
    }

    public void saveMessage(Message message) {
        messageRepository.save(message);
    }

    public void deleteIfOrphan(Long messageId) {
        Message message = findById(messageId);
        if (message.isOrphan()) {
            messageRepository.deleteById(messageId);
        }
    }

    public Page<Message> getSentMessagesPageByUsername(String username, int currentPage, int pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        return messageRepository.findSentMessagesByAccountUsername(username, pageable);
    }

    public Page<Message> getReceivedMessagesPageByUsername(String username, int currentPage, int pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        return messageRepository.findReceivedMessagesByAccountUsername(username, pageable);
    }
}
