package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.autismallianceofmichigan.navigator.dto.providerDto.ContactTypeOptionDto;
import org.autismallianceofmichigan.navigator.exception.ContactTypeNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.ContactType;
import org.autismallianceofmichigan.navigator.persistence.repository.ContactTypeRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContactTypeService {
    private final ContactTypeRepository contactTypeRepository;

    public ContactType findByType(String contactType) {
        Optional<ContactType> contactTypeOptional = contactTypeRepository.findByType(contactType);
        return contactTypeOptional.orElseThrow(() -> new ContactTypeNotFoundException("ContactType not found"));
    }

    @Transactional
    public List<ContactTypeOptionDto> getAllContactTypeOptionDtos() {
        return contactTypeRepository.streamAll().map(contactType ->
                ContactTypeOptionDto.builder()
                        .contactTypeId(contactType.getContactTypeId())
                        .type(contactType.getType())
                        .description(contactType.getDescription())
                        .build()
        ).collect(Collectors.toList());
    }

    public boolean hasContactType(String type) {
        return contactTypeRepository.findByType(type).isPresent();
    }

    public ContactType saveContactType(ContactType contactType) {
        return contactTypeRepository.save(contactType);
    }
}
