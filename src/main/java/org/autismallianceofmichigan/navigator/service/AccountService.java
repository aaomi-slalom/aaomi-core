package org.autismallianceofmichigan.navigator.service;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.InheritedPermissionDto;
import org.autismallianceofmichigan.navigator.dto.adminUserDto.UserViewDto;
import org.autismallianceofmichigan.navigator.dto.approvalDto.UnapprovedAccountDto;
import org.autismallianceofmichigan.navigator.dto.messageDto.MessageAccountGroupsDto;
import org.autismallianceofmichigan.navigator.dto.messageDto.MessageUserDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.AccountProviderOverviewDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderAccountRequest;
import org.autismallianceofmichigan.navigator.dto.providerDto.CreateProviderRequest;
import org.autismallianceofmichigan.navigator.dto.registrationDto.RegistrationRequest;
import org.autismallianceofmichigan.navigator.dto.userProfileDto.UserProfileCreateUpdateRequest;
import org.autismallianceofmichigan.navigator.exception.AccountNotFoundException;
import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountRecoveryRepository;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountRepository;
import org.autismallianceofmichigan.navigator.persistence.repository.AccountVerificationRepository;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleNotFoundException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AccountService implements UserDetailsService {
    private final AccountRepository accountRepository;
    private final AccountVerificationRepository accountVerificationRepository;
    private final AccountRecoveryRepository accountRecoveryRepository;
    private final AccountGroupService accountGroupService;
    private final ProviderInfoService providerInfoService;
    private final RoleService roleService;
    private final MessageService messageService;
    private final ProviderAuditService providerAuditService;
    private final MailService mailService;
    private final Environment env;

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        Optional<Account> accountOptionalUsername = accountRepository.findByUsername(usernameOrEmail);
        Optional<Account> accountOptionalEmail = accountRepository.findByEmail(usernameOrEmail);

        if (accountOptionalUsername.isPresent()) {
            return accountOptionalUsername.get();
        } else if (accountOptionalEmail.isPresent()) {
            return accountOptionalEmail.get();
        }

        return null;
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public Account toAccount(RegistrationRequest registrationRequest) throws RoleNotFoundException {
        String roleName = "ROLE_" + registrationRequest.getRole();
        Role role = roleService.findByRole(roleName);

        Account account = Account.builder()
                .username(registrationRequest.getUsername())
                .password(registrationRequest.getPassword())
                .email(registrationRequest.getEmail())
                .roles(new HashSet<>(Collections.singletonList(role)))
                .build();

        if (role.getRole().equals("ROLE_SERVICE_PROVIDER")) {
            AccountGroup group = accountGroupService.findByAccountGroup("GROUP_SERVICE_PROVIDER");
            account.setAccountGroups(new HashSet<>(Collections.singletonList(group)));
        } else if (role.getRole().equals("ROLE_FAMILY")) {
            AccountGroup group = accountGroupService.findByAccountGroup("GROUP_FAMILY");
            account.setAccountGroups(new HashSet<>(Collections.singletonList(group)));
        }

        return account;
    }

    public boolean hasUsername(String username) {
        return accountRepository.findByUsername(username).isPresent();
    }

    public Account findByUsername(String username) throws AccountNotFoundException {
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        return accountOptional.orElseThrow(() -> new AccountNotFoundException("Account not found"));
    }

    public boolean hasEmail(String email) {
        return accountRepository.findByEmail(email).isPresent();
    }

    public boolean isAccountEnabledWithEmail(String email) {
        return findByEmail(email).isEnabled();
    }

    public Account findByEmail(String email) {
        Optional<Account> accountOptional = accountRepository.findByEmail(email);
        return accountOptional.orElseThrow(() -> new AccountNotFoundException("Account Not Found"));
    }

    public AccountVerification getAccountVerificationByVerificationToken(String verificationToken) {
        return accountVerificationRepository.findByVerificationToken(verificationToken);
    }

    @Transactional
    public void createAccountVerificationToken(Account account, String verificationToken) {
        account.setAccountVerification(null);
        save(account);

        AccountVerification accountVerification = AccountVerification.builder()
                .account(account)
                .verificationToken(verificationToken)
                .build();

        accountVerificationRepository.save(accountVerification);
    }

    public void saveRegisteredAccount(Account account) {
        accountRepository.save(account);
    }

    @Transactional
    public void createAccountRecoveryForAccount(Account account, String passwordResetToken) {
        account.setAccountRecovery(null);
        save(account);

        AccountRecovery accountRecovery = AccountRecovery.builder()
                .account(account)
                .passwordResetToken(passwordResetToken)
                .build();

        accountRecoveryRepository.save(accountRecovery);
    }

    public String validatePasswordResetToken(long accountId, String token) {
        int expireInMinutes = 10;
        AccountRecovery accountRecovery = accountRecoveryRepository.findByPasswordResetToken(token);

        if ((accountRecovery == null) || (accountRecovery.getAccount().getAccountId() != accountId)) {
            return null;
        }

        if (DateUtils.addMinutes(accountRecovery.getCreatedAt(), expireInMinutes).compareTo(new Date()) < 0) {
            return "expired";
        }

        return "success";
    }

    public Account findAccountByPasswordResetToken(String passwordResetToken) {
        return accountRecoveryRepository.findByPasswordResetToken(passwordResetToken).getAccount();
    }

    public void updateAccountPassword(Account account, String encodedPassword) {
        account.setPassword(encodedPassword);
        accountRepository.save(account);
    }

    public void updateUserProfile(Account currentAccount, UserProfileCreateUpdateRequest userProfileCreateUpdateRequest) {
        UserProfile currentUserProfile = currentAccount.getUserProfile();

        currentUserProfile.setFirstName(userProfileCreateUpdateRequest.getFirstName());
        currentUserProfile.setLastName(userProfileCreateUpdateRequest.getLastName());
        currentUserProfile.setContactNumber(userProfileCreateUpdateRequest.getContactNumber());
        currentUserProfile.setWebsite(userProfileCreateUpdateRequest.getWebsite());
        currentUserProfile.setBio(userProfileCreateUpdateRequest.getBio());
        currentUserProfile.setImage(userProfileCreateUpdateRequest.getImage());

        currentAccount.setUserProfile(currentUserProfile);

        UserProfile userProfileAccountUpdate = currentAccount.getUserProfile();
        userProfileAccountUpdate.setAccount(currentAccount);

        currentAccount.setUserProfile(userProfileAccountUpdate);

        save(currentAccount);
    }

    public Page<Account> findAccountPage(String accountGroup, int currentPage, int pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        return accountRepository.findByAccountGroups_AccountGroupOrderByAccountIdDesc(accountGroup, pageable);
    }

    public Page<Account> findAccountPageByUsernameOrAccountGroup(String username, String accountGroup, int currentPage, int pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);

        if (accountGroup == null) {
            return accountRepository.findByUsernameIgnoreCaseContainingOrderByAccountIdDesc(username, pageable);
        } else if (accountGroup.isEmpty()) {
            return accountRepository.findByUsernameIgnoreCaseContainingOrderByAccountIdDesc(username, pageable);
        }

        return accountRepository.findByUsernameIgnoreCaseContainingAndAccountGroups_AccountGroupIgnoreCaseContainingOrUsernameIgnoreCaseContainingAndRoles_DefaultGroupIgnoreCaseContainingOrderByAccountIdDesc(username, accountGroup, username, accountGroup, pageable);
    }

    public UserViewDto toUserViewDto(Account currAccount) {
        String firstName = "New";
        String lastName = "User";
        String image = "";

        if (currAccount.getUserProfile() != null) {
            firstName = currAccount.getUserProfile().getFirstName();
            lastName = currAccount.getUserProfile().getLastName();
            image = currAccount.getUserProfile().getImage();
        }

        List<String> groups = currAccount.getAccountGroups().stream()
                .map(AccountGroup::getAccountGroup)
                .collect(Collectors.toList());

        List<InheritedPermissionDto> inheritedPermissionDtos =
                currAccount.getAccountGroups().stream()
                        .map(group ->
                                group.getPermissions()
                                        .stream()
                                        .map(permission -> InheritedPermissionDto.builder()
                                                .permission(permission.getPermCode())
                                                .group(group.getAccountGroup())
                                                .build()
                                        ).collect(Collectors.toList())
                        ).flatMap(List::stream).collect(Collectors.toList());

        List<String> permissions = currAccount.getPermissions().stream()
                .map(Permission::getPermCode)
                .collect(Collectors.toList());

        List<AccountProviderOverviewDto> providers = currAccount.getProviderInfos().stream().map(providerInfo -> {
            int serviceCount = providerInfoService.countGroupedServicesByProviderInfoId(providerInfo.getProviderInfoId());
            boolean approved = !providerInfoService.findPendingApprovedProviderInfoByProviderInfoId(providerInfo.getProviderInfoId()).isPresent();

            return AccountProviderOverviewDto.builder()
                    .providerInfoId(providerInfo.getProviderInfoId())
                    .providerName(providerInfo.getProviderName())
                    .locationCount(providerInfo.getProviderLocations().size())
                    .serviceCount(serviceCount)
                    .approved(approved)
                    .build();
        }).collect(Collectors.toList());

        return UserViewDto.builder()
                .username(currAccount.getUsername())
                .email(currAccount.getEmail())
                .enabled(currAccount.isEnabled())
                .lastLoggedIn(currAccount.getLastLoggedIn())
                .firstName(firstName)
                .lastName(lastName)
                .image(image)
                .groups(groups)
                .permissions(permissions)
                .inheritedPermissionDtos(inheritedPermissionDtos)
                .groups(groups)
                .providers(providers)
                .build();
    }

    @Transactional
    public void deleteAccountByUsername(String username) {
        accountRepository.deleteAccountByUsername(username);
    }

    @Transactional
    public void createOrLinkAccountInitialProviderInfo(CreateProviderRequest createProviderRequest, boolean createApproved, String editedBy, Account auditedBy) {
        CreateProviderAccountRequest createProviderAccountRequest = createProviderRequest.getAccount();
        ProviderInfo providerInfoSaved = providerInfoService.buildProviderInfo(
                createProviderRequest.getProviderInfo(),
                createProviderRequest.getProviderLocations(),
                createApproved,
                editedBy
        );
        String username = createProviderAccountRequest.getUsername();
        String email = createProviderAccountRequest.getEmail();
        String password = createProviderAccountRequest.getPassword();

        ProviderAudit providerAudit1 = providerAuditService.buildAudit(auditedBy, providerInfoSaved, providerInfoSaved.getProviderName() + " is created");
        providerInfoSaved.addAuditLogs(providerAudit1);

        if (username == null && email == null && password == null) {
            providerInfoService.save(providerInfoSaved);
            return;
        }

        Account currAccount;
        Set<ProviderInfo> providerInfos = new HashSet<>();
        if (username != null && email != null && !hasUsername(username) && !hasEmail(email)) {
            Role role = roleService.findByRole("ROLE_SERVICE_PROVIDER");
            Set<Role> roles = new HashSet<>();
            roles.add(role);

            AccountGroup accountGroup = accountGroupService.findByAccountGroup("GROUP_SERVICE_PROVIDER");
            Set<AccountGroup> accountGroups = new HashSet<>();
            accountGroups.add(accountGroup);

            Account account = Account.builder()
                    .email(email)
                    .username(username)
                    .password(password)
                    .enabled(true)
                    .locked(false)
                    .roles(roles)
                    .accountGroups(accountGroups)
                    .build();

            ProviderAudit providerAudit2 = providerAuditService.buildAudit(auditedBy, providerInfoSaved, "Account - " + account.getUsername() + " is created, and linked with Provider - " + providerInfoSaved.getProviderName());
            providerInfoSaved.addAuditLogs(providerAudit2);

            currAccount = save(account);
        } else {
            currAccount = findByUsername(createProviderAccountRequest.getUsername());

            ProviderAudit providerAudit2 = providerAuditService.buildAudit(auditedBy, providerInfoSaved, "Existing Account - " + currAccount.getUsername() + " is linked with Provider - " + providerInfoSaved.getProviderName());
            providerInfoSaved.addAuditLogs(providerAudit2);

            providerInfos = currAccount.getProviderInfos();
        }

        providerInfos.add(providerInfoSaved);
        currAccount.setProviderInfos(providerInfos);

        save(currAccount);
    }

    @Transactional
    public void deleteProviderInfos(ProviderInfo providerInfoToDelete) {
        Set<Account> accounts = providerInfoToDelete.getAccounts();
        providerInfoToDelete.setAccounts(null);

        accounts.forEach(account -> {
            Set<ProviderInfo> providerInfos = account.getProviderInfos();
            providerInfos.removeIf(providerInfo -> providerInfo.getProviderInfoId() == providerInfoToDelete.getProviderInfoId());
            account.setProviderInfos(providerInfos);
            save(account);
        });

        providerInfoService.deleteByProviderInfoId(providerInfoToDelete.getProviderInfoId());
    }

    @Transactional
    public List<UnapprovedAccountDto> findUnapprovedAccounts() {
        return accountRepository.findByAccountGroupsIsEmptyAndRolesIsNotNull().map(a ->
                UnapprovedAccountDto.builder()
                        .email(a.getEmail())
                        .username(a.getUsername())
                        .createdAt(a.getCreatedAt())
                        .roleRequested(new ArrayList<>(a.getRoles()).get(0).getRole())
                        .build()
        ).collect(Collectors.toList());
    }

    @Transactional
    public void approveAccountByUsername(String username) {
        Account account = findByUsername(username);

        List<Role> roles = new ArrayList<>(account.getRoles());

        if (account.getRoles().size() == 1) {
            AccountGroup accountGroup = accountGroupService.findByAccountGroup(roles.get(0).getDefaultGroup());
            account.setAccountGroups(new HashSet<>(Collections.singletonList(accountGroup)));
        }
    }

    @Transactional
    public void disapproveAndDeleteByUsername(String username) {
        if (!accountRepository.existsByUsername(username)) {
            return;
        }

        if (findByUsername(username).getAccountGroups().size() > 0) {
            return;
        }

        accountRepository.deleteAccountByUsername(username);
    }

    @Transactional
    public void deleteReceivedMessageById(String username, Long messageId) {
        Account account = findByUsername(username);
        Message message = messageService.findById(messageId);
        message.removeRecipientAccountByUsername(username);
        account.removeReceivedMessageById(messageId);
        messageService.saveMessage(message);
        save(account);

        messageService.deleteIfOrphan(messageId);
    }

    @Transactional
    public void deleteSentMessageById(String username, Long messageId) {
        Account account = findByUsername(username);
        Message message = messageService.findById(messageId);
        account.removeSentMessageById(messageId);
        messageService.saveMessage(message);
        save(account);

        messageService.deleteIfOrphan(messageId);
    }

    @Transactional
    public List<MessageAccountGroupsDto> getAllMessageAccountGroupDtos() {
        return accountGroupService.streamAllAccountGroups().map(accountGroup -> {
            List<MessageUserDto> messageUserDtos = accountGroup.getAccounts().stream().map(account ->
                    MessageUserDto.builder()
                            .accountId(account.getAccountId())
                            .username(account.getUsername())
                            .build()).collect(Collectors.toList());

            return MessageAccountGroupsDto.builder()
                    .accountGroupId(accountGroup.getAccountGroupId())
                    .accountGroup(accountGroup.getAccountGroup())
                    .recipients(messageUserDtos)
                    .build();
        }).collect(Collectors.toList());
    }

    @Transactional
    public Set<Account> getAccountsByAccountGroup(String accountGroupName) {
        AccountGroup accountGroup = accountGroupService.findByAccountGroup(accountGroupName);

        return new HashSet<>(accountGroup.getAccounts());
    }

    @Transactional
    public void sendApprovalNotificationToAllAdmins(String providerName, String actionRequested) {
        Set<Account> recipients = getAccountsByAccountGroup("GROUP_ADMIN");
        String angularAppUrl = env.getProperty("angular-app-url");
        String messageContent = "Requesting to " +
                actionRequested +
                "\r\n" +
                "\r\n" +
                "Click link to view approval: " +
                angularAppUrl + "/approvals";
        Message message = Message.builder()
                .subject("New Approval Pending For:  " + providerName)
                .message("Requesting to " + actionRequested)
                .messageTime(new Date())
                .build();

        recipients.forEach(account -> {
            List<Message> receivedMessages = account.getReceivedMessages();
            receivedMessages.add(message);
            account.setReceivedMessages(receivedMessages);

            try {
                mailService.sendMail(account.getEmail(), message.getSubject(), messageContent);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        message.setRecipientAccounts(recipients);
        messageService.saveMessage(message);
    }
}
