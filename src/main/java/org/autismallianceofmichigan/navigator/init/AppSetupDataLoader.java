package org.autismallianceofmichigan.navigator.init;

import org.autismallianceofmichigan.navigator.persistence.entity.*;
import org.autismallianceofmichigan.navigator.service.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;

@Component
public class AppSetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private final AccountService accountService;
    private final CountyService countyService;
    private final PermissionService permissionService;
    private final AccountGroupService accountGroupService;
    private final RoleService roleService;
    private final AddressTypeService addressTypeService;
    private final ContactTypeService contactTypeService;
    private final ServiceDeliveryService serviceDeliveryService;
    private final NoteTypeService noteTypeService;
    private final SystemSettingsService systemSettingsService;
    private final WaitTimeTypeService waitTimeTypeService;
    private final PasswordEncoder passwordEncoder;
    private final Environment env;
    boolean alreadySetup = false;

    public AppSetupDataLoader(AccountService accountService, CountyService countyService, PermissionService permissionService, AccountGroupService accountGroupService, RoleService roleService, AddressTypeService addressTypeService, ContactTypeService contactTypeService, NoteTypeService noteTypeService, ServiceDeliveryService serviceDeliveryService, SystemSettingsService systemSettingsService, WaitTimeTypeService waitTimeTypeService, PasswordEncoder passwordEncoder, Environment env) {
        this.accountService = accountService;
        this.countyService = countyService;
        this.permissionService = permissionService;
        this.accountGroupService = accountGroupService;
        this.roleService = roleService;
        this.addressTypeService = addressTypeService;
        this.contactTypeService = contactTypeService;
        this.serviceDeliveryService = serviceDeliveryService;
        this.noteTypeService = noteTypeService;
        this.systemSettingsService = systemSettingsService;
        this.waitTimeTypeService = waitTimeTypeService;
        this.passwordEncoder = passwordEncoder;
        this.env = env;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        // address types
        createAllAddressTypes();

        // contact types
        createAllContactTypes();

        // deliveries
        createAllDeliveries();

        // michigan counties
        createAllCounties();

        // note types
        createAllNoteTypes();

        //wait time types
        createAllWaitTimeTypes();

        //basic system settings
        createSystemSettingsIfNotFound();

        // permissions
        Permission PERM_VIEW_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_PROVIDER_INFO, "View existing service provider's profile and information");
        Permission PERM_VIEW_OWN_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_OWN_PROVIDER_INFO, "View linked provider profile with this account");
        Permission PERM_CREATE_OR_MODIFY_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_PROVIDER_INFO, "Create a new service provider's profile or modify existing information");
        Permission PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO, "Create a new provider profile or modify existing information, content needing to be approved by administrators");
        Permission PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO, "Provider can create and modify their own profile, content needing to be approved");
        Permission PERM_DELETE_PROVIDER_INFO = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_PROVIDER_INFO, "Delete a provider that will no longer be used and remove it from all associated accounts");
        Permission PERM_VIEW_FAMILY_INFO = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_FAMILY_INFO, "View existing family's profile and information");
        Permission PERM_CREATE_OR_MODIFY_FAMILY_INFO = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_FAMILY_INFO, "Create a new family's profile or modify existing information");

        Permission PERM_VIEW_APPROVALS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_APPROVALS, "View form information submitted for approval");
        Permission PERM_PROCESS_APPROVALS = createPermissionIfNotFound(PermissionCodes.PERM_PROCESS_APPROVALS, "Edit, approve or disapprove form information submitted");

        Permission PERM_VIEW_SERVICE_CATEGORIES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_SERVICE_CATEGORIES, "View all service categories");
        Permission PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY, "Create a new service category or change existing category name");
        Permission PERM_VIEW_SERVICES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_SERVICES, "View all services");
        Permission PERM_CREATE_OR_MODIFY_SERVICE = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_SERVICE, "Create a new service or change existing service's name or category");
        Permission PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES, "Create a new service delivery type or change an existing service delivery's name");
        Permission PERM_VIEW_INSURANCES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_INSURANCES, "View all insurances");
        Permission PERM_CREATE_OR_MODIFY_INSURANCE = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_INSURANCE, "Create a new insurance or change existing insurance's name");
        Permission PERM_VIEW_AGE_GROUPS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_AGE_GROUPS, "View all age groups");
        Permission PERM_CREATE_OR_MODIFY_AGE_GROUP = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_AGE_GROUP, "Create a new age group or change existing age group's ages");

        Permission PERM_VIEW_USERS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_USERS, "View all app users");
        Permission PERM_CREATE_OR_MODIFY_USER = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_USER, "Create a new app user or modify existing user's information");
        Permission PERM_VIEW_GROUPS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_GROUPS, "View all user groups (grouped system permissions)");
        Permission PERM_CREATE_OR_MODIFY_GROUP = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_GROUP, "Create a new user group or modify existing user group's users or permissions");
        Permission PERM_VIEW_SYSTEM_PERMISSIONS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_SYSTEM_PERMISSIONS, "View all system permissions");
        Permission PERM_VIEW_SYSTEM_ROLES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_SYSTEM_ROLES, "View all system roles");
        Permission PERM_MODIFY_ROLE_DEFAULT_GROUP = createPermissionIfNotFound(PermissionCodes.PERM_MODIFY_ROLE_DEFAULT_GROUP, "Modify the default user group (their default permissions) when app users who sign up as a certain role get approved");

        Permission PERM_VIEW_REGIONS = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_REGIONS, "View all regions");
        Permission PERM_CREATE_OR_MODIFY_REGION = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_REGION, "Create a new region or modify an existing region");
        Permission PERM_CHANGE_SYSTEM_SETTINGS = createPermissionIfNotFound(PermissionCodes.PERM_CHANGE_SYSTEM_SETTINGS, "Change system settings");

        Permission PERM_VIEW_PROVIDERS_ADMINISTRATIVE_CONTACT_INFO = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_PROVIDERS_ADMINISTRATIVE_CONTACT_INFO, "View service provider's administrative contact information");
        Permission PERM_BULK_MESSAGING = createPermissionIfNotFound(PermissionCodes.PERM_BULK_MESSAGING, "Send system messages to app users");
        Permission PERM_CHANGE_USER_PASSWORD = createPermissionIfNotFound(PermissionCodes.PERM_CHANGE_USER_PASSWORD, "Change an app user's password");
        Permission PERM_DELETE_USER = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_USER, "Delete an app user");
        Permission PERM_MANAGE_USER_PERMISSIONS = createPermissionIfNotFound(PermissionCodes.PERM_MANAGE_USER_PERMISSIONS, "Manage permissions for a specific app user");
        Permission PERM_MANAGE_USER_GROUPS = createPermissionIfNotFound(PermissionCodes.PERM_MANAGE_USER_GROUPS, "Manage user groups for a specific app user");
        Permission PERM_DELETE_GROUP = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_GROUP, "Delete a user group that does not have users in it");

        Permission PERM_CREATE_NOTE = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_NOTE, "Create notes for provider profile fields");
        Permission PERM_VIEW_NOTES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_NOTES, "View all notes created for provider profile fields");
        Permission PERM_DELETE_NOTE = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_NOTE, "Delete a note that is no longer applicable");

        Permission PERM_CREATE_PROVIDER_FILE = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_PROVIDER_FILE, "Upload file about service provider");
        Permission PERM_CREATE_OWN_PROVIDER_FILE = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OWN_PROVIDER_FILE, "Providers can upload files to their own account");
        Permission PERM_DELETE_PROVIDER_FILE = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_PROVIDER_FILE, "Delete a file regarding a certain service provider");
        Permission PERM_DELETE_OWN_PROVIDER_FILE = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_OWN_PROVIDER_FILE, "Providers can delete their own files on the account");

        Permission PERM_CREATE_OR_MODIFY_WAIT_TIME = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_WAIT_TIME, "Create a new wait time or modify existing wait time from any provider");
        Permission PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME = createPermissionIfNotFound(PermissionCodes.PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME, "Providers can create a new wait time, or modify their own existing wait times");
        Permission PERM_DELETE_WAIT_TIME = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_WAIT_TIME, "Delete an existing wait time");
        Permission PERM_DELETE_OWN_WAIT_TIME = createPermissionIfNotFound(PermissionCodes.PERM_DELETE_OWN_WAIT_TIME, "Delete an existing wait time from any provider");
        Permission PERM_VIEW_WAIT_TIMES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_WAIT_TIMES, "View all wait times on any provider");
        Permission PERM_VIEW_OWN_WAIT_TIMES = createPermissionIfNotFound(PermissionCodes.PERM_VIEW_OWN_WAIT_TIMES, "Providers can view their own wait times");

        Permission PERM_LINK_OR_UNLINK_ACCOUNT = createPermissionIfNotFound(PermissionCodes.PERM_LINK_OR_UNLINK_ACCOUNT, "Link or unlink a user account and a provider profile");

        // app admin setup
        Set<Permission> adminPermissions = new HashSet<>(
                Arrays.asList(
                        PERM_BULK_MESSAGING,
                        PERM_CHANGE_SYSTEM_SETTINGS,
                        PERM_CHANGE_USER_PASSWORD,
                        PERM_CREATE_OR_MODIFY_AGE_GROUP,
                        PERM_CREATE_OR_MODIFY_FAMILY_INFO,
                        PERM_CREATE_OR_MODIFY_GROUP,
                        PERM_CREATE_OR_MODIFY_INSURANCE,
                        PERM_CREATE_OR_MODIFY_PROVIDER_INFO,
                        PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO,
                        PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO,
                        PERM_CREATE_OR_MODIFY_REGION,
                        PERM_CREATE_OR_MODIFY_SERVICE,
                        PERM_CREATE_OR_MODIFY_SERVICE_CATEGORY,
                        PERM_CREATE_OR_MODIFY_SERVICE_DELIVERIES,
                        PERM_CREATE_OR_MODIFY_USER,
                        PERM_CREATE_NOTE,
                        PERM_CREATE_PROVIDER_FILE,
                        PERM_CREATE_OWN_PROVIDER_FILE,
                        PERM_CREATE_OR_MODIFY_WAIT_TIME,
                        PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME,
                        PERM_DELETE_USER,
                        PERM_DELETE_GROUP,
                        PERM_DELETE_NOTE,
                        PERM_DELETE_PROVIDER_FILE,
                        PERM_DELETE_OWN_PROVIDER_FILE,
                        PERM_DELETE_WAIT_TIME,
                        PERM_DELETE_OWN_WAIT_TIME,
                        PERM_DELETE_PROVIDER_INFO,
                        PERM_MANAGE_USER_GROUPS,
                        PERM_MANAGE_USER_PERMISSIONS,
                        PERM_MODIFY_ROLE_DEFAULT_GROUP,
                        PERM_LINK_OR_UNLINK_ACCOUNT,
                        PERM_PROCESS_APPROVALS,
                        PERM_VIEW_AGE_GROUPS,
                        PERM_VIEW_APPROVALS,
                        PERM_VIEW_FAMILY_INFO,
                        PERM_VIEW_GROUPS,
                        PERM_VIEW_INSURANCES,
                        PERM_VIEW_PROVIDER_INFO,
                        PERM_VIEW_OWN_PROVIDER_INFO,
                        PERM_VIEW_PROVIDERS_ADMINISTRATIVE_CONTACT_INFO,
                        PERM_VIEW_REGIONS,
                        PERM_VIEW_SERVICE_CATEGORIES,
                        PERM_VIEW_SERVICES,
                        PERM_VIEW_SYSTEM_PERMISSIONS,
                        PERM_VIEW_SYSTEM_ROLES,
                        PERM_VIEW_USERS,
                        PERM_VIEW_NOTES,
                        PERM_VIEW_WAIT_TIMES,
                        PERM_VIEW_OWN_WAIT_TIMES
                )
        );

        AccountGroup adminGroup = createAccountGroupIfNotFound(env.getProperty("app-admin-group"), adminPermissions);
        Set<AccountGroup> adminGroups = new HashSet<>(
                Collections.singletonList(adminGroup)
        );
        Set<Role> adminRoles = createAdminRoles();
        createAppAdminIfNotFound(new HashSet<>(), adminGroups, adminRoles);

        // other groups
        createRolesOtherThanAdmin();

        Set<Permission> internalPermissions = new HashSet<>(
                Arrays.asList(
                        PERM_BULK_MESSAGING,
                        PERM_VIEW_PROVIDER_INFO
                )
        );
        createAccountGroupIfNotFound("GROUP_INTERNAL", internalPermissions);

        Set<Permission> navigatorPermissions = new HashSet<>(
                Arrays.asList(
                        PERM_BULK_MESSAGING,
                        PERM_CHANGE_SYSTEM_SETTINGS,
                        PERM_CREATE_OR_MODIFY_FAMILY_INFO,
                        PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO,
                        PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO,
                        PERM_CREATE_OR_MODIFY_REGION,
                        PERM_CREATE_NOTE,
                        PERM_CREATE_PROVIDER_FILE,
                        PERM_CREATE_OWN_PROVIDER_FILE,
                        PERM_CREATE_OR_MODIFY_WAIT_TIME,
                        PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME,
                        PERM_DELETE_NOTE,
                        PERM_DELETE_PROVIDER_FILE,
                        PERM_DELETE_OWN_PROVIDER_FILE,
                        PERM_DELETE_WAIT_TIME,
                        PERM_DELETE_OWN_WAIT_TIME,
                        PERM_LINK_OR_UNLINK_ACCOUNT,
                        PERM_VIEW_AGE_GROUPS,
                        PERM_VIEW_APPROVALS,
                        PERM_VIEW_FAMILY_INFO,
                        PERM_VIEW_GROUPS,
                        PERM_VIEW_INSURANCES,
                        PERM_VIEW_PROVIDER_INFO,
                        PERM_VIEW_OWN_PROVIDER_INFO,
                        PERM_VIEW_PROVIDERS_ADMINISTRATIVE_CONTACT_INFO,
                        PERM_VIEW_REGIONS,
                        PERM_VIEW_SERVICE_CATEGORIES,
                        PERM_VIEW_SERVICES,
                        PERM_VIEW_SYSTEM_PERMISSIONS,
                        PERM_VIEW_SYSTEM_ROLES,
                        PERM_VIEW_USERS,
                        PERM_VIEW_NOTES,
                        PERM_VIEW_WAIT_TIMES,
                        PERM_VIEW_OWN_WAIT_TIMES
                )
        );
        createAccountGroupIfNotFound("GROUP_NAVIGATOR", navigatorPermissions);

        Set<Permission> providerPermissions = new HashSet<>(
                Arrays.asList(
                        PERM_CREATE_OR_MODIFY_OWN_UNAPPROVED_PROVIDER_INFO,
                        PERM_VIEW_OWN_PROVIDER_INFO,
                        PERM_VIEW_OWN_WAIT_TIMES,
                        PERM_CREATE_OWN_PROVIDER_FILE,
                        PERM_CREATE_OR_MODIFY_OWN_WAIT_TIME,
                        PERM_DELETE_OWN_PROVIDER_FILE,
                        PERM_DELETE_OWN_WAIT_TIME
                )
        );
        createAccountGroupIfNotFound("GROUP_SERVICE_PROVIDER", providerPermissions);

        Set<Permission> familyPermissions = new HashSet<>(
                Arrays.asList()
        );
        createAccountGroupIfNotFound("GROUP_FAMILY", familyPermissions);

        alreadySetup = true;
    }

    @Transactional
    void createAppAdminIfNotFound(Set<Permission> permissions, Set<AccountGroup> accountGroups, Set<Role> roles) {
        String appAdminUsername = env.getProperty("app-admin-username");

        if (!accountService.hasUsername(appAdminUsername)) {
            accountService.save(
                    Account.builder()
                            .username(appAdminUsername)
                            .email(env.getProperty("app-admin-email"))
                            .password(passwordEncoder.encode(env.getProperty("app-admin-password")))
                            .enabled(true)
                            .accountGroups(accountGroups)
                            .permissions(permissions)
                            .roles(roles)
                            .build()
            );
        }
    }

    @Transactional
    Permission createPermissionIfNotFound(String permCode, String description) {
        if (!permissionService.hasPermissionWithPermCode(permCode)) {
            return permissionService.savePermission(
                    Permission.builder()
                            .permCode(permCode)
                            .description(description)
                            .build()
            );
        }

        return null;
    }

    @Transactional
    AccountGroup createAccountGroupIfNotFound(String accountGroup, Set<Permission> permissions) {
        if (!accountGroupService.hasAccountGroup(accountGroup)) {
            return accountGroupService.saveAccountGroup(
                    AccountGroup.builder()
                            .accountGroup(accountGroup)
                            .permissions(permissions)
                            .build()
            );
        }

        return null;
    }

    @Transactional
    void createRolesOtherThanAdmin() {
        createRoleIfNotFound("ROLE_INTERNAL", "GROUP_INTERNAL", "Internal users who manage human resource");
        createRoleIfNotFound("ROLE_NAVIGATOR", "GROUP_NAVIGATOR", "Specialists who help connect family with service providers, gather and use all resources");
        createRoleIfNotFound("ROLE_SERVICE_PROVIDER", "GROUP_SERVICE_PROVIDER", "Provider users who offer relevant services to families");
        createRoleIfNotFound("ROLE_FAMILY", "GROUP_FAMILY", "Family users who can look up provider resources");
    }

    @Transactional
    Set<Role> createAdminRoles() {
        Set<Role> adminRoles = new HashSet<>();

        adminRoles.add(createRoleIfNotFound("ROLE_ADMIN", env.getProperty("app-admin-group"), "Admin user"));

        return adminRoles;
    }

    @Transactional
    Role createRoleIfNotFound(String role, String defaultGroup, String description) {
        if (!roleService.hasRole(role)) {
            return roleService.saveRole(
                    Role.builder()
                            .role(role)
                            .description(description)
                            .defaultGroup(defaultGroup)
                            .build()
            );
        }

        return null;
    }

    @Transactional
    void createAllAddressTypes() {
        createAddressTypeIfNotFound("Mailing");
        createAddressTypeIfNotFound("Service");
    }

    @Transactional
    void createAddressTypeIfNotFound(String type) {
        if (!addressTypeService.hasAddressType(type)) {
            addressTypeService.saveAddressType(
                    AddressType.builder()
                            .type(type)
                            .build()
            );
        }
    }

    @Transactional
    void createAllContactTypes() {
        createContactTypeIfNotFound("Informational", "Contact that will be shown to families at search pages");
        createContactTypeIfNotFound("Administrative", "Contact for internal administrative purposes");
        createContactTypeIfNotFound("Marketing", "Contact for receiving Autism Alliance of Michigan marketing information");
        createContactTypeIfNotFound("Other - Fax Number/Additional Email", "Additional contact information to share with families");
    }

    @Transactional
    void createAllDeliveries() {
        createServiceDeliveryIfNotFound("Home Based");
        createServiceDeliveryIfNotFound("Center Based");
        createServiceDeliveryIfNotFound("Telehealth");
    }

    @Transactional
    void createContactTypeIfNotFound(String type, String description) {
        if (!contactTypeService.hasContactType(type)) {
            contactTypeService.saveContactType(
                    ContactType.builder()
                            .type(type)
                            .description(description)
                            .build()
            );
        } else {
            ContactType contactType = contactTypeService.findByType(type);

            if (!contactType.getDescription().equals(description)) {
                contactType.setDescription(description);
                contactTypeService.saveContactType(contactType);
            }
        }
    }

    @Transactional
        // Id 1 - 83 are Michigan counties
    void createAllCounties() {
        List<String> counties = new ArrayList<>(Arrays.asList(
                "Alcona", "Alger", "Allegan", "Alpena",
                "Antrim", "Arenac", "Baraga", "Barry",
                "Bay", "Benzie", "Berrien", "Branch",
                "Calhoun", "Cass", "Charlevoix", "Cheboygan",
                "Chippewa", "Clare", "Clinton", "Crawford",
                "Delta", "Dickinson", "Eaton", "Emmet",
                "Genesee", "Gladwin", "Gogebic", "Grand Traverse",
                "Gratiot", "Hillsdale", "Houghton", "Huron",
                "Ingham", "Ionia", "Iosco", "Iron",
                "Isabella", "Jackson", "Kalamazoo", "Kalkaska",
                "Kent", "Keweenaw", "Lake", "Lapeer",
                "Leelanau", "Lenawee", "Livingston", "Luce",
                "Mackinac", "Macomb", "Manistee", "Marquette",
                "Mason", "Mecosta", "Menominee", "Midland",
                "Missaukee", "Monroe", "Montcalm", "Montmorency",
                "Muskegon", "Newaygo", "Oakland", "Oceana",
                "Ogemaw", "Ontonagon", "Osceola", "Oscoda",
                "Otsego", "Ottawa", "Presque Isle", "Roscommon",
                "Saginaw", "St. Clair", "St. Joseph", "Sanilac",
                "Schoolcraft", "Shiawassee", "Tuscola", "Van Buren",
                "Washtenaw", "Wayne", "Wexford"
        ));

        counties.forEach(this::createCountyIfNotFound);
    }

    @Transactional
    void createCountyIfNotFound(String county) {
        if (!countyService.hasCounty(county)) {
            countyService.saveCounty(
                    County.builder()
                            .county(county)
                            .build());
        }
    }

    @Transactional
    void createServiceDeliveryIfNotFound(String delivery) {
        if (!serviceDeliveryService.hasServiceDelivery(delivery)) {
            serviceDeliveryService.save(ServiceDelivery.builder().delivery(delivery).build());
        }
    }

    void createAllNoteTypes() {
        List<String> noteTypes = new ArrayList<>(Arrays.asList("Provider", "Location", "Service"));
        noteTypes.forEach(this::createNoteTypeIfNotFound);
    }

    @Transactional
    void createNoteTypeIfNotFound(String noteType) {
        if (!noteTypeService.hasNoteType(noteType)) {
            noteTypeService.save(
                    NoteType.builder()
                            .noteType(noteType)
                            .build());
        }
    }

    void createAllWaitTimeTypes() {
        List<String> waitTimeTypes = new ArrayList<>(Arrays.asList("Provider", "Location"));
        waitTimeTypes.forEach(this::createWaitTimeTypeIfNotFound);
    }

    @Transactional
    void createWaitTimeTypeIfNotFound(String waitTimeType) {
        if (!waitTimeTypeService.hasWaitTimeType(waitTimeType)) {
            waitTimeTypeService.saveWaitTimeType(WaitTimeType.builder()
                    .waitTimeType(waitTimeType)
                    .build());
        }
    }

    @Transactional
    void createSystemSettingsIfNotFound() {
        if (!systemSettingsService.hasSystemSetting()) {
            systemSettingsService.save(
                    SystemSetting.builder()
                            .systemSettingId(1L)
                            .backgroundImage("")
                            .disclaimer("<div style=\"text-align: center;\"><b><font color=\"#97d700\">Legal Disclaimer</font></b>&nbsp;—&nbsp;The resources listed in the Autism Alliance of Michigan (AAOM) Neighborhood Directory and on our website are being provided for informational purposes only. The AAOM does not endorse or recommend the organizations listed or services they provide, and assumes no responsibility or liability with regard to their use. AAOM does not review the qualifications or credentials of the organizations listed. Any decision to work with a resource in the AAOM Neighborhood Directory should be based on your independent investigation of the qualifications, credentials and skills in the area of expertise indicated by the service provider. The listings should not be construed as legal, financial, or professional advice, and users should seek appropriate professional advice to address the specific facts and circumstances of your matter. All users of the AAOM Neighborhood Directory agree to waive any claims against AAOM in consideration for AAOM providing this directory of information.</div>")
                            .searchSupportNote("<div style=\"text-align: center;\"><span style=\"background-color: transparent;\">If you are unable to find the information or resources you need or have any questions, please be sure to contact an AAOM Navigator at </span><b style=\"background-color: transparent;\">877-463-2266 (AAOM)</b><span style=\"background-color: transparent;\"> or </span><a href=\"mailto:navigator@aaomi.org\"><b><font color=\"#533f03\">navigator@aaomi.org</font></b></a></div>")
                            .technicalSupportNote("<div style=\"text-align: center;\"><span style=\"background-color: transparent;\">For technical support, please contact <b><a href=\"mailto:directory@aaomi.org\"><font color=\"#533f03\">directory@aaomi.org</font></a></b></span></div>")
                            .build()
            );
        }
    }
}
