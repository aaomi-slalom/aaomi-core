package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Data;

import java.util.List;

@Data
public class ExternalCreateMessageRequest {
    private String subject;
    private String message;
    private List<ExternalMessageAccountGroupsDto> accountGroups;
}
