package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CreateMessageRequest {
    private String subject;
    private String message;
    private List<String> recipients;
}
