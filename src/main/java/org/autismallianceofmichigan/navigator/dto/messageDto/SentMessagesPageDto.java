package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SentMessagesPageDto {
    private long totalElements;
    private int totalPages;
    List<SentMessageDto> sentMessages;
}
