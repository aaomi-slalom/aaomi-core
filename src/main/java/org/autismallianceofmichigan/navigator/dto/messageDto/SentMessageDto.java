package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class SentMessageDto {
    private long messageId;
    private List<String> recipientUsernames;
    private String subject;
    private String message;
    private Date messageTime;
}
