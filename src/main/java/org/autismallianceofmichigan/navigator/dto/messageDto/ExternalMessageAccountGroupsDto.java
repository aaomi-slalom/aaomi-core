package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExternalMessageAccountGroupsDto {
    private Long accountGroupId;
    private String accountGroup;
    private int userCount;
}
