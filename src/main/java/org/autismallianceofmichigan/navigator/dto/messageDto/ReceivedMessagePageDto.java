package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ReceivedMessagePageDto {
    private long totalElements;
    private int totalPages;
    List<ReceivedMessageDto> receivedMessages;
}
