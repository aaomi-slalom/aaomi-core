package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ReceivedMessageDto {
    private long messageId;
    private String sender;
    private String subject;
    private String message;
    private Date messageTime;
}
