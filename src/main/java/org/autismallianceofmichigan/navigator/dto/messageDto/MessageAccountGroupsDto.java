package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MessageAccountGroupsDto {
    private Long accountGroupId;
    private String accountGroup;
    private List<MessageUserDto> recipients;
}
