package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ViewMessagesRequest {
    private String username;
    private int currentPage;
    private int pageSize;
}
