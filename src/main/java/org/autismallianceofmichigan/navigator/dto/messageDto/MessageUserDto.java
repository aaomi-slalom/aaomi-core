package org.autismallianceofmichigan.navigator.dto.messageDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MessageUserDto {
    private Long accountId;
    private String username;
}
