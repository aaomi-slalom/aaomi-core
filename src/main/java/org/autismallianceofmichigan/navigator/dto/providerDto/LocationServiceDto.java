package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LocationServiceDto {
    private String identifier;
    private String serviceName;
    private String category;
    private String categoryDescription;
    private String serviceDescription;
    private boolean internal;
    private List<String> insurances;
    private List<LocationAgeGroupDto> locationAgeGroupDtos;
    private List<String> deliveries;
}
