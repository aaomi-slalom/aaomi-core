package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class CreateContactNumberRequest {
    private String number;
    private String contactType;
}
