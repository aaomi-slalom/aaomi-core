package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ViewProvidersRequest {
    @NotNull(message = "Current page is required")
    private int currentPage;

    @NotNull(message = "Page size is required")
    private int pageSize;

    private String providerNameSearch;
}
