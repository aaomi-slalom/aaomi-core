package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InsuranceOptionDto {
    private Long insuranceId;
    private String insuranceName;
}
