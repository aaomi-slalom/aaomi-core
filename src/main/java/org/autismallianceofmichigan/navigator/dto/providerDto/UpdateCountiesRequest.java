package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

import java.util.List;

@Data
public class UpdateCountiesRequest {
    private List<Long> countyIds;
}
