package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

import java.util.List;

@Data
public class CreateProviderRequest {
    private CreateProviderAccountRequest account;
    private CreateProviderInfoRequest providerInfo;
    private List<CreateProviderLocationRequest> providerLocations;
}
