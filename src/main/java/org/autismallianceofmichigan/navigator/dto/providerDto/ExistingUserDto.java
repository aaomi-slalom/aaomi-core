package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExistingUserDto {
    private String username;
    private String firstName;
    private String lastName;
    private String image;
}
