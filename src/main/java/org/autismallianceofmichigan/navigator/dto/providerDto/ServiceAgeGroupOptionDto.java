package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceAgeGroupOptionDto {
    private Long serviceAgeGroupId;
    private int ageStart;
    private int ageEnd;
    private String ageGroupDisplay;
}
