package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MichiganCountyOptionDto {
    private Long countyId;
    private String county;
    private int numberOfServices;
}
