package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class CreateLocationSrvcLinkRequest {
    private Long serviceId;
    private boolean internal;
    private Long serviceDeliveryId;
    private Long insuranceId;
    private Long ageGroupId;
    private String identifier;
}
