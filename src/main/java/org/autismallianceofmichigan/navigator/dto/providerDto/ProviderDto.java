package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProviderDto {
    private DetailedProviderInfoDto providerInfoDto;
    private List<ProviderLocationDto> providerLocationDtos;
    private ProviderAccountDto accountDto;
}
