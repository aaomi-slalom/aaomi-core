package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class UpdateLocationNameRequest {
    private String locationName;
}
