package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PublicProvidersPageDto {
    private long totalElements;
    private int totalPages;
    private List<PublicProviderInfoDto> publicProviderInfoDtos;
}
