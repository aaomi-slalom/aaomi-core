package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContactTypeOptionDto {
    private Long contactTypeId;
    private String type;
    private String description;
}
