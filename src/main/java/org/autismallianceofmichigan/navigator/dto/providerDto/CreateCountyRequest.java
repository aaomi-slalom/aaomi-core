package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class CreateCountyRequest {
    private String county;
}
