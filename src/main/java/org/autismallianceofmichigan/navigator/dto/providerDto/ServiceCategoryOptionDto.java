package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceCategoryOptionDto {
    private long serviceCategoryId;
    private String serviceCategory;
}
