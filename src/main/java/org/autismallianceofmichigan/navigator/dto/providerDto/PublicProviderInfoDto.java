package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PublicProviderInfoDto {
    private Long providerInfoId;
    private String providerName;
    private String logo;
    private String email;
    private String contactNumber;
    private String website;
    private String address;
    private String facebook;
    private String linkedin;
    private String twitter;
    private String instagram;
    private String youtube;
    private Long suitableLocationId;
    private String locationName;
}
