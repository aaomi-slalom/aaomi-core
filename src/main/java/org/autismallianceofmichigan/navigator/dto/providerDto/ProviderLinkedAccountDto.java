package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProviderLinkedAccountDto {
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String image;
}
