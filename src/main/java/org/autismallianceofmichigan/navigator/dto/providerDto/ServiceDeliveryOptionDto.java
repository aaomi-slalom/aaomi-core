package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceDeliveryOptionDto {
    private Long serviceDeliveryId;
    private String delivery;
    private int serviceCount;
}
