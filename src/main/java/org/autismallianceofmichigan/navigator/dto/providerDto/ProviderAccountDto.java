package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProviderAccountDto {
    private String firstName;
    private String lastName;
    private String email;
    private String image;
}
