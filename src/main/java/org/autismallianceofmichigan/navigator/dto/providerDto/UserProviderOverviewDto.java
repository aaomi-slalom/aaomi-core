package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserProviderOverviewDto {
    private Long providerInfoId;
    private String providerName;
    private String website;
    private int locationCount;
    private int serviceCount;
    private String logo;
}
