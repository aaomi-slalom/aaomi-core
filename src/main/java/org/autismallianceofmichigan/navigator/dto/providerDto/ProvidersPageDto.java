package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProvidersPageDto {
    private long totalElements;
    private int totalPages;
    private List<ProviderInfoDto> providerInfoDtos;
}
