package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

import java.util.List;

@Data
public class CreateProviderLocationRequest {
    private String locationName;
    private boolean internal;
    private List<CreateAddressRequest> addresses;
    private List<CreateEmailRequest> emails;
    private List<CreateContactNumberRequest> contactNumbers;
    private List<CreateCountyRequest> counties;
    private List<CreateLocationSrvcLinkRequest> locationSrvcLinks;
}
