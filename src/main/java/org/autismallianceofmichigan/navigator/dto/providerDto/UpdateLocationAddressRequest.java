package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class UpdateLocationAddressRequest {
    private String street;
    private String city;
    private String state;
    private String zip;
}
