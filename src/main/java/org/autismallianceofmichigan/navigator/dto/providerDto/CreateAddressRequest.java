package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class CreateAddressRequest {
    private String addressType;
    private String street;
    private String city;
    private String state;
    private String zip;
}
