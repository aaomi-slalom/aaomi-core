package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.persistence.entity.Address;

import java.util.List;

@Data
@Builder
public class ProviderLocationDto {
    private Long locationId;
    private String locationName;

    private Long serviceAddressId;
    private String serviceStreet;
    private String serviceCity;
    private String serviceState;
    private String serviceZip;

    private Long mailingAddressId;
    private String mailingStreet;
    private String mailingCity;
    private String mailingState;
    private String mailingZip;

    private List<EmailDto> emailDtos;
    private List<ContactNumberDto> contactNumberDtos;
    private List<MichiganCountyOptionDto> serviceCounties;
    private List<LocationServiceDto> locationServiceDtos;

    private String modifiedLocationName;
    private List<MichiganCountyOptionDto> modifiedCounties;
    private Address modifiedServiceAddress;
    private Address modifiedMailingAddress;
    private List<EmailDto> unapprovedAddedEmails;
    private List<EmailDto> unapprovedDeletedEmails;
    private List<ContactNumberDto> unapprovedAddedContactNumbers;
    private List<ContactNumberDto> unapprovedDeletedContactNumbers;
    private List<LocationServiceDto> unapprovedAddedGroupedServices;
    private List<LocationServiceDto> unapprovedDeletedGroupedServices;

    private boolean deleteRequested;
    private boolean modifyRequested;
    private boolean createApproved;
    private boolean internal;

    private List<LocationServiceDto> unapprovedAddedNewServices;
    private List<EmailDto> unapprovedAddedNewEmails;
    private List<ContactNumberDto> unapprovedAddedNewContactNumbers;
}
