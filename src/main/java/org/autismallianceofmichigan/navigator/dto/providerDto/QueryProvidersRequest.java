package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class QueryProvidersRequest {
    private int currentPage;
    private int pageSize;
    private String providerName;
    private List<Long> categoryIds;
    private List<Long> insuranceIds;
    private List<Long> ageGroupIds;
    private List<Long> serviceIds;
    private List<Long> deliveryIds;
    private List<Long> countyIds;
    private List<Long> regionIds;
    private String keyword;
}
