package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckProviderNameDto {
    private boolean hasProviderName;
}
