package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetailedProviderInfoDto {
    private Long providerInfoId;
    private String providerName;
    private String description;
    private String logo;
    private String website;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String instagram;
    private String youtube;

    private boolean modifyRequested;
    private boolean createApproved;
    private String modifiedProviderName;
    private String modifiedDescription;
    private String modifiedLogo;
    private String modifiedLinkedin;
    private String modifiedFacebook;
    private String modifiedTwitter;
    private String modifiedInstagram;
    private String modifiedYoutube;
    private String modifiedWebsite;
}
