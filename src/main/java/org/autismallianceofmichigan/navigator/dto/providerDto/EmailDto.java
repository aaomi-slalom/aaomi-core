package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDto {
    private Long emailId;
    private String email;
    private String type;
}
