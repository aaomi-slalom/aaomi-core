package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProviderInfoDto {
    private Long providerInfoId;
    private String providerName;
    private String logo;
    private String email;
    private String contactNumber;
    private int locationCount;
    private int serviceCount;
    private boolean isStandalone;
    private boolean approved;
}
