package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class UpdateInternalRequest {
    private boolean internal;
}
