package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContactNumberDto {
    private Long contactNumberId;
    private String number;
    private String type;
}
