package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Data;

@Data
public class CreateProviderInfoRequest {
    private String providerName;
    private String website;
    private String description;
    private String logo;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String instagram;
    private String youtube;
}
