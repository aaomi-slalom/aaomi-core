package org.autismallianceofmichigan.navigator.dto.providerDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountProviderOverviewDto {
    private Long providerInfoId;
    private String providerName;
    private int locationCount;
    private int serviceCount;
    private boolean approved;
}
