package org.autismallianceofmichigan.navigator.dto.authDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdatePasswordResponse {
    private String message;
}
