package org.autismallianceofmichigan.navigator.dto.authDto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UpdatePasswordRequest {
    @NotBlank(message = "Email is required")
    @Email(message = "Email Address should be valid")
    private String email;

    @NotBlank(message = "Password is required")
    @Size(min = 8, message = "Password minimum is 8 characters")
    @Pattern.List({
            @Pattern(regexp = "(?=.*[0-9]).+", message = "Password must contain at least 1 digit"),
            @Pattern(regexp = "(?=.*[a-z]).+", message = "Password must contain at least 1 lowercase letter"),
            @Pattern(regexp = "(?=.*[A-Z]).+", message = "Password must contain at least 1 uppercase letter"),
            @Pattern(regexp = "(?=\\S+$).+", message = "Password must contain no whitespace")
    })
    private String password;

    private String confirmPassword;
    private boolean passwordMatching;

    @NotNull(message = "Account Id is required")
    private long accountId;

    @NotBlank(message = "Password reset token is required")
    private String passwordResetToken;

    @AssertTrue(message = "Passwords must match")
    public boolean isPasswordMatching() {
        if (password != null) {
            return password.equals(confirmPassword);
        }
        return true;
    }
}
