package org.autismallianceofmichigan.navigator.dto.authDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PasswordResetRequest {
    @NotBlank(message = "Email is required")
    private String email;
}
