package org.autismallianceofmichigan.navigator.dto.authDto;

import lombok.Data;

@Data
public class VerifyResetRequest {
    private long id;
    private String token;
}
