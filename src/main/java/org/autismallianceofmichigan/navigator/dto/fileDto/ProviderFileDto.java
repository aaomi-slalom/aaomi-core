package org.autismallianceofmichigan.navigator.dto.fileDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ProviderFileDto {
    private Long providerFileId;
    private String file;
    private String fileName;
    private Date createdAt;
    private String author;
}
