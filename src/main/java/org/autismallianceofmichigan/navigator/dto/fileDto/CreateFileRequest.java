package org.autismallianceofmichigan.navigator.dto.fileDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateFileRequest {
    @NotNull(message = "providerInfoId is required")
    private Long providerInfoId;

    @NotBlank(message = "file is required")
    private String file;

    @NotBlank(message = "file name is required")
    private String fileName;
}
