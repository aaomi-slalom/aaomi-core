package org.autismallianceofmichigan.navigator.dto.locationDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.LocationServiceDto;

import java.util.List;

@Data
@Builder
public class PublicLocationDto {
    private Long providerInfoId;
    private String providerName;
    private String description;
    private String website;
    private String logo;

    private String facebook;
    private String twitter;
    private String linkedin;
    private String instagram;
    private String youtube;

    private List<OtherLocationDto> otherLocations;
    private String locationName;
    private String address;

    private List<String> emails;
    private List<String> additionalEmails;
    private List<String> contactNumbers;
    private List<String> faxNumbers;
    private List<String> files;
    private List<LocationServiceDto> locationServiceDtos;
}
