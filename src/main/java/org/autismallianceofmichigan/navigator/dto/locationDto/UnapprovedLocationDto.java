package org.autismallianceofmichigan.navigator.dto.locationDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.ContactNumberDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.EmailDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.LocationServiceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;

import java.util.List;

@Data
@Builder
public class UnapprovedLocationDto {
    private Long locationId;
    private String locationName;
    private Long providerInfoId;
    private String providerName;

    private String serviceAddress;
    private String mailingAddress;

    private List<EmailDto> emailDtos;
    private List<ContactNumberDto> contactNumberDtos;
    private List<MichiganCountyOptionDto> serviceCounties;
    private List<LocationServiceDto> locationServiceDtos;
}
