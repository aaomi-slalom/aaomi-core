package org.autismallianceofmichigan.navigator.dto.locationDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OtherLocationDto {
    private Long locationId;
    private String locationName;
}
