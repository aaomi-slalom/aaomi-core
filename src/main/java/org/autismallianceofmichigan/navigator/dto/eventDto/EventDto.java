package org.autismallianceofmichigan.navigator.dto.eventDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class EventDto {
    private long eventId;
    private String organizer;
    private String title;
    private String description;
    private String website;
    private String image;
    private String cost;
    private boolean free;
    private Date startDate;
    private Date endDate;
}
