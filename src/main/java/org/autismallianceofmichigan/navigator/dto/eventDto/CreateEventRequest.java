package org.autismallianceofmichigan.navigator.dto.eventDto;

import lombok.Data;

import java.sql.Date;

@Data
public class CreateEventRequest {
    private String title;
    private String description;
    private String website;
    private String cost;
    private Boolean free;
    private String image;
    private Date startDateTime;
    private Date endDateTime;
}
