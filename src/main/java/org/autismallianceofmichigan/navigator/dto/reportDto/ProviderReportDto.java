package org.autismallianceofmichigan.navigator.dto.reportDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProviderReportDto {
    private String providerName;
    private String providerDescription;
    private String website;
    private String facebook;
    private String linkedin;
    private String twitter;

    private String locationName;
    private String street;
    private String city;
    private String state;
    private String zip;

    private String contactNumber;
    private String email;

    private String categoryName;
    private String categoryDescription;
    private String serviceName;
    private String serviceDescription;

    private String insuranceName;
    private int ageStart;
    private int ageEnd;
    private String delivery;

    private String county;
    private String region;

    private int isLocationInternal;
    private int isLocationServiceInternal;
    private int isServiceInternal;
}
