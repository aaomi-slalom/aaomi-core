package org.autismallianceofmichigan.navigator.dto.reportDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegionCategoryServiceProviderCountDto {
    private Long serviceId;
    private String service;
    private int providerCount;
}
