package org.autismallianceofmichigan.navigator.dto.reportDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RegionCategoryProviderCountDto {
    private Long categoryId;
    private String category;
    private int providerCount;

    private List<RegionCategoryServiceProviderCountDto> serviceProviderCounts;
}
