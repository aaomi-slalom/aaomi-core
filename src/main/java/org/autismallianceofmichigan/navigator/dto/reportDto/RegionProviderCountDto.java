package org.autismallianceofmichigan.navigator.dto.reportDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RegionProviderCountDto {
    private Long regionId;
    private String region;
    private int providerCount;

    private List<RegionCategoryProviderCountDto> categoryProviderCounts;
}
