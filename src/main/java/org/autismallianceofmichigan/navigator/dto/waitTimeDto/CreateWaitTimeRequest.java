package org.autismallianceofmichigan.navigator.dto.waitTimeDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class CreateWaitTimeRequest {
    @NotNull(message = "ProviderInfoId is required")
    private Long providerInfoId;

    @NotNull(message = "WaitTimeLinkId is required")
    private String waitTimeLinkId;

    @NotBlank(message = "WaitTimeType is required")
    @Pattern(regexp = "Provider|Location", message = "Must be a valid WaitTimeType")
    private String waitTimeType;

    @NotBlank(message = "title is required")
    private String title;

    @NotBlank(message = "WaitTime is required")
    private String waitTime;
}
