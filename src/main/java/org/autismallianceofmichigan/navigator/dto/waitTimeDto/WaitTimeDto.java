package org.autismallianceofmichigan.navigator.dto.waitTimeDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class WaitTimeDto {
    private Long waitTimeId;
    private String title;
    private String waitTime;
    private String waitTimeType;
    private String source;
    private Date createdAt;
}
