package org.autismallianceofmichigan.navigator.dto.waitTimeDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WaitTimeOptionDto {
    private String waitTimeLinkId;
    private String source;
    private String waitTimeType;
}
