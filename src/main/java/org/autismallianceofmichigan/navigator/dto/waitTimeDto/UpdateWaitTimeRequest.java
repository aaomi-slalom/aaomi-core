package org.autismallianceofmichigan.navigator.dto.waitTimeDto;

import lombok.Data;

@Data
public class UpdateWaitTimeRequest {
    private String title;
    private String waitTime;
}
