package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class UserPasswordChangeRequest {
    private String username;

    @NotBlank(message = "New password is required")
    @Size(min = 8, message = "Password minimum is 8 characters")
    @Pattern.List({
            @Pattern(regexp = "(?=.*[0-9]).+", message = "Password must contain at least 1 digit"),
            @Pattern(regexp = "(?=.*[a-z]).+", message = "Password must contain at least 1 lowercase letter"),
            @Pattern(regexp = "(?=.*[A-Z]).+", message = "Password must contain at least 1 uppercase letter"),
            @Pattern(regexp = "(?=\\S+$).+", message = "Password must contain no whitespace")
    })
    private String newPassword;
    private String confirmNewPassword;
    private boolean passwordMatching;

    @AssertTrue(message = "Passwords must match")
    public boolean isPasswordMatching() {
        if (newPassword != null) {
            return newPassword.equals(confirmNewPassword);
        }
        return true;
    }
}
