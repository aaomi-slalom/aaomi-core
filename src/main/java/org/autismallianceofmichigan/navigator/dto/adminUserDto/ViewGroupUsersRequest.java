package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ViewGroupUsersRequest {
    @NotNull(message = "Current page is required")
    private int currentPage;

    @NotNull(message = "Page size is required")
    private int pageSize;

    @NotBlank(message = "Account group is required")
    @Pattern(regexp = "GROUP_.*", message = "Account group must be valid")
    private String accountGroup;
}
