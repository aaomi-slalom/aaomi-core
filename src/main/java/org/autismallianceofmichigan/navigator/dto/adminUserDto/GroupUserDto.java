package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GroupUserDto {
    private String userName;
    private String firstName;
    private String lastName;
}
