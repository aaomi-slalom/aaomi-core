package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class UpdateUsernameRequest {
    private String username;

    @NotBlank(message = "Username is required")
    @Size(max = 50, message = "Username maximum is 50 characters")
    @Pattern(regexp = "^(\\d|\\w)+$", message = "Username must contain no special characters or whitespace")
    private String newUsername;
}
