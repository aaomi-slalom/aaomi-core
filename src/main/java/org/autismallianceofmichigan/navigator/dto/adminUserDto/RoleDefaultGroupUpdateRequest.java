package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

@Data
public class RoleDefaultGroupUpdateRequest {
    private String role;
    private String defaultGroup;
}
