package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleDto {
    private String role;
    private String description;
    private String defaultGroup;
}
