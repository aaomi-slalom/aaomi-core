package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GroupAccountDto {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String image;
    private boolean rootAdmin;
}
