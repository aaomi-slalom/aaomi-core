package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserAddPermissionRequest {
    @NotBlank(message = "Username is required")
    private String username;
    
    @NotBlank(message = "Permission is required")
    @Pattern(regexp = "PERM_.*", message = "Permission must be valid")
    private String permission;
}
