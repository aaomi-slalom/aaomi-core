package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InheritedPermissionDto {
    private String permission;
    private String group;
}
