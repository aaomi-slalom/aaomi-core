package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ViewUsersRequest {
    @NotNull(message = "Current page is required")
    private int currentPage;

    @NotNull(message = "Page size is required")
    private int pageSize;

    private String usernameSearch;

    private String accountGroupSearch;
}
