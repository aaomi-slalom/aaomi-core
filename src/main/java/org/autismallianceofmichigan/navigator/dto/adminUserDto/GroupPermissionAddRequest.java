package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

@Data
public class GroupPermissionAddRequest {
    private String accountGroup;
    private String permission;
}
