package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.AccountProviderOverviewDto;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class UserViewDto {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String image;
    private boolean enabled;
    private boolean rootAdmin;
    private String adminGroup;
    private LocalDateTime lastLoggedIn;
    private List<String> groups;
    private List<String> permissions;
    private List<InheritedPermissionDto> inheritedPermissionDtos;
    private List<AccountProviderOverviewDto> providers;
}
