package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UpdateEmailRequest {
    private String username;

    @NotBlank(message = "Email is required")
    @Email(message = "Email Address should be valid")
    private String email;
}
