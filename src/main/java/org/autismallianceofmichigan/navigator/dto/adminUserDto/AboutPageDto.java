package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AboutPageDto {
    private String coreVersion;
    private String uxVersion;
}
