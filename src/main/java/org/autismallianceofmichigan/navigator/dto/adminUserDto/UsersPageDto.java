package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UsersPageDto {
    private long totalElements;
    private int totalPages;
    private List<UserDto> userDtos;
}
