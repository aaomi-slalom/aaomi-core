package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDto {
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private boolean enabled;
    private List<String> groups;
    private List<String> defaultGroups;
    private String image;
}
