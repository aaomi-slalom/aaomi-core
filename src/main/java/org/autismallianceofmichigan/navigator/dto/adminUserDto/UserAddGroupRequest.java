package org.autismallianceofmichigan.navigator.dto.adminUserDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserAddGroupRequest {
    @NotBlank(message = "Username is required")
    private String username;

    @NotBlank(message = "Account group is required")
    @Pattern(regexp = "GROUP_.*", message = "Account group must be valid")
    private String accountGroup;
}
