package org.autismallianceofmichigan.navigator.dto.systemSettingDto;

import lombok.Data;

@Data
public class CreateUpdateSystemSettingsRequest {
    private String backgroundImage;
    private String supportNote;
    private String disclaimer;
}
