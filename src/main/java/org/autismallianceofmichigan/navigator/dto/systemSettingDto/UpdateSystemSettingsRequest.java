package org.autismallianceofmichigan.navigator.dto.systemSettingDto;

import lombok.Data;

@Data
public class UpdateSystemSettingsRequest {
    private String updateString;
}
