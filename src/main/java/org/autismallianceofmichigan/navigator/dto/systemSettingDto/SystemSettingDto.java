package org.autismallianceofmichigan.navigator.dto.systemSettingDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemSettingDto {
    private String backgroundImage;
    private String searchSupportNote;
    private String technicalSupportNote;
    private String disclaimer;
}
