package org.autismallianceofmichigan.navigator.dto.userProfileDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserProfileResponse {
    private String firstName;
    private String lastName;
    private String contactNumber;
    private String website;
    private String image;
    private String bio;
}
