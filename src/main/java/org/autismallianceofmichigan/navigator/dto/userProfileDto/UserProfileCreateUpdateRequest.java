package org.autismallianceofmichigan.navigator.dto.userProfileDto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Builder
public class UserProfileCreateUpdateRequest {
    @NotBlank(message = "First name is required")
    private String firstName;

    @NotBlank(message = "Last name is required")
    private String lastName;

    @NotBlank(message = "Contact number is required")
    @Pattern(regexp = "^\\+?\\d{0,15}", message = "Contact number must be valid")
    private String contactNumber;

    private String website;

    private String image;

    private String bio;
}
