package org.autismallianceofmichigan.navigator.dto.auditDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AuditDto {
    private String changeLog;
    private String auditedBy;
    private String providerName;
    private Date auditedTime;
}
