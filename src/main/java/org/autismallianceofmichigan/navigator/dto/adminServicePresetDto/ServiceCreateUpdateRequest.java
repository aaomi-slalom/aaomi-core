package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ServiceCreateUpdateRequest {
    @NotBlank(message = "Category is required")
    private String category;

    @NotBlank(message = "Service is required")
    private String service;

    @NotNull(message = "Internal is required")
    private boolean internal;

    private String description;
}
