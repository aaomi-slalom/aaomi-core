package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

@Data
public class ServiceAgeGroupCreateUpdateRequest {
    @NotNull(message = "Start age is required")
    @Range(min = 0, max = 255)
    private int ageStart;

    @NotNull(message = "End age is required")
    @Range(min = 0, max = 255)
    private int ageEnd;

    private boolean endGreaterThanStart;

    @AssertTrue(message = "ageEnd must be greater than or equal to ageStart")
    public boolean isEndGreaterThanStart() {
        return ageEnd - ageStart >= 0;
    }
}
