package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceAgeGroupDto {
    private Long serviceAgeGroupId;
    private int ageStart;
    private int ageEnd;
    private int serviceCount;
}
