package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ServiceDto {
    private Long serviceId;
    private String service;
    private String description;
    private boolean internal;
    private boolean deletable;
}
