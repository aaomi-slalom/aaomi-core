package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Data;

@Data
public class ServiceDeliveryCreateUpdateRequest {
    private String delivery;
}
