package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ServiceCategoryCreateUpdateRequest {
    @NotBlank(message = "Category is required")
    private String category;
    private String description;
}
