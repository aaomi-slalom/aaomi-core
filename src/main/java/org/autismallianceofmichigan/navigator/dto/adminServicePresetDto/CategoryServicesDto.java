package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CategoryServicesDto {
    private String category;
    private String catDescription;
    private List<ServiceDto> serviceDtos;
}
