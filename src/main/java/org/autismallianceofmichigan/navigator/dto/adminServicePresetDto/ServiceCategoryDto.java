package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceCategoryDto {
    private long serviceCategoryId;
    private String serviceCategory;
    private String description;
    private int serviceCount;
}
