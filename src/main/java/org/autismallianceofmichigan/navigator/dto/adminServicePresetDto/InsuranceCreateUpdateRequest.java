package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class InsuranceCreateUpdateRequest {
    @NotBlank(message = "Insurance name is required")
    private String insuranceName;

    private String website;

    private String contactNumber;
}
