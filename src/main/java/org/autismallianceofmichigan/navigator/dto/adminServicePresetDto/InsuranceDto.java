package org.autismallianceofmichigan.navigator.dto.adminServicePresetDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InsuranceDto {
    private Long insuranceId;
    private String insuranceName;
    private String contactNumber;
    private String website;
    private int serviceCount;
}
