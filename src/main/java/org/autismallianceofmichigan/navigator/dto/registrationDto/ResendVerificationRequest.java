package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ResendVerificationRequest {
    @NotBlank(message = "Email is required")
    private String email;
}
