package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailAvailabilityResponse {
    private boolean isEmailAvailable;
    private boolean isAccountEnabled;
}