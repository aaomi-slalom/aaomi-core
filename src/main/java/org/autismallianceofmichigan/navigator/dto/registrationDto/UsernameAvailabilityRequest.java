package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Data;

@Data
public class UsernameAvailabilityRequest {
    private String username;
}