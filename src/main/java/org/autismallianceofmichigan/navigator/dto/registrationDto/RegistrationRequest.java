package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class RegistrationRequest {
    @NotBlank(message = "Username is required")
    @Size(max = 50, message = "Username maximum is 50 characters")
    @Pattern(regexp = "^(\\d|\\w)+$", message = "Username must contain no special characters or whitespace")
    private String username;

    @NotBlank(message = "Email is required")
    @Email(message = "Email Address should be valid")
    private String email;

    @NotBlank(message = "Password is required")
    @Size(min = 8, message = "Password minimum is 8 characters")
    @Pattern.List({
            @Pattern(regexp = "(?=.*[0-9]).+", message = "Password must contain at least 1 digit"),
            @Pattern(regexp = "(?=.*[a-z]).+", message = "Password must contain at least 1 lowercase letter"),
            @Pattern(regexp = "(?=.*[A-Z]).+", message = "Password must contain at least 1 uppercase letter"),
            @Pattern(regexp = "(?=\\S+$).+", message = "Password must contain no whitespace")
    })
    private String password;
    private String confirmPassword;
    private boolean passwordMatching;

    @NotBlank(message = "Role is required")
    @Pattern(regexp = "NAVIGATOR|INTERNAL|SERVICE_PROVIDER|FAMILY", message = "Role must be valid")
    private String role;

    @AssertTrue(message = "Passwords must match")
    public boolean isPasswordMatching() {
        if (password != null) {
            return password.equals(confirmPassword);
        }
        return true;
    }
}
