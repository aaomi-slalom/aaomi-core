package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Data;

@Data
public class EmailAvailabilityRequest {
    private String email;
}