package org.autismallianceofmichigan.navigator.dto.registrationDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegistrationResponse {
    private String message;
}
