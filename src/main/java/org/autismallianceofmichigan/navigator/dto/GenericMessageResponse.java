package org.autismallianceofmichigan.navigator.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GenericMessageResponse {
    private String message;
}
