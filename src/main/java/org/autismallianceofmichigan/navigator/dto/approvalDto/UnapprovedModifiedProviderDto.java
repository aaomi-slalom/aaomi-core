package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UnapprovedModifiedProviderDto {
    private Long providerInfoId;
    private String modifiedProviderName;
    private String modifiedLogo;
    private String modifiedDescription;
    private String modifiedWebsite;
    private String modifiedLinkedin;
    private String modifiedFacebook;
    private String modifiedTwitter;
    private boolean createdApproved;
    private List<UnapprovedModifiedLocationDto> unapprovedModifiedLocationDtos;
}
