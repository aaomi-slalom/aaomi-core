package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UnapprovedAccountDto {
    private String username;
    private String email;
    private String roleRequested;
    private Date createdAt;
}
