package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UnapprovedModifiedProviderOverviewDto {
    private Long providerInfoId;
    private String providerName;
    private LocalDateTime updatedAt;
    private String editedBy;
}
