package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.ContactNumberDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.EmailDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.LocationServiceDto;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;
import org.autismallianceofmichigan.navigator.persistence.entity.Address;

import java.util.List;

@Data
@Builder
public class UnapprovedModifiedLocationDto {
    private Long locationId;
    private String modifiedLocationName;

    private List<MichiganCountyOptionDto> modifiedCounties;

    private List<EmailDto> unapprovedAddedEmails;
    private List<EmailDto> unapprovedDeletedEmails;

    private List<ContactNumberDto> unapprovedAddedContactNumbers;
    private List<ContactNumberDto> unapprovedDeletedContactNumbers;

    private List<LocationServiceDto> unapprovedAddedGroupedServices;
    private List<LocationServiceDto> unapprovedDeletedGroupedServices;

    private Address modifiedMailingAddress;
    private Address modifiedServiceAddress;
    private boolean deleteRequested;
    private boolean modifyRequested;
}
