package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UnapprovedLocationOverviewDto {
    private Long locationId;
    private String locationName;
    private String providerName;
    private String editedBy;
    private Date createdAt;
}
