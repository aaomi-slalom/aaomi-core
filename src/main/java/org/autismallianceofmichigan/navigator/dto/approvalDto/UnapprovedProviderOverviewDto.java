package org.autismallianceofmichigan.navigator.dto.approvalDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UnapprovedProviderOverviewDto {
    private Long providerInfoId;
    private String providerName;
    private Date createdAt;
    private String editedBy;
}
