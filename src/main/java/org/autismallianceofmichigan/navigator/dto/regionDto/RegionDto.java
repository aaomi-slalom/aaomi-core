package org.autismallianceofmichigan.navigator.dto.regionDto;

import lombok.Builder;
import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;

import java.util.List;

@Data
@Builder
public class RegionDto {
    private Long regionId;
    private String region;
    private List<MichiganCountyOptionDto> counties;
    private int numberOfServices;
}
