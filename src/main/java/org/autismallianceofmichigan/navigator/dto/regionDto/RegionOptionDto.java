package org.autismallianceofmichigan.navigator.dto.regionDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegionOptionDto {
    private Long regionId;
    private String region;
}
