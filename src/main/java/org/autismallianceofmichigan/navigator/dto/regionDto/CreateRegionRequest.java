package org.autismallianceofmichigan.navigator.dto.regionDto;

import lombok.Data;
import org.autismallianceofmichigan.navigator.dto.providerDto.MichiganCountyOptionDto;

import java.util.List;

@Data
public class CreateRegionRequest {
    private String region;
    private List<MichiganCountyOptionDto> countyDtos;
}
