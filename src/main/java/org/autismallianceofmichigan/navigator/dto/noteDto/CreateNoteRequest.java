package org.autismallianceofmichigan.navigator.dto.noteDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class CreateNoteRequest {
    @NotNull(message = "ProviderInfoId is required")
    private Long providerInfoId;

    @NotBlank(message = "NoteLinkId is required")
    private String noteLinkId;

    @NotBlank(message = "Note type is required")
    @Pattern(regexp = "Provider|Location|Service", message = "Note type must be valid")
    private String type;

    @NotBlank(message = "Note title is required")
    private String title;

    @NotBlank(message = "Note is required")
    private String note;
}
