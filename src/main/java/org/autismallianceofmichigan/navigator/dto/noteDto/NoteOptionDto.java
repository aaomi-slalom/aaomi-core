package org.autismallianceofmichigan.navigator.dto.noteDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NoteOptionDto {
    private String noteLinkId;
    private String source;
    private String noteType;
}
