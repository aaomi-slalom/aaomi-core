package org.autismallianceofmichigan.navigator.dto.noteDto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class NoteDto {
    private Long noteId;
    private String title;
    private String note;
    private String type;
    private String source;
    private String author;
    private Date createdAt;
}
