package org.autismallianceofmichigan.navigator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class AddressTypeNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 6974644855556556894L;

    public AddressTypeNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
