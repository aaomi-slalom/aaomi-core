package org.autismallianceofmichigan.navigator.exception;

public class NoSecretKeyException extends Exception {
    public NoSecretKeyException(String errorMessage) {
        super(errorMessage);
    }
}